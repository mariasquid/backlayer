$(document).ready(function() {

    $('#usd_amount').numeric('.');
    $('#eth_amount').numeric('.');

    var price = $('#valueEthinUsd').text();
    console.log(price);

    $('#eth_amount').keyup(function(event) {
        event.preventDefault();
        $('#usd_amount').val(($(this).val() * price).toFixed(2));
    });

    $('#usd_amount').keyup(function(event) {
        event.preventDefault();
        $('#eth_amount').val(($(this).val() / price).toFixed(8));
    });

    var sender;
    var receiver;
    var eth_amount;
    var usd_amount;
    var privateKey;
    var save = "false";
    var description = "";
    var id = null;

    $('.reject').click(function(event) {
        id = $(this).data('id');
        $.get(dir_url + '/request/updateStatus/0/' + id, function(data) {
            console.log(data);
            location.reload(true);
        });
    });
});