$(document).ready(function() {

    $('#usd_amount').numeric('.');
    $("#expiration").mask("99/9999", {
        placeholder: "MM/YYYY"
    });

    $('#linkPlaid').hide();
    $('#linkCards').hide();

    var typeMethod = 0;
    var wire_method = 0;
    $('.payment_method').click(function(event) {
        $('.payment_method').removeClass('pay-active');
        $('input').removeAttr('disabled');
        $('#alert_bank').html('');
        $('#content_bank_accounts').hide();
        $('#content_card_accounts').hide();
        $('#linkPlaid').hide();
        $('#progress-bar').hide();
        $('#button-pay-usd-wallet').hide();
        $('#button-script').hide();
        $('#linkCards').hide();
        $('#addCard').hide();
        least = '1.00';
        $('#least').text(least);
        $('#usd_max_buy').show();
        $('#usd_amount').val('');
        $('#select_wire').hide();
        $('#subtotal').text('$0.00');
        $('#fee').text('$0.00');
        $('#total').text('$0.00');
        check = $(this).data('method');
        wire_method = 0;
        $(check).prop('checked', true);
        $(this).addClass('pay-active');
        $('.icon-check').removeClass('icon-active');
        $(this).children('.icon-check').addClass('icon-active');
        if (check == "#eth_wallet_check") {
            $('#fee').text('$0.00');
            $('#button-pay-usd-wallet').show();
            $('#progress-bar').show();
        } else if (check == "#usd_wallet_check") {
            typeMethod = '1';
            $('.buttons-buy').hide();
            $('#button-pay-usd-wallet').show();
            $('#progress-bar').show();
        } else if (check == "#card_check") {
            typeMethod = '2';
            $('.buttons-buy').hide();
            $('#button-script').show();
            $('#linkCards').show();
            $('.stripe-button-el span').text('add new card and pay');
            $('#button-pay-usd-wallet').show();
            $('#progress-bar').show();
            $('#content_card_accounts').show();
        } else if (check == "#bank_check") {
            typeMethod = '3';
            $('#button-pay-usd-wallet').show();
            $('#progress-bar').show();
            $('#content_bank_accounts').show();
            $('#linkPlaid').show();
        } else if (check == "#wire_transfer") {
            typeMethod = '4';
            $('#select_wire').show();
            $('#button-pay-usd-wallet').show();
            wire_method = parseInt($('#wire').val());
        }
    });

    $('.stripe-button-el').click(function(event) {
        if ($('#amount').val() < 1) {
            event.preventDefault();
            $('iframe').hide();
            $.notify({
                icon: 'pe-7s-info',
                message: 'To buy ethereum, the minimum amount is $1'
            }, {
                type: 'danger',
                timer: 2500
            });
        }
    });
    var total = 0;
    var commission = $('#commission').val();
    var discount = 0;

    $('#amount').keyup(function(event) {
        total = $(this).val();
        discount = (total * commission) / 100;
        total = Number(total) + Number(discount);
        $('#total').val(total.toFixed(2));
    });

    $('#wire').change(function(event) {
        $('#subtotal').text('$0.00');
        $('#fee').text('$0.00');
        $('#total').text('$0.00');
        $('#usd_amount').val('');
        wire_method = parseInt($(this).val());
    });

    $('#usd_amount').keyup(function(event) {
        $.getJSON(dir_url + '/accounts/calculations/' + $(this).val() + '/' + typeMethod + '/' + wire_method, function(json, textStatus) {
            $('#subtotal').text('$' + json.subtotal);
            $('#fee').text('$' + json.fee);
            $('#total').text('$' + json.total);

            if (!json.modal) {
                $('#button').html('<button id="button-pay-usd-wallet" type="submit" class="btn-m-l buttons-buy" style="display: inline-block;">Deposit</button>');
            } else {
                $('#total').text('$0.00');
                $('#button').html('');
            }
        });
    });
    

});