$(document).ready(function() {

    $('#category').change(function(event) {
        $('#subcategory').html('<option value="">Subcategory</option>');
        console.log("1");
        var id = $(this).val();
        $.getJSON(dir_url + '/register/subcategories/' + id, function(json, textStatus) {
        	console.log(dir_url + '/register/subcategories/' + id);
            $.each(json, function(index, val) {
                $('#subcategory').append('<option value="' + val.id + '">' + val.subcategory + '</option>');
            });
        });

    });

    $("#phone").numeric();

    $('#name, #last_name').keypress(function(key) {
        console.log(key);
        if((key.charCode < 97 || key.charCode > 122) && (key.charCode < 65 || key.charCode > 90) && (key.charCode != 45) && (key.charCode != 0)) return false;
    });

    $("#register_business").submit(function(event){
        var pass = $("#pwd").val();
        var patt = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z])(?=.*[#¿?&¡!*\.\-_]).*$/;
        if(!patt.test(pass)){
            $("#error_message").text("Password must contain a combination of minimum 8 characters, one uppercase, one lower case, numbers and one special character.");
            $("#error_message").show();
            event.preventDefault();
        }
    });    

    $("#register").submit(function(event){
        var pass = $("#pwd").val();
        var patt = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z])(?=.*[%$@#?&!*\.\-_]).*$/;
        if(!patt.test(pass)){
            $("#error_message").text("Password must contain at least uppercase letter, lower case letter, number and symbol");
            $("#error_message").show();
            event.preventDefault();
        }
    });

    $("input#pwd").on("focus keyup", function () {
            var score = 0;
            var a = $(this).val();
            var desc = new Array();
     
            // strength desc
            desc[0] = "Too short";
            desc[1] = "Weak";
            desc[2] = "Good";
            desc[3] = "Strong";
            desc[4] = "Best";
     
            $("#pwd_strength_wrap").fadeIn(400);
             
            // password length
            if (a.length >= 8) {
                $("#length").removeClass("invalid").addClass("valid");
                score++;
            } else {
                $("#length").removeClass("valid").addClass("invalid");
            }
     
            // at least 1 digit in password
            if (a.match(/\d/)) {
                $("#pnum").removeClass("invalid").addClass("valid");
                score++;
            } else {
                $("#pnum").removeClass("valid").addClass("invalid");
            }
     
            // at least 1 capital & lower letter in password
            if (a.match(/[A-Z]/) && a.match(/[a-z]/)) {
                $("#capital").removeClass("invalid").addClass("valid");
                score++;
            } else {
                $("#capital").removeClass("valid").addClass("invalid");
            }

            // at least 1 special character in password {
            if ( a.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) ) {
                    $("#spchar").removeClass("invalid").addClass("valid");
                    score++;
            } else {
                    $("#spchar").removeClass("valid").addClass("invalid");
            }
     
     
            if(a.length > 0) {
                    //show strength text
                    $("#passwordDescription").text(desc[score]);
                    // show indicator
                    $("#passwordStrength").removeClass().addClass("strength"+score);
            } else {
                    $("#passwordDescription").text("Password not entered");
                    $("#passwordStrength").removeClass().addClass("strength"+score);
            }
    });
     
    $("input#pwd").blur(function () {
            $("#pwd_strength_wrap").fadeOut(400);
    });

});