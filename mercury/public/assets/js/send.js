$(document).ready(function() {

    $('#usd_amount').numeric('.');
    $('#eth_amount').numeric('.');
    var price = $('#valueEthinUsd').text();
    price = parseFloat(price);

    $('#to_favorites').change(function(event) {
        var value = $(this).val();
        if (value == "new") {
            $('#save_address').modal();
        }
    });

    $('#saveAddress').submit(function(event) {
        $('.loading').show();
    });

    $('#eth_amount').keyup(function(event) {
        if ($(this).val() == '') {
            $('#usd_amount').val('');
            $('#total').val('$0.00');
        } else {
            calc_amount($(this).val(), 1);
        }
    });

    $('#usd_amount').keyup(function(event) {
        if ($(this).val() == '') {
            $('#eth_amount').val('');
            $('#total').val('$0.00');
        } else {
            calc_amount($(this).val(), 2);
        }
    });

    function calc_amount(amount, currency) {
        if (currency == '1') {
            eth_amount = $('#eth_amount').val();
            eth_amount = parseFloat(eth_amount);
            $('#usd_amount').val(parseFloat(price * eth_amount).toFixed(2));
            $('#total').val('$' + (parseFloat(price * eth_amount).toFixed(2)));
        } else if (currency == '2') {
            usd_amount = $('#usd_amount').val();
            usd_amount = parseFloat(usd_amount);
            $('#eth_amount').val(parseFloat(price / usd_amount).toFixed(5));
            $('#total').val('$' + parseFloat(usd_amount).toFixed(2));
        }
    }


    /*
    function calc_amount(amount, currency) {
        $.getJSON(dir_url + '/send/calc_amount/' + amount + '/' + currency, function(json, textStatus) {
            if (currency == '1') {
                $('#usd_amount').val(json.usd_amount);
            } else if (currency == '2') {
                $('#eth_amount').val(json.eth_amount);
            }
            $('#total').val('$' + json.usd_total);
        });
    }
    */

    function deleteAddress(url) {
        $('.loading').show();
        $.getJSON(url, function(json, textStatus) {
            if (json.status == 1) {
                $('.loading').hide();
                $('#myModal').modal();
                $('#myModal .modal-body').html(json.description);
                $('#myModal').on('hidden.bs.modal', function(e) {
                    location.reload(true);
                });
            } else {
                $('.loading').hide();
                $('#myModal').modal('hide');
                $.notify({
                    icon: 'pe-7s-close-circle',
                    message: data.description
                }, {
                    type: 'danger',
                    timer: 2500
                });
            }
        });
    }
    var url;
    var description;
    var address;
    $('.deleteAddress').click(function(event) {
        event.preventDefault();
        url = $(this).attr('href');
        description = $(this).data('description');
        address = $(this).data('address');
        $('#removeAddress').modal();
        $('#removeAddress .modal-body').html("Do you want to delete the address: " + address + " with the description: " + description + "?");
    });
    $('#removeAddressButton').click(function(event) {
        deleteAddress(url);
    });
    $('#transfer').click(function(event) {
        $('.loading').show();
    });

    $('#address').keyup(function(event) {

        var address = $(this).val();
        $.post(dir_url + '/send/validateNewFavorite/', {
            address: address
        }, function(data, textStatus, xhr) {
            if (data.status == 1) {
                $('#ico_favorite').attr('src', 'public/img/MercuryLogo.png');
                $('#ico_favorite').css('display', 'block');
            } else if (data.status == 0) {
                $('#ico_favorite').attr('src', 'public/img/ETHEREUM-ICON_Black.png');
                $('#ico_favorite').css('display', 'block');
            } else {
                $('#ico_favorite').css('display', 'none');
            }
        });

    });

    $('.between_wallets li a').click(function(event) {
        event.preventDefault();
        $('.between_wallets li, .between_wallets li a').removeClass('active');
        $(this).addClass('active');
        $(this).parent('li').addClass('active');
        $('#to_favorites, #to_myaddress').hide();

        var select = $(this).data('select');
        $('#' + select).show();
    });
});