$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();

    $('#tbody-wallets tr').click(function(event) {

    	$('#infoTransferLabel').text($(this).data('title'));
    	$('#address').html($(this).data('address'));
    	$('#linkEth').attr('href', 'https://etherscan.io/tx/' + $(this).data('txhash'));
    	$('#confirmations').text($(this).data('confirmations'));
    	$('#eth_amount').text($(this).data('eth') + " ETH");
    	$('#usd_amount').text($(this).data('usd') + " USD");

    });

    var dateFormat = "mm/dd/yy",
        from = $("#from")
        .datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 2
        })
        .on("change", function() {
            to.datepicker("option", "minDate", getDate(this));
        }),
        to = $("#to").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 2
        })
        .on("change", function() {
            from.datepicker("option", "maxDate", getDate(this));
        });

    function getDate(element) {
        var date;
        try {
            date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            date = null;
        }

        return date;
    }

});