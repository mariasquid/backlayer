$(document).ready(function() {

    $('#login-form').keyup(function(event) {
        if (event.keyCode == 13) {
            login();
        }
    });

    $('#enter').on('touchstart click', function(event) {
        login();
    });

    function login() {
        //var values = $('#login-form').serialize();
        var url = $('#login-form').attr('action');
        var data = $('#login-form').serialize();

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            beforeSend: function() {
                $.notify({
                    icon: 'pe-7s-info',
                    message: 'We are processing your data'

                }, {
                    type: 'info',
                    timer: 2500
                });
            },
            success: function(data) {
                if (data.status == 1) {
                    location.href = data.redirect;
                } else if (data.status == 2) {
                    $.notify({
                        icon: 'pe-7s-info',
                        message: data.description

                    }, {
                        type: 'danger',
                        timer: 2500
                    });

                    setTimeout(function() {
                        location.href = data.redirect;
                    }, 2500);

                } else {
                    $('#captcha').show();
                    $.notify({
                        icon: 'pe-7s-info',
                        message: data.description

                    }, {
                        type: 'danger',
                        timer: 2500
                    });
                };
            },
            timeout: 30000,
            error: function(err) {
                console.log(err);
            }
        });
    }


});