$(document).ready(function() {

    var id;
    var input;

    $('.button-edit').click(function(event) {
        event.preventDefault();

        id = $(this).data('id');
        input = $(this).data('input');
        var alias = $(input).val();

        $.ajax({
            url: dir_url + '/tools/editAlias',
            type: 'POST',
            dataType: 'json',
            data: {
                id: id,
                alias: alias
            },
            beforeSend: function() {
                console.log("Cargando...");
            },
            success: function(data) {
                if (data.status == 1) {
                    $.notify({
                        icon: 'pe-7s-info',
                        message: data.description

                    }, {
                        type: 'success',
                        timer: 4000
                    });
                } else {
                    $.notify({
                        icon: 'pe-7s-info',
                        message: data.description

                    }, {
                        type: 'danger',
                        timer: 4000
                    });
                };
            },
            timeout: 30000,
            error: function(err) {
                console.log(err);
            }
        });

    });


    /*CREAR WALLETS*/

    $('#new-wallet').click(function(event) {
        $('.loading').show();
    });

});