$(document).ready(function() {

    getProvinces($('#country_id').val());

    $('#country_id').change(function(event) {
        $('#province_id').html('');
        getProvinces($(this).val());
    });

    function getProvinces(country_id) {
        $.getJSON(dir_url + '/settings/getProvinces/' + country_id, function(json, textStatus) {
            $.each(json, function(index, val) {
                $('#province_id').append('<option value="' + val.id + '">' + val.exonimo_en + '</option>');
            });
        });
    }

    $('#update-profile').submit(function(event) {
        $('.loading').show();
    });

    $('#avatar_file').change(function(event) {
        $('.loading').show();
        $('#uploadAvatar').submit();
    });

    /*VERIFICAR TELEFONO*/
    $('#verify_phone').click(function(event) {
        event.preventDefault();
        $.notify({
            icon: 'pe-7s-info',
            message: "We are processing the request"
        }, {
            type: 'info',
            timer: 2500
        });
        $.getJSON(dir_url + '/settings/verify_phone', function(json, textStatus) {
            console.log(json);
            console.log(textStatus);
            if (json.status == 1) {
                $('#confirm_phone').hide();
                $('#confirm_code').show();
                $.notify({
                    icon: 'pe-7s-info',
                    message: json.description
                }, {
                    type: 'info',
                    timer: 4000
                });
            }
        });
    });

    $('#verify_code').submit(function(event) {
        event.preventDefault();
        var values = $('#verify_code').serialize();
        var url = $(this).attr('action');
        var type;
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: values,
            beforeSend: function() {
                console.log("Cargando...");
            },
            success: function(data) {
                console.log(data);
                if (data.status == 1) {
                    $('input').val('');
                    type = 'success';
                    location.reload(true);
                } else {
                    type = 'danger';
                }
                $.notify({
                    icon: 'pe-7s-info',
                    message: data.description
                }, {
                    type: type,
                    timer: 2500
                });
            },
            timeout: 30000,
            error: function(err) {
                console.log(err);
            }
        });
    });

    $('#authy').click(function(event) {
        $('.loading').show();
    });

    $('#google').click(function(event) {
        $('.loading').show();
    });

    $('.trashDevice').click(function(event) {
        $('.loading').show();
    });

    var modalForm;
    $('#modalNewAccount').on('hidden.bs.modal', function(e) {
        $('.contentButtonsBank').removeClass('active');
        modalForm = "";
    });

    $('#buttonDomestic').click(function(event) {
        $('.contentButtonsBank').removeClass('active');
        $(this).addClass('active');
        modalForm = $(this).data('formmodal');
    });

    $('#buttonInternational').click(function(event) {
        $('.contentButtonsBank').removeClass('active');
        $(this).addClass('active');
        modalForm = $(this).data('formmodal');
    });

    $('#openModalNewAccount').click(function(event) {
        event.preventDefault();
        $('#modalNewAccount').modal('hide');
        $(modalForm).modal();
        $('.contentButtonsBank').removeClass('active');
    });


    /* AVATAR */

    var cropper;

    $('input[type="file"]').change(function(event) {

        var file = document.getElementById('inputavatar').files;
        var browser = window.URL || window.webkitURL;

        if (file[0].type != 'image/jpeg' && file[0].type != 'image/jpg' && file[0].type != 'image/png' && file[0].type != 'application/pdf') {
            // nada
        } else {
            var objectUrl = browser.createObjectURL(file[0]);
            $('#result').removeClass('contentNewAvatar').addClass('contentNewAvatarFull');
            $('#result').html('<img src="' + objectUrl + '" id="image" style="width: 100%;" alt="Press Upload">');

            var image = document.getElementById('image');

            cropper = new Cropper(image, {
                aspectRatio: 1,
                viewMode: 1,
                ready: function() {
                    croppable = true;
                }
            });

        }

    });

    $('#button').click(function(event) {
        var croppedCanvas;
        var roundedCanvas;
        var roundedImage;

        if (!croppable) {
            return;
        }

        // Crop
        croppedCanvas = cropper.getCroppedCanvas();

        croppedCanvas.toBlob(function(blob) {
            var formData = new FormData();

            formData.append('croppedImage', blob);

            // Use `jQuery.ajax` method
            $.ajax(dir_url + '/settings/changeAvatar', {
                method: "POST",
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(data) {
                    console.log(data);
                    if (data.status == 1) {
                        $('#alertModal').html('<div class="row"><div class="col-md-12"><div class="alertGrey">we are saving your photo...</div></div></div>');
                        location.reload(true);
                    } else {
                        $('#alertModal').html('<div class="row"><div class="col-md-12"><div class="alertGrey">' + data.description + '</div></div></div>');
                    }
                },
                error: function(err) {
                    console.log(err);
                }
            });
        });

        // Show
        //roundedImage = document.createElement('img');
        //roundedImage.src = croppedCanvas.toDataURL()
        //$('#result').html('');
        //$('#result').html(roundedImage);
    });
});