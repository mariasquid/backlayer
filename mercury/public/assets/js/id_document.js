$(document).ready(function() {

    var filename;
    var id;
    var lengthname;
    $('input[type="file"]').change(function(event) {
        filename = $(this).val();
        lengthname = filename.length;
        filename = filename.substring(12, lengthname);

        $('#statusbar').css('width', '0');
        $('#name_file').html('');
        $('#fileContent p').text('');
        $('#namefileManual').text('');

        id = $(this).attr('id');
        //$('#preview').html('');
        var file = document.getElementById(id).files;
        var browser = window.URL || window.webkitURL;

        $('#statusbar').stop().animate({
                width: '100%'
            },
            1000,
            function() {
                $('#div-alert').text('Upload Completed');
                $('#name_file').html(filename + ' <span id="success_file">Success</span>');
                $('#fileContent p').text(filename);
                $('#namefileManual').text(filename);
                if (file[0].type != 'image/jpeg' && file[0].type != 'image/jpg' && file[0].type != 'image/png' && file[0].type != 'application/pdf') {
                    $.notify({
                        icon: 'pe-7s-close-circle',
                        message: 'The file type is invalid, the file must be JPEG, JPG or PNG'

                    }, {
                        type: 'danger',
                        timer: 4000
                    });
                } else {
                    var objectUrl = browser.createObjectURL(file[0]);
                    if (file[0].type == 'application/pdf') {
                        $('#preview').html('<h5>Example:</h5><img src=" ' + dir_url + '/public/img/document-pdf.jpg" height="100%" class="img-responsive img-thumbnail" style="height: 100%;">');
                    } else {
                        $('#preview').html('<h5>Example:</h5><img src=" ' + objectUrl + '" height="100%" class="img-responsive img-thumbnail" style="height: 100%;">');
                    }

                }
            });

    });


    $('#document_file').submit(function(event) {
        $('.loading').show();
    });

});