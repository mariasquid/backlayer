$(document).ready(function() {

	$('#usd_amount').keyup(function(event) {
		calculationsWithdraw($(this).val(), $('#bank_account').val());
	});

	$('#bank_account').change(function(event) {
		calculationsWithdraw($('#usd_amount').val(), $(this).val());
	});

    function calculationsWithdraw(amount, bankAccount) {

        $.getJSON(dir_url + '/accounts/calculationsWithdraw/' + amount + '/' + bankAccount, function(json, textStatus) {
            $('#subtotal').text('$' + json.subtotal);
            $('#fee').text('$' + json.commission);
            $('#total').text('$' + json.total);
        });

    }

});