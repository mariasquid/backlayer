$(document).ready(function() {

    getHistory("priceMonth");

    $('.filterHistoryPrice li a').click(function(event) {
        event.preventDefault();
        getHistory($(this).data('cut'));
        $('.filterHistoryPrice li a').css('color', '#f6c401');
        $(this).css('color', '#fff');
        $('.filterHistoryPrice li').removeClass('active');
        $(this).parent('li').addClass('active');
    });

    function getHistory(cut) {

        var labels = [];
        //$.getJSON('http://localhost/mercury_cash/Mercury-WEB/trunk/dashboard/' + cut, function(json, textStatus) {
        $.getJSON('http://mercury/dashboard/' + cut, function(json, textStatus) {

            for (var i = 0; i < json.length; i++) {
                labels.push(json[i].x);
            }

            var config = {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: [{
                        label: "Price USD",
                        backgroundColor: '#f6c401',
                        borderColor: '#f6c401',
                        data: json,
                        fill: false,
                    }]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: ''
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Date'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Price'
                            }
                        }]
                    }
                }
            };

            $('#canvas').remove();
            $('#history').append('<canvas id="canvas"></canvas>');
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myLine = new Chart(ctx, config);

        });

    }

    $('tbody tr').click(function(event) {

        $('#infoTransferLabel').text($(this).data('title'));
        $('#address').html($(this).data('address'));
        $('#linkEth').attr('href', 'https://etherscan.io/tx/' + $(this).data('txhash'));
        $('#confirmations').text($(this).data('confirmations'));
        $('#eth_amount').text($(this).data('eth') + " ETH");
        $('#usd_amount').text($(this).data('usd') + " USD");

    });

});