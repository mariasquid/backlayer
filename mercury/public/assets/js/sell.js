$(document).ready(function() {

    $('#usd_amount').numeric('.');
    $('#eth_amount').numeric('.');


    $('#eth_amount').keyup(function(event) {
        calculations($(this).val(), '1', '1');
        if ($(this).val() == "") {
            $('#usd_amount').val('');
        } else {
            calculations($(this).val(), '1', '1');
        }
    });

    $('#usd_amount').keyup(function(event) {
        calculations($(this).val(), '1', '2');
        if ($(this).val() == "") {
            $('#eth_amount').val('');
        } else {
            calculations($(this).val(), '1', '2');
        }
    });


    function calculations(amount, method, currency) {

        if ($('#eth_amount').val() == "" && $('#usd_amount').val() == "") {
            $('#subtotal').text('$0.00');
            $('#fee').text('$0.00');
            $('#total').text('$0.00');
        } else {

            $.getJSON(dir_url + '/sell/calculations/' + amount + '/' + method + '/' + currency, function(json, textStatus) {

                console.log(json);

                if (currency == '1') {
                    $('#usd_amount').val(json.subtotal);
                } else if (currency == '2') {
                    $('#eth_amount').val(json.eth_amount);
                }

                $('#subtotal').text('$' + json.subtotal);
                $('#fee').text('$' + json.fee);
                $('#total').text('$' + json.total);
            });
        }

    }

    $('#eth_wallet').change(function(event) {
        $('#eth_amount, #usd_amount').val('');
    });

    $('.max_buy').click(function(event) {
        event.preventDefault();
        var id_wallet = $('#eth_wallet').val();
        $.getJSON(dir_url + '/sell/calc_max_sell/' + id_wallet, function(json, textStatus) {
            $('#eth_amount').val(json.total_eth);
            $('#usd_amount').val(json.total_usd);
            $('#subtotal').text('$' + json.subtotal);
            $('#fee').text('$' + json.fee);
            $('#total').text('$' + json.total);
        });
    });

});