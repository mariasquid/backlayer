/**
 * Determina el sistema operativo del móvil.
 * Esta función retorna 'iOS', 'Android', 'Windows Phone', or 'desconocido'.
 *
 * @returns {String}
 */
function getMobileOperatingSystem() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    // Windows Phone debe ir primero porque su UA tambien contiene "Android"
    if (/windows phone/i.test(userAgent)) {
        //return "Windows Phone";
        //$('#appsmobile').show();
    }

    if (/android/i.test(userAgent)) {
        //return "Android";
        $('#appsmobile').show();
        $('#appsmobile a').attr('href', 'https://play.google.com/store/apps/details?id=com.adenter.mercurycash&hl=en');
        $('#appsmobile span').click(function(event) {
            $('#appsmobile').hide();
        });
    }

    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        //return "iOS";
        $('#appsmobile').show();
        $('#appsmobile a').attr('href', 'https://itunes.apple.com/us/app/mercury-cash/id1291394963?mt=8');
        $('#appsmobile span').click(function(event) {
            $('#appsmobile').hide();
        });
    }

    return "desconocido";
}

getMobileOperatingSystem();

$('#slider_home').width($(window).width());
$('.slider_home').width($(window).width());