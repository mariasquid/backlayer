$(document).ready(function() {

    var data = [],
    usd, time, total, date, month, year;
    $('#chartContainer').width($('#balanceTotal').width());
    $('#chartContainer').height($('#balanceTotal').height() - 75);
    
    $.getJSON('https://etherchain.org/api/statistics/price', function(json, textStatus) {
        total = json.data.length;
        for (var i = total-5000; i < total; i++) {
            usd = Number(json.data[i].usd);
            time = new Date(json.data[i].time);
            data.push({
                date: time,
                value: usd
            });
        }
        MG.data_graphic({
            data: data,
            width: $('#balanceTotal').width(),
            height: $('#balanceTotal').height() - 75,
            right: 20,
            target: document.getElementById('chartContainer'),
            x_accessor: 'date',
            y_accessor: 'value'
        });
    });

    $('tbody tr').click(function(event) {

        $('#infoTransferLabel').text($(this).data('title'));
        $('#address').html($(this).data('address'));
        $('#linkEth').attr('href', 'https://etherscan.io/tx/' + $(this).data('txhash'));
        $('#confirmations').text($(this).data('confirmations'));
        $('#eth_amount').text($(this).data('eth') + " ETH");
        $('#usd_amount').text($(this).data('usd') + " USD");

    });


});