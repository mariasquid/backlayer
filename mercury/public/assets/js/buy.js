$(document).ready(function() {

    $('#usd_amount').numeric('.');
    $('#eth_amount').numeric('.');

    var limit_init = 0;
    var limit_rest = 0;
    var typeMethod = 0;

    $('#progress-bar').hide();
    var price = parseFloat($('#valueEthinUsd').text());
    $('.payment_method').click(function(event) {
        $('.payment_method').removeClass('pay-active');
        $('input').removeAttr('disabled');
        $('#progress-bar').hide();
        $('#button-pay-usd-wallet').hide();
        $('#button-script').hide();
        $('#linkCards').hide();
        $('#addCard').hide();
        $('#usd_max_buy').show();
        $('#usd_amount').val('0');
        $('#eth_amount').val('0');
        check = $(this).data('method');
        $(check).prop('checked', true);
        $(this).addClass('pay-active');
        $('.icon-check').removeClass('icon-active');
        $(this).children('.icon-check').addClass('icon-active');

        if (check == "#usd_wallet_check") {
            typeMethod = '1';
            $('#type_method').text('USD Wallet');
            calculations(0, typeMethod, '2');
            $('.buttons-buy').hide();
            $('#button-pay-usd-wallet').show();
            $('#progress-bar').show();
        } else if (check == "#card_check") {
            typeMethod = '2';
            $('#type_method').text('Credit Card');
            calculations(0, typeMethod, '2');
            $('.buttons-buy').hide();
            $('#button-pay-usd-wallet').show();
            $('#progress-bar').show();
        } else if (check == "#bank_check") {
            alert("Comming soon");
        }

    });

    var percentage = 0;
    var value = 0;

    $('#eth_amount').keyup(function(event) {
        calculations($(this).val(), typeMethod, '1');
    });

    $('#usd_amount').keyup(function(event) {
        calculations($(this).val(), typeMethod, '2');
    });

    function calculations(amount, method, currency) {

        console.log(dir_url + '/buy/calculations/' + amount + '/' + method + '/' + currency);

        if (currency == '1' && $('#eth_amount').val() == '') {
            $('#usd_amount').val('');
            $('#subtotal').text('$0.00');
            $('#fee').text('$0.00');
            $('#total').text('$0.00');
        } else if (currency == '2' && $('#usd_amount').val() == '') {
            $('#eth_amount').val('');
            $('#subtotal').text('$0.00');
            $('#fee').text('$0.00');
            $('#total').text('$0.00');
        } else {
            $.getJSON(dir_url + '/buy/calculations/' + amount + '/' + method + '/' + currency, function(json, textStatus) {
                $('#subtotal').text('$' + json.subtotal);
                $('#fee').text('$' + json.fee);
                $('#total').text('$' + json.total);
                if (currency == '1') {
                    $('#usd_amount').val(json.subtotal);
                } else if (currency == '2') {
                    $('#eth_amount').val(json.ethers);
                }
                if (json.modal) {
                    $('#myModal .modal-body').html(json.description);
                    $('#myModal').modal();
                }
                $('#limit-progress > div[role="progressbar"]').attr('aria-valuenow', (100 - json.percentage));
                $('#limit-progress > div[role="progressbar"]').css('width', (100 - json.percentage) + '%');
                $('#limit-progress > div[role="progressbar"]').text('$' + (json.limit - json.total).toFixed(2));
                if (parseFloat(json.balance) < parseFloat(json.total)) {
                    $('#usd_amount').val('');
                    $('.progress-bar-success').css('background-color', '#c5a902');
                } else if (parseFloat(json.balance) > parseFloat(json.total)) {
                    $('.progress-bar-success').css('background-color', '#5cb85c');
                }
            });
        }
    }

    $('#usd_max_buy').click(function(event) {
        event.preventDefault();
        $.getJSON(dir_url + '/buy/calc_max/' + typeMethod, function(json, textStatus) {
            $('#subtotal').text('$' + json.subtotal);
            $('#usd_amount').val(json.subtotal);
            $('#eth_amount').val(json.ethers);
            $('#fee').text('$' + json.fee);
            $('#total').text('$' + json.total);
            //calc_usd_amount();
            $('#limit-progress > div[role="progressbar"]').attr('aria-valuenow', 0);
            $('#limit-progress > div[role="progressbar"]').css('width', 0 + '%');
            $('#limit-progress > div[role="progressbar"]').text('$0.00');
            if (parseFloat(json.total) < 0) {
                $('.progress-bar-success').css('background-color', '#c5a902');
            } else if (parseFloat(json.total) > 0) {
                $('.progress-bar-success').css('background-color', '#5cb85c');
            }
        });
    });

    //$('#pay_card_form').on('click', 'button.wpwl-button-pay[type="submit"]', function(event) {
    //    event.preventDefault();
    //    console.log($('input[name="card.number"]').val());
    //});

});