<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Provinces implements Crud {
    
    public static function create($data) {
        
    }
    
    public static function read($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `provinces` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function update($data) {
        
    }
    
    public static function delete($id) {
    
    }
    

    public static function getAll() {
    
        try {
	
    		$connection = Database::instance();
			$sql = "SELECT * FROM `provinces`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll(\PDO::FETCH_ASSOC);
		
        } catch(\PDOException $e) {
		
        	return "Error!: " . $e->getMessage();
		
        }
        
    }

    public static function getByCountry($country) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `provinces` WHERE `country` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $country, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

}
?>