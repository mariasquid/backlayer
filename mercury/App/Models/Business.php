<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Business implements Crud {
    
    public static function create($data) {
    
        try {
    
            $connection = Database::instance();
            $sql = "INSERT INTO `business`(`name`, `dba`, `taxid_ein`, `source_founds`, `address`, `subcategory`, `user`) VALUES (?, ?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['name'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['dba'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['taxid_ein'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['source_founds'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['subcategory'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['user'], \PDO::PARAM_INT);
            $query->execute();
            return $connection->lastInsertId();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function read($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `business` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function update($data) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `business` SET `name` = ?, `dba` = ?, `taxid_ein` = ?, `source_founds` = ?, `address` = ?, `subcategory` = ?, `user` = ?  WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['name'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['dba'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['taxid_ein'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['source_founds'], \PDO::PARAM_INT);
            $query->bindParam(5, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['subcategory'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['user'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['id'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function delete($id) {
    
    }
    

    public static function getAll() {
    
        try {
	
    		$connection = Database::instance();
			$sql = "SELECT * FROM `users`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll(\PDO::FETCH_ASSOC);
		
        } catch(\PDOException $e) {
		
        	return "Error!: " . $e->getMessage();
		
        }
        
    }

    public static function getByUser($user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `business` WHERE `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
}
?>