<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Cellphones implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `check_cellphones`(`cellphone`, `code`, `date`) VALUES (?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['cellphone'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['code'], \PDO::PARAM_INT);
            $query->bindParam(3, $data['date'], \PDO::PARAM_INT);
            return $query->execute();
            
        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `check_cellphones` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

			$connection = Database::instance();
			$sql = "SELECT * FROM usuarios";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll(\PDO::FETCH_ASSOC);

		} catch(\PDOException $e) {

			print "Error!: " . $e->getMessage();

		}

    }

    public static function getByCode($code) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `check_cellphones` WHERE `code` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $code, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

}
?>