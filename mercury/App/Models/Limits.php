<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Limits implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `limits`(`buy_eth_card`, `buy_eth_ach`, `buy_eth_usdwallet`, `sell`, `user`) VALUES (?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['buy_eth_card'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['buy_eth_ach'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['buy_eth_usdwallet'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['sell'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['user'], \PDO::PARAM_INT);
            $query->execute();
            return $connection->lastInsertId();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `limits` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `limits` SET `buy_eth_card` = ?, `buy_eth_ach` = ?, `buy_eth_usdwallet` = ?, `deposit_card` = ?, `deposit_ach` = ?  WHERE `id` = 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['buy_eth_card'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['buy_eth_ach'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['buy_eth_usdwallet'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['deposit_card'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['deposit_ach'], \PDO::PARAM_STR);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function delete($id) {

    }

    public static function getAll() {

    }


    public static function getByUser($user) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `limits` WHERE `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

}
?>