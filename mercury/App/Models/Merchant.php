<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Merchant implements Crud {

    /**
    * @param array  Crea un nuevo registro en la tabla
    * @return bool devuelve TRUE si el registro es exitoso!
    */
    
    public static function create($data) {
    
        try {
    
            $connection = Database::instance();
            $sql = "INSERT INTO `ids_merchant`(`id_merchant`, `user`) VALUES (?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['id_merchant'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['user'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    /**
    * @param int  lee un registro
    * @return bool devuelve un objeto de la tabla
    */
    
    public static function read($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `ids_merchant` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    /**
    * @param array  actualiza un registro
    * @return bool Devuelve TRUE si la actualización es exitoso!
    */
    
    public static function update($data) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `ids_merchant` SET `field1` = ?, `field2` = ?, `field3` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['field1'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['field2'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['field3'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['id'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    /**
    * @param int  elimina un registro
    * @return bool Devuelve TRUE si se elimina con exitoso!
    */
    
    public static function delete($id) {

    try {
    
            $connection = Database::instance();
            $sql = "DELETE FROM `ids_merchant` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(4, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
    
    }

    /**
    * @param void  obtener registros
    * @return array Devuelve todos los registros de la tabla!
    */    

    public static function getAll() {
    
        try {
	
    		$connection = Database::instance();
			$sql = "SELECT * FROM `ids_merchant`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll(\PDO::FETCH_ASSOC);
		
        } catch(\PDOException $e) {
		
        	return "Error!: " . $e->getMessage();
		
        }
        
    }

    public static function getByUser($user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `ids_merchant` WHERE `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

}
?>