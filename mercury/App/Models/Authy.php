<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Authy implements Crud {

    public static function create($data) {

    }

    public static function read($id) {

    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {

    }

    public static function RequestSMS($authy_id) {

        $url = "https://api.authy.com/protected/json/sms/" . $authy_id . "?force=false&api_key=m9Es6DLWIaM0ZNZr8VqSgH2Cevq5O8qe";

        $con = curl_init();

        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, TRUE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($con);

        curl_close($con);

        return json_decode($response);

    }

    public static function verifySMS($authy_id, $code) {

        $url = "https://api.authy.com/protected/json/verify/" . $code . "/" . $authy_id . "?api_key=m9Es6DLWIaM0ZNZr8VqSgH2Cevq5O8qe";

        $con = curl_init();

        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, TRUE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($con);

        curl_close($con);

        return json_decode($response);

    }

}

?>