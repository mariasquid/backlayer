<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Buy implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `purchases`(`transaction_id`, `eth_amount`, `usd_amount`, `date`, `user`) VALUES (?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['transaction_id'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['eth_amount'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(5, $data['user'], \PDO::PARAM_INT);
            $query->execute();
            return $connection->lastInsertId();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `users` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

			$connection = Database::instance();
			$sql = "SELECT * FROM usuarios";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll(\PDO::FETCH_ASSOC);

		} catch(\PDOException $e) {

			print "Error!: " . $e->getMessage();

		}

    }


}
?>