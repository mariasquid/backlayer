<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Administrators implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `administrators`(`name`, `last_name`, `email`, `password`) VALUES (?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['name'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['last_name'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['email'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['password'], \PDO::PARAM_STR);
            $query->execute();
            return $connection->lastInsertId();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `administrators` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

			$connection = Database::instance();
			$sql = "SELECT * FROM `administrators`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll(\PDO::FETCH_ASSOC);

		} catch(\PDOException $e) {

			print "Error!: " . $e->getMessage();

		}

    }

    public static function readByEmail($email) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `administrators` WHERE `email` = ? LIMIT 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $email, \PDO::PARAM_STR);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function addTry($try, $user) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `administrators` SET `tries_login` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $try, \PDO::PARAM_INT);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function userBlock($user) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `administrators` SET `status` = 0,`tries_login` = 0 WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function restoreLoginTries($user) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `administrators` SET `tries_login` = 0 WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function changeStatus($id, $status) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `administrators` SET `status`= ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $status, \PDO::PARAM_INT);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function saveGoogleCode($code, $id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `administrators` SET `google_2fa_code` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $code, \PDO::PARAM_STR);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

}
?>