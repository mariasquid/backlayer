<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Sales implements Crud {
    
    public static function create($data) {
    
        try {
            $connection = Database::instance();
            $sql = "INSERT INTO `sales`(`eth_amount`, `usd_amount`, `commission`, `eth_value`, `payment_method`, `eth_wallet`, `date`, `status`, `bank_account`, `user`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['eth_amount'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['commission'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['eth_value'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['payment_method'], \PDO::PARAM_INT);
            $query->bindParam(6, $data['eth_wallet'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['status'], \PDO::PARAM_INT);
            $query->bindParam(9, $data['bank_account'], \PDO::PARAM_INT);
            $query->bindParam(10, $data['user'], \PDO::PARAM_INT);
            $query->execute();
            return $connection->lastInsertId();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        }

    }
    
    public static function read($id) {
    
        try {
            $connection = Database::instance();
            $sql = "SELECT * FROM `sales` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        }

    }
    
    public static function update($data) {
    
        try {
            $connection = Database::instance();
            $sql = "UPDATE `purchases` SET `transaction_id` = ?, `eth_amount` = ?, `usd_amount` = ?, `payment_status` = ?, `processed` = ?, `date` = ?, `user` = ?  WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['transaction_id'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['eth_amount'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['payment_status'], \PDO::PARAM_INT);
            $query->bindParam(5, $data['processed'], \PDO::PARAM_INT);
            $query->bindParam(6, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['user'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['id'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        }

    }
    
    public static function delete($id) {
    
    }
    
    public static function getAll() {
    
        try {
            $connection = Database::instance();
            $sql = "SELECT * FROM usuarios";
            $query = $connection->prepare($sql);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        }

    }
    
    public static function changeStatus($id, $status) {
    
        try {
            $connection = Database::instance();
            $sql = "UPDATE `sales` SET `status` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $status, \PDO::PARAM_INT);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        }

    }
    
    public static function getLastFive($user) {
    
        try {
            $connection = Database::instance();
            $sql = "SELECT * FROM `sales` WHERE `user` = ? ORDER BY `date` DESC LIMIT 5";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        }

    }


    public static function getByMethod($method, $user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `sales` WHERE `payment_method` = ? AND `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $method, \PDO::PARAM_INT);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    public static function getByMethodDates($method, $from, $to, $user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `sales` WHERE `payment_method` = ? AND `date` BETWEEN ? AND ? AND `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $method, \PDO::PARAM_INT);
            $query->bindParam(2, $from, \PDO::PARAM_INT);
            $query->bindParam(3, $to, \PDO::PARAM_INT);
            $query->bindParam(4, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    public static function getCurrentDay($date, $user) {
    
        try {
            $connection = Database::instance();
            $sql = "SELECT * FROM `sales` WHERE `date` >= ? AND `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $date, \PDO::PARAM_INT);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
}
?>