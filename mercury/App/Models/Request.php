<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Request implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `request`(`eth_amount`, `usd_amount`, `date`, `applicant`, `eth_wallets_applicant`, `payer`) VALUES (?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['eth_amount'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(4, $data['applicant'], \PDO::PARAM_INT);
            $query->bindParam(5, $data['eth_wallets_applicant'], \PDO::PARAM_INT);
            $query->bindParam(6, $data['payer'], \PDO::PARAM_INT);
            $query->execute();
            return $connection->lastInsertId();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `request` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

			$connection = Database::instance();
			$sql = "SELECT * FROM `users`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();

		} catch(\PDOException $e) {

			print "Error!: " . $e->getMessage();

		}

    }

    public static function getRequestByPayer($payer) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `request` WHERE `payer` = ? AND `status` <> 0 ORDER BY `date` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $payer, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }


    public static function updateViewsByPayer($payer) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `request` SET `viewed`= 1 WHERE `payer` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $payer, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function updateStatus($status, $id) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `request` SET `status`= ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $status, \PDO::PARAM_INT);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

}
?>