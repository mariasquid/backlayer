<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Transfers implements Crud {
    
    public static function create($data) {

    }
    
    public static function read($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `transfers` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        }

    }
    
    public static function update($data) {
    
    }
    

    public static function delete($id) {
    
    }
    

    public static function getAll() {

    }

    public static function getByUser($user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT t.* FROM transfers t, eth_wallets etw, users u WHERE (t.from_address = etw.address OR t.to_address = etw.address) AND (etw.user = u.id) AND u.id = ? ORDER BY t.start_date DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
            
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function getByWallet($wallet, $type) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `transactions` WHERE `wallet` = ? ORDER BY `date` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $wallet, \PDO::PARAM_INT);
            //$query->bindParam(2, $type, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        }

    }
    
    public static function getLastActivityByWallet($wallet) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT `date` FROM `transactions` WHERE `wallet` = ? ORDER BY `date` DESC LIMIT 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $wallet, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        }

    }
    
    public static function getLastFiveActivitysByWallet($wallet, $type) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `transactions` WHERE `wallet` = ? ORDER BY `date` DESC LIMIT 5";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $wallet, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        }

    }
    
    public static function getDates($from, $to, $eth_wallet) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT t.* FROM vw_statement_eth t, eth_wallets etw, users u WHERE t.start_date BETWEEN ? AND ? AND (t.from_address = etw.address OR t.to_address = etw.address) AND etw.address = ? GROUP BY t.txhash ORDER BY t.start_date DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $from, \PDO::PARAM_INT);
            $query->bindParam(2, $to, \PDO::PARAM_INT);
            $query->bindParam(3, $eth_wallet, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        }

    }

    public static function getTransactionsByAddress($address) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `vw_statement_eth` WHERE `from_address` = ? OR `to_address` = ? ORDER BY `start_date` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $address, \PDO::PARAM_STR);
            $query->bindParam(2, $address, \PDO::PARAM_STR);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

}
?>