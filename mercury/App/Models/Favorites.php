<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");
use \Core\Database;
use \App\Interfaces\Crud;
class Favorites implements Crud {
    
    public static function create($data) {
    
        try {
            
            $connection = Database::instance();
            $sql = "INSERT INTO `favorites_address`(`address`, `description`, `token`, `status`, `is_mercury`, `user`) VALUES (?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['description'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['token'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['status'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['is_mercury'], \PDO::PARAM_INT);
            $query->bindParam(6, $data['user'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    
    }
    
    public static function read($id) {
    
        try {
            $connection = Database::instance();
            $sql = "SELECT * FROM `favorites_address` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    
    }
    
    public static function update($data) {
    
        try {
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `name` = ?, `last_name` = ?, `email` = ?, `birthdate` = ?, `address` = ?, `city` = ?, `country` = ?, `zip_code` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['name'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['last_name'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['email'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['birthdate'], \PDO::PARAM_INT);
            $query->bindParam(5, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['city'], \PDO::PARAM_STR);
            $query->bindParam(7, $data['country'], \PDO::PARAM_STR);
            $query->bindParam(8, $data['zip_code'], \PDO::PARAM_STR);
            $query->bindParam(9, $data['id'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    
    }
    
    public static function delete($id) {
    
        try {
            $connection = Database::instance();
            $sql = "DELETE FROM `favorites_address` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_STR);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    
    }
    
    public static function getAll() {
    
        try {
			$connection = Database::instance();
			$sql = "SELECT * FROM `users`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();
		
        } catch(\PDOException $e) {
		
        	print "Error!: " . $e->getMessage();
		
        }
    
    }

    public static function getByUser($user) {
    
        try {
            $connection = Database::instance();
            $sql = "SELECT * FROM `favorites_address` WHERE `user` = ? AND `status` = 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    
    }

    public static function getByAddress($address, $user) {
    
        try {
            $connection = Database::instance();
            $sql = "SELECT * FROM `favorites_address` WHERE `address` = ? AND `user` = ? AND `status` = 1 LIMIT 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $address, \PDO::PARAM_STR);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    
    }
    
    public static function validateAddress($address, $user) {
    
        try {
            $connection = Database::instance();
            $sql = "SELECT * FROM `favorites_address` WHERE `address` = ? AND `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $address, \PDO::PARAM_INT);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    
    }
    
    public static function getByToken($token) {
    
        try {
            $connection = Database::instance();
            $sql = "SELECT * FROM `favorites_address` WHERE `token` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $token, \PDO::PARAM_STR);
            $query->execute();
            return $query->fetch();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    
    }
    
    public static function confirm($id) {
    
        try {
            $connection = Database::instance();
            $sql = "UPDATE `favorites_address` SET `status`= 1 WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    
    }
}
?>