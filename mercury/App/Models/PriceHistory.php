<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class PriceHistory implements Crud {

    public static function create($data) {

    }

    public static function read($id) {

    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {

    }

    public static function getByDateDay() {

        try {

            $connection = Database::instance();
            $sql = "SELECT CONCAT(EXTRACT(HOUR FROM `time`), ':', EXTRACT(MINUTE FROM `time`)) AS `x`, `usd` AS `y` FROM `price_history` WHERE `time` BETWEEN CONCAT(CURRENT_DATE(), ' 00:00:00') AND CONCAT(CURRENT_DATE(), ' 23:59:59') ORDER BY `time` ASC";
            $query = $connection->prepare($sql);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function getByDateWeek() {

        try {

            $connection = Database::instance();
            $sql = "SELECT EXTRACT(DAY FROM DATE(`time`)) AS `x`, `usd` AS `y` FROM `price_history` WHERE `time` BETWEEN ADDDATE(CONCAT(CURRENT_DATE(), ' 00:00:00'), INTERVAL -7 DAY) AND CONCAT(CURRENT_DATE(), ' 23:59:59') AND TIME(`time`) BETWEEN '23:00:00' and '23:59:59' ORDER BY `time` ASC";
            $query = $connection->prepare($sql);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function getByDateMonth() {

        try {

            $connection = Database::instance();
            $sql = "SELECT EXTRACT(DAY FROM DATE(`time`)) AS `x`, `usd` AS `y` FROM `price_history` WHERE `time` BETWEEN ADDDATE(CONCAT(CURRENT_DATE(), ' 00:00:00'), INTERVAL -30 DAY) AND CONCAT(CURRENT_DATE(), ' 23:59:59') AND TIME(`time`) BETWEEN '23:00:00' and '23:59:59' ORDER BY `time` ASC";
            $query = $connection->prepare($sql);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function getByDateYear() {

        try {

            $connection = Database::instance();
            $sql = "SELECT CONCAT(EXTRACT(YEAR FROM DATE(`time`)), '-', EXTRACT(MONTH FROM DATE(`time`))) AS `x`, `usd` AS `y` FROM `price_history` WHERE `time` BETWEEN ADDDATE(CONCAT(CURRENT_DATE(), ' 00:00:00'), INTERVAL -12 MONTH) and CONCAT(CURRENT_DATE(), ' 23:59:59') AND TIME(`time`) BETWEEN '23:00:00' and '23:59:59' and DATE(`time`) = LAST_DAY(DATE(`time`)) ORDER BY `time` ASC";
            $query = $connection->prepare($sql);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

}
?>