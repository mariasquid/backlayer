<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Country implements Crud {

    public static function create($data) {

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `country` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `country`";
            $query = $connection->prepare($sql);
            $query->execute();
            return $query->fetchAll();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

}
?>