<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Devices implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `devices`(`disable_tfa`, `remember`, `date`, `browser`, `ip`, `token`, `status`, `user`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['disable_tfa'], \PDO::PARAM_INT);
            $query->bindParam(2, $data['remember'], \PDO::PARAM_INT);
            $query->bindParam(3, $data['date'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['browser'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['ip'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['token'], \PDO::PARAM_STR);
            $query->bindParam(7, $data['status'], \PDO::PARAM_STR);
            $query->bindParam(8, $data['user'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `devices` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `devices` SET `disable_tfa` = ?, `remember` = ?, `date` = ?, `browser` = ?, `ip` = ?, `token` = ?, `status` = ?, `user` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['disable_tfa'], \PDO::PARAM_INT);
            $query->bindParam(2, $data['remember'], \PDO::PARAM_INT);
            $query->bindParam(3, $data['date'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['browser'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['ip'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['token'], \PDO::PARAM_STR);
            $query->bindParam(7, $data['status'], \PDO::PARAM_STR);
            $query->bindParam(8, $data['user'], \PDO::PARAM_INT);
            $query->bindParam(9, $data['id'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function delete($id) {

        try {

            $connection = Database::instance();
            $sql = "DELETE FROM `devices` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            return$query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function getAll() {

        try {

			$connection = Database::instance();
			$sql = "SELECT * FROM usuarios";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();

		} catch(\PDOException $e) {

			print "Error!: " . $e->getMessage();

		}

    }


    public static function getAllByUser($user) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `devices` WHERE `user` = ? AND `status` = 1 ORDER BY `date` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }


    public static function getByIP($ip, $user) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `devices` WHERE `ip` = ? AND `user` = ? ORDER BY `date` DESC LIMIT 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $ip, \PDO::PARAM_STR);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }


    public static function changeStatus($id, $status, $disable_tfa) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `devices` SET `disable_tfa` = ?, `status` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $disable_tfa, \PDO::PARAM_INT);
            $query->bindParam(2, $status, \PDO::PARAM_INT);
            $query->bindParam(3, $id, \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function getByToken($token) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `devices` WHERE `token` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $token, \PDO::PARAM_STR);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }


    public static function changeToken($id, $token) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `devices` SET `token` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $token, \PDO::PARAM_STR);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }


}
?>