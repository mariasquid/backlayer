<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Withdraw implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `withdraw`(`wire_code`, `usd_amount`, `commission`, `eth_value`, `date`, `bank_account`, `user`) VALUES (?, ?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['wire_code'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['commission'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['eth_value'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(6, $data['bank_account'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['user'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `withdraw` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

			$connection = Database::instance();
			$sql = "SELECT * FROM `withdraw`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();

		} catch(\PDOException $e) {

			print "Error!: " . $e->getMessage();

		}

    }

    public static function getByUser($user) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `withdraw` WHERE `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function getDates($from, $to, $user) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `withdraw` WHERE `date` BETWEEN ? AND ? AND `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $from, \PDO::PARAM_INT);
            $query->bindParam(2, $to, \PDO::PARAM_INT);
            $query->bindParam(3, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function getLastFive($user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `withdraw` WHERE `user` = ? ORDER BY `date` DESC LIMIT 5";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }

    public static function getCurrentDay($date, $user) {
    
        try {
            $connection = Database::instance();
            $sql = "SELECT * FROM `withdraw` WHERE `date` >= ? AND `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $date, \PDO::PARAM_INT);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }


}
?>