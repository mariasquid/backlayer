<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class ETHWallets implements Crud {
   
    public static function create($data) {
   
        try {
   
            $connection = Database::instance();
            $sql = "INSERT INTO `eth_wallets`(`json_wallet`, `private_key`, `public_key`, `address`, `primary`, `user`) VALUES (?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['json_wallet'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['private_key'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['public_key'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['primary'], \PDO::PARAM_INT);
            $query->bindParam(6, $data['user'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
   
    public static function read($id) {
   
        try {
   
            $connection = Database::instance();
            $sql = "SELECT * FROM `eth_wallets` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
        
    }
   
    public static function update($data) {
   
        try {
   
            $connection = Database::instance();
            $sql = "UPDATE `eth_wallets` SET `json_wallet` = ?, `private_key` = ?, `public_key` = ?, `address` = ?, `alias` = ?, `primary` = ?, `user` = ?  WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['json_wallet'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['private_key'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['public_key'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['alias'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['primary'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['user'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['id'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
        
    }
   
    public static function delete($id) {
   
    }
   

    public static function getAll() {
   
        try {
	
    		$connection = Database::instance();
			$sql = "SELECT * FROM `eth_wallets`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll(\PDO::FETCH_ASSOC);
		
        } catch(\PDOException $e) {
		
        	print "Error!: " . $e->getMessage();
		
        }
        
    }
   
    public static function getByUser($user) {
   
        try {
   
            $connection = Database::instance();
            $sql = "SELECT etw.id, etw.primary, etw.address, etw.alias, v.wallet_balance FROM eth_wallets etw INNER JOIN vw_available_eth v ON v.id_wallet = etw.id WHERE etw.user = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
        
    }
   
    public static function getEthWallet($id) {
   
        try {
   
            $connection = Database::instance();
            $sql = "SELECT etw.id, etw.primary, etw.address, etw.alias, v.wallet_balance, u.id AS id_user FROM eth_wallets etw INNER JOIN vw_available_eth v ON v.id_wallet = etw.id INNER JOIN users u ON u.id = etw.user WHERE etw.id = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
        
    }
   
    public static function getByAddress($address) {
   
        try {
   
            $connection = Database::instance();
            $sql = "SELECT * FROM `eth_wallets` WHERE `address` = ? LIMIT 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $address, \PDO::PARAM_STR);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
        
    }
   
    public static function getPrimaryWalletEthByUser($user) {
   
        try {
   
            $connection = Database::instance();
            $sql = "SELECT * FROM `eth_wallets` WHERE `primary` = 1 AND `user` = ? LIMIT 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
        
    }
   
    public static function changePrimary($id, $user) {
   
        try {
   
            $connection = Database::instance();
            
            $sql = "UPDATE `eth_wallets` SET `primary` = 0 WHERE `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            
            if ($query->execute()) {
                
                $sql = "UPDATE `eth_wallets` SET `primary` = 1 WHERE `id` = ?";
                $query = $connection->prepare($sql);
                $query->bindParam(1, $id, \PDO::PARAM_INT);
                return $query->execute();
                
            }
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
        
    }
   
    public static function changeAlias($alias, $id) {
   
        try {
   
            $connection = Database::instance();
            $sql = "UPDATE `eth_wallets` SET `alias` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $alias, \PDO::PARAM_STR);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
        
    }


    public static function changeBalance($transactions, $id) {
   
        try {
   
            $connection = Database::instance();
            $sql = "UPDATE `eth_wallets` SET `transactions` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $transactions, \PDO::PARAM_STR);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    public static function getBalance($id_wallet) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `vw_available_eth` WHERE `id_wallet` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id_wallet, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    public static function getTotalBalance($user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT SUM(`wallet_balance`) AS `balance` FROM `vw_available_eth` WHERE `id_user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_STR);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    public static function statement_eth($start_date, $end_date, $address) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT t.* FROM vw_statement_eth t, eth_wallets etw, users u WHERE t.start_date BETWEEN ? AND ? AND (t.from_address = etw.address OR t.to_address = etw.address) AND etw.address = ? GROUP BY t.txhash ORDER BY t.start_date DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $start_date, \PDO::PARAM_STR);
            $query->bindParam(2, $end_date, \PDO::PARAM_STR);
            $query->bindParam(3, $address, \PDO::PARAM_STR);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
}
?>