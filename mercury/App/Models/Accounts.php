<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Accounts implements Crud {

    public static function create($data) {

    }

    public static function read($id) {

    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {

    }


    public static function getPrimaryWalletEthByUser($user) {

        try {

            $connection = Database::instance();
            $sql = "SELECT `wallets`.*, MAX(`transactions`.`date`) as `last_date` FROM `wallets` INNER JOIN `transactions` ON `transactions`.`wallet` = `wallets`.`id` WHERE `wallets`.`principal` = 1 AND `wallets`.`user` = ? LIMIT 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function getBankAccountByUser($user) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `bank_account` WHERE `user` = ? LIMIT 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }
}
?>