<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Purchases implements Crud {
    
    public static function create($data) {
    
        try {
    
            $connection = Database::instance();
            $sql = "INSERT INTO `purchases`(`checkout_id`, `eth_amount`, `usd_amount`, `commission`, `eth_value`, `payment_method`, `date`, `status`, `eth_wallet`, `user`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['checkout_id'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['eth_amount'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['commission'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['eth_value'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['payment_method'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['status'], \PDO::PARAM_INT);
            $query->bindParam(9, $data['eth_wallet'], \PDO::PARAM_INT);
            $query->bindParam(10, $data['user'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }
    
    public static function read($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `purchases` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }
    
    public static function update($data) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `purchases` SET `checkout_id` = ?, `eth_amount` = ?, `usd_amount` = ?, `commission` = ?, `eth_value` = ?, `payment_method` = ?, `date` = ?, `status` = ?, `eth_wallet` = ?, `user` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['checkout_id'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['eth_amount'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['commission'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['eth_value'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['payment_method'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['status'], \PDO::PARAM_INT);
            $query->bindParam(9, $data['eth_wallet'], \PDO::PARAM_INT);
            $query->bindParam(10, $data['user'], \PDO::PARAM_INT);
            $query->bindParam(11, $data['id'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }
    
    public static function updateCommission($id, $commission) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `purchases` SET `commission` = ?  WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $commission, \PDO::PARAM_STR);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }
    
    public static function delete($id) {
    
    }
    

    public static function getAll() {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT `purchases`.*, `users`.`name`, `users`.`last_name`, `users`.`email` FROM `purchases` INNER JOIN `users` ON `users`.`id` = `purchases`.`user`";
            $query = $connection->prepare($sql);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }
    
    public static function getLastFive($user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `purchases` WHERE `user` = ? ORDER BY `date` DESC LIMIT 5";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }

    public static function getByMethod($method, $user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `purchases` WHERE `payment_method` = ? AND `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $method, \PDO::PARAM_INT);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    public static function getByMethodDates($method, $from, $to, $user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `purchases` WHERE `payment_method` = ? AND `date` BETWEEN ? AND ? AND `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $method, \PDO::PARAM_INT);
            $query->bindParam(2, $from, \PDO::PARAM_INT);
            $query->bindParam(3, $to, \PDO::PARAM_INT);
            $query->bindParam(4, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }    

    public static function getCurrentDay($date, $user) {
    
        try {
            $connection = Database::instance();
            $sql = "SELECT * FROM `purchases` WHERE `date` >= ? AND `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $date, \PDO::PARAM_INT);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
}
?>