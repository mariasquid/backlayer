<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class USDWallets implements Crud {
    
    public static function create($data) {
    
        try {
            
            $connection = Database::instance();
            $sql = "INSERT INTO `usd_wallets`(`address`, `balance`, `user`) VALUES (?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['balance'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['user'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }

    }
    
    public static function read($id) {
    
        try {
            
            $connection = Database::instance();
            $sql = "SELECT * FROM `vw_available_usd` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }

    }
    
    public static function update($data) {
    
        try {
            
            $connection = Database::instance();
            $sql = "UPDATE `usd_wallets` SET `json_wallet` = ?, `private_key` = ?, `public_key` = ?, `address` = ?, `alias` = ?, `principal` = ?, `user` = ?  WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['json_wallet'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['private_key'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['public_key'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['alias'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['principal'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['user'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['id'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }

    }
    
    public static function delete($id) {
    
    }
    
    public static function getAll() {
    
        try {
            
			$connection = Database::instance();
			$sql = "SELECT * FROM `vw_available_usd`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();
		
        } catch(\PDOException $e) {
		
        	print "Error!: " . $e->getMessage();
		
        }

    }
    
    public static function getByUser($user) {
    
        try {
            
            $connection = Database::instance();
            $sql = "SELECT * FROM `vw_available_usd` WHERE `user`  = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }

    }
    
    public static function changeBalance($balance, $id) {
    
        try {
            
            $connection = Database::instance();
            $sql = "UPDATE `usd_wallets` SET `balance` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $balance, \PDO::PARAM_INT);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }

    }
}
?>