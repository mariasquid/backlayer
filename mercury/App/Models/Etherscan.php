<?php

namespace App\Models;

defined("APPPATH") OR die("Access denied");

use \Core\Database;

use \App\Interfaces\Crud;



class Etherscan implements Crud {

    

    public static function create($data) {



        try {

    

            $connection = Database::instance();

            $sql = "INSERT INTO `transacciones_eth`(`from`, `to`, `value`, `date`) VALUES (?, ?, ?, ?)";

            $query = $connection->prepare($sql);

            $query->bindParam(1, $data['from'], \PDO::PARAM_STR);

            $query->bindParam(2, $data['to'], \PDO::PARAM_STR);

            $query->bindParam(3, $data['value'], \PDO::PARAM_STR);

            $query->bindParam(4, $data['date'], \PDO::PARAM_INT);

            return $query->execute();

        

        } catch(\PDOException $e) {

        

            return "Error!: " . $e->getMessage();

        

        }

    

    }

    

    public static function read($id) {

    

    }

    

    public static function update($data) {

    

    }

    

    public static function delete($id) {

    

    }

    

    public static function getAll() {

    

    }



    public static function reporte($desde, $hasta) {

    

        try {

    

            $connection = Database::instance();

            $sql = "SELECT SUM(`value`) AS `suma` FROM `transacciones_eth` WHERE `date` BETWEEN ? AND ?";

            $query = $connection->prepare($sql);

            $query->bindParam(1, $desde, \PDO::PARAM_STR);

            $query->bindParam(2, $hasta, \PDO::PARAM_STR);

            $query->execute();

            return $query->fetch();

        

        } catch(\PDOException $e) {

        

            return "Error!: " . $e->getMessage();

        

        }

        

    }

    

    public static function getLastPrice() {

    

        $url = "https://api.etherscan.io/api?module=stats&action=ethprice&apikey=A87DU99Y5WQFV3HY5AJCEV6511TQZDGDRX";

        $json = file_get_contents($url);

        $obj = json_decode($json);

        $obj->result->ethusd = number_format($obj->result->ethusd, 2);

        

        return $obj->result;

        /*

        ethbtc: "0.036",

        ethbtc_timestamp: "1491805323",

        ethusd: "43.72",

        ethusd_timestamp: "1491805299"

        */

    }

    

    public static function _getBalance($address) {

    

        $url = "https://api.etherscan.io/api?module=account&action=balance&address=" . $address . "&tag=latest&apikey=A87DU99Y5WQFV3HY5AJCEV6511TQZDGDRX";

        $json = file_get_contents($url);

        $obj = json_decode($json);

        

        return $obj->result;

    }

    

    public static function getlistTransactions($address) {

    

        $url = "http://api.etherscan.io/api?module=account&action=txlist&address=" . $address . "&startblock=0&endblock=99999999&sort=asc&apikey=A87DU99Y5WQFV3HY5AJCEV6511TQZDGDRX";

        $json = file_get_contents($url);

        $obj = json_decode($json);

        

        return $obj->result;

        /*

        [

            {

                blockNumber: "3502775",

                timeStamp: "1491708621",

                hash: "0xe8b62c8ef8c2832991e65e3550fde2d5d21aeaa7de61cfa0f9b34ffdde08590e",

                nonce: "0",

                blockHash: "0x7448917a3072a9b24d30b5764058161a6ecc615a0ecb903561709c9b30cba8d9",

                transactionIndex: "17",

                from: "0xf67b8d0ca4d45fad7b30261211387229ad39abe2",

                to: "0x3b8fa2811bb6c863c807da6d0d702c6a759eec12",

                value: "22737610000000000",

                gas: "90000",

                gasPrice: "20000000000",

                isError: "0",

                input: "0x",

                contractAddress: "",

                cumulativeGasUsed: "710652",

                gasUsed: "21000",

                confirmations: "6911"

            }

        ]

        */

    }

    

    public static function fromWei($eth_amount) {

    

        $eth_amount = $eth_amount / 1000000000000000000;

        return number_format($eth_amount, 5);

    }

    

    public static function getBalance($address) {

    

        $data = '{"jsonrpc":"2.0","method":"eth_getBalance","params":["' . $address . '", "latest"],"id":1}';

        //$url = 'http://167.114.81.219:8545';
        $url = 'http://192.95.5.106:8545';

        $con = curl_init();

        curl_setopt($con, CURLOPT_URL, $url);

        curl_setopt($con, CURLOPT_HTTPGET, FALSE);

        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));

        curl_setopt($con, CURLOPT_POST, 1);

        curl_setopt($con, CURLOPT_POSTFIELDS, $data);

        curl_setopt($con, CURLOPT_HEADER, FALSE);

        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($con);

        curl_close($con);

        $response = json_decode($response);

        $balance = hexdec($response->result);

        $balance = $balance / 1000000000000000000;

        return $balance;

    }

    /* Implementado por Maria para utilizar los end points
     * Devuelve un json decodificado si la api retorna un json
     * Devuelve un array si la api retorna un array
     * Funciona para todos los M�todos y env�a el Autorization que toma de la Session
     * M�todos: POST, PUT, GET etc
     * Data: array("param" => "value") ==> index.php?param=value
     */

    public static function callAPI($method, $url, $data = false)
    {
        $curl = curl_init();
        $header = array();
        $header[] = 'Content-length: 0';
        $header[] = 'Content-type: application/json';
        $header[] = 'Accept: application/json';
        $header[] = 'Authorization: bearer '.$_SESSION['jwt'];
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, true);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, true);
                break;
            default:
                if ($data){
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }else{
                    $url = $url;
                }
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $body = substr($result, $header_size);
        curl_close($curl);
        if(!is_array($body)){
            $response = json_decode($body);
            return $response;
        }else{
            return $body;
        }
    }
}

?>