<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Transactions implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `transactions`(`transaction_id_stripe`, `wire_code`, `value_eth`, `usd_amount`, `commission`, `eth_amount`, `type_wallet`, `type_transaction`, `payment_method`, `description`, `date`, `wallet`, `status`, `user`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['transaction_id_stripe'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['wire_code'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['value_eth'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['commission'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['eth_amount'], \PDO::PARAM_STR);
            $query->bindParam(7, $data['type_wallet'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['type_transaction'], \PDO::PARAM_INT);
            $query->bindParam(9, $data['payment_method'], \PDO::PARAM_INT);
            $query->bindParam(10, $data['description'], \PDO::PARAM_STR);
            $query->bindParam(11, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(12, $data['wallet'], \PDO::PARAM_INT);
            $query->bindParam(13, $data['status'], \PDO::PARAM_INT);
            $query->bindParam(14, $data['user'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            return "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `users` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

			$connection = Database::instance();
			$sql = "SELECT * FROM usuarios";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();

		} catch(\PDOException $e) {

			print "Error!: " . $e->getMessage();

		}

    }

    public static function getByWallet($wallet, $type) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `transactions` WHERE `wallet` = ? ORDER BY `date` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $wallet, \PDO::PARAM_INT);
            //$query->bindParam(2, $type, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function getLastActivityByWallet($wallet) {

        try {

            $connection = Database::instance();
            $sql = "SELECT `date` FROM `transactions` WHERE `wallet` = ? ORDER BY `date` DESC LIMIT 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $wallet, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function getLastFiveActivitysByWallet($wallet, $type) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `transactions` WHERE `wallet` = ? ORDER BY `date` DESC LIMIT 5";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $wallet, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

}
?>