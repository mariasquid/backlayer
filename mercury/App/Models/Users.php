<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");
use \Core\Database;
use \App\Interfaces\Crud;
class Users implements Crud {
    
    public static function create($data) {
    
        try {
    
            $connection = Database::instance();
            $sql = "INSERT INTO `users`(`name`, `last_name`, `email`, `phonecode`, `phone`, `authy_id`, `password`, `country_id`, `token`, `date_admission`, `type_user`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['name'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['last_name'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['email'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['phonecode'], \PDO::PARAM_INT);
            $query->bindParam(5, $data['phone'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['authy_id'], \PDO::PARAM_STR);
            $query->bindParam(7, $data['password'], \PDO::PARAM_STR);
            $query->bindParam(8, $data['country_id'], \PDO::PARAM_INT);
            $query->bindParam(9, $data['token'], \PDO::PARAM_STR);
            $query->bindParam(10, $data['date_admission'], \PDO::PARAM_INT);
            $query->bindParam(11, $data['type_user'], \PDO::PARAM_INT);
            $query->execute();
            return $connection->lastInsertId();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function read($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `users` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function update($data) {
    
        try {
    
            $connection = Database::instance();
            //$sql = "UPDATE `users` SET `name` = ?, `last_name` = ?, `email` = ?, `birthdate` = ?, `occupation` = ?, `profession` = ?, `ssn` = ?, `address` = ?, `state` = ?, `city` = ?, `country` = ?, `zip_code` = ? WHERE `id` = ?";
            $sql = "UPDATE `users` SET `name` = ?, `last_name` = ?, `ndocument` = ?, `email` = ?, `phonecode` = ?, `phone` = ?, `birthdate` = ?, `occupation` = ?, `profession` = ?, `ssn` = ?, `address` = ?, `city` = ?, `province_id` = ?, `country_id` = ?, `zip_code` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['name'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['last_name'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['ndocument'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['email'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['phonecode'], \PDO::PARAM_INT);
            $query->bindParam(6, $data['phone'], \PDO::PARAM_STR);
            $query->bindParam(7, $data['birthdate'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['occupation'], \PDO::PARAM_STR);
            $query->bindParam(9, $data['profession'], \PDO::PARAM_STR);
            $query->bindParam(10, $data['ssn'], \PDO::PARAM_STR);
            $query->bindParam(11, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(12, $data['city'], \PDO::PARAM_STR);
            $query->bindParam(13, $data['province_id'], \PDO::PARAM_INT);
            $query->bindParam(14, $data['country_id'], \PDO::PARAM_INT);
            $query->bindParam(15, $data['zip_code'], \PDO::PARAM_STR);
            $query->bindParam(16, $data['id'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function delete($id) {
    
    }
    

    public static function getAll() {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `users`";
            $query = $connection->prepare($sql);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function readByEmail($email) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `users` WHERE `email` = ? LIMIT 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $email, \PDO::PARAM_STR);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    public static function getByToken($token) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `users` WHERE `token` = ? LIMIT 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $token, \PDO::PARAM_STR);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function addTry($try, $user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `tries_login` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $try, \PDO::PARAM_INT);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function userBlock($user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `status` = 0,`tries_login` = 0 WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function restoreLoginTries($user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `tries_login` = 0 WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function validateEmail($token) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `users` WHERE `token` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $token, \PDO::PARAM_STR);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function changeStatus($user, $status) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `status`= ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $status, \PDO::PARAM_INT);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function updateToken($user, $token) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `token`= ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $token, \PDO::PARAM_STR);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function updateAvatar($avatar, $user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `avatar` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $avatar, \PDO::PARAM_STR);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function addStripeId($stripe, $user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `stripe_id` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $stripe, \PDO::PARAM_STR);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function changePassword($password, $user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `password` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $password, \PDO::PARAM_STR);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function addAuthyId($authy_id, $id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `authy_id` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $authy_id, \PDO::PARAM_STR);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function verifyPhone($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `verified_phone` = 1, `2fa` = 1 WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function updateLimits($data) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `limit_buy_eth_card` = ?, `limit_buy_eth_ach` = ?, `limit_buy_eth_usd` = ?  WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['limit_buy_eth_card'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['limit_buy_eth_ach'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['limit_buy_eth_usd'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['id'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function disableTFA($date, $id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `disable_tfa` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $date, \PDO::PARAM_INT);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function activeAuthyTFA($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `2fa_authy` = 1 WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function activeGoogleTFA($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `2fa_google` = 1 WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function inactiveAuthyTFA($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `2fa_authy` = 0 WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function inactiveGoogleTFA($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `2fa_google` = 0 WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function saveGoogleCode($code, $id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `google_2fa_code` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $code, \PDO::PARAM_STR);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }


    public static function updateCountryId($country_id, $id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `country_id` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $country_id, \PDO::PARAM_INT);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    public static function addPhone($data) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `phonecode` = ?, `phone` = ?, `authy_id` = ?, `verified_phone` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['phonecode'], \PDO::PARAM_INT);
            $query->bindParam(2, $data['phone'], \PDO::PARAM_INT);
            $query->bindParam(3, $data['authy_id'], \PDO::PARAM_INT);
            $query->bindParam(4, $data['verified_phone'], \PDO::PARAM_INT);
            $query->bindParam(5, $data['id'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }


    public static function getUsers($from, $to) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `sales` WHERE `date` BETWEEN ? AND ? and `status` = 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $from, \PDO::PARAM_STR);
            $query->bindParam(2, $to, \PDO::PARAM_STR);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    public static function getJWT($email, $password) {
    
        $data = 'email=' . $email . '&password=' . $password;
        $url = 'https://apidev.mercury.cash/security/loginweb';
        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($data)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $data);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        if (!empty($response->token) && isset($response->token)) {
            return $response->token;
        } else {
            return NULL;
        }
    }

}
?>