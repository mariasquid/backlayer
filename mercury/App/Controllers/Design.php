<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Users;

class Design {

    public function index() {

        View::set("title", "Diseño");
        View::render("design");

    }

}
?>