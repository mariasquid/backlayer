<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Users,
	\App\Models\Etherscan,
    \App\Models\ETHWallets,
    \App\Models\USDWallets,
    \App\Models\Transactions,
    \App\Models\Request,
    \App\Models\Purchases,
    \App\Models\Sales,
    \App\Models\Withdraw,
    \App\Models\Transfers,
    \App\Models\PriceHistory,
    \App\Models\Deposits;

class Dashboard {
   
    public function index() {
   
    	if (!empty($_SESSION['id'])) {
            
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }
            
            /* HEADER */
            /* Modificado por Maria, se coment� las sentencias que hac�an referencia a M�todos de consulta de la base de datos
             * Se llama a la funcion implementada en el Modelo Etherscan callAPI y se procesa el response en base a la salida
             * obtenida, el resultado se env�a a la vista tal y como se ha hecho hasta ahora desde este Controlador
             */
            //$price = Etherscan::getLastPrice();
            //$ethusd = $price->ethusd;
            //View::set("ethusd", $ethusd);
            //Generic Method to get response

            $result=Etherscan::callAPI("GET","https://apidev.mercury.cash/ethereum/price", false);

            if (!empty($result->result->ethusd) && isset($result->result->ethusd)) {
                View::set("ethusd", $result->result->ethusd);
                $ethusd = $result->result->ethusd;
            }else{
                View::set("ethusd", "Not Value");
                $ethusd = 0.0;
            }
            $_SESSION['ethusd']=$ethusd;
            /* Modificado por Maria, se coment� las sentencias que hac�an referencia a M�todos de consulta de la base de datos
             * Se llama a la funcion implementada en el Modelo Etherscan callAPI y se procesa el response en base a la salida
             * obtenida, el resultado se env�a a la vista tal y como se ha hecho hasta ahora desde este Controlador
             */
            //$eth_total_balance = ETHWallets::getTotalBalance($_SESSION['id']);
            $res_total_wallet = Etherscan::callAPI("GET","https://apidev.mercury.cash/ethereum/ethbalance", false);
            //var_dump($res_total_wallet);
            if(!empty($res_total_wallet)){
                if(is_array($res_total_wallet)){
                    $parsed_result = $res_total_wallet;
                }else{
                    $parsed_result = $res_total_wallet;
                }
                $sum = 0;

                if (is_array($parsed_result) || is_object($parsed_result))
                {
                    foreach ($parsed_result as $value) {
                        $sum += $value->eth;
                    }
                }else{
                    $sum = $parsed_result[0]->eth;
                }
                //echo "sum is {$sum}";
                $eth_total_balance = $sum;
            }else{
                $eth_total_balance = 0.0;
            }
            $res=Etherscan::callAPI("GET","https://apidev.mercury.cash/ethereum/usdbalance", false);
            if(!empty($res)){
                if(is_array($res)){
                    $res_encode = json_encode($res[0]); //Porque este endpoint devuelve un array
                    $res_decode_again = json_decode($res_encode);
                }
                $usd_total_balance = $res_decode_again->usd;
            }else{
                $usd_total_balance = 0.0;
            }

            //echo $usd_total_balance;

            //$usd_total_balance = USDWallets::read($_SESSION['id']);

            
            //TRANSACTIONS
            
            $purchases = Purchases::getLastFive($_SESSION['id']);
            $purchases = json_encode($purchases);
            $purchases = json_decode($purchases);
            
            $sales = Sales::getLastFive($_SESSION['id']);
            $sales = json_encode($sales);
            $sales = json_decode($sales);
            
            $withdraw = Withdraw::getLastFive($_SESSION['id']);
            $withdraw = json_encode($withdraw);
            $withdraw = json_decode($withdraw);
            
            $deposits = Deposits::getLastFive($_SESSION['id']);
            $deposits = json_encode($deposits);
            $deposits = json_decode($deposits);

            $transfers = Transfers::getByUser($_SESSION['id']);
            $transfers = json_encode($transfers);
            $transfers = json_decode($transfers);
            
            $transactions = array();
            
            for ($i=0; $i < 5; $i++) {
            
                if (!empty($purchases[$i])) {
                    $purchases[$i]->type_transaction = 1;
                    $purchases[$i]->title_transaction = 'Bought Ethereum';
                    array_push($transactions, $purchases[$i]);
                }
            
                if (!empty($sales[$i])) {
                    $sales[$i]->type_transaction = 2;
                    $sales[$i]->title_transaction = 'Sold Ethereum';
                    array_push($transactions, $sales[$i]);
                }

                if (!empty($withdraw[$i])) {
                    $withdraw[$i]->type_transaction = 3;
                    $withdraw[$i]->title_transaction = 'Withdraw from USD Wallet';
                    array_push($transactions, $withdraw[$i]);
                }

                if (!empty($deposits[$i])) {
                    $deposits[$i]->type_transaction = 4;
                    $deposits[$i]->title_transaction = 'Deposit to USD Wallet';
                    array_push($transactions, $deposits[$i]);
                }
            
                if (!empty($transfers[$i])) {
                    $transfers[$i]->type_transaction = 5;
                    $transfers[$i]->usd_amount = number_format($ethusd * $transfers[$i]->value, 2);
                    $transfers[$i]->title_transaction = 'Sent Ethereum';
                    $date = date_create($transfers[$i]->start_date);
                    $transfers[$i]->date = date_format($date, 'U');;
                    array_push($transactions, $transfers[$i]);
                }
            }
            
            $total = count($transactions);
            
            for ($i=1; $i < $total; $i++) {
                for ($j=0; $j < $total-$i; $j++) {
                    if ($transactions[$j]->date < $transactions[$j+1]->date) {
                        $k = $transactions[$j + 1];
                        $transactions[$j + 1] = $transactions[$j];
                        $transactions[$j] = $k;
                    }
                }
            }
            
            $t = array();
            
            for ($i=0; $i < 5; $i++) { 
            
                if (!empty($transactions[$i])) {
                    array_push($t, $transactions[$i]);
                }
            
            }

            $eth_wallets = array();
            $myethwallets = ETHWallets::getByUser($_SESSION['id']);
            foreach ($myethwallets as $ethwallet) {
                array_push($eth_wallets, $ethwallet['address']);
            }
            
            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            
            for ($i=0; $i < count($requests); $i++) {
            
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            
            }
            
            View::set("eth_wallets", $eth_wallets);
            View::set("eth_total_balance", $eth_total_balance);
            View::set("usd_total_balance", $usd_total_balance);
            View::set("transactions", $t);
            View::set("title", "Dashboard");
            View::set("requestsUnread", $unread);
            View::render("dashboard");
        } else {
            header("Location: " . DIR_URL . "/login");
        }
    }


    public function priceDay() {

        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
        header("Content-Type: application/json");

        if (!empty($_SESSION['id'])) {
            
            $price = PriceHistory::getByDateDay();
            
            echo json_encode($price);

        }

    }

    public function priceWeek() {

        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
        header("Content-Type: application/json");

        if (!empty($_SESSION['id'])) {

            $price = PriceHistory::getByDateWeek();

            echo json_encode($price);

        }

    }

    public function priceMonth() {

        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
        header("Content-Type: application/json");

        if (!empty($_SESSION['id'])) {
            
            $price = PriceHistory::getByDateMonth();

            echo json_encode($price);

        }

    }

    public function priceYear() {

        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
        header("Content-Type: application/json");

        if (!empty($_SESSION['id'])) {
            
            $price = PriceHistory::getByDateYear();

            echo json_encode($price);

        }

    }
}
?>