<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Buy,
    \App\Models\Users,
    \App\Models\ETHWallets,
    \App\Models\WalletAdmin,
    \App\Models\Purchases as PurchasesModel,
    \App\Models\Transactions,
    \App\Controllers\Mailer;

class Purchases {

    public function index() {

    	if (!empty($_SESSION)) {

            $purchases = Transactions::getPurchases();

            View::set("purchases", $purchases);
            View::set("title", "Purchases");
            View::render("purchases");

        } else {
            header("Location: " . DIR_URL . "/home");
        }

    }

    public function details($id) {

        if (!empty($_SESSION)) {

            $purchase = Transactions::read($id);
            $user = Users::read($purchase['user']);
            $wallets = ETHWallets::getByUser($purchase['user']);
            $address = NULL;

            foreach ($wallets as $wallet) {
                if ($wallet['principal'] == 1) {
                    $address = $wallet['address'];
                }
            }

            View::set("purchase", $purchase);
            View::set("user", $user);
            View::set("address", $address);
            View::set("title", "Purchases");
            View::render("purchase-details");

        } else {
            header("Location: " . DIR_URL . "/home");
        }

    }

    public function process() {

        if (!empty($_SESSION)) {

            $wallet = WalletAdmin::read(1);

            echo json_encode($wallet);

        } else {
            header("Location: " . DIR_URL . "/home");
        }

    }

    public function changeStatus() {

        if (!empty($_SESSION)) {

            $purchase = PurchasesModel::read($_POST['id']);

            $data = array();

            $data['id'] = $_POST['id'];
            $data['transaction_id'] = $purchase['transaction_id'];
            $data['eth_amount'] = $purchase['eth_amount'];
            $data['usd_amount'] = $purchase['usd_amount'];
            $data['payment_status'] = $purchase['payment_status'];
            $data['processed'] = 1;
            $data['date'] = $purchase['date'];
            $data['user'] = $purchase['user'];

            $result = PurchasesModel::update($data);

            $user = Users::read($purchase['user']);
            $mail = Mailer::pay_processed($user['name'], $user['email'], $purchase['eth_amount']);

            $messageMail = "el correo se envio";

            echo $result . " + " . $messageMail;

        } else {
            header("Location: " . DIR_URL . "/home");
        }

    }

}
?>