<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Etherscan;

class Terms {

    public function index() {

        session_destroy();

		$price = Etherscan::getLastPrice();
		$ethusd = $price->ethusd;

		View::set("ethusd", $ethusd);
        View::set("title", "Terms and Conditions (User Agreement)");
        View::render("terms");

    }

}
?>