<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Users,
    \App\Models\ETHWallets,
    \App\Models\Logs,
    \App\Models\Authy,
    \App\Models\Accounts,
    \App\Controllers\Mailer,
    \App\Controllers\Functions,
    \App\Models\TransferValidation,
    \App\Models\Devices;

class Login {

    public function matar() {
        session_destroy();
    }

    public function logininternoabcd($user) {

        $user = Users::read($user);

        $wallet = ETHWallets::getPrimaryWalletEthByUser($user['id']);
        $_SESSION['id'] = $user['id'];
        $_SESSION['name'] = $user['name'] . " " . $user['last_name'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['avatar'] = DIR_URL . "/administrator/public/avatars/" . $user['avatar'];
        $_SESSION['status'] = $user['status'];
        $_SESSION['address'] = $wallet['address'];
        $_SESSION['wallet'] = $wallet['id'];
        $_SESSION['time'] = time();
        $_SESSION['validateToken'] = hash('sha256', md5(uniqid(mt_rand(), true)));

        header("Location: " . DIR_URL . "/dashboard");
        exit;

    }
    
    public function index() {

        $tries = NULL;
        if (!empty($_SESSION['tries'])) {
            $tries = $_SESSION['tries'];
        }
    
        if (empty($_SESSION['id'])) {

            //session_destroy();

            if (!is_null($tries)) {
                $_SESSION['tries'] = $tries;
            }

            View::set("title", "Sign In");
            View::render("login");
        
        } else {
            
            if (!empty($_SERVER['HTTP_REFERER'])) {
                header("Location: " . $_SERVER['HTTP_REFERER']);
                exit;
            } else {
                header("Location: " . DIR_URL . "/dashboard");
                exit;
            }

        }
    }
    
    public function sign() {
    
        if (empty($_SESSION['id'])) {
            
            if (!empty($_POST['email']) && !empty($_POST['password'])) {
                
                $email = htmlspecialchars($_POST['email'], ENT_QUOTES);
                $provicional = $_POST['password'];
                $password = md5($_POST['password']);
                $response = TRUE;
                $res = NULL;
                $user = Users::readByEmail($email);
                $device = Devices::getByIP($_SERVER['REMOTE_ADDR'], $user['id']);
                
                if (!empty($_SESSION['tries'])) {
                    $response = NULL;
                    $res = NULL;
                    
                    require_once LIBSPATH . "/recaptcha/recaptchalib.php";
                    
                    if (!empty($_POST['g-recaptcha-response'])) {
                    
                        $secret = "6LftmSQUAAAAAM_9SAp4XlIKc_kBcEy6wr2eIoo7";
                        $reCaptcha = new \ReCaptcha($secret);
                        
                        $res = $reCaptcha->verifyResponse(
                            $_SERVER["REMOTE_ADDR"],
                            $_POST["g-recaptcha-response"]
                            );
                        
                        $response = ($res->success) ? $res->success : NULL;
                    }
                }
                
                if (!empty($user)) {
                    
                    $tries = 0;
                    
                    if ($email == $user['email'] && $password == $user['password']) {
                        
                        if ($response) {
                            
                            if ($user['status'] != 0) {
                                $today = time();
                                
                                if (empty($device['id']) || $device['disable_tfa'] == 0 || $today >= $device['disable_tfa'] || ($today <= $device['disable_tfa'] && $device['status'] == 0)) {
                                    
                                    if ($device['disable_tfa'] != 0) {
                                    
                                        if ($today >= $device['disable_tfa']) {
                                            Devices::changeStatus($device['id'], 1, 0);
                                        }
                                    
                                    }
                                    
                                    if ($user['2fa_authy'] == 1) {
                                        $_SESSION['id'] = $user['id'];
                                        $_SESSION['authy_id'] = $user['authy_id'];
                                        $_SESSION['email'] = $user['email'];
                                        $authy = Authy::RequestSMS($_SESSION['authy_id']);
                                        
                                        if ($authy->success) {
                                            
                                            header("Location: " . DIR_URL . "/login/authy_authentication");
                                            exit;
                                        }

                                    } elseif ($user['2fa_google'] == 1) {
                                        
                                        $_SESSION['id'] = $user['id'];
                                        $_SESSION['email'] = $user['email'];
                                        header("Location: " . DIR_URL . "/login/google_authentication");
                                        exit;
                                    }

                                } else {

                                    $localjwt = Users::getJWT($user['email'], $user['password']);
                                    if (!is_null($localjwt)) {
                                        
                                        $transferValidation = TransferValidation::getByUser($user['id']);
                                        if (($transferValidation['trial_count'] == 2 || $transferValidation['amount'] == 1000) && $user['status'] == 3) {
                                            $_SESSION['trialInfo'] = TRUE;
                                        }
                                        
                                        $wallet = ETHWallets::getPrimaryWalletEthByUser($user['id']);
                                        $_SESSION['id'] = $user['id'];
                                        $_SESSION['name'] = $user['name'] . " " . $user['last_name'];
                                        $_SESSION['email'] = $user['email'];
                                        $_SESSION['avatar'] = DIR_URL . "/administrator/public/avatars/" . $user['avatar'];
                                        $_SESSION['status'] = $user['status'];
                                        $_SESSION['address'] = $wallet['address'];
                                        $_SESSION['wallet'] = $wallet['id'];
                                        $_SESSION['time'] = time();
                                        $_SESSION['validateToken'] = hash('sha256', md5(uniqid(mt_rand(), true)));
                                        $_SESSION['jwt'] = $localjwt;

                                        $data = array();
                                        $data['ip'] =  $_SERVER['REMOTE_ADDR'];
                                        $data['browser'] =  $_SERVER['HTTP_USER_AGENT'];
                                        $data['date'] =  time();
                                        $data['user'] =  $_SESSION['id'];
                                        Logs::create($data);
                                        
                                        if (!empty($_SESSION['tries'])) {
                                            unset($_SESSION['tries']);
                                        }
                                        
                                        Users::restoreLoginTries($_SESSION['id']);
                                        
                                        require_once LIBSPATH . '/jwt/init.php';
                                        
                                        $shared_secret = "A?Z8A1hy}{Fl!s)";
                                        $payload = array(
                                            'iat' => time(),
                                            'jti' => md5($shared_secret . ':' . time()),
                                            'email' => $_SESSION['email'],
                                            'name' => $_SESSION['name'],
                                            'phone_number' => '+' . $user['phonecode'] . '' . $user['phone'],
                                            'picture' => $_SESSION['avatar']
                                            );
                                        
                                        $_SESSION['kayako_token'] = \Firebase\JWT\JWT::encode($payload, $shared_secret, 'HS256');
                                        
                                        if (array_key_exists('returnto', $_REQUEST)) {
                                        
                                            header('Location: ' . $_REQUEST['returnto'] . '&jwt=' . $_SESSION['kayako_token']);
                                            exit;
                                        
                                        } else {
                                        
                                            header("Location: " . DIR_URL . "/dashboard");
                                            exit;
                                        
                                        }

                                    } else {

                                        header("Location: " . DIR_URL . "/login?error=notoken");
                                        exit;

                                    }

                                }

                            } elseif ($user['status'] == 0) {
                                
                                header("Location: " . DIR_URL . "/login?error=blockuser");
                                exit;

                            }

                        } else {
                            header("Location: " . DIR_URL . "/login?error=captchaempty");
                            exit;
                        }

                    } else {
                        
                        $tries = $user['tries_login'] + 1;
                        
                        if ($tries >= 5) {
                            
                            Users::userBlock($user['id']);
                            header("Location: " . DIR_URL . "/login?error=errorblockuser");
                            exit;
                        
                        } else {
                        
                            Users::addTry($tries, $user['id']);
                            $_SESSION['tries'] = TRUE;
                            header("Location: " . DIR_URL . "/login?error=credentialerror");
                            exit;
                        
                        }
                    }

                } else {

                    $_SESSION['tries'] = TRUE;
                    header("Location: " . DIR_URL . "/login?error=credentialerror");
                    exit;

                }

            } else {

                header("Location: " . DIR_URL . "/login?error=empty");
                exit;

            }

        } else {

            if (!empty($_SERVER['HTTP_REFERER'])) {

                header("Location: " . $_SERVER['HTTP_REFERER']);
                exit;

            } else {

                header("Location: " . DIR_URL . "/dashboard");
                exit;

            }
        }
    }
    
    public function signout($token) {
    
        if ($token == $_SESSION['validateToken']) {
            
            session_destroy();
            header("Location: " . DIR_URL . "/login");
            
        }
    }
    
    public function google_authentication() {
    
        if (!empty($_SESSION['id'])) {
            require_once LIBSPATH . "/gauthentication/init.php";
            $auth = new \Auth();
            $tfa = $auth->getTfa();
            $auth->setSecret();
            $user = Users::read($_SESSION['id']);
            if (is_null($user['google_2fa_code'])) {
                $_SESSION['secret_google'] = $auth->getSecret();
            } else {
                $_SESSION['secret_google'] = $user['google_2fa_code'];
            }
            $qr = $auth->getQr($_SESSION['email'], $_SESSION['secret_google']);
            
            View::set("key", $user['google_2fa_code']);
            View::set("secret", $_SESSION['secret_google']);
            View::set("codeqr", $qr);
            View::set("title", "Sign In");
            View::render("google_authentication");
        } else {
            session_destroy();
            header("Location: " . DIR_URL . "/login");
            exit;
        }
    }

    public function skip_google() {
    
        if (!empty($_SESSION['id'])) {

            unset($_SESSION['secret_google']);

            $user = Users::read($_SESSION['id']);

            $time = 86400 * 15;
            $date = time();

            $data = array();

            $data['disable_tfa'] = $date + $time;
            $data['remember'] = 1;
            $data['date'] = time();
            $data['browser'] = $_SERVER['HTTP_USER_AGENT'];
            $data['ip'] = $_SERVER['REMOTE_ADDR'];
            $data['token'] = md5($data['browser'] . $data['ip'] . uniqid());
            $data['status'] = 1;
            $data['user'] = $_SESSION['id'];
            Devices::create($data);
            
            $fullname = $user['name'] . " " . $user['last_name'];
            $email = $user['email'];
            $subject = "New Device Confirmation";
            $ip_address = $_SERVER['REMOTE_ADDR'];
            $browser =  Functions::getBrowser($_SERVER['HTTP_USER_AGENT']);
            $location = Functions::location($ip_address);
            
            $dataMail = array(
                "to" => $email,
                "location" => $location,
                "ip" => $ip_address,
                "browser" => $browser,
                "date" => date("m-d-Y: H:i:s", time()),
                "token" => $data['token']
                );

            Mailer::newDevice($dataMail);

            header("Location: " . DIR_URL . "/login/newDevice");
            exit;

        } else {
            session_destroy();
            header("Location: " . DIR_URL . "/login");
            exit;
        }
    }
    
    public function google_validate() {
    
        if (!empty($_SESSION['id'])) {
            
            if (!empty($_POST['code']) && is_numeric($_POST['code'])) {
                $code = htmlspecialchars($_POST['code'], ENT_QUOTES);
                $disable_tfa = (!empty($_POST['disable_tfa'])) ? htmlspecialchars($_POST['disable_tfa'], ENT_QUOTES) : FALSE;
                $user = Users::read($_SESSION['id']);
                $device = Devices::getByIP($_SERVER['REMOTE_ADDR'], $user['id']);
                require_once LIBSPATH . "/gauthentication/init.php";
                $auth = new \Auth();
                $result = NULL;
                if (is_null($user['google_2fa_code'])) {
                    $result = $auth->validate($_SESSION['secret_google'], $code);
                } else {
                    $result = $auth->validate($user['google_2fa_code'], $code);
                }
                if ($result) {
                    
                    if (!empty($device['id']) && $device['status'] == 1) {

                        if (is_null($user['google_2fa_code'])) {
                            Users::saveGoogleCode($_SESSION['secret_google'], $user['id']);
                        }

                        $localjwt = Users::getJWT($user['email'], $user['password']);
                        if (!is_null($localjwt)) {
                            
                            $wallet = ETHWallets::getPrimaryWalletEthByUser($user['id']);
                            
                            $_SESSION['name'] = $user['name'] . " " . $user['last_name'];
                            $_SESSION['avatar'] = DIR_URL . "/administrator/public/avatars/" . $user['avatar'];
                            $_SESSION['status'] = $user['status'];
                            $_SESSION['address'] = $wallet['address'];
                            $_SESSION['wallet'] = $wallet['id'];
                            $_SESSION['time'] = time();
                            $_SESSION['validateToken'] = hash('sha256', md5(uniqid(mt_rand(), true)));
                            $_SESSION['jwt'] = $localjwt;

                            if (!empty($disable_tfa) && $device['disable_tfa'] == 0) {
                                $time = 86400 * 15;
                                $date = time();
                                $data = array();
                                $data['disable_tfa'] = $date + $time;
                                $data['remember'] = 1;
                                $data['date'] = $device['date'];
                                $data['browser'] = $device['browser'];
                                $data['ip'] = $device['ip'];
                                $data['token'] = md5($device['browser'] . "-" . $device['ip'] . "-" . uniqid($date));
                                $data['status'] = 1;
                                $data['user'] = $device['user'];
                                $data['id'] = $device['id'];
                                Devices::update($data);

                                if (is_null($device['disable_tfa'])) {
                                    Users::saveGoogleCode($_SESSION['secret_google'], $_SESSION['id']);
                                }

                                require_once LIBSPATH . '/jwt/init.php';
                                $shared_secret = "A?Z8A1hy}{Fl!s)";
                                $payload = array(
                                    'iat' => time(),
                                    'jti' => md5($shared_secret . ':' . time()),
                                    'email' => $_SESSION['email'],
                                    'name' => $_SESSION['name'],
                                    'phone_number' => '+' . $user['phonecode'] . '' . $user['phone'],
                                    'picture' => $_SESSION['avatar']
                                    );
                                $_SESSION['kayako_token'] = \Firebase\JWT\JWT::encode($payload, $shared_secret, 'HS256');
                                if (array_key_exists('returnto', $_REQUEST)) {
                                    header('Location: ' . $_REQUEST['returnto'] . '&jwt=' . $_SESSION['kayako_token']);
                                    exit;
                                
                                } else {
                                
                                    header("Location: " . DIR_URL . "/dashboard");
                                    exit;
                                
                                }
                            } else {
                                require_once LIBSPATH . '/jwt/init.php';
                                $shared_secret = "A?Z8A1hy}{Fl!s)";
                                $payload = array(
                                    'iat' => time(),
                                    'jti' => md5($shared_secret . ':' . time()),
                                    'email' => $_SESSION['email'],
                                    'name' => $_SESSION['name'],
                                    'phone_number' => '+' . $user['phonecode'] . '' . $user['phone'],
                                    'picture' => $_SESSION['avatar']
                                    );
                                $_SESSION['kayako_token'] = \Firebase\JWT\JWT::encode($payload, $shared_secret, 'HS256');
                                if (array_key_exists('returnto', $_REQUEST)) {
                                    header('Location: ' . $_REQUEST['returnto'] . '&jwt=' . $_SESSION['kayako_token']);
                                    exit;
                                
                                } else {
                                
                                    header("Location: " . DIR_URL . "/dashboard");
                                    exit;
                                
                                }
                            }

                        }

                    } elseif (empty($device['id']) || $device['status'] == 0) {
                        $data = array();
                        $data['disable_tfa'] = 0;
                        $data['remember'] = 0;
                        $data['date'] = time();
                        $data['browser'] = $_SERVER['HTTP_USER_AGENT'];
                        $data['ip'] = $_SERVER['REMOTE_ADDR'];
                        $data['token'] = md5($data['browser'] . $data['ip'] . uniqid());
                        $data['status'] = 0;
                        $data['user'] = $_SESSION['id'];
                        if ($disable_tfa) {
                            $time = 86400 * 15;
                            $date = time();
                            $data['disable_tfa'] = $date + $time;
                            $data['remember'] = 1;
                        }
                        Devices::create($data);
                        $fullname = $user['name'] . " " . $user['last_name'];
                        $email = $user['email'];
                        $subject = "New Device Confirmation";
                        $ip_address = $_SERVER['REMOTE_ADDR'];
                        $browser =  Functions::getBrowser($_SERVER['HTTP_USER_AGENT']);
                        $location = Functions::location($ip_address);

                        $dataMail = array(
                            "to" => $email,
                            "location" => $location,
                            "ip" => $ip_address,
                            "browser" => $browser,
                            "date" => date("m-d-Y: H:i:s", time()),
                            "token" => $data['token']
                            );

                        Mailer::newDevice($dataMail);

                        header("Location: " . DIR_URL . "/login/newDevice");
                        exit;
                    }
                } else {
                    header("Location: " . DIR_URL . "/login/google_authentication?error=codeinvalid");
                    exit;
                }
            } else {
                header("Location: " . DIR_URL . "/login/google_authentication?error=codeinvalid");
                exit;
            }
        } else {
            session_destroy();
            header("Location: " . DIR_URL . "/login");
            exit;
        }
    }
    
    public function newDevice() {
    
        View::set("title", "Sign In");
        View::render("new_device");
    }
    
    public function errorDevice() {
    
        View::set("title", "Sign In");
        View::render("error_device");
    }
    
    public function authy_authentication() {
    
        if (!empty($_SESSION['id'])) {
            
            View::set("title", "Sign In");
            View::render("authy_authentication");
        } else {
            session_destroy();
            header("Location: " . DIR_URL . "/login");
            exit;
        }
    }
    
}
?>