<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Users,
    \App\Models\Etherscan,
    \App\Models\Transactions,
    \App\Models\USDWallets,
    \App\Models\ETHWallets,
    \App\Models\USDWalletAdmin,
    \App\Models\Commissions,
    \App\Models\Request,
    \App\Models\Limits,
    \App\Models\Purchases,
    \App\Models\Earnings,
    \App\Models\TransferValidation,
    \App\Models\Merchant,
    \App\Controllers\Mailer,
    \App\Controllers\Functions;

class Buy {

    public function index() {

        if (!empty($_SESSION['id'])) {

            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }

            if (!empty($_SESSION['price_ether']) || isset($_SESSION['price_ether'])) {
                unset($_SESSION['price_ether']);
            }

            /* HEADER */
            /*$price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            var_dump($ethusd);*/
            /*$result=Etherscan::callAPI("GET","https://apidev.mercury.cash/ethereum/price", false);

            if (!empty($result->result->ethusd) && isset($result->result->ethusd)) {
                View::set("ethusd", $result->result->ethusd);
                $ethusd = $result->result->ethusd;
            }else{
                View::set("ethusd", "Not Value");
                $ethusd = 0.0;
            }*/

            $ethusd = $_SESSION['ethusd'];
            $earnings = Earnings::read(1);
            $plus = ($ethusd * $earnings['buy']) / 100;
            $ethusd = number_format($ethusd + $plus, 2);
            $_SESSION['price_ether'] = $ethusd;
            View::set("ethusd", $_SESSION['price_ether']);
            /* HEADER */

            $usd_wallet = USDWallets::read($_SESSION['id']);
            $commission = Commissions::read(1);
            $accounts = array();
            $cards = array();
            $user = Users::read($_SESSION['id']);
            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;

            $ethwallets = ETHWallets::getByUser($_SESSION['id']);

            for ($i=0; $i < count($requests); $i++) {

                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }

            }

            View::set("commission", $commission);
            View::set("ethwallets", $ethwallets);
            View::set("usd_wallet", $usd_wallet);
            View::set("title", "Buy");
            View::set("requestsUnread", $unread);
            View::render("buy");

        } else {

            header("Location: " . DIR_URL . "/login");

        }
    }

    public function limits() {
        
        if (!empty($_SESSION['id'])) {
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }
            /* HEADER */
            
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            /* HEADER */
            
            $user = Users::read($_SESSION['id']);
            $limits = Limits::getByUser($_SESSION['id']);
            $resta = array();
            $resta['buy_eth_card'] = $limits['buy_eth_card'] - $user['limit_buy_eth_card'];
            $resta['buy_eth_ach'] = $limits['buy_eth_ach'] - $user['limit_buy_eth_ach'];
            $resta['buy_eth_usdwallet'] = $limits['buy_eth_usdwallet'] - $user['limit_buy_eth_usd'];
            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;

            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }
            
            View::set("resta", $resta);
            View::set("limits", $limits);
            View::set("title", "Limits");
            View::set("requestsUnread", $unread);
            View::render("limits");

        } else {

            header("Location: " . DIR_URL . "/login");

        }        
    }
    public function calculations($amount, $method, $currency) {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        
        if (!empty($_SESSION['id'])) {

            if (is_numeric($amount) && is_numeric($method) && is_numeric($currency)) {

                if ($currency == '1') {
                    $amount = $_SESSION['price_ether'] * $amount;
                }

                $modal = FALSE;
                $amount = (!empty($amount) || $amount == 0) ? $amount : 0;
                
                $user = Users::read($_SESSION['id']);
                $limits = Limits::getByUser($_SESSION['id']);
                $commissions = Commissions::read(1);
                $usdwallet = USDWallets::read($_SESSION['id']);

                if ($method == '1') {
                    
                    $limit = $limits['buy_eth_usdwallet'] - $user['limit_buy_eth_usd'];
                    $fee = ($amount * $commissions['buy_usdwallet']) / 100;
                    $total = $amount + $fee;
                    $ethers = $amount / $_SESSION['price_ether'];
                    $percentage = ($user['limit_buy_eth_usd'] * 100) / $limits['buy_eth_usdwallet'];

                    if (!empty($amount)) {
                        $percentage = $percentage + ($amount * 100) / $limits['buy_eth_usdwallet'];
                    }

                    $description = "This would exceed your USDWallet purchase limit of $" . number_format($limits['buy_eth_usdwallet'], 2) . " per week. Please wait or try a smaller amount";
                    
                    if ($total > $limit) {
                        $modal = TRUE;
                        $description = "This would exceed your USDWallet purchase limit of $" . number_format($limits['buy_eth_usdwallet'], 2) . " per week. Please wait or try a smaller amount";
                    } elseif ($total > $usdwallet['balance']) {
                        $modal = TRUE;
                        $description = "The amount exceeds the balance you have in the USD Wallet";
                    }
                    
                    echo json_encode(array(
                        "status" => 1,
                        "limit" => $limit,
                        "subtotal" => number_format($amount, 2),
                        "fee" => number_format($fee, 2),
                        "total" => number_format($total, 2),
                        "available" => number_format($limit-$total, 2),
                        "balance" => $usdwallet['balance'],
                        "ethers" => number_format($ethers, 5),
                        "percentage" => $percentage,
                        "description" => $description,
                        "modal" => $modal,
                        "sesion" => $_SESSION['price_ether']
                        ));
                
                } elseif ($method == '2') {
                    
                    $limit = $limits['buy_eth_card'] - $user['limit_buy_eth_card'];
                    $fee = ($commissions['buy_card'] * $amount) / 100;
                    $total = $amount + $fee;
                    $ethers = $amount / $_SESSION['price_ether'];
                    $percentage = ($user['limit_buy_eth_card'] * 100) / $limits['buy_eth_card'];

                    if (!empty($amount)) {
                        $percentage = $percentage + ($amount * 100) / $limits['buy_eth_card'];
                    }

                    $modal = ($total > $limit) ? TRUE : FALSE;
                    $description = "This would exceed your credit/debit card purchase limit of $" . number_format($limits['buy_eth_card'], 2) . " per week. Please wait or try a smaller amount";
                    
                    echo json_encode(array(
                        "status" => 1,
                        "limit" => $limit,
                        "subtotal" => number_format($amount, 2),
                        "fee" => number_format($fee, 2),
                        "total" => number_format($total, 2),
                        "ethers" => number_format($ethers, 5),
                        "percentage" => $percentage,
                        "description" => $description,
                        "modal" => $modal
                        ));

                } elseif ($method == '3') {
                    
                    $limit = $limits['buy_eth_ach'] - $user['limit_buy_eth_ach'];
                    $fee = ($commissions['buy_ach'] * $amount) / 100;
                    $total = $amount + $fee;
                    $ethers = $amount / $_SESSION['price_ether'];
                    $percentage = ($user['limit_buy_eth_ach'] * 100) / $limits['buy_eth_ach'];
                    
                    if (!empty($amount)) {
                        $percentage = $percentage + ($amount * 100) / $limits['buy_eth_ach'];
                    }

                    $modal = ($total > $limit) ? TRUE : FALSE;
                    $description = "This would exceed your ACH purchase limit of $" . number_format($limits['buy_eth_ach'], 2) . " per week. Please wait or try a smaller amount";
                    
                    echo json_encode(array(
                        "status" => 1,
                        "limit" => $limit,
                        "subtotal" => number_format($amount, 2),
                        "fee" => number_format($fee, 2),
                        "total" => number_format($total, 2),
                        "balance" => $usdwallet['balance'],
                        "ethers" => number_format($ethers, 5),
                        "percentage" => $percentage,
                        "description" => $description,
                        "modal" => $modal
                        ));

                }

            } else {

                echo json_encode(array(
                    "status" => 0,
                    "description" => "You must enter numeric data"
                    ));
            }

        } else {

            echo json_encode(array(
                "status" => 0,
                "description" => "You must enter numeric data"
                ));

        }
    }

    public function removeCard() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        
        if (!empty($_SESSION['id'])) {
            $card = $_POST['card'];
            
            require_once LIBSPATH . "/stripe/init.php";
            $user = Users::read($_SESSION['id']);
            $customer = \Stripe\Customer::retrieve($user['stripe_id']);
            $delete = $customer->sources->retrieve($card)->delete();
            if ($delete->deleted) {
                
                echo json_encode(array(

                    "status" => 1,
                    "description" => "Your card has been successfuly deleted!"

                    ));

            } else {

                echo json_encode(array(
                    "status" => 0,
                    "description" => "An error has occurred, please try again."
                    ));

            }
        } else {

            echo json_encode(array(
                "status" => 0,
                "description" => "Error!"
                ));

        }
    }
    public function removeAch() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        
        if (!empty($_SESSION['id'])) {
            $ach = $_POST['ach'];
            
            require_once LIBSPATH . "/stripe/init.php";
            $user = Users::read($_SESSION['id']);
            $customer = \Stripe\Customer::retrieve($user['stripe_id']);
            $delete = $customer->sources->retrieve($ach)->delete();
            $customer = \Stripe\Customer::retrieve($user['stripe_id']);
            $customer->sources->retrieve($ach)->delete();

            if ($delete->deleted) {
                
                echo json_encode(array(
                    "status" => 1,
                    "description" => "Your ACH has been successfuly deleted!"
                    ));

            } else {

                echo json_encode(array(
                    "status" => 0,
                    "description" => "An error has occurred, please try again."
                    ));

            }

        } else {

            echo json_encode(array(
                "status" => 0,
                "description" => "Error!"
                ));

        }
    }
    public function calc_percentage_limit($amount, $type) {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        
        if (!empty($_SESSION['id'])) {
            
            $user = Users::read($_SESSION['id']);
            $limit = Limits::getByUser($_SESSION['id']);
            $commission = Commissions::read(1);
            $usdwallet = USDWallets::read($_SESSION['id']);
            $total = 0;
            $commission_percentage = 0;
            $description = NULL;

            switch ($type) {
                case '1':
                    $total = $limit['buy_eth_card'] - $user['limit_buy_eth_card'];
                    $commission_percentage = $commission['card'];
                    $description = "This would exceed your credit/debit card purchase limit of $" . number_format($limits['buy_eth_card'], 2) . " per week. Please wait or try a smaller amount";
                    break;
                case '2':
                    $total = $limit['buy_eth_ach'] - $user['limit_buy_eth_ach'];
                    $commission_percentage = $commission['ach'];
                    $description = "This would exceed your ACH purchase limit of $" . number_format($limits['buy_eth_ach'], 2) . " per week. Please wait or try a smaller amount";
                    break;
                case '3':
                    $total = $limit['buy_eth_usdwallet'] - $user['limit_buy_eth_usd'];
                    $commission_percentage = $commission['usd_wallet'];
                    $description = "This would exceed your USDWallet purchase limit of $" . number_format($limits['buy_eth_usdwallet'], 2) . " per week. Please wait or try a smaller amount";
                    break;
            }

            $limit_init = $user['limit_buy_eth_card'];
            $modal = FALSE;
            $commission_usd = ($commission_percentage * $amount) / 100;
            $value = $amount + $commission_usd;
            
            $amount = $amount + $limit_init;
            $percentage = ($amount * 100) / $total;
            $modal = ($percentage >= 100) ? TRUE : FALSE;
            $percentage = ($percentage >= 100) ? $limit_init : $percentage;
            $percentage = intval($percentage);

            if (($total - $value) <= 0) {
                $_SESSION['status_calc'] = FALSE;
            } else {
                $_SESSION['status_calc'] = TRUE;
            }

            if ($type == 3) {
                
                if ($value > $usdwallet['balance']) {
                    $description = "The amount exceeds the balance you have in the USD Wallet";
                    $modal = TRUE;
                }

            }

            echo json_encode(array(
                "status" => $_SESSION['status_calc'],
                "modal" => $modal,
                "subtotal" => number_format($amount, 2),
                "fee" => number_format($commission_usd, 2),
                "total" => number_format($value, 2),
                "percentage" => $percentage,
                "description" => $description,
                "balance" => $usdwallet['balance'],
                "ethers" => $_SESSION['price_ether'] / $value,
                "ethereum" => $_SESSION['price_ether']
                ));
        }
    }

    public function getLimits($type) {
        
        if (!empty($_SESSION['id'])) {
            
            $user = Users::read($_SESSION['id']);
            $limit = Limits::getByUser($_SESSION['id']);
            $usdwallet = USDWallets::read($_SESSION['id']);
            $commissions = Commissions::read(1);

            switch ($type) {
                case '1':
                    echo json_encode(array(
                        "limit_init" => ($user['limit_buy_eth_usd'] * 100) / $limit['buy_eth_usdwallet'],
                        "limit_rest" => $limit['buy_eth_usdwallet'] - $user['limit_buy_eth_usd']
                        ));
                    break;
                case '2':
                    echo json_encode(array(
                        "limit_init" => ($user['limit_buy_eth_card'] * 100) / $limit['buy_eth_card'],
                        "limit_rest" => $limit['buy_eth_card'] - $user['limit_buy_eth_card']
                        ));
                    break;
                case '3':
                    echo json_encode(array(
                        "limit_init" => ($user['limit_buy_eth_ach'] * 100) / $limit['buy_eth_ach'],
                        "limit_rest" => $limit['buy_eth_ach'] - $user['limit_buy_eth_ach']
                        ));
                    break;
            }
        }
    }

    public function calc_max($type) {
        
        if (!empty($_SESSION['id'])) {
            
            $user = Users::read($_SESSION['id']);
            $limits = Limits::getByUser($_SESSION['id']);
            $commissions = Commissions::read(1);

            switch ($type) {
                case '1':
                    $usdwallet = USDWallets::read($_SESSION['id']);
                    $balance = $usdwallet['balance'];
                    $commission = ($commissions['buy_usdwallet'] * $balance) / 100;
                    $total = $balance - $commission;

                    $fee = ($commissions['buy_usdwallet'] / 100) + 1;
                    $subtotal = $balance / $fee;
                    $fee = $balance - $subtotal;


                    echo json_encode(array(
                        "ethers" => number_format(($total / $_SESSION['price_ether']), 5),
                        "subtotal" => number_format($subtotal, 2),
                        "fee" => number_format($fee, 2),
                        "total" => number_format($balance, 2),
                        "max" => number_format($balance, 2),
                        ));
                    break;
                case '2':
                    $limit = $limits['buy_eth_card'];
                    $limit = $limit - $user['limit_buy_eth_card'];
                    $commission = ($commissions['buy_card'] * $limit) / 100;
                    $total = $limit - $commission;
                    echo json_encode(array(
                        "ethers" => number_format(($total / $_SESSION['price_ether']), 5),
                        "subtotal" => number_format($total, 2),
                        "fee" => number_format($commission, 2),
                        "total" => number_format($limit, 2),
                        ));
                    break;
                case '3':
                    $limit = $limits['buy_eth_ach'];
                    $limit = $limit - $user['limit_buy_eth_ach'];
                    $commission = ($commissions['buy_ach'] * $limit) / 100;
                    $total = $limit - $commission;
                    echo json_encode(array(
                        "ethers" => number_format(($total / $_SESSION['price_ether']), 5),
                        "subtotal" => number_format($total, 2),
                        "fee" => number_format($commission, 2),
                        "total" => number_format($limit, 2),
                        ));
                    break;
            }
        }
    }

    public function process() {

        if (!empty($_SESSION['id'])) {
            
            if (!empty($_POST['eth_wallet']) && is_numeric($_POST['eth_wallet']) &&
                !empty($_POST['eth_amount']) && is_numeric($_POST['eth_amount']) &&
                !empty($_POST['payment_method']) && is_numeric($_POST['payment_method'])) {
                
                $transferValidation = TransferValidation::getByUser($_SESSION['id']);

                $eth_wallet = $_POST['eth_wallet'];
                $eth_amount = $_POST['eth_amount'];
                $usd_amount = $eth_amount * $_SESSION['price_ether'];
                $payment_method = $_POST['payment_method'];

                $user = Users::read($_SESSION['id']);

                if (!is_null($user['last_name']) &&
                    !is_null($user['ndocument']) &&
                    $user['country_id'] > 0 &&
                    $user['province_id'] > 0 &&
                    !is_null($user['city']) &&
                    !is_null($user['zip_code']) &&
                    !is_null($user['address'])) {
                    
                    if ($_SESSION['status'] == 2 || $_SESSION['status'] == 3) {

                        if (($usd_amount + $transferValidation['amount']) <= 1000) {
                            
                            if ($transferValidation['trial_count'] < 2) {
                                
                                if ($usd_amount >= 1) {
                                    
                                    $data = array();
                                    $data['trial_count'] = $transferValidation['trial_count'] + 1;
                                    $data['amount'] = $transferValidation['amount'] + $usd_amount;
                                    $data['user'] = $_SESSION['id'];
                                    $data['id'] = $transferValidation['id'];
                                    TransferValidation::update($data);

                                    $this->makeProcess($eth_wallet, $usd_amount, $eth_amount, $payment_method);

                                } else {

                                    $_SESSION['alert'] = "Purchase must be greater than $1.00 USD";
                                    header("Location: " . DIR_URL . "/buy");
                                    exit;

                                }

                            } else {

                                $_SESSION['alert'] = "<p>Your two transactions trial has expired, you need to validate your documents in order to continue using Mercury Cash services</p>";
                                header("Location: " . DIR_URL . "/buy");
                                exit;

                            }

                        } else {

                            $_SESSION['alert'] = "<p>Your two transactions trial or one for less than $1000 USD has expired, you need to validate your documents in order to continue using Mercury Cash services</p><p>You can make a transaction of maximun $" . (1000 - $transferValidation['amount']) . " USD</p>";
                            header("Location: " . DIR_URL . "/buy");
                            exit;

                        }

                    } elseif ($_SESSION['status'] == 1) {
                        
                        $this->makeProcess($eth_wallet, $usd_amount, $eth_amount, $payment_method);

                    } else {

                        header("Location: " . DIR_URL);
                        exit;

                    }

                } else {

                    $_SESSION['alert'] = "You need to complete TIER 1 in order to continue. <a href='" . DIR_URL . "/settings'>Update your profile</a> and submit your TIER 1 data.";
                    header("Location: " . DIR_URL . "/buy");
                    exit;

                }

            } else {

                $_SESSION['alert'] = "The data you are trying to enter is invalid";
                header("Location: " . DIR_URL . "/buy");
                exit;

            }

        } else {

            header("Location: " . DIR_URL);
            exit;

        }

    }

    private function makeProcess($eth_wallet, $usd_amount, $eth_amount, $payment_method) {

        $price = Etherscan::getLastPrice();
        $ethusd = $price->ethusd;

        $user = Users::read($_SESSION['id']);
        $usdwallet = USDWallets::read($_SESSION['id']);
        $commissions = Commissions::read(1);
            
        if ($payment_method == 1) {

            $commission = ($usd_amount * $commissions['buy_usdwallet']) / 100;

            if ($usdwallet['balance'] >= ($usd_amount + $commission)) {
                
                $data = array();
                $data['checkout_id'] = NULL;
                $data['eth_amount'] = $eth_amount;
                $data['usd_amount'] = $usd_amount;
                $data['commission'] = $commission;
                $data['eth_value'] = $_SESSION['price_ether'];
                $data['payment_method'] = $payment_method;
                $data['date'] = time();
                $data['status'] = 2;
                $data['eth_wallet'] = $eth_wallet;
                $data['user'] = $_SESSION['id'];

                $result = Purchases::create($data);
                
                if ($result) {

                    $data['limit_buy_eth_card'] = $user['limit_buy_eth_card'];
                    $data['limit_buy_eth_ach'] = $user['limit_buy_eth_ach'];
                    $data['limit_buy_eth_usd'] = $user['limit_buy_eth_usd'] + $usd_amount;
                    $data['id'] = $_SESSION['id'];
                    Users::updateLimits($data);
                   
                   $dataMail = array(
                        "to" => $_SESSION['email'],
                        "payment_method" => "USD Wallet",
                        "date" => date("Y-m-d", time()),
                        "eth_amount" => $eth_amount,
                        "eth_rate" => number_format($_SESSION['price_ether'], 2),
                        "subtotal" => number_format($usd_amount, 2),
                        "fee" => number_format($commission, 2),
                        "total" => number_format(($usd_amount + $commission), 2)
                        );

                   Mailer::buyUser($dataMail);
                   
                    $user = Users::read($_SESSION['id']);
                    $message = '<h3 style="text-align: center;">Hello,</h3>';
                    $message .= '<p style="text-align: center;">' . $user['name'] . ' ' . $user['last_name'] . ' under the email ' . $user['email'] . ' has registered a purchase of ' . $data['eth_amount'] .' ETH.</p>';
                    $message .= '<p style="text-align: center;">Regards.</p>';
                    $message .= '<h5 style="text-align: center;">Mercury Cash support team</h5>';
                    Mailer::sendMail("buys@mercury.cash", "Mercury Cash - Buy", $message, "Go to Mercury Cash", DIR_URL . "/administrator/transactions");
                    Functions::usd_alert();
                    $_SESSION['alert'] = "Your transaction was successful";
                    header("Location: " . DIR_URL . "/buy");
                    exit;

                } else {

                    $_SESSION['alert'] = "An error has occurred, please try again.";
                    header("Location: " . DIR_URL . "/buy");
                    exit;

                }

            } else {

                $_SESSION['alert'] = "Your USD Wallet doesn't have sufficient funds, please add funds to buy ETH.";
                header("Location: " . DIR_URL . "/buy");
                exit;

            }

        } elseif ($payment_method == 2) {

            $commission = ($usd_amount * $commissions['buy_card']) / 100;
            $total = number_format($usd_amount + $commission, 2);

            $_SESSION['eth_amount'] = $eth_amount;
            $_SESSION['usd_amount'] = $usd_amount;
            $_SESSION['commission'] = $commission;
            $_SESSION['eth_wallet'] = $eth_wallet;

            $merchant = Merchant::getByUser($_SESSION['id']);
            $count = 0;
            $registrations = "";
            foreach ($merchant as $id) {
                $registrations = $registrations . "&registrations[" . $count . "].id=" . $id['id_merchant'];
                $count++;
            }

            $url = "https://test.oppwa.com/v1/checkouts";
            $data = "authentication.userId=8a8294185f116e86015f255ae6d02a94" .
                    "&authentication.password=2w5MEDz9tp" .
                    "&authentication.entityId=8a8294185f116e86015f255b241b2a99" .
                    "&amount=" . $total .
                    "&currency=USD" .
                    "&paymentType=DB" .
                    $registrations .
                    "&createRegistration=true";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $responseData = curl_exec($ch);
            $response = json_decode($responseData);

            header("Location: " . DIR_URL . "/buy?checkout=" . $response->id);

        }

    }

    public function processPayCard() {
    
        if (!empty($_SESSION['id'])) {
            if (isset($_GET['id']) && isset($_GET['resourcePath'])) {

                $user = Users::read($_SESSION['id']);

                $url = "https://test.oppwa.com" . $_GET['resourcePath'];
                $url .= "?authentication.userId=8a8294185f116e86015f255ae6d02a94";
                $url .= "&authentication.password=2w5MEDz9tp";
                $url .= "&authentication.entityId=8a8294185f116e86015f255b241b2a99";

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $responseData = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($responseData);

                if(preg_match('/^(000\.000\.|000\.100\.1|000\.[36])/', $response->result->code) ||
                    preg_match('/^(000\.400\.0|000\.400\.100)/', $response->result->code) ||
                    preg_match('/^(000\.200)/', $response->result->code)) {
                    
                    $status = 2;
                    if (preg_match('/^(000\.000\.|000\.100\.1|000\.[36])/', $response->result->code) ||
                        preg_match('/^(000\.400\.0|000\.400\.100)/', $response->result->code)) {
                        $status = 1;
                    } elseif (preg_match('/^(000\.200)/', $response->result->code)) {
                        $status = 2;
                    }
                    
                    $data = array();
                    $data['checkout_id'] = $_GET['resourcePath'];
                    $data['eth_amount'] = $_SESSION['eth_amount'];
                    $data['usd_amount'] = $_SESSION['usd_amount'];
                    $data['commission'] = $_SESSION['commission'];
                    $data['eth_value'] = $_SESSION['price_ether'];
                    $data['payment_method'] = 2;
                    $data['date'] = time();
                    $data['status'] = $status;
                    $data['eth_wallet'] = $_SESSION['eth_wallet'];
                    $data['user'] = $_SESSION['id'];
                    $data['id_merchant'] = $response->id;

                    $result = Purchases::create($data);
                    Merchant::create($data);
                    
                    if ($result) {

                        $data['limit_buy_eth_card'] = $user['limit_buy_eth_card'] + $_SESSION['usd_amount'];
                        $data['limit_buy_eth_ach'] = $user['limit_buy_eth_ach'];
                        $data['limit_buy_eth_usd'] = $user['limit_buy_eth_usd'];
                        $data['id'] = $_SESSION['id'];
                        Users::updateLimits($data);
                       
                       $dataMail = array(
                            "to" => $_SESSION['email'],
                            "payment_method" => "Credit Card",
                            "date" => date("Y-m-d", time()),
                            "eth_amount" => $_SESSION['eth_amount'],
                            "eth_rate" => number_format($_SESSION['price_ether'], 2),
                            "subtotal" => number_format($_SESSION['usd_amount'], 2),
                            "fee" => number_format($_SESSION['commission'], 2),
                            "total" => number_format(($_SESSION['usd_amount'] + $_SESSION['commission']), 2)
                            );

                        Mailer::buyUser($dataMail);
                       
                        $user = Users::read($_SESSION['id']);
                        $message = '<h3 style="text-align: center;">Hello,</h3>';
                        $message .= '<p style="text-align: center;">' . $user['name'] . ' ' . $user['last_name'] . ' under the email ' . $user['email'] . ' has registered a purchase of ' . $data['eth_amount'] .' ETH.</p>';
                        $message .= '<p style="text-align: center;">Regards.</p>';
                        $message .= '<h5 style="text-align: center;">Mercury Cash support team</h5>';
                        Mailer::sendMail("buys@mercury.cash", "Mercury Cash - Buy", $message, "Go to Mercury Cash", DIR_URL . "/administrator/transactions");
                        Functions::usd_alert();
                        $_SESSION['alert'] = $response->result->description;
                        header("Location: " . DIR_URL . "/buy");
                        exit;

                    } else {

                        unset($_SESSION['eth_amount']);
                        unset($_SESSION['usd_amount']);
                        unset($_SESSION['commission']);
                        unset($_SESSION['eth_wallet']);

                        $_SESSION['alert'] = "An error has occurred, please try again.";
                        header("Location: " . DIR_URL . "/buy");
                        exit;

                    }

                } else {

                    $_SESSION['alert'] = $response->result->description;
                    header("Location: " . DIR_URL . "/buy");
                    exit;

                }

            }
        }
    
    }

    /* COMPRA POR iOS ANDROID */

    public function registerBuy() {

        require_once LIBSPATH . "/jwt/init.php";
        $data = \Firebase\JWT\JWT::decode($POST['jwt'], "mercurykey", array('HS256'));
        $data = $data->data;
        $user = Users::readByEmail($data->email);

        if (!empty($user['id'])) {
            if ($data->password == $user['password']) {
                if (!empty($_POST['usd_amount']) && is_numeric($_POST['usd_amount']) &&
                    !empty($_POST['eth_wallet']) && is_numeric($_POST['eth_wallet'])) {

                    $commissions = Commissions::read(1);
                    $commission = ($_POST['usd_amount'] * $commissions['buy_card']) / 100;
                    $total = number_format($_POST['usd_amount'] + $commission, 2);

                    $merchant = Merchant::getByUser($_SESSION['id']);
                    $count = 0;
                    $registrations = "";
                    foreach ($merchant as $id) {
                        $registrations = $registrations . "&registrations[" . $count . "].id=" . $id['id_merchant'];
                        $count++;
                    }

                    $url = "https://test.oppwa.com/v1/checkouts";
                    $data = "authentication.userId=8a8294185f116e86015f255ae6d02a94" .
                            "&authentication.password=2w5MEDz9tp" .
                            "&authentication.entityId=8a8294185f116e86015f255b241b2a99" .
                            "&amount=" . $total .
                            "&currency=USD" .
                            "&paymentType=DB" .
                            $registrations .
                            "&createRegistration=true";

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $responseData = curl_exec($ch);
                    $response = json_decode($responseData);

                    echo json_encode(array(
                        "status" => 1,
                        "url" => DIR_URL . "buy/formtobuy/" . $_POST['eth_wallet'] . "/?checkout=" . $response->id
                    ));

                }
            }
        }

    }

    public function formtobuy($eth_wallet) {
    
        View::set("title", "Buy");
        View::render("form_to_buy");
    
    }

    public function _processPayCard($eth_wallet) {

        require_once LIBSPATH . "/jwt/init.php";
        $data = \Firebase\JWT\JWT::decode($_POST['jwt'], "mercurykey", array('HS256'));
        $data = $data->data;
        $user = Users::readByEmail($data->email);
    
        if (!empty($user['id'])) {
            if ($data->password == $user['password']) {
                if (isset($_GET['id']) && isset($_GET['resourcePath'])) {

                    $commissions = Commissions::read(1);

                    $price = Etherscan::getLastPrice();
                    $ethusd = $price->ethusd;
                    $earnings = Earnings::read(1);
                    $plus = ($ethusd * $earnings['buy']) / 100;
                    $ethusd = number_format($ethusd + $plus, 2);

                    $url = "https://test.oppwa.com" . $_GET['resourcePath'];
                    $url .= "?authentication.userId=8a8294185f116e86015f255ae6d02a94";
                    $url .= "&authentication.password=2w5MEDz9tp";
                    $url .= "&authentication.entityId=8a8294185f116e86015f255b241b2a99";

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $responseData = curl_exec($ch);
                    curl_close($ch);
                    $response = json_decode($responseData);

                    if(preg_match('/^(000\.000\.|000\.100\.1|000\.[36])/', $response->result->code) ||
                        preg_match('/^(000\.400\.0|000\.400\.100)/', $response->result->code) ||
                        preg_match('/^(000\.200)/', $response->result->code)) {
                        
                        $status = 2;
                        if (preg_match('/^(000\.000\.|000\.100\.1|000\.[36])/', $response->result->code) ||
                            preg_match('/^(000\.400\.0|000\.400\.100)/', $response->result->code)) {
                            $status = 1;
                        } elseif (preg_match('/^(000\.200)/', $response->result->code)) {
                            $status = 2;
                        }

                        $commission = ($response->amount * $commissions['buy_card']) / 100;
                        $eth_amount = $response->amount / $ethusd;
                        
                        $data = array();
                        $data['checkout_id'] = $_GET['resourcePath'];
                        $data['eth_amount'] = $eth_amount;
                        $data['usd_amount'] = $response->amount;
                        $data['commission'] = $commission;
                        $data['eth_value'] = $ethusd;
                        $data['payment_method'] = 2;
                        $data['date'] = time();
                        $data['status'] = $status;
                        $data['eth_wallet'] = $eth_wallet;
                        $data['user'] = $user['id'];
                        $data['id_merchant'] = $response->id;

                        $result = Purchases::create($data);
                        Merchant::create($data);
                        
                        if ($result) {

                            $data['limit_buy_eth_card'] = $user['limit_buy_eth_card'] + $response->amount;
                            $data['limit_buy_eth_ach'] = $user['limit_buy_eth_ach'];
                            $data['limit_buy_eth_usd'] = $user['limit_buy_eth_usd'];
                            $data['id'] = $_SESSION['id'];
                            Users::updateLimits($data);
                           
                           $dataMail = array(
                                "to" => $_SESSION['email'],
                                "payment_method" => "Credit Card",
                                "date" => date("Y-m-d", time()),
                                "eth_amount" => $eth_amount,
                                "eth_rate" => number_format($ethusd, 2),
                                "subtotal" => number_format($response->amount, 2),
                                "fee" => number_format($commission, 2),
                                "total" => number_format(($response->amount + $commission), 2)
                                );

                            Mailer::buyUser($dataMail);
                           
                            $user = Users::read($_SESSION['id']);
                            $message = '<h3 style="text-align: center;">Hello,</h3>';
                            $message .= '<p style="text-align: center;">' . $user['name'] . ' ' . $user['last_name'] . ' under the email ' . $user['email'] . ' has registered a purchase of ' . $data['eth_amount'] .' ETH.</p>';
                            $message .= '<p style="text-align: center;">Regards.</p>';
                            $message .= '<h5 style="text-align: center;">Mercury Cash support team</h5>';
                            Mailer::sendMail("buys@mercury.cash", "Mercury Cash - Buy", $message, "Go to Mercury Cash", DIR_URL . "/administrator/transactions");
                            Functions::usd_alert();
                            $_SESSION['alert'] = $response->result->description;
                            header("Location: " . DIR_URL . "/buy");
                            exit;

                        } else {

                            unset($_SESSION['eth_amount']);
                            unset($_SESSION['usd_amount']);
                            unset($_SESSION['commission']);
                            unset($_SESSION['eth_wallet']);

                            $_SESSION['alert'] = "An error has occurred, please try again.";
                            header("Location: " . DIR_URL . "/buy");
                            exit;

                        }

                    } else {

                        $_SESSION['alert'] = $response->result->description;
                        header("Location: " . DIR_URL . "/buy");
                        exit;

                    }

                }
            }

        }
    
    }

}
?>