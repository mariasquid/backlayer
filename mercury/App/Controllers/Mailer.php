<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Users;

class Mailer {

    public static function receiveEth($data) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "receive_eth";

        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $data['to'] . "&merge_txhash=" . $data['txhash'] . "&merge_sender=" . $data['sender'] . "&merge_amount=" . $data['amount'];

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    public static function addFavorite($data) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "add_favorite";

        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $data['to'] . "&merge_address=" . $data['address'] . "&merge_description=" . $data['description'] . "&merge_token=" . $data['token'];

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    public static function invitation($data) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "invitation";

        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $data['to'] . "&merge_name=" . $data['name'];

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    public static function requestEth($data) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "request_eth";

        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $data['to'] . "&merge_name=" . $data['name'] . "&merge_eth_amount=" . $data['eth_amount'];

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    public static function documentsResponse($email) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "documents_response";

        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $email;

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    public static function transferEth($data) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "transfer_eth";

        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $data['to'] . "&merge_address_to=" . $data['address_to'] . "&merge_date=" . $data['date'] . "&merge_eth_amount=" . $data['eth_amount'] . "&merge_eth_rate=" . $data['eth_rate'] . "&merge_txhash=" . $data['txhash'];

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    public static function recoverPassword($data) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "recover_password";

        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $data['to'] . "&merge_email=" . $data['email'] . "&merge_token=" . $data['token'];

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    public static function newBankAccount($data) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "bank_account";

        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $data['to'] . "&merge_beneficiary=" . $data['beneficiary'] . "&merge_number=" . $data['number'] . "&merge_type=" . $data['type'] . "&merge_token=" . $data['token'];

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    public static function newDevice($data) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "new_device";

        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $data['to'] . "&merge_location=" . $data['location'] . "&merge_ip=" . $data['ip'] . "&merge_browser=" . $data['browser'] . "&merge_date=" . $data['date'] . "&merge_token=" . $data['token'];

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    public static function signup($data) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "signup";

        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $data['to'] . "&merge_token=" . $data['token'] . "&merge_email=" . $data['email'];

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    public static function withdrawUser($data) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "withdraw_user";

        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $data['to'] . "&merge_wire_code=" . $data['wire_code'] . "&merge_bank_account=" . $data['bank_account'] . "&merge_date=" . $data['date'] . "&merge_eth_rate=" . $data['eth_rate'] . "&merge_subtotal=" . $data['subtotal'] . "&merge_fee=" . $data['fee'] . "&merge_total=" . $data['total'];

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    public static function sellUser($data) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "sell_user";

        if ($data['payment_method'] == "USD Wallet") {
            $message = "YOUR TRANSACTION WAS PROCESSED SUCCESFULLY AND YOUR FUNDS WILL BE AVAILABLE IN A FEW MINUTES IN YOUR DESTINATION ACCOUNT. CHECK THE DETAILS BELOW.";
        } else {
            $message = "YOUR TRANSACTION WAS PROCESSED SUCCESFULLY AND YOUR FUNDS WILL BE AVAILABLE IN 2-5 BUSINESS DAYS IN YOUR DESTINATION ACCOUNT. CHECK THE DETAILS BELOW.";
        }



        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $data['to'] . "&merge_message=" . $message . "&merge_payment_method=" . $data['payment_method'] . "&merge_date=" . $data['date'] . "&merge_eth_amount=" . $data['eth_amount'] . "&merge_eth_rate=" . $data['eth_rate'] . "&merge_subtotal=" . $data['subtotal'] . "&merge_fee=" . $data['fee'] . "&merge_total=" . $data['total'];

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    public static function buyUser($data) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "buy_user";

        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $data['to'] . "&merge_payment_method=" . $data['payment_method'] . "&merge_date=" . $data['date'] . "&merge_eth_amount=" . $data['eth_amount'] . "&merge_eth_rate=" . $data['eth_rate'] . "&merge_subtotal=" . $data['subtotal'] . "&merge_fee=" . $data['fee'] . "&merge_total=" . $data['total'];

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    public static function depositUser($data) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "deposit_user";

        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $data['to'] . "&merge_wire_code=" . $data['wire_code'] . "&merge_payment_method=" . $data['payment_method'] . "&merge_date=" . $data['date'] . "&merge_subtotal=" . $data['subtotal'] . "&merge_fee=" . $data['fee'] . "&merge_total=" . $data['total'];

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }




    public static function sendMail($email, $subject, $message, $button, $redirect) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "new_favorite";

        $post = "apikey=" . $apikey . "&subject=" . $subject . "&to=" . $email . "&template=" . $template . "&merge_message=" . $message . "&merge_redirect=" . $redirect . "&merge_button=" . $button . "&to" . $button;

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }



    public static function mail($fullname, $email, $subject, $message, $button, $redirect) {



        $body = file_get_contents("public/email-templates/template.html");

        $body = str_replace("[\]", '', $body);

        $body = str_replace("{{DIR_URL}}", DIR_URL, $body);

        $body = str_replace("{{message}}", $message, $body);

        $body = str_replace("{{button}}", $button, $body);

        $body = str_replace("{{redirect}}", $redirect, $body);



        //para el envío en formato HTML 

        $headers = 'MIME-Version: 1.0' . "\r\n";

        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        $headers .= "From: Mercury <mining@adenter.io>\r\n";



        // ENVIAR CORREOS

        if (mail($email, $subject, $body, $headers)) {

            return 1;

        } else {

            return 0;

        }



    }



    public static function confirm_email($fullname, $email, $token) {



        $body = file_get_contents("public/email-templates/validate_email.html");

        $body = str_replace("[\]", '', $body);

        $body = str_replace("{{DIR_URL}}", DIR_URL, $body);

        $body = str_replace("{{token}}", $token, $body);

        $body = str_replace("{{email}}", $email, $body);



        //para el envío en formato HTML 

        $headers = 'MIME-Version: 1.0' . "\r\n";

        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        $headers .= "From: Mercury <support@mercury.cash>\r\n";



        // ENVIAR CORREOS

        if (mail($email, "Mercury Cash - Email Validation Process", $body, $headers)) {

            return 1;

        } else {

            return 0;

        }



    }





    public static function success_purchase($date, $eth_amount, $usd_amount, $payment_method, $email) {



        $body = file_get_contents("public/email-templates/process_success.html");

        $body = str_replace("[\]", '', $body);

        $body = str_replace("{{ETH_AMOUNT}}", $eth_amount, $body);

        $body = str_replace("{{DATE}}", $date, $body);

        $body = str_replace("{{USD_AMOUNT}}", $usd_amount, $body);

        $body = str_replace("{{PAYMENT_METHOD}}", $payment_method, $body);



        //para el envío en formato HTML 

        $headers = 'MIME-Version: 1.0' . "\r\n";

        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        $headers .= "From: Mercury <mining@adenter.io>\r\n";



        // ENVIAR CORREOS

        if (mail($email, "Confirm Email", $body, $headers)) {

            return 1;

        } else {

            return 0;

        }



    }





}

?>