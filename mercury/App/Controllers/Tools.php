<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Etherscan,
	\App\Models\ETHWallets,
    \App\Models\Request,
    \App\Models\Users,
    \App\Controllers\Mailer;

class Tools {

    public function index() {
    
        if (!empty($_SESSION['id'])) {
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }
            /* HEADER */
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            /* HEADER */
            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }
            $wallets = ETHWallets::getByUser($_SESSION['id']);
            View::set("wallets", $wallets);
            View::set("title", "Tools");
            View::set("requestsUnread", $unread);
            View::render("user-wallets");
        } else {
            header("Location: " . DIR_URL . "/login");
        }
    }
    
    public function create() {
        
        if (!empty($_SESSION['id'])) {
            $user = Users::read($_SESSION['id']);
            
            $url = "https://node.mercury.cash/create";
            $post = "user=" . $_SESSION['id'] . "&password=" . $user['password'] . "";
            $con = curl_init();
            curl_setopt($con, CURLOPT_URL, $url);
            curl_setopt($con, CURLOPT_HTTPGET, FALSE);
            curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
            curl_setopt($con, CURLOPT_POST, 1);
            curl_setopt($con, CURLOPT_POSTFIELDS, $post);
            curl_setopt($con, CURLOPT_HEADER, FALSE);
            curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($con);
            curl_close($con);
            $response = json_decode($response);
            
            if (!empty($response)) {

                $data = array();
                $data['json_wallet'] = $response->json_wallet;
                $data['private_key'] = $response->private_key;
                $data['public_key'] = $response->public_key;
                $data['address'] = $response->address;
                $data['primary'] = 0;
                $data['user'] = $_SESSION['id'];
                $result = ETHWallets::create($data);

                if ($result) {

                    $message = "<p>New Wallet Create</p>";
                    $message .= "<p>" . $_SESSION['name'] . "</p>";
                    $message .= "<p>" . $data['address'] . "</p>";

                    Mailer::sendMail("registrations@mercury.cash", "Mercury Cash - New Wallet Create", $message, "Go to Mercury Cash", DIR_URL);

                    $_SESSION['alert'] = "Your new wallet has been successfully created";
                    header("Location: " . DIR_URL . "/accounts");
                    exit;

                } else {

                    $_SESSION['alert'] = "An error has occurred, please try again.";
                    header("Location: " . DIR_URL . "/accounts");
                    exit;
                }

            } else {

                $_SESSION['alert'] = "An error has occurred, please try again.";
                header("Location: " . DIR_URL . "/accounts");
                exit;

            }

        } else {

            header("Location: " . DIR_URL . "/home");
            exit;

        }
    }
    
    public function editAlias() {
    
        if (!empty($_SESSION['id'])) {
            header("Access-Control-Allow-Origin: *");
            header("Content-Type: application/json");
            $wallet = ETHWallets::read($_POST['id']);
            $data = array();
            $data['id'] = $_POST['id'];
            $data['json_wallet'] = $wallet['json_wallet'];
            $data['private_key'] = $wallet['private_key'];
            $data['public_key'] = $wallet['public_key'];
            $data['address'] = $wallet['address'];
            $data['alias'] = (!empty($_POST['alias'])) ? htmlspecialchars($_POST['alias'], ENT_QUOTES) : $data['alias'];
            $data['principal'] = $wallet['principal'];
            $data['user'] = $wallet['user'];
            $result = ETHWallets::update($data);
            if ($result) {
                echo json_encode(array(
                    "status" => 1,
                    "description" => "Alias successfully saved"
                    ));
            } else {
                echo json_encode(array(
                    "status" => 0,
                    "description" => "An error has occurred"
                    ));
            }
        } else {
            echo json_encode(array(
                "status" => 0,
                "description" => "Missing data required for this operation"
                ));
        }
    }
    
    public function total() {
    

        if (!empty($_SESSION['id'])) {
            header("Access-Control-Allow-Origin: *");
            header("Content-Type: application/json");
            
            $wallets = ETHWallets::getByUser($_SESSION['id']);
            if (count($wallets) <= 10) {
                echo json_encode(array(
                    "status" => 1,
                    "description" => "The wallet has been created successfully"
                ));
            } else {
                echo json_encode(array(
                    "status" => 0,
                    "description" => "Has reached the limit of wallets per user"
                    ));
            }
        } else {
            echo json_encode(array(
                "status" => 0,
                "description" => "Missing data required for this operation"
                ));
        }
    }
}
?>