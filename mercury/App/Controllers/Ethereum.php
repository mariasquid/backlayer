<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\ETHWallets,
    \App\Models\Etherscan,
    \App\Models\Transfers;

class Ethereum {

    public function wallet() {

		error_reporting(E_ALL);
		ini_set('display_errors', 1);

    	$url = "https://node.mercury.cash/create";
    	$data = "user=200&password=rafaelmoreno";
        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($data)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $data);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);

        $data = array();
        $data['json_wallet'] = $response->json_wallet;
        $data['private_key'] = $response->private_key;
        $data['public_key'] = $response->public_key;
        $data['address'] = $response->address;
        $data['primary'] = 1;
        $data['user'] = $response->user;

        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
    
    public function trasaction() {

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $url = "https://node.mercury.cash/transaccion";
        $data = "sender=0x3b8fa2811bb6c863c807da6d0d702c6a759eec12&receiver=0x62ec74af61943a007664d5165cd41050b7f60295&amount=0.0001&privateKey=f8c38f72a94765549fdf6d6805470700f5a4d6d7205f49ee4974ba3245527b3a&save=0";
        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($data)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $data);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);

        echo "<pre>";
        print_r($response);
        echo "</pre>";

    }

    public function address() {

        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");

        $wallets = ETHWallets::getAll();

        $address = array();

        foreach ($wallets as $wallet) {
            array_push($address, $wallet['address']);
        }

        echo json_encode($address);

    }

    public function getBalance() {

        $data = '{"jsonrpc":"2.0","method":"eth_getBalance","params":["0x3b8fa2811bb6c863c807da6d0d702c6a759eec12", "latest"],"id":1}';
        $url = 'http://167.114.81.219:8545';
        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $data);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);

        $balance = hexdec($response->result);
        $balance = number_format($balance / 1000000000000000000, 8);

        echo $balance;

    }


    public function getCoinbase() {

        $data = '{"jsonrpc":"2.0","method":"eth_accounts","params":["0x3b8fa2811bb6c863c807da6d0d702c6a759eec12"],"id":1}';
        $url = 'http://167.114.81.219:8545';
        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $data);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);

        print_r($response);

    }

}
?>