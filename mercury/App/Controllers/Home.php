<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Etherscan;

class Home {

    public function index() {

        session_destroy();

		$price = Etherscan::getLastPrice();
		$ethusd = $price->ethusd;

		View::set("ethusd", $ethusd);
        View::set("title", "Mercury");
        View::render("home");

    }

}
?>