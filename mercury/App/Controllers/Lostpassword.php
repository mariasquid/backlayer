<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Users,
    \App\Controllers\Mailer;

class Lostpassword {
    
    public function index() {
    
        View::set("title", "Lost Password");
        View::render("lost_password");
    
    }
    
    public function validateMail() {
    
        $email = htmlspecialchars($_POST['email'], ENT_QUOTES);
        $user = Users::readByEmail($email);
        $status = 0;
        $description = NULL;
        if ($user) {

            $dataMail = array(
                "to" => $user['email'],
                "email" => $user['email'],
                "token" => $user['token']
                );
            
            Mailer::recoverPassword($dataMail);
            $status = 1;
            $description = "<h5>An <b>email</b> has been sent to your inbox</h5>";
        
        } else {
        
            $status = 0;
            $description = "<h5>The email does not exist in Mercury Cash, if you want to register you can do it through <a href='" .DIR_URL . "/register'>here</a></h5>";
        
        }
        
        View::set("status", $status);
        View::set("description", $description);
        View::set("title", "Lost Password");
        View::render("recover_pass");
    
    }
    
    public function validate($token) {
    
        $token = htmlspecialchars($token, ENT_QUOTES);
        $user = Users::getByToken($token);
        if ($user) {
            if ($token == $user['token']) {
                View::set("email", $user['email']);
                View::set("token", $user['token']);
                View::set("title", "Lost Password");
                View::render("new_password");
            
            } else {
            
                View::set("title", "Lost Password");
                View::render("error_data_password");
            
            }
            

        
        } else {
        
            View::set("title", "Lost Password");
            View::render("error_data_password");
        
        }
    

    }
    
    public function newPass() {

        $token = htmlspecialchars($_POST['token'], ENT_QUOTES);
        $email = htmlspecialchars($_POST['email'], ENT_QUOTES);

        require_once LIBSPATH . "/recaptcha/recaptchalib.php";

        $response = NULL;
        $res = NULL;

        if (!empty($_POST['g-recaptcha-response'])) {
                    
            $secret = "6LftmSQUAAAAAM_9SAp4XlIKc_kBcEy6wr2eIoo7";
            $reCaptcha = new \ReCaptcha($secret);
            
            $res = $reCaptcha->verifyResponse(
                $_SERVER["REMOTE_ADDR"],
                $_POST["g-recaptcha-response"]
                );
            
            $response = ($res->success) ? $res->success : NULL;
        }

        if ($response) {

            if ($_POST['password'] == $_POST['password_rp']) {

                $password = md5($_POST['password']);
                $user = Users::readByEmail($email);
                if ($user) {
                    if ($token == $user['token']) {
                        $result = Users::changePassword($password, $user['id']);
                        if ($result) {
                            $new = md5($user['email'] . time());
                            Users::updateToken($user['id'], $new);
                            header("Location: " . DIR_URL . "/login");
                            exit;
                        
                        }
                    

                    } else {
                    
                        View::set("title", "Lost Password");
                        View::render("error_data_password");
                    
                    }
                    
                } else {
                
                    View::set("title", "Lost Password");
                    View::render("error_data_password");
                
                }

            } else {

                $_SESSION['alert'] = "Passwords do not match";
                header("Location: " . DIR_URL . "/lostpassword/validate/" . $token);
                exit;

            }

        } else {

            $_SESSION['alert'] = "Do not forget to pass the captcha test";
            header("Location: " . DIR_URL . "/lostpassword/validate/" . $token);
            exit;

        }

    }


}

?>