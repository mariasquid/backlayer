<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Users as UsersModel,
    \App\Models\Suspensions,
    \App\Models\Wallets,
    \App\Models\Etherscan;

class Users {

    public function index() {

        if (!empty($_SESSION)) {

            $users = UsersModel::getAll();

        	View::set("users", $users);
        	View::set("title", "Users");
            View::render("all-users");

        } else {
            header("Location: " . DIR_URL . "/home");
        }

    }

    public function add() {

        if (!empty($_SESSION)) {

            
            View::set("title", "Create User");
            View::render("create_user");

        } else {
            header("Location: " . DIR_URL . "/home");
        }

    }

    public function details($user) {

        if (!empty($_SESSION)) {

            $wallet = Wallets::getByUser($user);
            $suspension = Suspensions::getByUser($user);
            $user = UsersModel::read($user);

            View::set("user", $user);
            View::set("wallet", $wallet);
            View::set("suspension", $suspension);
            View::set("title", "Details of " . $user['name'] . " " . $user['last_name']);
            View::render("details-user");

        } else {
            header("Location: " . DIR_URL . "/home");
        }

    }

    public function create() {

        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");

        if (!empty($_POST['name']) &&
            !empty($_POST['last_name']) &&
            !empty($_POST['email']) &&
            !empty($_POST['password']) &&
            !empty($_POST['birthdate']) &&
            !empty($_POST['address']) &&
            !empty($_POST['city']) &&
            !empty($_POST['country']) &&
            !empty($_POST['zip_conde'])) {
            
            $password = $_POST['password'];

            $_POST['name'] = htmlspecialchars($_POST['name'], ENT_QUOTES);
            $_POST['last_name'] = htmlspecialchars($_POST['last_name'], ENT_QUOTES);
            $_POST['email'] = htmlspecialchars($_POST['email'], ENT_QUOTES);
            $_POST['password'] = md5($_POST['password']);
            $_POST['token'] = md5($_POST['email'] . time());
            
            $user = UsersModel::create($_POST);

            if ($user > 0) {

                echo json_encode(array(
                    "status" => 1,
                    "description" => "User successfully created",
                    "redirect" => DIR_URL . "/users",
                    "user" => $user,
                    "password" => $password
                    ));
            } else {
                echo json_encode(array(
                    "status" => 0,
                    "description" => "An error occurred while trying to register, please try again"
                    ));
            }

        } else {
            echo json_encode(array(
                "status" => 0,
                "description" => "There are empty fields that are required"
                ));
        }

    }

    public function update() {

        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");

        $user = UsersModel::read($_POST['id']);
        $data = array();

        $data['id'] = $_POST['id'];
        $data['name'] = htmlspecialchars($_POST['name'], ENT_QUOTES);
        $data['last_name'] = htmlspecialchars($_POST['last_name'], ENT_QUOTES);
        $data['email'] = htmlspecialchars($_POST['email'], ENT_QUOTES);
        $data['password'] = $user['password'];
        $data['address'] = htmlspecialchars($_POST['address'], ENT_QUOTES);
        $data['city'] = htmlspecialchars($_POST['city'], ENT_QUOTES);
        $data['country'] = htmlspecialchars($_POST['country'], ENT_QUOTES);
        $data['zip_code'] = htmlspecialchars($_POST['zip_code'], ENT_QUOTES);
        $data['status'] = htmlspecialchars($_POST['status'], ENT_QUOTES);

        if (!empty($_POST['birthdate'])) {
            $birthdate = str_replace("/", "-", $_POST['birthdate']);
            $birthdate = strtotime($birthdate);
            $data['birthdate'] = $birthdate;
        } else {
            $data['birthdate'] = $user['birthdate'];
        }

        $result = UsersModel::update($data);

        if ($user['status'] != 0) {
            if (!empty($_POST['suspension']) && $_POST['status'] == 0) {
                $data['description'] = htmlspecialchars($_POST['suspension'], ENT_QUOTES);
                $data['date'] = time();
                $data['user'] = $_POST['id'];
                Suspensions::create($data);
            }
        }

        if ($result) {

            echo json_encode(array(
                "status" => 1,
                "description" => "User successfully update",
                "redirect" => DIR_URL . "/users"
                ));
        } else {
            echo json_encode(array(
                "status" => 0,
                "description" => "An error occurred while trying to update, please try again"
                ));
        }

    }

    public function suspend($user) {

        if (!empty($_SESSION)) {

            $wallet = Wallets::getByUser($user);
            $balance = Etherscan::getBalance($wallet['address']);
            $balance = $balance / 1000000000000000000;
            $user = UsersModel::read($user);

            View::set("user", $user);
            View::set("wallet", $wallet);
            View::set("balance", $balance);
            View::set("title", "Suspend user");
            View::render("suspend_user");

        } else {
            header("Location: " . DIR_URL . "/home");
        }

    }

    public function suspendUser() {

        if (!empty($_SESSION)) {

            if (!empty($_POST['description'])) {
                $description = htmlspecialchars($_POST['description'], ENT_QUOTES);

                $data = array(
                    "description" => $description,
                    "date" => time(),
                    "user" => $_POST['user']
                    );

                $result = Suspensions::create($data);
                if ($result) {
                    $result = UsersModel::changeStatus($_POST['user'], 0);
                    if ($result) {
                        header("Location: " . DIR_URL . "/users");
                    }
                }
            }

        } else {
            header("Location: " . DIR_URL . "/home");
        }

    }

    public function delete($user) {

        if (!empty($_SESSION)) {

            $result = Wallets::deleteByUser($user);

            if ($result) {
                $result = UsersModel::delete($user);
                if ($result) {
                    echo json_encode(array(
                        "status" => 1,
                        "description" => "User deleted successfully"
                        ));
                } else {
                    echo json_encode(array(
                        "status" => 0,
                        "description" => "There was an error deleting the user, try again"
                        ));
                }
            } else {
                echo json_encode(array(
                    "status" => 0,
                    "description" => "There was an error deleting the user, try again"
                    ));
            }

        } else {
            header("Location: " . DIR_URL . "/home");
        }

    }

}
?>