<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Users,
    \App\Models\Etherscan,
    \App\Models\Account as AccountModel;

class Account {

    public function index() {

        if (!empty($_SESSION['id'])) {
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;

            View::set("ethusd", $ethusd);
            /* HEADER */

            View::set("title", "Account");
            View::render("account");

        } else {
            header("Location: " . DIR_URL . "/login");
        }

    }

    public function deposit() {

        if (!empty($_SESSION['id'])) {
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;

            View::set("ethusd", $ethusd);
            /* HEADER */

            View::set("title", "Deposit USD");
            View::render("deposit_dollars");

        } else {
            header("Location: " . DIR_URL . "/login");
        }

    }

    public function processDeposit() {

        if (!empty($_SESSION['id'])) {
            
            if (!empty($_POST['amount'])) {

                $amount = $_POST['amount'];

                if (is_numeric($amount)) {

                    $user = Users::read($_SESSION['id']);

                    require_once LIBSPATH . "/stripe/init.php";
                    $token = $_POST['stripeToken'];
                    $customerId = NULL;

                    if (is_null($user['stripe_id'])) {
                        $customer = \Stripe\Customer::create(array(
                            'email' => $_SESSION['email'],
                            'card' => $token
                            ));

                        Users::addStripeId($customer->id, $_SESSION['id']);

                        $customerId = $customer->id;
                    } else {
                        $customerId = $user['stripe_id'];
                    }

                    $amountStripe = $amount * 100;

                    $charge = \Stripe\Charge::create(array(
                        'customer' => $customerId,
                        'amount' => $amountStripe,
                        'currency' => 'usd'
                        ));

                    $data = array();
                    
                    $data['transaction_id'] = $charge->id;
                    $data['usd_amount'] = $amount;
                    $data['type'] = 1;
                    $data['description'] = "$" . $amount . " personal account deposit";
                    $data['date'] = time();
                    $data['user'] = $_SESSION['id'];

                    $result = AccountModel::create($data);

                    if ($result) {
                        header("Location: " . DIR_URL . "/account/processDepositResponse/1");
                    }

                } else {
                    header("Location: " . DIR_URL . "/account/processDepositResponse/2");
                }

            } else {
                header("Location: " . DIR_URL . "/account/processDepositResponse/3");
            }

        } else {
            header("Location: " . DIR_URL . "/login");
        }

    }

    function processDepositResponse($status) {

        if (!empty($_SESSION['id'])) {
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;

            View::set("ethusd", $ethusd);
            /* HEADER */

            $message = NULL;
            $alert = NULL;
            if (isset($status)) {
                switch ($status) {
                    case '1':
                        $message = 'Is checking if I deposit, will soon receive an email with the status of your transaction';
                        $alert = 'info';
                        break;
                    case '2':
                        $message = 'The amount must be a numeric data <b><a href="' . DIR_URL . '/account/deposit" style="color: #fff;font-weight: bold;text-decoration: underline;">go back</a></b>';
                        $alert = 'danger';
                        break;
                    case '3':
                        $message = 'You must indicate the amount you want to deposit <b><a href="' . DIR_URL . '/account/deposit" style="color: #fff;font-weight: bold;text-decoration: underline;">go back</a></b>';
                        $alert = 'danger';
                        break;
                }
            }

            View::set("message", $message);
            View::set("alert", $alert);
            View::set("title", "Deposit USD");
            View::render("process_deposit_response");

        } else {
            header("Location: " . DIR_URL . "/login");
        }

    }

    public function transactions() {

        if (!empty($_SESSION['id'])) {
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;

            View::set("ethusd", $ethusd);
            /* HEADER */

            $transactions = AccountModel::getByUser($_SESSION['id']);

            View::set("transactions", $transactions);
            View::set("title", "Transactions USD");
            View::render("transactions_usd");

        } else {
            header("Location: " . DIR_URL . "/login");
        }

    }

}
?>