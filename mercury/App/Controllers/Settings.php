<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Users,
    \App\Models\Etherscan,
    \App\Models\Documents,
    \App\Models\Authy,
    \App\Models\Request,
    \App\Models\ETHWallets,
    \App\Models\Devices,
    \App\Models\BankAccounts,
    \App\Models\Country,
    \App\Models\Provinces,
    \App\Models\Business,
    \App\Controllers\Functions,
    \App\Controllers\Mailer;

class Settings {

    public function index() {
    
        if (!empty($_SESSION['id'])) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                
                header("Location: " . DIR_URL . "/login");
                exit;

            
            } else {

                $_SESSION['time'] = time();
            }
            /* HEADER */

            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            /* HEADER */

            $user = Users::read($_SESSION['id']);
            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }

            $countryUser = Country::read($user['country_id']);
            $provinceUser = Provinces::read($user['province_id']);
            $countries = Country::getAll();
            $provinces = Provinces::getAll();

            View::set("type_user", $user['type_user']);
            View::set("provinceUser", $provinceUser);
            View::set("countryUser", $countryUser);
            View::set("countries", $countries);
            View::set("provinces", $provinces);
            View::set("user", $user);
            View::set("title", "Settings");
            View::set("requestsUnread", $unread);
            View::render("settings");
        
        } else {

            header("Location: " . DIR_URL . "/login");
            exit;

        }
    }

    public function businessInfo() {

        if (!empty($_SESSION['id'])) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
                exit;
            } else {
                $_SESSION['time'] = time();
            }
            /* HEADER */
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            /* HEADER */

            $user = Users::read($_SESSION['id']);
            $business = Business::getByUser($_SESSION['id']);


            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }

            View::set("type_user", $user['type_user']);
            View::set("business", $business);
            View::set("title", "Business Info");
            View::set("requestsUnread", $unread);
            View::render("business_info");
        
        } else {

            header("Location: " . DIR_URL . "/login");
            exit;

        }

    }

    public function updateBusinessInfo() {

        if (!empty($_SESSION['id'])) {
            
            $user = Users::read($_SESSION['id']);
            $business = Business::getByUser($_SESSION['id']);

            $data = array();
            $data['name'] = (!empty($_POST['name'])) ? htmlspecialchars($_POST['name'], ENT_QUOTES) : $business['name'];
            $data['dba'] = (!empty($_POST['dba'])) ? htmlspecialchars($_POST['dba'], ENT_QUOTES) : $business['dba'];
            $data['taxid_ein'] = (!empty($_POST['taxid_ein'])) ? htmlspecialchars($_POST['taxid_ein'], ENT_QUOTES) : $business['taxid_ein'];
            $data['source_founds'] = (!empty($_POST['source_founds'])) ? htmlspecialchars($_POST['source_founds'], ENT_QUOTES) : $business['source_founds'];
            $data['address'] = (!empty($_POST['address'])) ? htmlspecialchars($_POST['address'], ENT_QUOTES) : $business['address'];
            $data['subcategory'] = (!empty($_POST['subcategory']) && is_numeric($_POST['subcategory'])) ? $_POST['subcategory'] : $business['subcategory'];
            $data['user'] = $_SESSION['id'];
            $data['id'] = $business['id'];

            $result = Business::update($data);
            if ($result) {
                
                $_SESSION['alert'] = "data has been updated successfully";
                header("Location: " . DIR_URL . "/settings/businessInfo");
                exit;

            } else {

                $_SESSION['alert'] = "An error has occurred, please try again.";
                header("Location: " . DIR_URL . "/settings/businessInfo");
                exit;

            }

        } else {

            header("Location: " . DIR_URL . "/login");
            exit;

        }

    }
    
    public function document() {
    
        if (!empty($_SESSION['id'])) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                
                header("Location: " . DIR_URL . "/login");
                exit;

            
            } else {

                $_SESSION['time'] = time();
            }
            /* HEADER */

            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            /* HEADER */

            $user = Users::read($_SESSION['id']);

            $documents = Documents::getByUser($_SESSION['id']);
            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }

            View::set("type_user", $user['type_user']);
            View::set("documents", $documents);
            View::set("title", "Identification Document");
            View::set("requestsUnread", $unread);
            View::render("id_document");
        
        } else {

            
            header("Location: " . DIR_URL . "/login");
            exit;

        }
    }
    
    public function uploadIdDocument() {
    
        if (!empty($_SESSION['id']) && !empty($_POST['token']) && $_POST['token'] == $_SESSION['validateToken']) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                
                header("Location: " . DIR_URL . "/login");
                exit;

            } else {

                $_SESSION['time'] = time();
            }

            if (!empty($_FILES['document']['name']) || !empty($_FILES['document_manual']['name'])) {
                $file = (!empty($_FILES['document']['name'])) ? $_FILES['document'] : $_FILES['document_manual'];
                if (is_numeric($_POST['document_type'])) {
                    
                    if ($file['type'] != "image/jpeg" && $file['type'] != "image/jpg" && $file['type'] != "image/png" && $file['type'] != "application/pdf") {
                        $_SESSION['alert'] = "The file type is invalid, the file must be JPEG, JPG, PNG or PDF";
                        header("Location: " . DIR_URL . "/settings/document");
                        exit;

                    } else {

                        $type = ($file['type'] == "application/pdf") ? ".pdf" : ".jpg";
                        $nameFile = md5($file['name'] . time());
                        if (move_uploaded_file($file['tmp_name'], "administrator/public/identifications/" . $nameFile . $type)) {
                            $data = array();
                            $data['name'] = $nameFile . $type;
                            $data['original_name'] = $file['name'];
                            $data['date'] = time();
                            $data['type'] = $_POST['document_type'];
                            $data['description'] = NULL;
                            $data['status'] = 2;
                            $data['size'] = substr($file['size'] * 1024, 0, 3) . "KB";
                            $data['user'] = $_SESSION['id'];
                            $result = Documents::create($data);
                            if ($result) {

                                Mailer::documentsResponse($_SESSION['email']);

                                $_SESSION['alert'] = "Your document has been saved, we are evaluating it, we will send you the reply to the reassurance";
                                header("Location: " . DIR_URL . "/settings/document");
                                exit;

                            } else {

                                $_SESSION['alert'] = "An error has occurred, please try again.";
                                header("Location: " . DIR_URL . "/settings/document");
                                exit;

                            }
                        
                        } else {

                            $_SESSION['alert'] = "There was an error saving your file. Try again please";
                            header("Location: " . DIR_URL . "/settings/document");
                            exit;

                        }
                    }
                
                } else {

                    $_SESSION['alert'] = "Document type is not valid";
                    
                    header("Location: " . DIR_URL . "/settings/document");
                    exit;

                }
            
            } else {

                $_SESSION['alert'] = "You must provide the identity file";
                
                header("Location: " . DIR_URL . "/settings/document");
                exit;

            }
        
        } else {

            header("Location: " . DIR_URL . "/home");
            exit;

        }
    }    

    public function uploadDocumentApp() {

        $auth = getallheaders();
        $header = array();
        $header[] = 'Content-length: 0';
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: '.$auth["Authorization"];
        $url = "localhost:3000/security/verifytoken";
        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, 1);
        curl_setopt($con, CURLOPT_HTTPHEADER, $header);
        curl_setopt($con, CURLOPT_HEADER, false);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        $httpcode = curl_getinfo($con, CURLINFO_HTTP_CODE);
        curl_close($con);
        $response = json_decode($response);
        if ($httpcode == 401) {
            header("HTTP/1.0 401 UNAUTHORIZED");
            echo json_encode($response);
            exit();
        }else{
            $id_user = $response->id_user;
            $email = $response->email;
        }

        foreach ($_FILES as $key=>$file) {
            echo $key;
            var_dump($file);
            if ($key=="statement") {
                $type=2;
            }elseif ($key=="license") {
                $type=3;
            }elseif ($key=="passport") {
                $type=1;
            }elseif ($key=="photo") {
                $type=4;
            }else{
                continue;
            }
            echo $type;
            $info = new \SplFileInfo($file['name']);
            $ext = $info->getExtension();
            $nameFile = md5($file['name'] . time());
            if (move_uploaded_file($file['tmp_name'], "administrator/public/identifications/" . $nameFile .'.'. $ext)) {
                $data = array();
                $data['name'] = $nameFile . $ext;
                $data['original_name'] = $file['name'];
                $data['date'] = time();
                $data['type'] = $type;
                $data['description'] = NULL;
                $data['status'] = 2;
                $data['size'] = substr($file['size'] * 1024, 0, 3) . "KB";
                $data['user'] = $id_user;
                $result = Documents::create($data);
                if ($result) {
                    $save = true;
                }
            }
        }
        if ($save) {
            //Mailer::documentsResponse($email);
            $response = array(
                "message"=>"Your document has been saved, we are evaluating it, we will send you the reply to the reassurance"
            );
            echo json_encode($response);
            exit;
        } else {
            $response = array(
                "message"=>"An error has occurred, please try again."
            );
            header("HTTP/1.0 401 UNAUTHORIZED");
            echo json_encode($response);
            exit;
        }
    }
    
    public function updateProfile() {
    
        if (!empty($_SESSION['id']) && !empty($_POST['token']) && $_POST['token'] == $_SESSION['validateToken']) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                
                header("Location: " . DIR_URL . "/login");
                exit;

            
            } else {

                $_SESSION['time'] = time();
            }

            if ($_SESSION['status'] != 4) {

                $user = Users::read($_SESSION['id']);

                $data = array();
                $data['email'] = $user['email'];

                //TIER I
                $data['name'] = (!empty($_POST['name'])) ? htmlspecialchars($_POST['name'], ENT_QUOTES) : $user['name'];
                $data['last_name'] = (!empty($_POST['last_name'])) ? htmlspecialchars($_POST['last_name'], ENT_QUOTES) : $user['last_name'];
                $data['ndocument'] = $user['ndocument'];
                $data['country_id'] = (!empty($_POST['country_id']) && is_numeric($_POST['country_id'])) ? $_POST['country_id'] : $user['country_id'];
                $data['province_id'] = (!empty($_POST['province_id']) && is_numeric($_POST['province_id'])) ? $_POST['province_id'] : $user['province_id'];
                $data['city'] = (!empty($_POST['city'])) ? htmlspecialchars($_POST['city'], ENT_QUOTES) : $user['city'];
                $data['zip_code'] = (!empty($_POST['zip_code'])) ? htmlspecialchars($_POST['zip_code'], ENT_QUOTES) : $user['zip_code'];
                $data['address'] = (!empty($_POST['address'])) ? htmlspecialchars($_POST['address'], ENT_QUOTES) : $user['address'];
                
                if (!empty($_POST['type_document']) && is_numeric($_POST['type_document'])) {
                    $type_document = "P-";
                    if ($_POST['type_document'] == 1) {
                        $type_document = "P-";
                    } elseif ($_POST['type_document'] == 2) {
                        $type_document = "L-";
                    }
                    $data['ndocument'] = (!empty($_POST['ndocument'])) ? htmlspecialchars($type_document . $_POST['ndocument'], ENT_QUOTES) : $user['ndocument'];
                }

                //TIER II
                $data['birthdate'] = (!empty($_POST['birthdate'])) ? htmlspecialchars($_POST['birthdate'], ENT_QUOTES) : NULL;
                $data['occupation'] = (!empty($_POST['occupation'])) ? htmlspecialchars($_POST['occupation'], ENT_QUOTES) : NULL;
                $data['profession'] = (!empty($_POST['profession'])) ? htmlspecialchars($_POST['profession'], ENT_QUOTES) : NULL;

                if (!empty($_POST['type_document_s']) && is_numeric($_POST['type_document_s'])) {
                    $type_document = "P-";
                    if ($_POST['type_document_s'] == 1) {
                        $type_document = "P-";
                    } elseif ($_POST['type_document_s'] == 2) {
                        $type_document = "";
                    }
                    $data['ssn'] = (!empty($_POST['ssn'])) ? htmlspecialchars($type_document . $_POST['ssn'], ENT_QUOTES) : NULL;
                }

                $data['phonecode'] = (is_numeric($_POST['phonecode'])) ? $_POST['phonecode'] : NULL;
                $data['phone'] = (is_numeric($_POST['phone'])) ? $_POST['phone'] : NULL;
                $data['id'] = $_SESSION['id'];

                $result = Users::update($data);
                if ($result) {

                    $_SESSION['alert'] = "Your data has been successfully updated";
                    header("Location: " . DIR_URL . "/settings");
                    exit;

                } else {

                    $_SESSION['alert'] = "An error occurred while trying to update your data, please try again";
                    header("Location: " . DIR_URL . "/settings");
                    exit;

                }
            
            } else {

                header("Location: " . DIR_URL . "/settings/document");
                exit;

            }
        
        } else {

            header("Location: " . DIR_URL . "/home");
            exit;

        }
    }

    public function changeAvatar() {

        header('Content-Type: application/json');

        if (!empty($_SESSION['id'])) {
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                
                echo json_encode(array(
                    "status" => 0,
                    "description" => "Timeout",
                    "redirect" => DIR_URL . "/login"
                    ));

            } else {
                $_SESSION['time'] = time();

                if (isset($_FILES['croppedImage'])) {
                    $file = $_FILES['croppedImage'];
                    if ($file['error'] === 0) {
                        if ($file['type'] != "image/jpg" &&
                            $file['type'] != "image/jpeg" &&
                            $file['type'] != "image/png") {

                            echo json_encode(array(
                                "status" => 0,
                                "description" => "The file you are trying to upload is invalid. Make sure the file is in JPG, JPEG or PNG format.",
                                "redirect" => DIR_URL . "/settings"
                                ));

                        } elseif ($file['size'] > 2097152) {

                            echo json_encode(array(
                                "status" => 0,
                                "description" => "The avatar that you are trying to upload is too heavy, please use an image with a size below of 2MB",
                                "redirect" => DIR_URL . "/settings"
                                ));

                        } else {
                            $user = Users::read($_SESSION['id']);
                            $avatar = ($user['avatar'] == "placeholder.jpg") ? md5($_SESSION['id'] . time()) . ".jpg" : $user['avatar'];

                            if (is_file("administrator/public/avatars/" . $avatar)) {
                                if (unlink("administrator/public/avatars/" . $avatar)) {
                                    $avatar = md5($_SESSION['id'] . time()) . ".jpg";
                                }
                            }

                            if (move_uploaded_file($file['tmp_name'], "administrator/public/avatars/" . $avatar)) {
                                if (Users::updateAvatar($avatar, $_SESSION['id'])) {
                                    $_SESSION['avatar'] = DIR_URL . "/administrator/public/avatars/" . $avatar;

                                    echo json_encode(array(
                                        "status" => 1,
                                        "description" => "Your photo has been successfully changed.",
                                        "redirect" => DIR_URL . "/settings"
                                        ));
                                
                                } else {

                                    echo json_encode(array(
                                        "status" => 0,
                                        "description" => "An error occurred, please try again.",
                                        "redirect" => DIR_URL . "/settings"
                                        ));

                                }

                            } else {

                                echo json_encode(array(
                                    "status" => 0,
                                    "description" => "An error occurred, please try again.",
                                    "redirect" => DIR_URL . "/settings"
                                    ));

                            }

                        }
                    
                    } else {

                        echo json_encode(array(
                            "status" => 0,
                            "description" => "An error occurred, please try again.",
                            "redirect" => DIR_URL . "/settings"
                            ));

                    }
                } else {

                    echo json_encode(array(
                        "status" => 0,
                        "description" => "You can not upload a blank file.",
                        "redirect" => DIR_URL . "/settings"
                        ));

                }

            }

        } else {
            echo json_encode(array(
                "status" => 0,
                "description" => "Timeout",
                "redirect" => DIR_URL . "/login"
                ));
        }

    }
    
    public function _changeAvatar() {
    
        if (!empty($_SESSION['id']) && !empty($_POST['token']) && $_POST['token'] == $_SESSION['validateToken']) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                
                header("Location: " . DIR_URL . "/login");
                exit;

            } else {

                $_SESSION['time'] = time();
            }

            if ($_SESSION['status'] == 1 || $_SESSION['status'] == 2) {
                $user = Users::read($_SESSION['id']);
                
                if (isset($_FILES['avatar'])) {

                    if ($_FILES['avatar']['error'] === 0) {
                        
                        if ($_FILES['avatar']['type'] != "image/jpg" &&
                            $_FILES['avatar']['type'] != "image/jpeg" &&
                            $_FILES['avatar']['type'] != "image/png") {
                            
                            $_SESSION['alert'] = "The file you are trying to upload is invalid. Make sure the file is in JPG, JPEG or PNG format.";
                            header("Location: " . DIR_URL . "/settings");
                            exit;

                        } elseif ($_FILES['avatar']['size'] > 2097152) {

                            $_SESSION['alert'] = "The avatar that you are trying to upload is too heavy, please use an image with a size below of 2MB";
                            header("Location: " . DIR_URL . "/settings");
                            exit;

                        } else {

                            $avatar = ($user['avatar'] == "placeholder.jpg") ? md5($_SESSION['id'] . time()) . ".jpg" : $user['avatar'];

                            if (move_uploaded_file($_FILES['avatar']['tmp_name'], "administrator/public/avatars/" . $avatar)) {

                                if (Users::updateAvatar($avatar, $_SESSION['id'])) {
                                    $_SESSION['avatar'] = DIR_URL . "/administrator/public/avatars/" . $avatar;

                                    $_SESSION['alert'] = "Your photo has been successfully changed.";
                                    header("Location: " . DIR_URL . "/settings");
                                    exit;
                                
                                } else {

                                    $_SESSION['alert'] = "An error occurred, please try again.";
                                    header("Location: " . DIR_URL . "/settings");
                                    exit;

                                }

                            } else {

                                $_SESSION['alert'] = "An error occurred, please try again.";
                                header("Location: " . DIR_URL . "/settings");
                                exit;

                            }

                        }

                    } else {

                        $_SESSION['alert'] = "An error occurred, please try again.";
                        header("Location: " . DIR_URL . "/settings");
                        exit;

                    }
                
                } else {

                    $_SESSION['alert'] = "You can not upload a blank file.";
                    header("Location: " . DIR_URL . "/settings");
                    exit;

                }
            
            } else {

                $_SESSION['alert'] = "<p>In order to verify your account, please upload your Driver's License (US Citizens Only) or your Passport.</p><p>Example of Selfie's ID Verification</p>";
                header("Location: " . DIR_URL . "/settings");
                exit;

            }
        
        } else {

            header("Location: " . DIR_URL . "/login");
            exit;

        }
    }
    
    public function password($status = NULL) {
    
        /*
        STATUS:
        1 = EXITO
        2 = LA CLAVE VIEJA NO COINCIDE CON LA CLAVE ACTUAL
        3 = LA CLAVE NUEVA NO COINCIDE CON LA CLAVE REPETIR
        4 = LOS CAMPOS ESTAN VACIOS
        */
        if (!empty($_SESSION['id'])) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                
                header("Location: " . DIR_URL . "/login");
                exit;

            
            } else {

                $_SESSION['time'] = time();
            }

            /* HEADER */

            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            /* HEADER */

            $message = NULL;
            $alert = NULL;
            
            if (!is_null($status)) {
                switch ($status) {
                    case '1':
                        $_SESSION['message'] = "The password was successfully updated";
                        break;
                    case '2':
                        $_SESSION['message'] = "The old password does not match the current password";
                        break;
                    case '3':
                        $_SESSION['message'] = 'The "NEW PASSWORD" and "REPEAT PASSWORD" fields do not match';
                        break;
                    case '4':
                        $_SESSION['message'] = 'Fields should not be empty';
                        break;
                }
            
            }

            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }

            $user = Users::read($_SESSION['id']);

            View::set("type_user", $user['type_user']);
            View::set("title", "Change Password");
            View::set("requestsUnread", $unread);
            View::render("change_password");
        
        } else {

            header("Location: " . DIR_URL . "/login");
            exit;

        }
    }
    
    public function changePassword() {
    
        if (!empty($_SESSION['id']) && !empty($_POST['token']) && $_POST['token'] == $_SESSION['validateToken']) {
        
            if (!empty($_POST['old_password']) && !empty($_POST['new_password']) && !empty($_POST['repeat_password'])) {
                
                if ($_POST['new_password'] == $_POST['repeat_password']) {
                    
                    $user = Users::read($_SESSION['id']);
                    $old_password = md5($_POST['old_password']);
                    
                    if ($old_password == $user['password']) {
                        
                        $password = md5($_POST['new_password']);
                        $result = Users::changePassword($password, $_SESSION['id']);
                        
                        header("Location: " . DIR_URL . "/settings/password/1");
                        exit;

                    } else {

                        header("Location: " . DIR_URL . "/settings/password/2");
                        exit;

                    }
                
                } else {

                    header("Location: " . DIR_URL . "/settings/password/3");
                    exit;

                }
            
            } else {

                header("Location: " . DIR_URL . "/settings/password/4");
                exit;

            }
        
        } else {

            header("Location: " . DIR_URL . "/login");
            exit;

        }
    }
    
    public function security() {
    
        if (!empty($_SESSION['id'])) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                
                header("Location: " . DIR_URL . "/login");
                exit;
            
            } else {

                $_SESSION['time'] = time();
            }
            /* HEADER */

            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            /* HEADER */

            $user = Users::read($_SESSION['id']);
            $phone = "+" . $user['phonecode'] . $user['phone'];
            $lng = strlen($phone);
            $out = strlen(substr($phone, 1, $lng - 5));
            $in =  substr($phone, ($lng - 4), $lng);
            $phone = "+" . str_repeat("x", $out) . $in;
            $devices = Devices::getAllByUser($_SESSION['id']);
            
            for ($i=0; $i < count($devices); $i++) { 
                $devices[$i]['browser'] = Functions::getBrowser($devices[$i]['browser']);
                $details = json_decode(file_get_contents("http://ipinfo.io/" . $devices[$i]['ip']));
                $devices[$i]['near'] = $details->country . ", " . $details->region;
            }

            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }

            View::set("type_user", $user['type_user']);
            View::set("devices", $devices);
            View::set("user", $user);
            View::set("phone", $phone);
            View::set("title", "Two Factor Authenticator");
            View::set("requestsUnread", $unread);
            View::render("two_factor_authenticator");
        
        } else {

            header("Location: " . DIR_URL . "/login");
            exit;

        }
    }
    
    public function trashDevice($id, $token) {
    
        if (!empty($_SESSION['id']) && !empty($token) && $token == $_SESSION['validateToken']) {
        
            if (!empty($id) && is_numeric($id)) {
                
                $device = Devices::read($id);
                if ($device['user'] == $_SESSION['id']) {
                    
                    $result = Devices::changeStatus($id, 0, 0);
                    if ($result) {
                        $_SESSION['alert_device'] = "The device has been removed from the list";
                        
                        header("Location: " . DIR_URL . "/settings/security");
                        exit;

                    } else {

                        $_SESSION['alert_device'] = "An error has occurred, please try again.";
                        
                        header("Location: " . DIR_URL . "/settings/security");
                        exit;

                    }
                
                } else {

                    $_SESSION['alert_device'] = "Error!";
                    
                    header("Location: " . DIR_URL . "/settings/security");
                    exit;

                }
            
            } else {

                $_SESSION['alert_device'] = "Error!";
                
                header("Location: " . DIR_URL . "/settings/security");
                exit;

            }
        
        } else {

            header("Location: " . DIR_URL . "/home");
            exit;

        }
    }
    
    public function addDevice($token) {
    
        if (!empty($_SESSION['id'])) {
        
            if (!empty($token)) {
                $token = htmlspecialchars($token, ENT_QUOTES);
                $device = Devices::getByToken($token);
                
                if (!empty($device['id']) && $device['ip'] == $_SERVER['REMOTE_ADDR']) {
                    
                    if ($device['user'] == $_SESSION['id']) {
                        $user = Users::read($_SESSION['id']);
                        $wallet = ETHWallets::getPrimaryWalletEthByUser($user['id']);
                        
                        $change = Devices::changeToken($device['id'], md5($device['id'] . time()));
                        if ($change) {
                            
                            $result = Devices::changeStatus($device['id'], 1, $device['disable_tfa']);
                            if ($result) {
                                
                                if (!empty($_SESSION['secret_google'])) {
                                    Users::saveGoogleCode($_SESSION['secret_google'], $_SESSION['id']);
                                    unset($_SESSION['secret_google']);
                                }

                                $localjwt = Users::getJWT($user['email'], $user['password']);
                                if (!is_null($localjwt)) {
                                    $_SESSION['name'] = $user['name'] . " " . $user['last_name'];
                                    $_SESSION['avatar'] = DIR_URL . "/administrator/public/avatars/" . $user['avatar'];
                                    $_SESSION['status'] = $user['status'];
                                    $_SESSION['address'] = $wallet['address'];
                                    $_SESSION['wallet'] = $wallet['id'];
                                    $_SESSION['time'] = time();
                                    $_SESSION['validateToken'] = hash('sha256', md5(uniqid(mt_rand(), true)));
                                    
                                    header("Location: " . DIR_URL . "/dashboard");
                                    exit;
                                }

                            }
                        }
                    
                    } else {

                        session_destroy();
                        
                        header("Location: " . DIR_URL . "/login/errorDevice");
                        exit;

                    }
                
                } else {

                    session_destroy();
                    
                    header("Location: " . DIR_URL . "/login/errorDevice");
                    exit;

                }
            
            } else {

                session_destroy();
                
                header("Location: " . DIR_URL . "/login/errorDevice");
                exit;

            }
        
        } else {

            session_destroy();
            
            header("Location: " . DIR_URL . "/login/errorDevice");
            exit;

        }
    }
    
    public function verify_phone() {
    
        if (!empty($_SESSION['id'])) {
        
            header("Access-Control-Allow-Origin: *");
            header("Content-Type: application/json");
            $user = Users::read($_SESSION['id']);
            $authy_id = NULL;
            
            if (is_null($user['authy_id'])) {
                require_once LIBSPATH . "/authy/init.php";
                $register = $authy_api->registerUser($user['email'], $user['phone'], $user['phonecode']);
                
                if ($register->ok()) {
                    $authy_id = $register->id();
                    Users::addAuthyId($authy_id, $_SESSION['id']);
                }
            
            } else {

                $authy_id = $user['authy_id'];
            }
            $authy = Authy::RequestSMS($authy_id);
            
            if ($authy->success) {
                echo json_encode(array(
                    "status" => 1,
                    "description" => "An SMS has been sent to your cell phone with the verification code. You can also use the Authy app to generate the code"
                    ));
            
            } else {

                echo json_encode(array(
                    "status" => 0,
                    "description" => "Error",
                    "object" => $authy
                    ));
            }
        }
    }
    
    public function verify_code() {
    
        if (!empty($_SESSION['id'])) {
        
            header("Access-Control-Allow-Origin: *");
            header("Content-Type: application/json");
            if (!empty($_POST['code_authy'])) {
                $user = Users::read($_SESSION['id']);
                $code = htmlspecialchars($_POST['code_authy'], ENT_QUOTES);
                $authy = Authy::verifySMS($user['authy_id'], $code);
                
                if ($authy->success == "true") {
                    $result = Users::verifyPhone($_SESSION['id']);
                    echo json_encode(array(
                        "status" => 1,
                        "description" => "We have verified your phone successfully"
                        ));
                
                } else {

                    echo json_encode(array(
                        "status" => 0,
                        "description" => "There was an error verifying your phone."
                        ));
                }
            
            } else {

                echo json_encode(array(
                    "status" => 0,
                    "description" => "There was an error verifying your phone."
                    ));
            }
        }
    }
    
    public function changeTFA($method) {
    
        if (!empty($_SESSION['id'])) {
        
            if (!empty($method)) {
                $user = Users::read($_SESSION['id']);
                $method = htmlspecialchars($method, ENT_QUOTES);
                
                if ($method == "google") {
                    
                    if ($user['2fa_authy'] == 1 && $user['2fa_google'] == 0) {
                        
                        $result = Users::activeGoogleTFA($_SESSION['id']);
                        
                        if ($result) {
                            Users::inactiveAuthyTFA($_SESSION['id']);
                            //Users::resetTimeAuth($_SESSION['id']);
                            $_SESSION['alert'] = "Google Authentication has been enabled";
                            
                            header("Location: " . DIR_URL . "/settings/security");
                            exit;

                        
                        } else {

                            $_SESSION['alert'] = "An error has occurred, please try again.";
                            
                            header("Location: " . DIR_URL . "/settings/security");
                            exit;

                        }
                    
                    } else {

                        $_SESSION['alert'] = "Google Authentication is already enabled.";
                        
                        header("Location: " . DIR_URL . "/settings/security");
                        exit;

                    }

                } elseif ($method == "authy") {
                    
                    if ($user['2fa_google'] == 1 && $user['2fa_authy'] == 0) {
                        
                        $result = Users::activeAuthyTFA($_SESSION['id']);
                        if ($result) {
                            Users::inactiveGoogleTFA($_SESSION['id']);
                            //Users::resetTimeAuth($_SESSION['id']);
                            $_SESSION['alert'] = "Authy Authentication has been enabled";
                            
                            header("Location: " . DIR_URL . "/settings/security");
                            exit;

                        
                        } else {

                            $_SESSION['alert'] = "An error has occurred, please try again.";
                            
                            header("Location: " . DIR_URL . "/settings/security");
                            exit;

                        }
                    
                    } else {

                        $_SESSION['alert'] = "Authy Authentication is already enabled.";
                        
                        header("Location: " . DIR_URL . "/settings/security");
                        exit;

                    }
                }
            
            } else {

                $_SESSION['alert'] = "You must send an authentication method";
                
                header("Location: " . DIR_URL . "/settings/security");
                exit;

            }
        
        } else {

            header("Location: " . DIR_URL . "/settings/security");
            exit;

        }
    }


    public function bank() {
    
        if (!empty($_SESSION['id'])) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
                exit;
            } else {
                $_SESSION['time'] = time();
            }

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            /* HEADER */

            $user = Users::read($_SESSION['id']);
            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }

            $accounts = BankAccounts::getBankAccountByUser($_SESSION['id']);

            View::set("type_user", $user['type_user']);
            View::set("accounts", $accounts);
            View::set("title", "Your Financial Information");
            View::set("requestsUnread", $unread);
            View::render("bank_info");
        
        } else {

            header("Location: " . DIR_URL . "/login");
            exit;

        }

    }


    public function AddBankAccount() {
    
        if (!empty($_SESSION['id']) && !empty($_POST['token']) && $_POST['token'] == $_SESSION['validateToken']) {

            if (!empty($_POST['type']) && !empty($_POST['number']) && !empty($_POST['address']) && !empty($_POST['city']) && !empty($_POST['state']) && !empty($_POST['zip_code'])) {

                if (is_numeric($_POST['type'])) {

                    $token = md5($_POST['number'] . "-" . time());
                    $data = array();
                    $data['type'] = htmlspecialchars($_POST['type'], ENT_QUOTES);
                    $data['number'] = htmlspecialchars($_POST['number'], ENT_QUOTES);
                    $data['address'] = htmlspecialchars($_POST['address'], ENT_QUOTES);
                    $data['city'] = htmlspecialchars($_POST['city'], ENT_QUOTES);
                    $data['state'] = htmlspecialchars($_POST['state'], ENT_QUOTES);
                    $data['zip_code'] = htmlspecialchars($_POST['zip_code'], ENT_QUOTES);
                    $data['beneficiary_name'] = htmlspecialchars($_POST['beneficiary_name'], ENT_QUOTES);
                    $data['token'] = $token;
                    $data['user'] = $_SESSION['id'];

                    if ($data['type'] == 1) {

                        if (!empty($_POST['routing'])) {
                            
                            $data['routing'] = htmlspecialchars($_POST['routing'], ENT_QUOTES);

                            $result = BankAccounts::create($data);

                            if ($result > 0) {

                                similar_text($data['beneficiary_name'], $_SESSION['name'], $percentage);

                                if ($percentage <= 85) {
                                    $message = '<p style="text-align: center;">' . $_SESSION['name'] . ' has added a new bank account, however, the payee name does not match his name</p>';
                                    $message .= '<p style="text-align: center;"><b>Beneficiary Name: ' . $data['beneficiary_name'] . '</b></p>';
                                    $message .= '<p style="text-align: center;"><b>Bank Account Number: ' . $data['number'] . '</b></p>';
                                    Mailer::sendMail("registrations@mercury.cash", "Mercury Cash - New bank account", $message, "Go to Mercury Cash", DIR_URL . "/login");
                                }
                            
                                $dataMail = array(
                                    "to" => $_SESSION['email'],
                                    "beneficiary" => $data['beneficiary_name'],
                                    "number" => $data['number'],
                                    "type" => "Domestic",
                                    "token" => $token
                                    );

                                Mailer::newBankAccount($dataMail);

                                $_SESSION['alert'] = "We have sent you an email to confirm the bank account to finish adding";
                                header("Location: " . DIR_URL . "/settings/bank");
                                exit;

                            } else {

                                $_SESSION['alert'] = "An error has occurred, please try again";
                                header("Location: " . DIR_URL . "/settings/bank");
                                exit;

                            }

                        } else {

                            $_SESSION['alert'] = "There are empty fields that are required";
                            header("Location: " . DIR_URL . "/settings/bank");
                            exit;

                        }

                    } else {

                        if (!empty($_POST['swif_bic']) && !empty($_POST['account_number_intermediary'])) {
                            
                            $data['swif_bic'] = htmlspecialchars($_POST['swif_bic'], ENT_QUOTES);
                            $data['account_number_intermediary'] = htmlspecialchars($_POST['account_number_intermediary'], ENT_QUOTES);

                            $result = BankAccounts::create($data);

                            if ($result > 0) {

                                similar_text($data['beneficiary_name'], $_SESSION['name'], $percentage);

                                if ($percentage <= 85) {
                                    $message = '<p style="text-align: center;">' . $_SESSION['name'] . ' has added a new bank account, however, the payee name does not match his name</p>';
                                    $message .= '<p style="text-align: center;"><b>Beneficiary Name: ' . $data['beneficiary_name'] . '</b></p>';
                                    $message .= '<p style="text-align: center;"><b>Bank Account Number: ' . $data['number'] . '</b></p>';
                                    Mailer::sendMail("registrations@mercury.cash", "Mercury Cash - New bank account", $message, "Go to Mercury Cash", DIR_URL . "/login");
                                }
                                
                                $message = '<p style="text-align: center;">You have added a new bank account in mercury</p>';
                                $message .= '<p style="text-align: center;"><b>' . $data['number'] . '</b></p>';

                                $dataMail = array(
                                    "to" => $_SESSION['email'],
                                    "beneficiary" => $data['beneficiary_name'],
                                    "number" => $data['number'],
                                    "type" => "International",
                                    "token" => $token
                                    );

                                Mailer::newBankAccount($dataMail);

                                $_SESSION['alert'] = "We have sent you an email to confirm the bank account to finish adding";
                                header("Location: " . DIR_URL . "/settings/bank");
                                exit;

                            } else {

                                $_SESSION['alert'] = "An error has occurred, please try again";
                                header("Location: " . DIR_URL . "/settings/bank");
                                exit;

                            }

                        } else {

                            $_SESSION['alert'] = "There are empty fields that are required";
                            header("Location: " . DIR_URL . "/settings/bank");
                            exit;

                        }

                    }

                } else {

                    $_SESSION['alert'] = "You are trying to send data that is not valid";
                    header("Location: " . DIR_URL . "/settings/bank");
                    exit;

                }

            } else {

                $_SESSION['alert'] = "There are empty fields that are required";
                header("Location: " . DIR_URL . "/settings/bank");
                exit;

            }
        
        } else {

            header("Location: " . DIR_URL . "/login");
            exit;

        }
        
    }


    public function confirmBankAccount($token) {

        if (!empty($_SESSION['id'])) {
            
            $token = htmlspecialchars($token, ENT_QUOTES);

            $account = BankAccounts::getByToken($token);
            if ($account['user'] == $_SESSION['id'] && $account['token'] == $token) {

                $result = BankAccounts::changeStatus(1, $account['id']);
                if ($result) {
                    
                    $_SESSION['alert'] = "Your bank account has been activated";
                    header("Location: " . DIR_URL . "/settings/bank");
                    exit;

                } else {

                    $_SESSION['alert'] = "An error has occurred, please try again";
                    header("Location: " . DIR_URL . "/settings/bank");
                    exit;

                }

            } else {

                $_SESSION['alert'] = "Your data does not match the account you want to activate";
                header("Location: " . DIR_URL . "/settings/bank");
                exit;

            }

        } else {

            header("Location: " . DIR_URL . "/login");
            exit;

        }

    }

    public function getProvinces($country_id) {

        header('Content-Type: application/json');
        echo json_encode(Provinces::getByCountry($country_id));

    }

}
?>