<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Users,
    \App\Models\ETHWallets,
    \App\Models\Etherscan,
    \App\Models\Transfers,
    \App\Controllers\Mailer;

class Notifications {
    
    public function validate() {

        $wallets = ETHWallets::getAll();

        $lastPrice = Etherscan::getLastPrice();
        $lastPrice = $lastPrice->ethusd;

        foreach ($wallets as $wallet) {

            $transactions = Etherscan::getlistTransactions($wallet['address']);
            $total = count($transactions);
            if ($total > 0) {

                if ($total > $wallet['transactions']) {

                    $last = $total - 1;

                    $difference = $total - $wallet['transactions'];
                    
                    $result = ETHWallets::changeBalance($total, $wallet['id']);
                    if ($result) {
                        
                        if ($wallet['address'] == $transactions[$last]->to) {
                            
                            $user = Users::read($wallet['user']);
                            
                            $dataMail = array(
                                "to" => $user['email'],
                                "txhash" => $transactions[$last]->hash,
                                "sender" => $transactions[$last]->from,
                                "amount" => number_format(Etherscan::fromWei($transactions[$last]->value), 5)
                                );

                            Mailer::receiveEth($dataMail);

                            //echo "cambio de transacion -> " . $wallet['address'];
                            //echo "<hr>";

                            $data = array();
                            $data['eth_amount'] = Etherscan::fromWei($transactions[$last]->value);
                            $data['usd_amount'] = number_format($lastPrice * Etherscan::fromWei($transactions[$last]->value), 2);
                            $data['eth_rate'] = number_format($lastPrice, 2);
                            $data['address'] = $transactions[$last]->to;
                            $data['hash'] = $transactions[$last]->hash;
                            $data['type'] = "IN";
                            $data['date'] = $transactions[$last]->timeStamp;
                            $data['eth_wallet'] = $wallet['id'];
                            $data['user'] = $wallet['user'];
                            Transfers::create($data);

                        }

                    }

                }

            }

        }

    }

}
?>