<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Users,
	\App\Models\Etherscan,
    \App\Models\ETHWallets,
    \App\Models\Request as RequestModel,
    \App\Models\Favorites,
    \App\Controllers\Mailer;

class Request {
   
    public function index() {
   
        if (!empty($_SESSION['id'])) {

            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }

             /* HEADER */
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            /* HEADER */
            $requests = RequestModel::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
                $user = Users::read($requests[$i]['applicant']);
                $requests[$i]['fullname'] = $user['name'] . " " . $user['last_name'];
                $requests[$i]['address'] = ETHWallets::getPrimaryWalletEthByUser($requests[$i]['applicant'])['address'];
            }

            $ethwallets = array();

            $ethwallets1 = ETHWallets::getByUser($_SESSION['id']);
            foreach ($ethwallets1 as $ethwallet) {
                array_push($ethwallets, array(
                    "id" => $ethwallet['id'],
                    "address" => $ethwallet['address'],
                    "balance" => Etherscan::fromWei(Etherscan::getBalance($ethwallet['address'])),
                    "usd" => number_format($ethusd * Etherscan::fromWei(Etherscan::getBalance($ethwallet['address'])), 2),
                    "alias" => $ethwallet['alias']
                    ));
            }

            unset($ethwallets1);

            View::set("ethwallets", $ethwallets);
            View::set("requests", $requests);
            View::set("title", "Request");
            View::set("requestsUnread", $unread);
            View::render("request");
            RequestModel::updateViewsByPayer($_SESSION['id']);
        } else {
            header("Location: " . DIR_URL . "/login");
        }
    }
   
    public function request() {
   
        if (!empty($_SESSION['id'])) {

            if (!empty($_POST['email']) && !empty($_POST['eth_wallet']) && !empty($_POST['eth_amount'])) {
                
                if (is_numeric($_POST['eth_wallet']) && is_numeric($_POST['eth_amount'])) {
                    
                    $email = htmlspecialchars($_POST['email'], ENT_QUOTES);

                    if ($email != $_SESSION['email']) {
                        
                        $payer = Users::readByEmail($email);
                        if (!empty($payer['id'])) {

                            $price = Etherscan::getLastPrice();
                            $ethusd = $price->ethusd;

                            $eth_wallet = $_POST['eth_wallet'];
                            $eth_amount = $_POST['eth_amount'];
                            $usd_amount = $ethusd * $eth_amount;
                            
                            $data = array();
                            $data['eth_amount'] = $eth_amount;
                            $data['usd_amount'] = $usd_amount;
                            $data['date'] = time();
                            $data['applicant'] = $_SESSION['id'];
                            $data['eth_wallets_applicant'] = $eth_wallet;
                            $data['payer'] = $payer['id'];

                            $result = RequestModel::create($data);
                            if ($result) {

                                $message = '<p style="text-align: center;">' . $_SESSION['name'] . ' has requested a payment from you:</p>';
                                $message .= '<p><b>' . $eth_amount . ' ETH</b></p>';

                                $dataMail = array(
                                    "to" => $email,
                                    "name" => $_SESSION['name'],
                                    "eth_amount" => number_format($eth_amount, 5)
                                    );

                                Mailer::requestEth($dataMail);
                                
                                $_SESSION['alert'] = "Your transaction was successful";
                                header("Location: " . DIR_URL . "/request");
                                exit;

                            } else {

                                $_SESSION['alert'] = "An error has occurred, please try again";
                                header("Location: " . DIR_URL . "/request");
                                exit;

                            }

                        } else {

                            $dataMail = array(
                                "to" => $email,
                                "name" => $_SESSION['name']
                                );

                            Mailer::requestEth($dataMail);
                            
                            $_SESSION['alert'] = "Mail " . $email . " is not registered in Mercury Cash, however we have sent you an email inviting you to join our platform";
                            header("Location: " . DIR_URL . "/request");
                            exit;

                        }

                    } else {

                        $_SESSION['alert'] = "You can not request money from yourself";
                        header("Location: " . DIR_URL . "/request");
                        exit;

                    }

                } else {

                    $_SESSION['alert'] = "The data you are trying to send is invalid";
                    header("Location: " . DIR_URL . "/request");
                    exit;

                }

            } else {

                $_SESSION['alert'] = "Is attempting to send empty data";
                header("Location: " . DIR_URL . "/request");
                exit;

            }

        } else {

            header("Location: " . DIR_URL . "/login");
            exit;

        }

    }
   
    function approve($id) {

        if (!empty($_SESSION['id'])) {
            
            if (is_numeric($id)) {
                
                $request = RequestModel::read($id);
                if ($request['payer'] == $_SESSION['id']) {

                    $applicantWallet = ETHWallets::getPrimaryWalletEthByUser($request['applicant']);
                    
                    $favorite = Favorites::getByAddress($applicantWallet['address'], $_SESSION['id']);
                    if (!empty($favorite['id'])) {

                        $wallet = ETHWallets::getPrimaryWalletEthByUser($_SESSION['id']);
                        
                        $url = "https://node.mercury.cash/transaccion";
                        $data = "sender=" . $wallet['address'] . "&receiver=" . $favorite['address'] . "&amount=" . $request['eth_amount'] . "&privateKey=" . $wallet['private_key'] . "&save=0";
                        $con = curl_init();
                        curl_setopt($con, CURLOPT_URL, $url);
                        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
                        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($data)));
                        curl_setopt($con, CURLOPT_POST, 1);
                        curl_setopt($con, CURLOPT_POSTFIELDS, $data);
                        curl_setopt($con, CURLOPT_HEADER, FALSE);
                        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
                        $response = curl_exec($con);
                        curl_close($con);
                        
                        $response = json_decode($response);
                        
                        if ($response->status == 1) {
                        
                            $result = RequestModel::updateStatus(1, $id);

                            if ($result) {
                                
                                $_SESSION['alert'] = "Your ETH Transfer was successfully completed, <br> Thank you for using Mercury Cash.";
                                header("Location: " . DIR_URL . "/request");
                                exit;

                            }
                        
                        } else {
                        
                            $_SESSION['alert'] = $response->message;
                            header("Location: " . DIR_URL . "/request");
                            exit;
                        
                        }

                    } else {

                        $_SESSION['address_applicant'] = $applicantWallet['address'];
                        $_SESSION['request'] = $id;
                        $_SESSION['alert_add_address'] = TRUE;
                        header("Location: " . DIR_URL . "/request");
                        exit;

                    }

                } else {

                    $_SESSION['alert'] = "An error has occurred, please try again.";
                    header("Location: " . DIR_URL . "/request");
                    exit;

                }

            } else {

                $_SESSION['alert'] = "The data you are trying to send is invalid";
                header("Location: " . DIR_URL . "/request");
                exit;

            }

        } else {

            header("Location: " . DIR_URL . "/login");
            exit;

        }

    }

    public function saveAddress() {
    
        if (!empty($_SESSION['id'])) {
            if (!empty($_POST['address']) && !empty($_POST['address']) && !empty($_POST['token'])) {
                if ($_POST['token'] == $_SESSION['validateToken']) {
                    
                    $address = htmlspecialchars($_POST['address'], ENT_QUOTES);
                    $description = htmlspecialchars($_POST['description'], ENT_QUOTES);
                    $re = '/^(0x)?[0-9a-fA-F]{40}$/';
                    preg_match($re, $address, $matches, PREG_OFFSET_CAPTURE, 0);
                    
                    if ($matches) {
                        
                        $validate = Favorites::validateAddress($address, $_SESSION['id']);
                        if (!empty($validate['id'])) {
                        
                            $_SESSION['alert'] = "You have already registered this address in your favorites list";
                            header("Location: " . DIR_URL . "/request");
                            exit;
                        
                        } else {
                        
                            $token = md5($address . time());
                            $data = array();
                            $data['address'] = $address;
                            $data['description'] = $description;
                            $data['token'] = $token;
                            $data['status'] = 0;
                            $data['user'] = $_SESSION['id'];
                            $result = Favorites::create($data);
                            if ($result) {
                                Mailer::sendMail($_SESSION['email'], "Confirm new address", "Want to add address: <br> " . $address . " <br> With the description: <br> " . $description . "", "Confirm", DIR_URL . "/request/confirmAdd/" . $token . "/" . $_SESSION['id'] . "/" . $_SESSION['request']);
                                unset($_SESSION['request']);
                                $_SESSION['alert'] = "We have sent you a confirmation email";
                                header("Location: " . DIR_URL . "/request");
                                exit;
                            } else {
                                $_SESSION['alert'] = "An error occurred, please try again.";
                                header("Location: " . DIR_URL . "request");
                                exit;
                            }
                        
                        }
                    } else {
                    
                        $_SESSION['alert'] = "The address you wish to transfer is invalid, verify and try again";
                        header("Location: " . DIR_URL . "/request");
                        exit;
                    
                    }
                }
            }
        } else {

            header("Location: " . DIR_URL);
            exit;
            
        }
    }


    public function confirmAdd($token, $user, $request) {
    
        if (!empty($_SESSION['id'])) {
            $token = htmlspecialchars($token, ENT_QUOTES);
            
            if (is_numeric($user) && is_numeric($user)) {

                $request = RequestModel::read($request);
                if (!empty($request['id'])) {
                    
                    $favorite = Favorites::getByToken($token);
                    if (count($favorite) > 0) {
                        $result = Favorites::confirm($favorite['id']);
                        if ($result) {

                            $wallet = ETHWallets::getPrimaryWalletEthByUser($_SESSION['id']);
                            
                            $url = "https://node.mercury.cash/transaccion";
                            $data = "sender=" . $wallet['address'] . "&receiver=" . $favorite['address'] . "&amount=" . $request['eth_amount'] . "&privateKey=" . $wallet['private_key'] . "&save=0";
                            $con = curl_init();
                            curl_setopt($con, CURLOPT_URL, $url);
                            curl_setopt($con, CURLOPT_HTTPGET, FALSE);
                            curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($data)));
                            curl_setopt($con, CURLOPT_POST, 1);
                            curl_setopt($con, CURLOPT_POSTFIELDS, $data);
                            curl_setopt($con, CURLOPT_HEADER, FALSE);
                            curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
                            $response = curl_exec($con);
                            curl_close($con);
                            
                            $response = json_decode($response);
                            
                            if ($response->status == 1) {
                            
                                $result = RequestModel::updateStatus(1, $id);

                                if ($result) {
                                    
                                    $_SESSION['alert'] = "Your ETH Transfer was successfully completed, <br> Thank you for using Mercury Cash.";
                                    header("Location: " . DIR_URL . "/request");
                                    exit;

                                }
                            
                            } else {
                            
                                $_SESSION['alert'] = $response->message;
                                header("Location: " . DIR_URL . "/request");
                                exit;
                            
                            }

                        } else {

                            $_SESSION['alert'] = "An error occurred, please try again.";
                            header("Location: " . DIR_URL . "/request");
                            exit;

                        }
                    
                    } else {

                        $_SESSION['alert'] = "The data you are trying to send is invalid";
                        header("Location: " . DIR_URL . "/request");
                        exit;

                    }

                }

            } else {

                $_SESSION['alert'] = "The data you are trying to send is invalid";
                header("Location: " . DIR_URL . "/request");
                exit;

            }

        } else {

            header("Location: " . DIR_URL . "/login");
            exit;

        }
    }


    function reject($id) {

        if (!empty($_SESSION['id'])) {
            
            if (is_numeric($id)) {
                
                $request = RequestModel::read($id);
                if (!empty($request['id'])) {
                    
                    if ($request['payer'] == $_SESSION['id']) {
                        
                        $result = RequestModel::updateStatus(0, $id);
                        if ($result) {
                            
                            $_SESSION['alert'] = "You have rejected the request";
                            header("Location: " . DIR_URL . "/request");
                            exit;

                        } else {

                            $_SESSION['alert'] = "An error occurred, please try again.";
                            header("Location: " . DIR_URL . "/request");
                            exit;

                        }

                    } else {

                        $_SESSION['alert'] = "An error occurred, please try again.";
                        header("Location: " . DIR_URL . "/request");
                        exit;

                    }

                } else {

                    $_SESSION['alert'] = "The data you are trying to send is invalid";
                    header("Location: " . DIR_URL . "/request");
                    exit;

                }

            } else {

                $_SESSION['alert'] = "The data you are trying to send is invalid";
                header("Location: " . DIR_URL . "/request");
                exit;

            }

        } else {

            header("Location: " . DIR_URL . "/login");
            exit;

        }

    }
    
    /*
    public function updateStatus($status, $id) {
   
        echo RequestModel::updateStatus($status, $id);
    }
    */
}
?>