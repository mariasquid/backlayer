<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Users,
	\App\Models\ETHWallets,
	\App\Models\Etherscan,
    \App\Models\Transactions,
    \App\Models\Favorites,
    \App\Models\Request,
    \App\Models\Transfers,
    \App\Models\TransferValidation,
    \App\Controllers\Mailer;

class Send {
    
    public function index() {
    
        if (!empty($_SESSION['id'])) {
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }
            /* HEADER */
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            $_SESSION['ethusd'] = $ethusd;
            View::set("ethusd", $ethusd);
            /* HEADER */

            $favorites = Favorites::getByUser($_SESSION['id']);

            $list_favorites = array();
            foreach ($favorites as $favorite) {
                $is_mercury = ETHWallets::getByAddress($favorite['address']);
                if (!empty($is_mercury['id'])) {
                    $user = Users::read($is_mercury['user']);

                    array_push($list_favorites, array(
                        "id" => $favorite['id'],
                        "description" => $favorite['description'],
                        "address" => $user['email'],
                        "is_mercury" => $favorite['is_mercury']
                    ));

                } else {
                    array_push($list_favorites, array(
                        "id" => $favorite['id'],
                        "description" => $favorite['description'],
                        "address" => $favorite['address'],
                        "is_mercury" => $favorite['is_mercury']
                    ));
                }

            }

            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }

            $ethwallets = ETHWallets::getByUser($_SESSION['id']);

            View::set("ethwallets", $ethwallets);
            View::set("favorites", $list_favorites);
            View::set("title", "Send");
            View::set("requestsUnread", $unread);
            View::render("send");
        } else {
            header("Location: " . DIR_URL . "/login");
        }
    }

    private static function validateNewFavoriteLocal($address) {

        if (!empty($address)) {
            
            $address = htmlspecialchars($address, ENT_QUOTES);
            $re = '/^(0x)?[0-9a-fA-F]{40}$/';
            preg_match($re, $address, $matches, PREG_OFFSET_CAPTURE, 0);

            if ($matches) {
                $ethwallet = ETHWallets::getByAddress($address);
                if (!empty($ethwallet['id'])) {
                    return 1;
                } else {
                    return 0;
                }

            } else {

                $re = '/[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}/im';
                preg_match($re, $address, $matches, PREG_OFFSET_CAPTURE, 0);

                if ($matches) {
                    $user = Users::readByEmail($address);
                    if ($user['id']) {
                        return 1;
                    } else {
                        return 0;
                    }

                } else {
                    return 3;
                }

            }

        } else {

            return 3;

        }

    }

    public function validateNewFavorite() {

        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json");

        $result = self::validateNewFavoriteLocal($_POST['address']);
        echo json_encode(array(
            "status" => $result
        ));

    }
    
    public function saveAddress() {
    
        if (!empty($_SESSION['id'])) {
            if (!empty($_POST['address']) && !empty($_POST['token'])) {
                if ($_POST['token'] == $_SESSION['validateToken']) {
                    
                    $address = htmlspecialchars($_POST['address'], ENT_QUOTES);
                    $description = htmlspecialchars($_POST['description'], ENT_QUOTES);
                    $reWallet = '/^(0x)?[0-9a-fA-F]{40}$/';
                    $reEmail = '/[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}/im';

                    $math = FALSE;
                    if (preg_match($reWallet, $address, $matches, PREG_OFFSET_CAPTURE, 0)) {
                        $math = TRUE;
                    } elseif (preg_match($reEmail, $address, $matches, PREG_OFFSET_CAPTURE, 0)) {
                        $math = TRUE;
                    }
                    
                    if ($math) {
                        
                        $validate = Favorites::validateAddress($address, $_SESSION['id']);
                        if (!empty($validate['id'])) {
                            $_SESSION['alert'] = "You have already registered this address in your favorites list";
                            header("Location: " . DIR_URL . "/send");
                            exit;
                        } else {
                            $token = md5($address . time());

                            $validate = self::validateNewFavoriteLocal($address);

                            $data = array();
                            $data['address'] = $address;
                            $data['description'] = $description;
                            $data['token'] = $token;
                            $data['status'] = 0;
                            $data['is_mercury'] = $validate;
                            $data['user'] = $_SESSION['id'];
                            $result = Favorites::create($data);

                            if ($result) {

                                $dataMail = array(
                                    "to" => $_SESSION['email'],
                                    "address" => $address,
                                    "description" => $description,
                                    "token" => $token
                                    );

                                Mailer::addFavorite($dataMail);

                                $_SESSION['alert'] = "We have sent you a confirmation email";
                                header("Location: " . DIR_URL . "/send");
                                exit;

                            } else {
                                $_SESSION['alert'] = "An error occurred, please try again.";
                                header("Location: " . DIR_URL . "/send");
                                exit;
                                
                            }
                        }

                    } else {
                        $_SESSION['alert'] = "The address you wish to transfer is invalid, verify and try again";
                        header("Location: " . DIR_URL . "/send");
                        exit;
                    }
                }
            }
        } else {

            header("Location: " . DIR_URL);
            exit;
            
        }
    }
    
    public function confirmAdd($token) {
    
        if (!empty($_SESSION['id'])) {
            $token = htmlspecialchars($token, ENT_QUOTES);
            
            $favorite = Favorites::getByToken($token);
            if (count($favorite) > 0) {
                $result = Favorites::confirm($favorite['id']);
                if ($result) {
                    header("Location: " . DIR_URL . "/send&1");
                }
            }
        }
    }
    
    public function calc_amount($amount, $currency) {
    
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        if (!empty($_SESSION['id'])) {
            if (is_numeric($amount) && is_numeric($currency)) {
                
                $price = Etherscan::getLastPrice();
                $ethusd = $price->ethusd;

                if ($currency == 1) {
                    
                    $subtotal = $amount * $ethusd;
                    $total = ($amount * $ethusd);
                    echo json_encode(array(
                        "status" => 1,
                        "usd_amount" => number_format($subtotal, 2),
                        "commission" => number_format((0.002 * $ethusd), 2),
                        "usd_total" => number_format($total, 2)
                        ));
                } elseif ($currency == 2) {
                    

                    $subtotal = $amount / $ethusd;
                    $total = $amount;
                    echo json_encode(array(
                        "status" => 1,
                        "eth_amount" => number_format($subtotal, 8),
                        "commission" => number_format((0.002 * $ethusd), 2),
                        "usd_total" => number_format($total, 2)
                        ));
                }
            }
        }
    }
    
    public function deleteFavorite($id) {
    
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        if (!empty($_SESSION['id'])) {
            
            if (is_numeric($id)) {
                
                $favorite = Favorites::read($id);
                if ($favorite['user'] == $_SESSION['id']) {
                    
                    $result = Favorites::delete($id);
                    if ($result) {
                        echo json_encode(array(
                            "status" => 1,
                            "description" => "The address " . $favorite['address'] . " has been removed from your list"
                            ));
                    } else {
                        echo json_encode(array(
                            "status" => 0,
                            "description" => "An error occurred, please try again."
                            ));
                    }
                } else {
                    echo json_encode(array(
                        "status" => 0,
                        "description" => "The address you are trying to remove does not belong to your favorites list"
                        ));
                }
            } else {
                echo json_encode(array(
                    "status" => 0,
                    "description" => "Invalid data"
                    ));
            }
        } else {
            echo json_encode(array(
                    "status" => 0,
                    "description" => "Error!"
                    ));
        }
    }
    
    public function wallet() {
    
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        if (!empty($_SESSION['id'])) {
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }
            if ($_SESSION['status'] == 1) {
                $wallets = ETHWallets::getByUser($_SESSION['id']);
                $privateKey = NULL;
                foreach ($wallets as $wallet) {
                    if ($wallet['primary'] == 1) {
                        $privateKey = $wallet['private_key'];
                    }
                }
                
                echo json_encode(array(
                    "privateKey" => $privateKey,
                    "address" => $_SESSION['address']
                    ));
            } else {
                header("Location: " . DIR_URL . "/settings/document");
            }
        } else {
            echo json_encode(array(
                "status" => 0,
                "description" => "Missing data required for this operation"
                ));
        }
    }

    public function createTransaction() {

        if (!empty($_SESSION['id']) && !empty($_POST['token']) && $_POST['token'] == $_SESSION['validateToken']) {
            
            if (!empty($_POST['from']) && is_numeric($_POST['from']) &&
                !empty($_POST['eth_amount']) && is_numeric($_POST['eth_amount'])) {

                $to = NULL;
                $type = NULL;
                if (!empty($_POST['to_favorites']) && is_numeric($_POST['to_favorites'])) {
                    $to = $_POST['to_favorites'];
                    $type = TRUE;
                } elseif (!empty($_POST['to_myaddress']) && is_numeric($_POST['to_myaddress'])) {
                    $to = $_POST['to_myaddress'];
                    $type = FALSE;
                    
                }

                if (!is_null($to)) {
                    $result = self::sendTransfer($_POST['from'], $to, $_POST['eth_amount'], $type);
                    $_SESSION['alert'] = $result->message;
                    header("Location: " . DIR_URL . "/send");
                    exit;

                } else {

                    $_SESSION['alert'] = "There are some invalid fields, please verify and try again.";
                    header("Location: " . DIR_URL . "/send");
                    exit;

                }

            } else {

                $_SESSION['alert'] = "There are some invalid fields, please verify and try again.";
                header("Location: " . DIR_URL . "/send");
                exit;

            }

        } else {

            header("Location: " . DIR_URL . "/login");
            exit;
            
        }

    }

    private static function sendTransfer($from, $to, $eth_amount, $type) {

        $endpoint = NULL;
        $data = NULL;
        if ($type === TRUE) {
            $endpoint = "https://apidev.mercury.cash/vitrina/addtransfer";
            $data = 'id_address=' . $from . '&id_favorite=' . $to . '&eth=' . $eth_amount;
        } elseif ($type === FALSE) {
            $endpoint = "https://apidev.mercury.cash/vitrina/addinternaltransfer";
            $data = 'from_address=' . $from . '&to_address=' . $to . '&eth=' . $eth_amount;
        }

        $url = $endpoint;
        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $_SESSION['jwt'], 'Content-Length: ' . strlen($data)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $data);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }
    
    public function _createTransaction() {
    
        if (!empty($_SESSION['id']) && !empty($_POST['token']) && $_POST['token'] == $_SESSION['validateToken']) {

            if (!empty($_POST['receiver']) && !empty($_POST['eth_amount']) && is_numeric($_POST['eth_amount'])) {

                $user = Users::read($_SESSION['id']);
                $transferValidation = TransferValidation::getByUser($_SESSION['id']);
                $balance = Etherscan::getBalance($_SESSION['address']);
                $balance = Etherscan::fromWei($balance);
                $price = Etherscan::getLastPrice();
                $ethusd = $price->ethusd;
                $receiver = htmlspecialchars($_POST['receiver'], ENT_QUOTES);
                $eth_amount = htmlspecialchars($_POST['eth_amount'], ENT_QUOTES);
                $usd_amount = number_format($ethusd * $ethusd, 2);
                $wallet = ETHWallets::getPrimaryWalletEthByUser($_SESSION['id']);

                $total = $eth_amount + 0.002;
                $totalUSD = $total * $price->ethusd;

                if ($user['status'] == 2 || $user['status'] == 3) {
                        
                    if (($totalUSD + $transferValidation['amount']) <= 1000) {
                        
                        if ($transferValidation['trial_count'] < 2) {

                            if ($total < $balance) {
                                
                                $data = array();
                                $data['trial_count'] = $transferValidation['trial_count'] + 1;
                                $data['amount'] = $transferValidation['amount'] + $totalUSD;
                                $data['user'] = $_SESSION['id'];
                                $data['id'] = $transferValidation['id'];
                                TransferValidation::update($data);

                                $this->makeTransfer($wallet['address'], $receiver, $eth_amount, $wallet['private_key']);

                            } else {

                                $_SESSION['alert'] = "You must have at least " . $total . " ETH to make this transfer";
                                header("Location: " . DIR_URL . "/send");
                                exit;

                            }

                        } else {

                            $_SESSION['alert'] = "<p>Your two transactions trial has expired, you need to validate your documents in order to continue using Mercury Cash services</p>";
                            header("Location: " . DIR_URL . "/send");
                            exit;

                        }

                    } else {

                        $_SESSION['alert'] = "<p>Your two transactions trial or one for less than $1000 USD has expired, you need to validate your documents in order to continue using Mercury Cash services</p><p>You can make a transaction of maximun $" . (1000 - $transferValidation['amount']) . " USD</p>";
                        header("Location: " . DIR_URL . "/send");
                        exit;

                    }

                } elseif ($user['status'] == 1) {
                    
                    $this->makeTransfer($wallet['address'], $receiver, $eth_amount, $wallet['private_key']);

                } else {

                    header("Location: " . DIR_URL);
                    exit;

                }

            } else {

                $_SESSION['alert'] = "You are sending empty data";
                header("Location: " . DIR_URL . "/send");
                exit;
            }

        } else {

            header("Location: " . DIR_URL);
            exit;

        }
    }

    private function makeTransfer($sender, $receiver, $eth_amount, $privateKey) {

        $price = Etherscan::getLastPrice();
        $ethusd = $price->ethusd;
        $usd_amount = $ethusd * $eth_amount;
        $wallet = ETHWallets::getPrimaryWalletEthByUser($_SESSION['id']);

        $url = "https://node.mercury.cash/transaccion";
        $data = "sender=" . $sender . "&receiver=" . $receiver . "&amount=" . $eth_amount . "&privateKey=" . $privateKey . "&save=0";
        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($data)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $data);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        if ($response->status == 1) {
            $data = array();
            $data['eth_amount'] = $eth_amount;
            $data['usd_amount'] = $usd_amount;
            $data['eth_rate'] = $ethusd;
            $data['address'] = $receiver;
            $data['hash'] = $response->hash;
            $data['type'] = "OUT";
            $data['date'] = time();
            $data['eth_wallet'] = $wallet['id'];
            $data['user'] = $_SESSION['id'];
            
            $result = Transfers::create($data);

            $dataMail = array(
                "to" => $_SESSION['email'],
                "address_to" => $receiver,
                "date" => date("m-d-Y H:i:s"),
                "eth_amount" => number_format($eth_amount, 5),
                "eth_rate" => number_format($ethusd, 2),
                "txhash" => $response->hash
                );

            Mailer::transferEth($dataMail);

            $_SESSION['alert'] = "Your ETH Transfer was successfully completed, <br> Thank you for using Mercury Cash.";
            header("Location: " . DIR_URL . "/send");
            exit;
        } else {

            $re = '/Error: Insufficient funds\. The account you tried to send transaction from does not have enough funds\. Required ([0-9]{1,50}) and got: ([0-9]{1,50})\./';
            preg_match_all($re, $response->message, $matches, PREG_SET_ORDER, 0);
            if (count($matches) > 0) {
                
                $patron = array();
                $patron[0] = "/[0-9]{1,45}/";
                $patron[1] = "/[0-9]{1,45}/";
                $change = array();
                $change[0] = Etherscan::fromWei($matches[0][1]);
                $change[1] = Etherscan::fromWei($matches[0][2]);

                $_SESSION['alert'] = preg_replace($patron, $change, $response->message);
                header("Location: " . DIR_URL . "/send");
                exit;

            } else {

                $_SESSION['alert'] = $response->message;
                header("Location: " . DIR_URL . "/send");
                exit;

            }

        }

    }
    
    public function walletSender($email) {
    
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        if (!empty($_SESSION['id'])) {
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }
            if ($_SESSION['status'] == 1) {
                $user = Users::readByEmail($email);
                $wallets = ETHWallets::getByUser($user['id']);
                $walletP = NULL;
                foreach ($wallets as $wallet) {
                    if ($wallet['principal'] == 1) {
                        $walletP = $wallet['address'];
                    }
                }
                echo json_encode(array(
                    "address" => $walletP
                    ));
            } else {
                header("Location: " . DIR_URL . "/settings/document");
            }
        } else {
            header("Location: " . DIR_URL . "/login");
        }
    }
}
?>