<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\ETHWallets,
    \App\Models\Etherscan,
    \App\Models\Purchases;

class Jobs {

    public function updateBalance() {

        $wallets = ETHWallets::getAll();
        foreach ($wallets as $wallet) {

        	$balance = Etherscan::getBalance($wallet['address']);

        	if ($balance > 0.01) {
	            echo "<pre>";
	            print_r($balance);
	            echo "</pre>";
	            echo "<hr>";
        	}
        }
    }

    public function updateIncome() {

        $wallets = ETHWallets::getAll();
        foreach ($wallets as $wallet) {

        	$balance = Etherscan::getBalance($wallet['address']);

        	if ($balance > 0) {
        		$url = "https://apidev.mercury.cash/vitrina/updateincome";
	            //$data = "id_wallet=" . $wallet['id'];
	            $con = curl_init();
	            curl_setopt($con, CURLOPT_URL, $url);
	            curl_setopt($con, CURLOPT_HTTPGET, TRUE);
	            curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
	            //curl_setopt($con, CURLOPT_POST, 1);
	            //curl_setopt($con, CURLOPT_POSTFIELDS, $data);
	            curl_setopt($con, CURLOPT_HEADER, FALSE);
	            curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
	            curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
	            $response = curl_exec($con);
	            curl_close($con);

	            echo "<pre>";
	            var_dump($response);
	            echo "</pre>";
	            exit;
        	}
        }
    }

    public function validatePurchaseCard() {
    
        $purchases = Purchases::getAll();

        foreach ($purchases as $buy) {
        	if (!is_null($buy['checkout_id']) && $buy['payment_method'] == 2 && $buy['status'] == 2) {
        		
        		$url = "https://test.oppwa.com" . $buy['checkout_id'];
				$url .= "?authentication.userId=8a8294185f116e86015f255ae6d02a94";
				$url .= "&authentication.password=2w5MEDz9tp";
				$url .= "&authentication.entityId=8a8294185f116e86015f255b241b2a99";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$responseData = curl_exec($ch);
				curl_close($ch);
				$response = json_decode($responseData);

				if (preg_match('/^(000\.000\.|000\.100\.1|000\.[36])/', $response->result->code) ||
                    preg_match('/^(000\.400\.0|000\.400\.100)/', $response->result->code)) {

					$data = array();
	                $data['checkout_id'] = $buy['checkout_id'];
					$data['eth_amount'] = $buy['eth_amount'];
					$data['usd_amount'] = $buy['usd_amount'];
					$data['commission'] = $buy['commission'];
					$data['eth_value'] = $buy['eth_value'];
					$data['payment_method'] = $buy['payment_method'];
					$data['date'] = $buy['date'];
					$data['status'] = 1;
					$data['eth_wallet'] = $buy['eth_wallet'];
					$data['user'] = $buy['user'];
					$data['id'] = $buy['id'];

					echo "<pre>";
					print_r(Purchases::update($data));
					echo "</pre>";

                } elseif (preg_match('/^(000\.400\.[1][0-9][1-9]|000\.400\.2)/', $response->result->code) ||
    				preg_match('/^(800\.[17]00|800\.800\.[123])/', $response->result->code) ||
        			preg_match('/^(800\.5|999\.|600\.1|800\.800\.8)/', $response->result->code) ||
            		preg_match('/^(100\.39[765])/', $response->result->code) ||
            		preg_match('/^(100\.400|100\.38|100\.370\.100|100\.370\.11)/', $response->result->code) ||
            		preg_match('/^(800\.400\.1)/', $response->result->code) ||
            		preg_match('/^(800\.400\.2|100\.380\.4|100\.390)/', $response->result->code) ||
            		preg_match('/^(100\.100\.701|800\.[32])/', $response->result->code) ||
            		preg_match('/^(800\.1[123456]0)/', $response->result->code) ||
            		preg_match('/^(600\.[23]|500\.[12]|800\.121)/', $response->result->code) ||
            		preg_match('/^(100\.[13]50)/', $response->result->code) ||
            		preg_match('/^(100\.250|100\.360)/', $response->result->code) ||
            		preg_match('/^(700\.[1345][05]0)/', $response->result->code) ||
            		preg_match('/^(200\.[123]|100\.[53][07]|800\.900|100\.[69]00\.500)/', $response->result->code) ||
            		preg_match('/^(100\.800)/', $response->result->code) ||
            		preg_match('/^(100\.[97]00)/', $response->result->code) ||
            		preg_match('/^(100\.100|100.2[01])/', $response->result->code) ||
            		preg_match('/^(100\.55)/', $response->result->code) ||
                	preg_match('/^(100\.380\.[23]|100\.380\.101)/', $response->result->code) ||
					preg_match('/^(000\.100\.2)/', $response->result->code)) {
                    
                    $data = array();
	                $data['checkout_id'] = $buy['checkout_id'];
					$data['eth_amount'] = $buy['eth_amount'];
					$data['usd_amount'] = $buy['usd_amount'];
					$data['commission'] = $buy['commission'];
					$data['eth_value'] = $buy['eth_value'];
					$data['payment_method'] = $buy['payment_method'];
					$data['date'] = $buy['date'];
					$data['status'] = 0;
					$data['eth_wallet'] = $buy['eth_wallet'];
					$data['user'] = $buy['user'];
					$data['id'] = $buy['id'];

					echo "<pre>";
					print_r(Purchases::update($data));
					echo "</pre>";

                }

        	}
        }
    
    }

}
?>