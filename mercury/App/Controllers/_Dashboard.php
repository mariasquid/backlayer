<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Users,
	\App\Models\Etherscan,
    \App\Models\ETHWallets,
    \App\Models\USDWallets,
    \App\Models\Transactions,
    \App\Models\Request,
    \App\Models\Purchases,
    \App\Models\Sales,
    \App\Models\Withdraw,
    \App\Models\Deposits;

class Dashboard {
   
    public function index() {
   
    	if (!empty($_SESSION['id'])) {
            
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }
            
            /* HEADER */
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            /* HEADER */

            $balance_eth = Etherscan::getBalance($_SESSION['address']);
            $usd_wallet = USDWallets::getByUser($_SESSION['id']);
            $transfers = Etherscan::getlistTransactions($_SESSION['address']);
            $transfers = array_reverse($transfers);
            $purchases = Purchases::getLastFive($_SESSION['id']);
            $purchases = json_encode($purchases);
            $purchases = json_decode($purchases);
            $sales = Sales::getLastFive($_SESSION['id']);
            $sales = json_encode($sales);
            $sales = json_decode($sales);
            $withdraw = Withdraw::getLastFive($_SESSION['id']);
            $withdraw = json_encode($withdraw);
            $withdraw = json_decode($withdraw);
            $deposits = Deposits::getLastFive($_SESSION['id']);
            $deposits = json_encode($deposits);
            $deposits = json_decode($deposits);
            
            $transactions = array();
            
            for ($i=0; $i < 5; $i++) {
            
                if (!empty($purchases[$i])) {
                    $purchases[$i]->type_transaction = 'Purchase';
                    array_push($transactions, $purchases[$i]);
                }
            
                if (!empty($sales[$i])) {
                    $sales[$i]->type_transaction = 'Sell';
                    array_push($transactions, $sales[$i]);
                }

                if (!empty($withdraw[$i])) {
                    $withdraw[$i]->type_transaction = 'Withdraw';
                    array_push($transactions, $withdraw[$i]);
                }

                if (!empty($deposits[$i])) {
                    $deposits[$i]->type_transaction = 'Deposits';
                    array_push($transactions, $deposits[$i]);
                }
            
                if (!empty($transfers[$i])) {
                    $transfers[$i]->value = Etherscan::fromWei($transfers[$i]->value);
                    $transfers[$i]->usd_amount = number_format($ethusd * $transfers[$i]->value, 2);
                    $transfers[$i]->type_transaction = 'Transfer';
                    $transfers[$i]->date = $transfers[$i]->timeStamp;
                    array_push($transactions, $transfers[$i]);
                }
            }
            
            $total = count($transactions);
            
            for ($i=1; $i < $total; $i++) {
                for ($j=0; $j < $total-$i; $j++) {
                    if ($transactions[$j]->date < $transactions[$j+1]->date) {
                        $k = $transactions[$j + 1];
                        $transactions[$j + 1] = $transactions[$j];
                        $transactions[$j] = $k;
                    }
                }
            }
            
            $t = array();
            
            for ($i=0; $i < 5; $i++) { 
            
                if (!empty($transactions[$i])) {
                    array_push($t, $transactions[$i]);
                }
            
            }
            
            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            
            for ($i=0; $i < count($requests); $i++) {
            
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            
            }
            
            View::set("balance_eth", Etherscan::fromWei($balance_eth));
            View::set("usd_wallet", $usd_wallet);
            View::set("transactions", $t);
            View::set("title", "Dashboard");
            View::set("requestsUnread", $unread);
            View::render("dashboard");
        } else {
            header("Location: " . DIR_URL . "/login");
        }
    }
}
?>