<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Users,
    \App\Models\Etherscan,
    \App\Models\ETHWallets,
    \App\Models\USDWallets,
    \App\Models\Commissions,
    \App\Models\USDWalletAdmin,
    \App\Models\Deposits,
    \App\Models\Request,
    \App\Models\Withdraw,
    \App\Models\Transfers,
    \App\Models\BankAccounts,
    \App\Models\Purchases,
    \App\Models\TransferValidation,
    \App\Models\Sales,
    \App\Models\Merchant,
    \App\Controllers\Mailer,
    \App\Controllers\Functions;

class Accounts {
    
    public function index() {
        if (!empty($_SESSION['id'])) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }
            
            /* HEADER */
            
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            
            /* HEADER */
            
            $ethwallets = ETHWallets::getByUser($_SESSION['id']);
            $usdwallet = USDWallets::read($_SESSION['id']);
            //$cantWallets = count($ethWallets);
            $commission = Commissions::read(1);
            /*
            for ($i=0; $i < $cantWallets; $i++) { 
                $ethWallets[$i]['balance'] = Etherscan::getBalance($ethWallets[$i]['address']);
                $ethWallets[$i]['balance'] = Etherscan::fromWei($ethWallets[$i]['balance']);
            }
            */
            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }
            View::set("commission", $commission);
            View::set("ethwallets", $ethwallets);
            View::set("usdwallet", $usdwallet);
            View::set("title", "Accounts");
            View::set("requestsUnread", $unread);
            View::render("accounts");
        } else {
            header("Location: " . DIR_URL . "/login");
            exit;
        }
    }
    
    public function changePrimary($id) {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        
        if (!empty($_SESSION['id'])) {
        
            if (is_numeric($id)) {
                
                $wallet = ETHWallets::getEthWallet($id, $_SESSION['id']);
                if (!empty($wallet)) {
                    $result = ETHWallets::changePrimary($id, $_SESSION['id']);
                    if ($result) {
                        $_SESSION['address'] = $wallet['address'];
                        
                        echo json_encode(array(
                            "status" => 1,
                            "description" => "Your primary address has changed"
                            ));
                    } else {
                        echo json_encode(array(
                            "status" => 0,
                            "description" => "An error has occurred, please try again"
                            ));
                    }
                } else {
                    echo json_encode(array(
                        "status" => 0,
                        "description" => "This address does not apply to your account"
                        ));
                }
            } else {
                echo json_encode(array(
                    "status" => 0,
                    "description" => "The data you are trying to pass is invalid"
                    ));
            }
        } else {
            
            echo json_encode(array(
                "status" => 0,
                "description" => "Missing data required for this operation"
                ));
        }
    }
    
    public function ethwallet($id) {
        if (!empty($_SESSION['id'])) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }
            
            /* HEADER */
            
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            
            /* HEADER */
            
            $wallet = ETHWallets::getBalance($id);
            $transactions = Transfers::getTransactionsByAddress($wallet['wallet_address']);

            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }

            View::set("wallet", $wallet);
            View::set("transactions", $transactions);
            View::set("title", "Accounts");
            View::set("requestsUnread", $unread);
            View::render("transactions_ethwallets");
        } else {
            header("Location: " . DIR_URL . "/login");
            exit;
            
        }
    }

    public function genPdfReport() {
    
        if (!empty($_SESSION['id'])) {
            if (!empty($_POST['from']) && !empty($_POST['eth_wallet'])) {
                $from = htmlspecialchars($_POST['from'], ENT_QUOTES);
                $to = (!empty($_POST['to'])) ? htmlspecialchars($_POST['to'], ENT_QUOTES) : date("Y-m-d");
                $eth_wallet = htmlspecialchars($_POST['eth_wallet'], ENT_QUOTES);

                $transactions = Transfers::getDates($from . " 00:00:00", $to . " 23:59:59", $eth_wallet);

                $user = Users::read($_SESSION['id']);
                $wallet = ETHWallets::getByAddress($eth_wallet);

                require_once LIBSPATH . "/mpdf/mpdf.php";
                $html = file_get_contents("public/assets/mpdf/archivo.html");
                $css = file_get_contents("public/assets/mpdf/archivo.css");
                $trs = '';

                foreach ($transactions as $transaction) {
                    $type = ($transaction['from_address'] == $eth_wallet) ? "OUT" : "IN";
                    $to_address = ($transaction['from_address'] == $eth_wallet) ? $transaction['to_address'] : $transaction['from_address'];
                    $trs .= '<tr><td>' . $type . '</td><td>' . $transaction['start_date'] . '</td><td>' . $to_address . '</td><td>' . $transaction['value'] . ' ETH</td></tr>';
                }

                $table = '<table><thead><tr><th>TYPE</th><th>DATE</th><th>ADDRESS</th><th>ETH QTY</th><th></th></thead><tbody>' . $trs . '</tbody></table>';
                $html = str_replace("{{USER}}", $_SESSION['name'], $html);
                $html = str_replace("{{EMAIL}}", $_SESSION['email'], $html);
                $html = str_replace("{{ADDRESS}}", $user['address'], $html);
                $html = str_replace("{{DATE}}", $from . " - ". $to, $html);
                $html = str_replace("{{ID}}", "<br>" . $wallet['address'], $html);
                $html = str_replace("{{CURRENCY}}", "ETH", $html);
                $html = str_replace("{{BALANCE}}", ETHWallets::getBalance($wallet['id'])['balance'] . " ETH", $html);
                $html = str_replace("{{TABLE}}", $table, $html);
                $mpdf = new \mPDF('c', 'A4');
                $mpdf->writeHTML($css, 1);
                $mpdf->writeHTML($html);
                $mpdf->Output('file.pdf', 'I');
            }
        }
            
    }


    public function _genPdfReport() {
        if (!empty($_SESSION['id'])) {
            if (!empty($_POST['from']) && !empty($_POST['to']) && !empty($_POST['eth_wallet'])) {
                $from = htmlspecialchars($_POST['from'], ENT_QUOTES);
                $to = htmlspecialchars($_POST['to'], ENT_QUOTES);
                $eth_wallet = htmlspecialchars($_POST['eth_wallet'], ENT_QUOTES);
                $from = $from . " 23:59:59";
                $to = $to . " 23:59:59";
                $from = strtotime($_POST['from']);
                $to = strtotime($_POST['to']);
                if (is_numeric($from) && is_numeric($to) && is_numeric($eth_wallet)) {
                    $fromText = $_POST['from'];
                    $toText = $_POST['to'];
                    $transfers = Transfers::getDates($from, $to, $eth_wallet);
                    $transfers = array_reverse($transfers);
                    
                    $user = Users::read($_SESSION['id']);
                    $wallet = ETHWallets::read($eth_wallet);
            
                    require_once LIBSPATH . "/mpdf/mpdf.php";
                    $html = file_get_contents("public/assets/mpdf/archivo.html");
                    $css = file_get_contents("public/assets/mpdf/archivo.css");
                    $trs = '';
                    foreach ($transfers as $transaction) {
                        
                        $trs .= '<tr><td>' . $transaction['type'] . '</td><td>' . date("m/d/Y H:i:s", $transaction['date']) . '</td><td>' . $transaction['address'] . '</td><td>' . number_format($transaction['eth_amount'], 5) . ' ETH</td></tr>';
                    }
                    $table = '<table><thead><tr><th>TYPE</th><th>DATE</th><th>ADDRESS</th><th>ETH QTY</th><th></th></thead><tbody>' . $trs . '</tbody></table>';
                    $html = str_replace("{{USER}}", $_SESSION['name'], $html);
                    $html = str_replace("{{EMAIL}}", $_SESSION['email'], $html);
                    $html = str_replace("{{ADDRESS}}", $user['address'], $html);
                    $html = str_replace("{{DATE}}", $fromText . " - ". $toText, $html);
                    $html = str_replace("{{ID}}", "<br>" . $wallet['address'], $html);
                    $html = str_replace("{{CURRENCY}}", "ETH", $html);
                    $html = str_replace("{{BALANCE}}", Etherscan::fromWei(Etherscan::getBalance($wallet['address'])) . " ETH", $html);
                    $html = str_replace("{{TABLE}}", $table, $html);
                    $mpdf = new \mPDF('c', 'A4');
                    $mpdf->writeHTML($css, 1);
                    $mpdf->writeHTML($html);
                    $mpdf->Output('file.pdf', 'I');
                }
            }
        }
    }
    
    public function transactions($wallet, $type) {
        if (!empty($_SESSION['id'])) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }
            
            /* HEADER */
            
            /* Modificado por Maria, para que tome el valor desde la api,
             * Que sea el mismo valor que se guard� en la variable de sesi�n
             */
            $ethusd = $_SESSION['ethusd'];
            View::set("ethusd", $ethusd);
            
            /* HEADER */
            
            $wallet = ETHWallets::read($wallet);
            $etherscan = Etherscan::getlistTransactions($wallet['address']);
            $transactions = Transactions::getByWallet($wallet['id'], $type);
            foreach ($transactions as $transaction) {
                $transaction['eth_amount'] = Etherscan::fromWei($transaction['eth_amount']);
            }
            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }
            View::set("transactions", $transactions);
            View::set("type", $type);
            View::set("title", "Accounts");
            View::set("requestsUnread", $unread);
            View::render("transactions");
        } else {
            header("Location: " . DIR_URL . "/login");
            exit;
        }
    }
    /* DEPOSITAR */
    
    public function deposit() {
        if (!empty($_SESSION['id'])) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }
            
            /* HEADER */
            
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            
            /* HEADER */
            
            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }

            View::set("title", "Deposit USD");
            View::set("requestsUnread", $unread);
            View::render("deposit_dollars");
        } else {
            header("Location: " . DIR_URL . "/login");
            exit;
        }
    }
    
    public function processDeposit() {

        if (!empty($_SESSION['id'])) {
        
            if (!empty($_POST['amount'])) {

                if (is_numeric($_POST['amount'])) {
                    $amount = $_POST['amount'];
                    $commission = Commissions::read(1);
                    $commission = ($amount * $commission['commission']) / 100;
                    $amount = $amount + $commission;
                    $user = Users::read($_SESSION['id']);
                    $usdWallet = USDWallets::getByUser($_SESSION['id']);
                    require_once LIBSPATH . "/stripe/init.php";
                    $token = $_POST['stripeToken'];
                    $customerId = NULL;
                    
                    if (is_null($user['stripe_id'])) {
                        $customer = \Stripe\Customer::create(array(
                            'email' => $_SESSION['email'],
                            'card' => $token
                            ));

                        Users::addStripeId($customer->id, $_SESSION['id']);
                        $customerId = $customer->id;

                    } else {

                        $customerId = $user['stripe_id'];

                    }
                    
                    $amountStripe = number_format($amount * 100, 0);
                    $charge = \Stripe\Charge::create(array(
                        'customer' => $customerId,
                        'amount' => $amountStripe,
                        'currency' => 'usd'
                        ));
                    
                    $data = array();
                    $data['transaction_id_stripe'] = $charge->id;
                    $data['usd_amount'] = number_format($amount, 2);
                    $data['eth_amount'] = 0;
                    $data['type_wallet'] = 2;
                    $data['type_transaction'] = 3;
                    $data['description'] = "Deposit $" . number_format($amount, 2) . " in USD Wallet";
                    $data['date'] = time();
                    $data['wallet'] = $usdWallet['id'];
                    $data['status'] = 2;
                    $result = Transactions::create($data);
                    
                    if ($result) {
                        $adminBalance = USDWalletAdmin::read(1);
                        $data = array();
                        $data['balance'] = $adminBalance['balance'] + number_format($commission, 2);
                        $data['id'] = 1;
                        USDWalletAdmin::update($data);
                        Functions::usd_alert();
                        header("Location: " . DIR_URL . "/accounts/processDepositResponse/1");
                        exit;
                    }

                } else {
                    header("Location: " . DIR_URL . "/accounts/processDepositResponse/2");
                    exit;
                
                }

            } else {
                header("Location: " . DIR_URL . "/accounts/processDepositResponse/3");
                exit;
            
            }

        } else {
            header("Location: " . DIR_URL . "/login");
            exit;
        
        }

    }

    function processDepositResponse($status) {
        if (!empty($_SESSION['id'])) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }
            
            /* HEADER */
            
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            
            /* HEADER */
            
            $message = NULL;
            $alert = NULL;
            if (isset($status)) {
                switch ($status) {
                    case '1':
                        $message = 'Is checking if I deposit, will soon receive an email with the status of your transaction';
                        $alert = 'info';
                        break;
                    case '2':
                        $message = 'The amount must be a numeric data <b><a href="' . DIR_URL . '/accounts/deposit" style="color: #fff;font-weight: bold;text-decoration: underline;">go back</a></b>';
                        $alert = 'danger';
                        break;
                    case '3':
                        $message = 'You must indicate the amount you want to deposit <b><a href="' . DIR_URL . '/accounts/deposit" style="color: #fff;font-weight: bold;text-decoration: underline;">go back</a></b>';
                        $alert = 'danger';
                        break;
                }
            }
            View::set("message", $message);
            View::set("alert", $alert);
            View::set("title", "Deposit USD");
            View::render("process_deposit_response");
        } else {
            header("Location: " . DIR_URL . "/login");
            exit;
        }
    }
    
    public function changeAlias() {
        if (!empty($_SESSION['id'])) {
        
            $alias = htmlspecialchars($_POST['alias'], ENT_QUOTES);
            if (is_numeric($_POST['id'])) {
                $id = $_POST['id'];
                $result = ETHWallets::changeAlias($alias, $id);
                if ($result) {
                    echo json_encode(array(
                        "status" => 1
                        ));
                }
            }
        }
    }
    
    public function calculations($amount, $method, $wire_method = '0') {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        
        if (!empty($_SESSION['id'])) {
        
            if (is_numeric($amount) && is_numeric($method)) {
                $modal = FALSE;
                if (empty($amount)) {
                    $amount = 0;
                    $modal = TRUE;
                }
                
                $user = Users::read($_SESSION['id']);
                $commissions = Commissions::read(1);
                if ($method == '1') {
                    
                    $limit = $limits['buy_eth_usdwallet'] - $user['limit_buy_eth_usd'];
                    $fee = ($commissions['buy_usdwallet'] * $amount) / 100;
                    $total = $amount + $fee;
                    $ethers = $amount / $_SESSION['price_ether'];
                    $percentage = ($user['limit_buy_eth_usd'] * 100) / $limits['buy_eth_usdwallet'];
                    if (!empty($amount)) {
                        $percentage = $percentage + ($amount * 100) / $limits['buy_eth_usdwallet'];
                    }
                    
                    echo json_encode(array(
                        "status" => 1,
                        "limit" => $limit,
                        "subtotal" => number_format($amount, 2),
                        "fee" => number_format($fee, 2),
                        "total" => $total,
                        "ethers" => $ethers,
                        "percentage" => $percentage,
                        "description" => $description,
                        "modal" => $modal
                        ));
                } elseif ($method == '2') {
                    
                    $fee = ($commissions['deposit_card'] * $amount) / 100;
                    $total = $amount + $fee;
                    
                    echo json_encode(array(
                        "subtotal" => number_format($amount, 2),
                        "fee" => number_format($fee, 2),
                        "total" => number_format($total, 2),
                        "modal" => $modal
                        ));

                } elseif ($method == '3') {
                    
                    $fee = ($commissions['deposit_ach'] * $amount) / 100;
                    $total = $amount + $fee;
                    
                    echo json_encode(array(
                        "subtotal" => number_format($amount, 2),
                        "fee" => number_format($fee, 2),
                        "total" => number_format($total, 2)
                        ));
                }  elseif ($method == '4') {
                    $commission = $commissions['deposit_wire_transfer_domestic'];
                    if (is_numeric($wire_method) && $wire_method != '0') {
                        if ($wire_method == '2') {
                            $commission = $commissions['deposit_wire_transfer_international'];
                        }
                    }
                    
                    $total = $amount - $commission;
                    
                    echo json_encode(array(
                        "subtotal" => number_format($amount, 2),
                        "fee" => number_format($commission, 2),
                        "total" => number_format($total, 2),
                        "modal" => ($total < 1) ? TRUE : FALSE
                        ));
                }
            } else {
                echo json_encode(array(
                    "status" => 0,
                    "description" => "You must enter numeric data"
                    ));
            }
        } else {
            echo json_encode(array(
                "status" => 0,
                "description" => "You must enter numeric data"
                ));
        }
    }

    public function depositProcess() {

        if (!empty($_SESSION['id'])) {
            if (!empty($_POST['usd_amount']) && is_numeric($_POST['usd_amount']) && !empty($_POST['payment_method']) && is_numeric($_POST['payment_method'])) {
                $usd_amount = $_POST['usd_amount'];
                $payment_method = $_POST['payment_method'];
                $commissions = Commissions::read(1);

                if ($payment_method == 2) {
                    
                    $commission = ($usd_amount * $commissions['deposit_card']) / 100;
                    $total = number_format($usd_amount + $commission, 2);

                    $_SESSION['usd_amount'] = $usd_amount;
                    $_SESSION['commission'] = $commission;

                    $merchant = Merchant::getByUser($_SESSION['id']);
                    $count = 0;
                    $registrations = "";
                    foreach ($merchant as $id) {
                        $registrations = $registrations . "&registrations[" . $count . "].id=" . $id['id_merchant'];
                        $count++;
                    }

                    $url = "https://test.oppwa.com/v1/checkouts";
                    $data = "authentication.userId=8a8294185f116e86015f255ae6d02a94" .
                            "&authentication.password=2w5MEDz9tp" .
                            "&authentication.entityId=8a8294185f116e86015f255b241b2a99" .
                            "&amount=" . $total .
                            "&currency=USD" .
                            "&paymentType=DB" .
                            $registrations .
                            "&createRegistration=true";

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $responseData = curl_exec($ch);
                    $response = json_decode($responseData);

                    header("Location: " . DIR_URL . "/accounts/deposit?checkout=" . $response->id);

                } elseif ($payment_method == 4) {

                    $wire = $_POST['wire'];
                    
                    $type_wire = NULL;
                    if ($wire == 1) {
                        $commission = $commissions['deposit_wire_transfer_domestic'];
                        $type_wire = "Domestic";
                    } elseif($wire == 2) {
                        $commission = $commissions['deposit_wire_transfer_international'];
                        $type_wire = "International";
                    }

                    $total = $usd_amount;
                    $usd_amount = $usd_amount - $commission;
                    
                    $an = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    $su = strlen($an) - 1;
                    $wire_code = substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1);
                    $data = array();
                    $data['stripe_id'] = NULL;
                    $data['wire_code'] = $wire_code;
                    $data['usd_amount'] = $usd_amount;
                    $data['commission'] = $commission;
                    $data['method'] = 4;
                    $data['date'] = time();
                    $data['status'] = 2;
                    $data['user'] = $_SESSION['id'];

                    $result = Deposits::create($data);
                    
                    if ($result) {
                        $alert = '<p><b>Bank Name:</b> BB&T</p>';
                        $alert .= '<p><b>Bank Address:</b> 2051 Town Center Blvd, Orlando, FL 32837</p>';
                        $alert .= '<p><b>Business Name:</b> Mercury Cash</p>';
                        $alert .= '<p><b>Bank Account:</b> 0000245857667</p>';
                        $alert .= '<p><b>Routing Number:</b> 263191387</p>';
                        $alert .= '<p><b>Address:</b> 6427 Milner Blvd #4, Orlando FL 32809</p>';
                        $_SESSION['alert_success'] = "<p>We have received your wire transfer deposit request, please be advised of the following:</p><p>Reference Code: <b>" . $wire_code . "</b></p><p>1) Use the reference code that you find above in your transfer in order to better identify your transaction, if you do not use the code you will be charged with a $" . $commission . " penalty.</p>";
                        $_SESSION['alert_success'] .= $alert;
                
                        $dataMail = array(
                            "to" => $_SESSION['email'],
                            "wire_code" => $wire_code,
                            "payment_method" => "Wire Transfer",
                            "date" => date("Y-m-d", time()),
                            "subtotal" => number_format($usd_amount, 2),
                            "fee" => number_format($commission, 2),
                            "total" => number_format($total, 2)
                            );

                        Mailer::depositUser($dataMail);

                        $user = Users::read($_SESSION['id']);
                        $message = "<p>New Deposit</p>";
                        $message .= "<p>" . $user['name'] . " " . $user['last_name'] . "</p>";
                        $message .= "<p>" . $user['email'] . "</p>";
                        $message .= "<p>Amount: $" . number_format($usd_amount, 2) . " USD</p>";
                        $message .= "<p>Wire Code: <b>" . $wire_code . "</b></p>";
                        Mailer::sendMail("deposits@mercury.cash", "Mercury Cash - Deposit", $message, 'Go to Mercury Cash', DIR_URL . "/administrator/dashboard");

                        header("Location: " . DIR_URL . "/accounts/deposit");
                        exit;
                    
                    } else {
                        $_SESSION['alert'] = "An error has occurred, please try again.";
                        header("Location: " . DIR_URL . "/accounts/deposit");
                        exit;
                    }

                }

            } else {

                $_SESSION['alert'] = "datos vacios";
                header("Location: " . DIR_URL . "/accounts/deposit");
                exit;

            }

        } else {
            header("Location: " . DIR_URL . "/login");
            exit;
        }

    }

    public function processDepositCard() {
    
        if (!empty($_SESSION['id'])) {
            if (isset($_GET['id']) && isset($_GET['resourcePath'])) {

                $url = "https://test.oppwa.com" . $_GET['resourcePath'];
                $url .= "?authentication.userId=8a8294185f116e86015f255ae6d02a94";
                $url .= "&authentication.password=2w5MEDz9tp";
                $url .= "&authentication.entityId=8a8294185f116e86015f255b241b2a99";

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $responseData = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($responseData);

                if(preg_match('/^(000\.000\.|000\.100\.1|000\.[36])/', $response->result->code) ||
                    preg_match('/^(000\.400\.0|000\.400\.100)/', $response->result->code) ||
                    preg_match('/^(000\.200)/', $response->result->code)) {
                    
                    $status = 2;
                    if (preg_match('/^(000\.000\.|000\.100\.1|000\.[36])/', $response->result->code) ||
                        preg_match('/^(000\.400\.0|000\.400\.100)/', $response->result->code)) {
                        $status = 1;
                    } elseif (preg_match('/^(000\.200)/', $response->result->code)) {
                        $status = 2;
                    }
                    
                    $data = array();
                    $data['checkout_id'] = $_GET['resourcePath'];
                    $data['wire_code'] = NULL;
                    $data['usd_amount'] = $_SESSION['usd_amount'];
                    $data['commission'] = $_SESSION['commission'];
                    $data['method'] = 2;
                    $data['date'] = time();
                    $data['status'] = $status;
                    $data['user'] = $_SESSION['id'];
                    $data['id_merchant'] = $response->id;

                    $result = Deposits::create($data);
                    Merchant::create($data);
                    
                    if ($result) {
                        $_SESSION['alert_success'] = $response->result->description;
                
                        $dataMail = array(
                            "to" => $_SESSION['email'],
                            "wire_code" => "N/A",
                            "payment_method" => "Credit Card",
                            "date" => date("Y-m-d", time()),
                            "subtotal" => number_format($_SESSION['usd_amount'], 2),
                            "fee" => number_format($_SESSION['commission'], 2),
                            "total" => number_format(($_SESSION['usd_amount'] + $_SESSION['commission']), 2)
                            );

                        Mailer::depositUser($dataMail);

                        $user = Users::read($_SESSION['id']);
                        $message = "<p>New Deposit</p>";
                        $message .= "<p>" . $user['name'] . " " . $user['last_name'] . "</p>";
                        $message .= "<p>" . $user['email'] . "</p>";
                        $message .= "<p>Amount: $" . number_format($_SESSION['usd_amount'], 2) . " USD</p>";
                        $message .= "<p>Payment Method: <b>Credit Card</b></p>";
                        Mailer::sendMail("deposits@mercury.cash", "Mercury Cash - Deposit", $message, 'Go to Mercury Cash', DIR_URL . "/administrator/dashboard");

                        header("Location: " . DIR_URL . "/accounts/deposit");
                        exit;
                    
                    } else {
                        $_SESSION['alert'] = "An error has occurred, please try again.";
                        header("Location: " . DIR_URL . "/accounts/deposit");
                        exit;
                    }

                } else {

                    $_SESSION['alert'] = $response->result->description;
                    header("Location: " . DIR_URL . "/accounts/deposit");
                    exit;

                }

            }

        } else {
            header("Location: " . DIR_URL . "/login");
            exit;
        }
    
    }
    
    public function _depositProcess() {
        
        if (!empty($_SESSION['id'])) {
        
            if ($_SESSION['status'] == 1) {
                
                if (is_numeric($_POST['usd_amount']) && is_numeric($_POST['wire']) && is_numeric($_POST['payment_method'])) {
                    
                    $usd_amount = $_POST['usd_amount'];
                    $payment_method = $_POST['payment_method'];
                    $wire = $_POST['wire'];
                    $commission = 0;
                    if ($usd_amount >= 1) {
                        $commissions = Commissions::read(1);
                        $user = Users::read($_SESSION['id']);
                        
                        if ($payment_method == '2') {
                            
                            $commission = ($usd_amount * $commissions['deposit_card']) / 100;
                            $total = number_format($usd_amount + $commission, 2);

                            $_SESSION['usd_amount'] = $usd_amount;
                            $_SESSION['commission'] = $commission;

                            $merchant = Merchant::getByUser($_SESSION['id']);
                            $count = 0;
                            $registrations = "";
                            foreach ($merchant as $id) {
                                $registrations = $registrations . "&registrations[" . $count . "].id=" . $id['id_merchant'];
                                $count++;
                            }

                            $url = "https://test.oppwa.com/v1/checkouts";
                            $data = "authentication.userId=8a8294185f116e86015f255ae6d02a94" .
                                    "&authentication.password=2w5MEDz9tp" .
                                    "&authentication.entityId=8a8294185f116e86015f255b241b2a99" .
                                    "&amount=" . $total .
                                    "&currency=USD" .
                                    "&paymentType=DB" .
                                    $registrations .
                                    "&createRegistration=true";

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $responseData = curl_exec($ch);
                            $response = json_decode($responseData);

                            header("Location: " . DIR_URL . "/accounts/deposit?checkout=" . $response->id);

                        } elseif ($payment_method == '3') {
                            $var = 1;
                        } elseif ($payment_method == '4') {
                            
                            $type_wire = NULL;
                            if ($wire == 1) {
                                $commission = $commissions['deposit_wire_transfer_domestic'];
                                $type_wire = "Domestic";
                            } elseif($wire == 2) {
                                $commission = $commissions['deposit_wire_transfer_international'];
                                $type_wire = "International";
                            }

                            $total = $usd_amount;
                            $usd_amount = $usd_amount - $commission;
                            
                            $an = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                            $su = strlen($an) - 1;
                            $wire_code = substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1) .  substr($an, rand(0, $su), 1);
                            $data = array();
                            $data['stripe_id'] = NULL;
                            $data['wire_code'] = $wire_code;
                            $data['usd_amount'] = $usd_amount;
                            $data['commission'] = $commission;
                            $data['method'] = 4;
                            $data['date'] = time();
                            $data['status'] = 2;
                            $data['user'] = $_SESSION['id'];

                            $result = Deposits::create($data);
                            
                            if ($result) {
                                $alert = '<p><b>Bank Name:</b> BB&T</p>';
                                $alert .= '<p><b>Bank Address:</b> 2051 Town Center Blvd, Orlando, FL 32837</p>';
                                $alert .= '<p><b>Business Name:</b> Mercury Cash</p>';
                                $alert .= '<p><b>Bank Account:</b> 0000245857667</p>';
                                $alert .= '<p><b>Routing Number:</b> 263191387</p>';
                                $alert .= '<p><b>Address:</b> 6427 Milner Blvd #4, Orlando FL 32809</p>';
                                $_SESSION['alert_success'] = "<p>We have received your wire transfer deposit request, please be advised of the following:</p><p>Reference Code: <b>" . $wire_code . "</b></p><p>1) Use the reference code that you find above in your transfer in order to better identify your transaction, if you do not use the code you will be charged with a $" . $commission . " penalty.</p>";
                                $_SESSION['alert_success'] .= $alert;
                        
                                $dataMail = array(
                                    "to" => $_SESSION['email'],
                                    "wire_code" => $wire_code,
                                    "payment_method" => "Wire Transfer",
                                    "date" => date("Y-m-d", time()),
                                    "subtotal" => number_format($usd_amount, 2),
                                    "fee" => number_format($commission, 2),
                                    "total" => number_format($total, 2)
                                    );

                                Mailer::depositUser($dataMail);

                                $user = Users::read($_SESSION['id']);
                                $message = "<p>New Deposit</p>";
                                $message .= "<p>" . $user['name'] . " " . $user['last_name'] . "</p>";
                                $message .= "<p>" . $user['email'] . "</p>";
                                $message .= "<p>Amount: $" . number_format($usd_amount, 2) . " USD</p>";
                                $message .= "<p>Wire Code: <b>" . $wire_code . "</b></p>";
                                Mailer::sendMail("deposits@mercury.cash", "Mercury Cash - Deposit", $message, 'Go to Mercury Cash', DIR_URL . "/administrator/dashboard");

                                header("Location: " . DIR_URL . "/accounts/deposit");
                                exit;
                            
                            } else {
                                $_SESSION['alert'] = "An error has occurred, please try again.";
                                header("Location: " . DIR_URL . "/accounts/deposit");
                                exit;
                            }
                        }
                    } else {
                        $_SESSION['alert'] = "The minimum amount to carry out this transaction is $1";
                        header("Location: " . DIR_URL . "/accounts/deposit");
                        exit;
                    }
                } else {
                    $_SESSION['alert'] = "The data is invalid";
                    header("Location: " . DIR_URL . "/accounts/deposit");
                    exit;
                }
            } else {
                $_SESSION['alert'] = "You need to validate your email or your identity document";
                header("Location: " . DIR_URL . "/accounts/deposit");
                exit;
            }
        } else {
            header("Location: " . DIR_URL . "/login");
            exit;
        }
    }
    public function withdraw() {
        if (!empty($_SESSION['id'])) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }
            
            /* HEADER */
            
            $price = Etherscan::getLastPrice();
            $balance = Etherscan::getBalance($_SESSION['address']);
            $balance = Etherscan::fromWei($balance);
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            
            /* HEADER */
            $accounts = BankAccounts::getBankAccountByUser($_SESSION['id']);
            $usdwallet = USDWallets::read($_SESSION['id']);
            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }            
            
            View::set("balance", $balance);
            View::set("usdwallet", $usdwallet);
            View::set("accounts", $accounts);
            View::set("title", "Withdraw");
            View::set("requestsUnread", $unread);
            View::render("withdraw");
        } else {
            header("Location: " . DIR_URL . "/login");
            exit;
        }
    }
    public function newRequestsWithdraw() {
        
        if (!empty($_SESSION['id'])) {
        
            if (!empty($_POST['usd_amount']) && !empty($_POST['bank_account'])) {
        
                if (is_numeric($_POST['usd_amount']) && is_numeric($_POST['bank_account'])) {
        
                    if (!empty($_POST['usd_amount']) && is_numeric($_POST['usd_amount'])) {
        
                        $transferValidation = TransferValidation::getByUser($_SESSION['id']);

                        $user = Users::read($_SESSION['id']);

                        if (!is_null($user['last_name']) &&
                            !is_null($user['ndocument']) &&
                            $user['country_id'] > 0 &&
                            $user['province_id'] > 0 &&
                            !is_null($user['city']) &&
                            !is_null($user['zip_code']) &&
                            !is_null($user['address'])) {
                            
                            if ($_SESSION['status'] == 2 || $_SESSION['status'] == 3) {
                    
                                if (($_POST['usd_amount'] + $transferValidation['amount']) <= 1000) {
                                    
                                    if ($transferValidation['trial_count'] < 2) {
                                        
                                        if ($_POST['usd_amount'] >= 1) {
                                            
                                            $data = array();
                                            $data['trial_count'] = $transferValidation['trial_count'] + 1;
                                            $data['amount'] = $transferValidation['amount'] + $_POST['usd_amount'];
                                            $data['user'] = $_SESSION['id'];
                                            $data['id'] = $transferValidation['id'];
                                            $tr = TransferValidation::update($data);
                                            $this->makeRequestWithdraw($_POST['bank_account'], $_POST['usd_amount']);
            
                                        } else {
                                            $_SESSION['alert'] = "Purchase must be greater than $1 USD";
                                            header("Location: " . DIR_URL . "/accounts/withdraw");
                                            exit;
            
                                        }
            
                                    } else {
                                        $_SESSION['alert'] = "<p>Your two transactions trial has expired, you need to validate your documents in order to continue using Mercury Cash services</p>";
                                        header("Location: " . DIR_URL . "/accounts/withdraw");
                                        exit;
                                    }
            
                                } else {
                                    $_SESSION['alert'] = "<p>Your two transactions trial or one for less than $1000 USD has expired, you need to validate your documents in order to continue using Mercury Cash services</p><p>You can make a transaction of maximun $" . (1000 - $transferValidation['amount']) . " USD</p>";
                                    header("Location: " . DIR_URL . "/accounts/withdraw");
                                    exit;
            
                                }
            
                            } elseif ($_SESSION['status'] == 1) {
                                
                                $this->makeRequestWithdraw($_POST['bank_account'], $_POST['usd_amount']);
            
                            } else {
            
                                header("Location: " . DIR_URL);
                                exit;
            
                            }

                        } else {

                            $_SESSION['alert'] = "You need to complete TIER 1 in order to continue. <a href='" . DIR_URL . "/settings'>Update your profile</a> and submit your TIER 1 data.";
                            header("Location: " . DIR_URL . "/accounts/withdraw");
                            exit;

                        }
        
                    } else {
        
                        $_SESSION['alert'] = "The data you are trying to send is invalid";
                        header("Location: " . DIR_URL . "/accounts/withdraw");
                        exit;
        
                    }
        
                } else {
        
                    $_SESSION['alert'] = "The data you are trying to send is invalid";
                    header("Location: " . DIR_URL . "/accounts/withdraw");
                    exit;
        
                }
        
            } else {
        
                $_SESSION['alert'] = "Is attempting to send empty data";
                header("Location: " . DIR_URL . "/accounts/withdraw");
                exit;
        
            }
        
        } else {
        
            header("Location: " . DIR_URL . "/login");
            exit;
        
        }
    
    }

    private function makeRequestWithdraw($bank_account, $usd_amount) {
        $usdwallet = USDWallets::read($_SESSION['id']);
        $bankAccount = BankAccounts::read($bank_account);
        $commissions = Commissions::read(1);
        $commission = ($bankAccount['type'] == 1) ? $commissions['withdraw_wire_transfer_domestic'] : $commissions['withdraw_wire_transfer_international'];
        if ($usd_amount <= ($usdwallet['balance'] - $commission)) {
            
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            $an = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $su = strlen($an) - 1;
            $wire_code = substr($an, rand(0, $su), 1) . substr($an, rand(0, $su), 1) . substr($an, rand(0, $su), 1) . substr($an, rand(0, $su), 1) . substr($an, rand(0, $su), 1) . substr($an, rand(0, $su), 1) . substr($an, rand(0, $su), 1) . substr($an, rand(0, $su), 1) . substr($an, rand(0, $su), 1) . substr($an, rand(0, $su), 1);
            
            $data = array();
            $data['wire_code'] = $wire_code;
            $data['usd_amount'] = $usd_amount;
            $data['commission'] = $commission;
            $data['eth_value'] = $ethusd;
            $data['date'] = time();
            $data['bank_account'] = $bank_account;
            $data['user'] = $_SESSION['id'];
            $result = Withdraw::create($data);
            if ($result) {

                $dataMail = array(
                    "to" => $_SESSION['email'],
                    "wire_code" => $wire_code,
                    "bank_account" => $bankAccount['number'],
                    "date" => date("Y-m-d", time()),
                    "eth_rate" => number_format($ethusd, 2),
                    "subtotal" => number_format($usd_amount, 2),
                    "fee" => number_format($commission, 2),
                    "total" => number_format(($usd_amount - $commission), 2)
                    );

                $sendmail = Mailer::withdrawUser($dataMail);
                Functions::usd_alert();
                
                //Mailer::sendMail($_SESSION['email'], "Mercury Cash - Withdraw", '<p style="text-align: center;">We have received your wire transfer withdrawal request, please be advised of the following:</p><p style="text-align: center;">Reference Code: <b>' . $wire_code . '</b></p>', 'Go to Mercury Cash', DIR_URL . "/accounts/withdraw");
                //Mailer::sendMail("sells@mercury.cash", $_SESSION['name'] . " - Sell", '<p style="text-align: center;">' . $_SESSION['name'] . ' made a sale using the USD Wallet payment method</p>', 'Go to Mercury Cash', DIR_URL . "/administrator/sales/sale/" . $result);
                $_SESSION['alert'] = "<p>We have received your wire transfer withdrawal request, please be advised of the following:</p><p>Reference Code: <b>" . $wire_code . "</b></p><p></p>";
                header("Location: " . DIR_URL . "/accounts/withdraw");
                exit;

            } else {
                $_SESSION['alert'] = "An error has occurred, please try again.";
                header("Location: " . DIR_URL . "/accounts/withdraw");
                exit;
            }
        } else {
            $_SESSION['alert'] = "<p>You do not have enough money to make this USD Wallet withdrawal</p><p>You can withdraw a maximum amount of $" . ($usdwallet['balance'] - $commission) . "</p>";
            header("Location: " . DIR_URL . "/accounts/withdraw");
            exit;
        }
    }
    public function calculationsWithdraw($amount, $bankAccount) {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        if (!empty($_SESSION['id'])) {
            if (is_numeric($amount) && is_numeric($bankAccount)) {
                
                $account = BankAccounts::read($bankAccount);
                $commissions = Commissions::read(1);
                $commission = ($account['type'] == 1) ? $commissions['withdraw_wire_transfer_domestic'] : $commissions['withdraw_wire_transfer_international'];
                $total = $amount - $commission;
                echo json_encode(array(
                    "subtotal" => number_format($amount, 2),
                    "commission" => number_format($commission, 2),
                    "total" => number_format($total, 2)
                    ));
            }
        } else {
            echo json_encode(array(
                "description" => "Error!"
                ));
        }
    }
    public function usdwallet() {
        if (!empty($_SESSION['id'])) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }
            /* HEADER */
            
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;
            View::set("ethusd", $ethusd);
            
            /* HEADER */
            $usdwallet = USDWallets::getByUser($_SESSION['id']);
            $deposits = Deposits::getByUser($_SESSION['id']);
            $purchase = Purchases::getByMethod(1, $_SESSION['id']);;
            $sales = Sales::getByMethod(1, $_SESSION['id']);
            $withdraw = Withdraw::getByUser($_SESSION['id']);
            $transactions = array();
            for ($i=0; $i < count($deposits); $i++) {
                $deposits[$i]['type_transaction'] = 1;
                array_push($transactions, $deposits[$i]);
            }
            for ($i=0; $i < count($purchase); $i++) {
                $purchase[$i]['type_transaction'] = 2;
                array_push($transactions, $purchase[$i]);
            }
            for ($i=0; $i < count($sales); $i++) {
                $sales[$i]['type_transaction'] = 3;
                array_push($transactions, $sales[$i]);
            }
            for ($i=0; $i < count($withdraw); $i++) {
                $withdraw[$i]['type_transaction'] = 4;
                array_push($transactions, $withdraw[$i]);
            }
            $total = count($transactions);
            for ($i=0; $i < $total; $i++) { 
                for ($j=0; $j < $total-$i; $j++) { 
                    if (@$transactions[$j]['date'] < @$transactions[$j + 1]['date']) {
                        $k = $transactions[$j+1];
                        $transactions[$j+1] = $transactions[$j];
                        $transactions[$j] = $k;
                    }
                }
            }
            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }
            View::set("transactions", $transactions);
            View::set("title", "USD Wallet");
            View::set("requestsUnread", $unread);
            View::render("usdwallet");
        } else {
            header("Location: " . DIR_URL . "/login");
            exit;
        }
    }
    public function genPdfReportUsdWallet() {
        if (!empty($_SESSION['id'])) {
            if (!empty($_POST['from']) && !empty($_POST['to'])) {
                $from = htmlspecialchars($_POST['from'], ENT_QUOTES);
                $to = htmlspecialchars($_POST['to'], ENT_QUOTES);
                $from = $from . " 23:59:59";
                $to = $to . " 23:59:59";
                $from = strtotime($_POST['from']);
                $to = strtotime($_POST['to']);
                if (is_numeric($from) && is_numeric($to)) {
                    $fromText = $_POST['from'];
                    $toText = $_POST['to'];
                    
                    $usdwallet = USDWallets::getByUser($_SESSION['id']);
                    $user = Users::read($_SESSION['id']);
                    $deposits = Deposits::getDates($from, $to, $_SESSION['id']);
                    $purchase = Purchases::getByMethodDates(1, $from, $to, $_SESSION['id']);
                    $sales = Sales::getByMethodDates(1, $from, $to, $_SESSION['id']);
                    $withdraw = Withdraw::getDates($from, $to, $_SESSION['id']);
                    $transactions = array();
                    for ($i=0; $i < count($deposits); $i++) {
                        $deposits[$i]['type_transaction'] = 1;
                        array_push($transactions, $deposits[$i]);
                    }
                    for ($i=0; $i < count($purchase); $i++) {
                        $purchase[$i]['type_transaction'] = 2;
                        array_push($transactions, $purchase[$i]);
                    }
                    for ($i=0; $i < count($sales); $i++) {
                        $sales[$i]['type_transaction'] = 3;
                        array_push($transactions, $sales[$i]);
                    }
                    for ($i=0; $i < count($withdraw); $i++) {
                        $withdraw[$i]['type_transaction'] = 4;
                        array_push($transactions, $withdraw[$i]);
                    }
                    $total = count($transactions);
                    for ($i=0; $i < $total; $i++) { 
                        for ($j=0; $j < $total-$i; $j++) { 
                            if (@$transactions[$j]['date'] < @$transactions[$j + 1]['date']) {
                                $k = $transactions[$j+1];
                                $transactions[$j+1] = $transactions[$j];
                                $transactions[$j] = $k;
                            }
                        }
                    }
                    $transactions = array_reverse($transactions);
                    $html = file_get_contents("public/assets/mpdf/archivo.html");
                    $css = file_get_contents("public/assets/mpdf/archivo.css");
                    $trs = '';
                    foreach ($transactions as $transaction) {
                    $trs .= '<tr>';
                    $trs .= '<td>' . date("m/d/Y H:i:s", $transaction['date']) . '</td>';
                    if (!empty($transaction['wire_code'])) {
                        $trs .= '<td>' . $transaction['wire_code'] . '</td>';
                    } else {
                        $trs .= '<td>does not apply</td>';
                    }
                    if (!empty($transaction['eth_value'])) {
                        $trs .= '<td>$' . $transaction['eth_value'] . ' USD</td>';
                    } else {
                        $trs .= '<td>does not apply</td>';
                    }
                    if (!empty($transaction['eth_amount'])) {
                        $trs .= '<td>' . $transaction['eth_amount'] . ' ETH</td>';
                    } else {
                        $trs .= '<td>does not apply</td>';
                    }
                    if (!empty($transaction['usd_amount'])) {
                        $trs .= '<td>$' . $transaction['usd_amount'] . ' USD</td>';
                    } else {
                        $trs .= '<td>does not apply</td>';
                    }
                    if (!empty($transaction['commission'])) {
                        $trs .= '<td>$' . $transaction['commission'] . ' USD</td>';
                    } else {
                        $trs .= '<td>does not apply</td>';
                    }
                    
                    if ($transaction['status'] == 0) { 
                        $trs .= '<td><span class="label label-danger mr5 mb10 ib lh15">Rejected</span></td>';
                    } elseif ($transaction['status'] == 1) { 
                        $trs .= '<td><span class="label label-success mr5 mb10 ib lh15">Approved</span></td>';
                    } elseif ($transaction['status'] == 2) { 
                        $trs .= '<td><span class="label label-warning mr5 mb10 ib lh15">pending</span></td>';
                    }
                    if ($transaction['type_transaction'] == 1) { 
                        $trs .= '<td>Deposit</td>';
                    } elseif ($transaction['type_transaction'] == 2) { 
                        $trs .= '<td>Buy</td>';
                    } elseif ($transaction['type_transaction'] == 3) { 
                        $trs .= '<td>Sell</td>';
                    } elseif ($transaction['type_transaction'] == 4) { 
                        $trs .= '<td>Withdraws</td>';
                    }
                    $trs .= '</tr>';
                        
                    //$trs .= '<tr><td>' . date("m/d/Y H:i:s", $transaction['date']) . '</td></tr>';
                    }
                    $table = '<table><thead><tr><th>Date</th><th>Wire Code</th><th>Rate ETH</th><th>ETH QTY.</th><th>USD Amount</th><th>Fee</th><th>Status</th><th>TX Type</th></tr>ead><tbody>' . $trs . '</tbody></table>';
                    $html = str_replace("{{USER}}", $_SESSION['name'], $html);
                    $html = str_replace("{{EMAIL}}", $_SESSION['email'], $html);
                    $html = str_replace("{{ADDRESS}}", $user['address'], $html);
                    $html = str_replace("{{DATE}}", $fromText . " - ". $toText, $html);
                    $html = str_replace("{{ID}}", $_SESSION['id'], $html);
                    $html = str_replace("{{CURRENCY}}", "USD", $html);
                    $html = str_replace("{{BALANCE}}", "$" . number_format($usdwallet['balance'], 2) . " USD", $html);
                    $html = str_replace("{{TABLE}}", $table, $html);
                    require_once LIBSPATH . "/mpdf/mpdf.php";
                    $mpdf = new \mPDF('c', 'A4');
                    $mpdf->writeHTML($css, 1);
                    $mpdf->writeHTML($html);
                    $mpdf->Output('file.pdf', 'I');
                } else {
                    $_SESSION['alert'] = "An error has occurred, the date is not correct";
                    header("Location: " . DIR_URL . "/accounts/usdwallet");
                    exit;
                }
            } else {
                $_SESSION['alert'] = "You must enter the <b>field</b> from and the field <b>to</b>";
                header("Location: " . DIR_URL . "/accounts/usdwallet");
                exit;
            }
        } else {
            header("Location: " . DIR_URL . "/login");
            exit;
        }
    }
}
?>