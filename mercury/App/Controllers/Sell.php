<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Users,
    \App\Models\Etherscan,
    \App\Models\Transactions,
    \App\Models\USDWallets,
    \App\Models\ETHWallets,
    \App\Models\ETHWalletAdmin,
    \App\Models\USDWalletAdmin,
    \App\Models\Commissions,
    \App\Models\Request,
    \App\Models\Limits,
    \App\Models\BankAccounts,
    \App\Models\Sales,
    \App\Models\Earnings,
    \App\Models\TransferValidation,
    \App\Controllers\Functions,
    \App\Controllers\Mailer;

class Sell {

    public function index() {
        
        if (!empty($_SESSION['id'])) {
        
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }

            if (!empty($_SESSION['price_ether']) || isset($_SESSION['price_ether'])) {
                unset($_SESSION['price_ether']);
            }
            
            /* HEADER */
            /*$price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;

            $res_ethusd = $price->ethusd;*/
            $ethusd=$_SESSION['ethusd'];
            $res_ethusd = $_SESSION['ethusd'];

            $earnings = Earnings::read(1);
            $percentage = ($ethusd * $earnings['sell']) / 100;
            $res_ethusd = number_format($res_ethusd - $percentage, 2);
            $_SESSION['price_ether'] = $res_ethusd;
            View::set("ethusd", $_SESSION['price_ether']);
            /* HEADER */

            $accounts = BankAccounts::getBankAccountByUser($_SESSION['id']);
            
            $commissions = Commissions::read(1);
            $usd_wallet = USDWallets::getByUser($_SESSION['id']);
            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;

            $ethwallets = ETHWallets::getByUser($_SESSION['id']);

            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }

            //View::set("eth_balance", number_format(ETHWallets::getBalance($_SESSION['id'])['balance'], 5));
            View::set("ethwallets", $ethwallets);
            View::set("accounts", $accounts);
            View::set("commission", $commissions['sell_usdwallet']);
            View::set("usd_wallet", $usd_wallet);
            View::set("title", "Sell");
            View::set("requestsUnread", $unread);
            View::render("sell");

        } else {
            header("Location: " . DIR_URL . "/login");
        }
    }

    public function _calculations($amount, $method, $currency) {
       
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        
        if (!empty($_SESSION['id'])) {
        
            if (is_numeric($amount) && is_numeric($currency)) {
                
                if ($currency == '1') {
                    $amount = $_SESSION['price_ether'] * $amount;
                }

                $modal = FALSE;
                $user = Users::read($_SESSION['id']);
                $limits = Limits::getByUser($_SESSION['id']);
                $commissions = Commissions::read(1);
                $usdwallet = USDWallets::getByUser($_SESSION['id']);

                if ($method == '1') {
                    
                    $limit = $limits['buy_eth_usdwallet'] - $user['limit_buy_eth_usd'];
                    $fee = ($commissions['buy_usdwallet'] * $amount) / 100;
                    $total = $amount + $fee;
                    $ethers = $amount / $_SESSION['price_ether'];
                    $percentage = ($amount * 100) / $limit;
                    $description = "This would exceed your USDWallet purchase limit of $" . number_format($limit['buy_eth_usdwallet'], 2) . " per week. Please wait or try a smaller amount";
                    
                    if ($total > $limit) {
                        $modal = TRUE;
                        $description = "This would exceed your USDWallet purchase limit of $" . number_format($limit['buy_eth_usdwallet'], 2) . " per week. Please wait or try a smaller amount";
                    } elseif ($total > $usdwallet['balance']) {
                        $modal = TRUE;
                        $description = "The amount exceeds the balance you have in the USD Wallet";
                    }
                    
                    echo json_encode(array(
                        "status" => 1,
                        "limit" => $limit,
                        "subtotal" => number_format($amount, 2),
                        "fee" => number_format($fee, 2),
                        "total" => number_format($total, 2),
                        "balance" => $usdwallet['balance'],
                        "ethers" => $ethers,
                        "percentage" => $percentage,
                        "description" => $description,
                        "modal" => $modal
                        ));
                }
            }
        }
    }

    public function process() {
        
        if ($_SESSION['status'] == 1) {
        
            $usd_amount = htmlspecialchars($_POST['usd_amount'], ENT_QUOTES);
            $eth_amount = htmlspecialchars($_POST['eth_amount'], ENT_QUOTES);
            $payment_method = htmlspecialchars($_POST['deposit_to'], ENT_QUOTES);

            if (is_numeric($usd_amount) && is_numeric($eth_amount)) {
                
                if ($usd_amount >= 0.05) {
                    
                    $balance = Etherscan::getBalance($_SESSION['address']);
                    $balance = Etherscan::fromWei($balance);
                    
                    if ($balance < $eth_amount) {

                        $_SESSION['alert'] = "Currently does not have ETH sufficient to perform this transaction";
                        header("Location: " . DIR_URL . "/sell");
                        exit;

                    } else {

                        $commissions = Commissions::read(1);

                        if ($payment_method == '1') {

                            $wallet = ETHWallets::getPrimaryWalletEthByUser($_SESSION['id']);
                            $walletAdmin = ETHWalletAdmin::read(1);
                            $commission = number_format((($usd_amount * $commissions['sell_usdwallet']) / 100) + 0.02, 2);

                            $data['stripe_id'] = NULL;
                            $data['eth_amount'] = $eth_amount;
                            $data['usd_amount'] = $usd_amount;
                            $data['commission'] = $commission;
                            $data['eth_value'] = $_SESSION['price_ether'];
                            $data['payment_method'] = 1;
                            $data['ach_token'] = NULL;
                            $data['date'] = time();
                            $data['status'] = 2;
                            $data['user'] = $_SESSION['id'];
                            $result = Sales::create($data);

                            if ($result > 0) {
                                $_SESSION['id_sale_transaction'] = $result;
                                
                                echo json_encode(array(
                                    "status" => 1,
                                    "sender" => $_SESSION['address'],
                                    "receiver" => $walletAdmin['address'],
                                    "amount" => $eth_amount,
                                    "privateKey" => $wallet['private_key'],
                                    "payment_method" => 1
                                    ));

                            } else {

                                $_SESSION['alert'] = $result;
                                header("Location: " . DIR_URL . "/sell");
                                exit;
                            }

                        } else {

                            $commission = number_format((($usd_amount * $commissions['sell_ach']) / 100) + 0.02, 2);
                            $data['stripe_id'] = NULL;
                            $data['eth_amount'] = $eth_amount;
                            $data['usd_amount'] = $usd_amount;
                            $data['commission'] = $commission;
                            $data['eth_value'] = $_SESSION['price_ether'];
                            $data['payment_method'] = 2;
                            $data['ach_token'] = $payment_method;
                            $data['date'] = time();
                            $data['status'] = 2;
                            $data['user'] = $_SESSION['id'];
                            $result = Sales::create($data);

                            if ($result > 0) {

                                Mailer::sendMail($_SESSION['email'], "Mercury Cash - Sell", "Your transaction is being processed", "Go to Mercury Cash", DIR_URL . "/sell");
                                
                                $_SESSION['alert'] = "We have registered your transaction, you will shortly receive an email with information about your transaction";
                                header("Location: " . DIR_URL . "/sell");
                                exit;

                            } else {
                                
                                $_SESSION['alert'] = $result;
                                header("Location: " . DIR_URL . "/sell");
                                exit;
                            }
                        }
                    }

                } else {

                    $_SESSION['alert'] = "The minimum amount to make is $1 transaction";
                    header("Location: " . DIR_URL . "/sell");
                    exit;
                }

            } else {

                $_SESSION['alert'] = "Please enter numerical data in the fields";
                header("Location: " . DIR_URL . "/sell");
                exit;
            }
            
        } else {

            $_SESSION['alert'] = "Missing data required for this operation";
            header("Location: " . DIR_URL . "/sell");
            exit;
        }
    }

    public function transfer() {
       
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        
        if (!empty($_SESSION['id']) && !empty($_SESSION['id_sale_transaction'])) {
            $sale = Sales::read($_SESSION['id_sale_transaction']);
            $usdwallet = USDWallets::getByUser($_SESSION['id']);
            $balance = $usdwallet['balance'] + $sale['usd_amount'];
            $changeBalance = USDWallets::changeBalance($balance, $usdwallet['id']);
            
            if ($changeBalance) {
                $result = Sales::changeStatus($_SESSION['id_sale_transaction'], '1');
                
                if ($result) {

                    unset($_SESSION['id_sale_transaction']);
                    
                    echo json_encode(array(
                        "status" => 1,
                        "description" => "Your transaction has been processed successfully and we have transferred a total of $" . $sale['usd_amount'] . " to your USDWallet"
                        ));

                } else {
                    
                    echo json_encode(array(
                        "status" => 0,
                        "description" => $result
                        ));
                }

            } else {
                
                echo json_encode(array(
                    "status" => 0,
                    "description" => $changeBalance
                    ));
            }

        } else {
            
            echo json_encode(array(
                "status" => 0,
                "description" => "Error!"
                ));
        }
    }


    public function calculations($amount, $method, $currency) {

        if (!empty($_SESSION['id'])) {

            if ($method == 1) { //method: 1 = usdwallet: 2 = ach

                $commissions = Commissions::read(1);
                
                if ($currency == 1) { //currency: 1 = eth: 2 = usd

                    $subtotal = $_SESSION['price_ether'] * $amount;
                    $fee = ($subtotal * $commissions['sell_usdwallet']) / 100;
                    $total = $subtotal - $fee;
                    
                    echo json_encode(array(
                        "subtotal" => number_format($subtotal, 2),
                        "fee" => number_format($fee, 2),
                        "total" => number_format($total, 2)
                        ));

                } elseif ($currency == 2) { //currency: 1 = eth: 2 = usd
                    
                    $eth_amount = $amount / $_SESSION['price_ether'];
                    $subtotal = $_SESSION['price_ether'] * $eth_amount;
                    $fee = ($subtotal * $commissions['sell_usdwallet']) / 100;
                    $total = $subtotal - $fee;
                    
                    echo json_encode(array(
                        "eth_amount" => number_format($eth_amount, 8),
                        "subtotal" => number_format($subtotal, 2),
                        "fee" => number_format($fee, 2),
                        "total" => number_format($total, 2)
                        ));

                }

            }

        }

    }

    public function createTransaction() {

        if (!empty($_SESSION['id'])) {
            
            if (!empty($_POST['eth_wallet']) && !empty($_POST['eth_amount']) && !empty($_POST['bank_account'])) {

                $transferValidation = TransferValidation::getByUser($_SESSION['id']);
                $commission = 0;
                $usd_amount = $_POST['eth_amount'] * $_SESSION['price_ether'];

                $user = Users::read($_SESSION['id']);
                $balance = ETHWallets::getBalance($_POST['eth_wallet']);

                if (!is_null($user['last_name']) &&
                    !is_null($user['ndocument']) &&
                    $user['country_id'] > 0 &&
                    $user['province_id'] > 0 &&
                    !is_null($user['city']) &&
                    !is_null($user['zip_code']) &&
                    !is_null($user['address'])) {
                    
                    if ($_SESSION['status'] == 2 || $_SESSION['status'] == 3) {

                        if ($balance['wallet_balance'] >= $_POST['eth_amount']) {
                            
                            if (($usd_amount + $transferValidation['amount']) <= 1000) {
                            
                                if ($transferValidation['trial_count'] < 2) {
                                    
                                    if ($usd_amount >= 1) {
                                        
                                        $data = array();
                                        $data['trial_count'] = $transferValidation['trial_count'] + 1;
                                        $data['amount'] = $transferValidation['amount'] + $usd_amount;
                                        $data['user'] = $_SESSION['id'];
                                        $data['id'] = $transferValidation['id'];
                                        TransferValidation::update($data);

                                        $this->makeSale($_POST['eth_wallet'], $_POST['bank_account'], $_POST['eth_amount'], $usd_amount);

                                    } else {

                                        $_SESSION['alert'] = "Purchase must be greater than $1 USD";
                                        header("Location: " . DIR_URL . "/sell");
                                        exit;

                                    }

                                } else {

                                    $_SESSION['alert'] = "<p>Your two transactions trial has expired, you need to validate your documents in order to continue using Mercury Cash services</p>";
                                    header("Location: " . DIR_URL . "/sell");
                                    exit;

                                }

                            } else {

                                $_SESSION['alert'] = "<p>Your two transactions trial or one for less than $1000 USD has expired, you need to validate your documents in order to continue using Mercury Cash services</p><p>You can make a transaction of maximun $" . (1000 - $transferValidation['amount']) . " USD</p>";
                                header("Location: " . DIR_URL . "/sell");
                                exit;

                            }

                        } else {

                            $_SESSION['alert'] = "You  don't have enough funds";
                            header("Location: " . DIR_URL . "/sell");
                            exit;

                        }

                    } elseif ($_SESSION['status'] == 1) {

                        if ($balance['wallet_balance'] >= $_POST['eth_amount']) {
                            $this->makeSale($_POST['eth_wallet'], $_POST['bank_account'], $_POST['eth_amount'], $usd_amount);
                        } else {
                            $_SESSION['alert'] = "You  don't have enough funds";
                            header("Location: " . DIR_URL . "/sell");
                            exit;
                        }

                    } else {

                        header("Location: " . DIR_URL);
                        exit;

                    }

                } else {

                    $_SESSION['alert'] = "You need to complete TIER 1 in order to continue. <a href='" . DIR_URL . "/settings'>Update your profile</a> and submit your TIER 1 data.";
                    header("Location: " . DIR_URL . "/sell");
                    exit;

                }

            } else {

                $_SESSION['alert'] = "Is attempting to send empty data";
                header("Location: " . DIR_URL . "/sell");
                exit;

            }

        } else {

            header("Location: " . DIR_URL . "/login");
            exit;

        }

    }

    private function makeSale($eth_wallet, $bank_account, $eth_amount, $usd_amount) {

        if (is_numeric($bank_account)) {
            
            $commissions = Commissions::read(1);
            $account = BankAccounts::read($bank_account);
            $commission = ($usd_amount * $commissions['sell_ach']) / 100;

            $data = array();
            $data['eth_amount'] = $eth_amount;
            $data['usd_amount'] = $usd_amount;
            $data['commission'] = $commission;
            $data['eth_value'] = $_SESSION['price_ether'];
            $data['payment_method'] = 2;
            $data['eth_wallet'] = $eth_wallet;
            $data['date'] = time();
            $data['status'] = 2;
            $data['bank_account'] = $bank_account;
            $data['user'] = $_SESSION['id'];

            $result = Sales::create($data);
            if ($result > 0) {

                $dataMail = array(
                    "to" => $_SESSION['email'],
                    "payment_method" => "Account Number: " . $account['number'],
                    "date" => date("Y-m-d", time()),
                    "eth_amount" => number_format($eth_amount, 5),
                    "eth_rate" => number_format($_SESSION['price_ether'], 2),
                    "subtotal" => number_format($usd_amount, 2),
                    "fee" => number_format($commission, 2),
                    "total" => number_format(($usd_amount - $commission), 2)
                    );
                
                Mailer::sellUser($dataMail);
                Functions::usd_alert();
                Mailer::sendMail($_SESSION['email'], "Mercury Cash - Sell", '<p style="text-align: center;">Your sale is being processed, you will soon receive an email with information about the status of your sale</p>', 'Go to Mercury Cash', DIR_URL . "/sell");
                Mailer::sendMail("sells@mercury.cash", $_SESSION['name'] . " - Sell", '<p style="text-align: center;">' . $_SESSION['name'] . ' has made a sale using the method of payment by bank account</p>', 'Go to Mercury Cash', DIR_URL . "/administrator/sales/sale/" . $result);
                $_SESSION['alert'] = "Your sale is being processed, you will soon receive an email with information about the status of your sale";
                header("Location: " . DIR_URL . "/sell");
                exit;

            } else {

                $_SESSION['alert'] = "An error occurred, please try again.";
                header("Location: " . DIR_URL . "/sell");
                exit;

            }

        } elseif ($bank_account == "usdwallet") {
            
            $commissions = Commissions::read(1);
            $usdwallet = USDWallets::getByUser($_SESSION['id']);
            $commission = ($usd_amount * $commissions['sell_usdwallet']) / 100;
            $ethWalletAdmin = ETHWalletAdmin::read(1);

            $data = array();
            $data['eth_amount'] = $eth_amount;
            $data['usd_amount'] = $usd_amount;
            $data['commission'] = $commission;
            $data['eth_value'] = $_SESSION['price_ether'];
            $data['payment_method'] = 1;
            $data['eth_wallet'] = $eth_wallet;
            $data['date'] = time();
            $data['status'] = 1;
            $data['bank_account'] = NULL;
            $data['user'] = $_SESSION['id'];
            $result = Sales::create($data);
            if ($result > 0) {

                $dataMail = array(
                    "to" => $_SESSION['email'],
                    "payment_method" => "USD Wallet",
                    "date" => date("m-d-Y", time()),
                    "eth_amount" => number_format($eth_amount, 8),
                    "eth_rate" => number_format($_SESSION['price_ether'], 2),
                    "subtotal" => number_format($usd_amount, 2),
                    "fee" => number_format($commission, 2),
                    "total" => number_format(($usd_amount - $commission), 2)
                    );
                
                Mailer::sellUser($dataMail);
                Mailer::sendMail("sells@mercury.cash", $_SESSION['name'] . " - Sell", '<p style="text-align: center;">' . $_SESSION['name'] . ' made a sale using the USD Wallet payment method</p>', 'Go to Mercury Cash', DIR_URL . "/administrator/sales/sale/" . $result);
                $_SESSION['alert'] = "Your transaction has been processed successfully";
                header("Location: " . DIR_URL . "/sell");
                exit;

            } else {

                $_SESSION['alert'] = "An error occurred, please try again.";
                header("Location: " . DIR_URL . "/sell");
                exit;

            }

        } else {

            $_SESSION['alert'] = "The data you are trying to send is invalid";
            header("Location: " . DIR_URL . "/sell");
            exit;

        }

    }

    private function _makeSale($eth_wallet, $bank_account, $eth_amount, $usd_amount) {

        if (is_numeric($bank_account)) {
            
            $commissions = Commissions::read(1);
            $account = BankAccounts::read($bank_account);
            $commission = ($usd_amount * $commissions['sell_ach']) / 100;
            $ethwallet = ETHWallets::read($eth_wallet);
            $ethWalletAdmin = ETHWalletAdmin::read(1);

            $url = "https://node.mercury.cash/sellcito";
            $data = "sender=" . $ethwallet['address'] . "&wm=" . $ethWalletAdmin['address'] . "&amount=" . $eth_amount . "&privateKey=" . $ethwallet['private_key'] . "&save=0";
            $con = curl_init();
            curl_setopt($con, CURLOPT_URL, $url);
            curl_setopt($con, CURLOPT_HTTPGET, FALSE);
            curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($data)));
            curl_setopt($con, CURLOPT_POST, 1);
            curl_setopt($con, CURLOPT_POSTFIELDS, $data);
            curl_setopt($con, CURLOPT_HEADER, FALSE);
            curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($con);
            curl_close($con);

            $response = json_decode($response);
            if ($response->status == 1) {

                $data = array();
                $data['txhash'] = $response->hash;
                $data['eth_amount'] = $eth_amount;
                $data['usd_amount'] = $usd_amount;
                $data['commission'] = $commission;
                $data['eth_value'] = $_SESSION['price_ether'];
                $data['payment_method'] = 2;
                $data['date'] = time();
                $data['status'] = 2;
                $data['bank_account'] = $bank_account;
                $data['eth_wallet'] = $eth_wallet;
                $data['user'] = $_SESSION['id'];

                $result = Sales::create($data);
                if ($result > 0) {

                    $dataMail = array(
                        "to" => $_SESSION['email'],
                        "payment_method" => "Account Number: " . $account['number'],
                        "date" => date("m-d-Y", time()),
                        "eth_amount" => number_format($eth_amount, 8),
                        "eth_rate" => number_format($_SESSION['price_ether'], 2),
                        "subtotal" => number_format($usd_amount, 2),
                        "fee" => number_format($commission, 2),
                        "total" => number_format(($usd_amount - $commission), 2)
                        );
                    
                    Mailer::sellUser($dataMail);
                    Functions::usd_alert();
                    Mailer::sendMail($_SESSION['email'], "Mercury Cash - Sell", '<p style="text-align: center;">Your sale is being processed, you will soon receive an email with information about the status of your sale</p>', 'Go to Mercury Cash', DIR_URL . "/sell");
                    Mailer::sendMail("sells@mercury.cash", $_SESSION['name'] . " - Sell", '<p style="text-align: center;">' . $_SESSION['name'] . ' has made a sale using the method of payment by bank account</p>', 'Go to Mercury Cash', DIR_URL . "/administrator/sales/sale/" . $result);
                    $_SESSION['alert'] = "Your sale is being processed, you will soon receive an email with information about the status of your sale";
                    header("Location: " . DIR_URL . "/sell");
                    exit;

                } else {

                    $_SESSION['alert'] = "An error occurred, please try again.";
                    header("Location: " . DIR_URL . "/sell");
                    exit;

                }

            } else {

                $_SESSION['alert'] = $response->message;
                header("Location: " . DIR_URL . "/sell");
                exit;

            }

        } elseif ($bank_account == "usdwallet") {
            
            $commissions = Commissions::read(1);
            $usdwallet = USDWallets::getByUser($_SESSION['id']);
            $ethwallet = ETHWallets::read($eth_wallet);
            $commission = ($usd_amount * $commissions['sell_usdwallet']) / 100;
            $ethWalletAdmin = ETHWalletAdmin::read(1);


            $url = "https://node.mercury.cash/sellcito";
            $data = "sender=" . $ethwallet['address'] . "&wm=" . $ethWalletAdmin['address'] . "&amount=" . $eth_amount . "&privateKey=" . $ethwallet['private_key'] . "&save=0";
            $con = curl_init();
            curl_setopt($con, CURLOPT_URL, $url);
            curl_setopt($con, CURLOPT_HTTPGET, FALSE);
            curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($data)));
            curl_setopt($con, CURLOPT_POST, 1);
            curl_setopt($con, CURLOPT_POSTFIELDS, $data);
            curl_setopt($con, CURLOPT_HEADER, FALSE);
            curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($con);
            curl_close($con);

            $response = json_decode($response);
            if ($response->status == 1) {

                $data = array();
                $data['balance'] = $usdwallet['balance'] + ($usd_amount - $commission);
                $data['id'] = $usdwallet['id'];
                
                $result = USDWallets::changeBalance($data['balance'], $data['id']);
                if ($result) {
                    
                    $usdwalletadmin = USDWalletAdmin::read(1);
                    $data = array();
                    $data['balance'] = $usdwalletadmin['balance'] + $commission;
                    $data['id'] = $usdwallet['id'];

                    $result = USDWalletAdmin::update($data);
                    if ($result) {

                        $data = array();
                        $data['txhash'] = $response->hash;
                        $data['eth_amount'] = $eth_amount;
                        $data['usd_amount'] = $usd_amount;
                        $data['commission'] = $commission;
                        $data['eth_value'] = $_SESSION['price_ether'];
                        $data['payment_method'] = 1;
                        $data['date'] = time();
                        $data['status'] = 2;
                        $data['bank_account'] = NULL;
                        $data['eth_wallet'] = $eth_wallet;
                        $data['user'] = $_SESSION['id'];

                        $result = Sales::create($data);
                        if ($result > 0) {

                            $dataMail = array(
                                "to" => $_SESSION['email'],
                                "payment_method" => "USD Wallet",
                                "date" => date("m-d-Y", time()),
                                "eth_amount" => number_format($eth_amount, 8),
                                "eth_rate" => number_format($_SESSION['price_ether'], 2),
                                "subtotal" => number_format($usd_amount, 2),
                                "fee" => number_format($commission, 2),
                                "total" => number_format(($usd_amount - $commission), 2)
                                );
                            
                            Mailer::sellUser($dataMail);
                            Mailer::sendMail("sells@mercury.cash", $_SESSION['name'] . " - Sell", '<p style="text-align: center;">' . $_SESSION['name'] . ' made a sale using the USD Wallet payment method</p>', 'Go to Mercury Cash', DIR_URL . "/administrator/sales/sale/" . $result);
                            $_SESSION['alert'] = "Your transaction has been processed successfully";
                            header("Location: " . DIR_URL . "/sell");
                            exit;

                        } else {

                            $_SESSION['alert'] = "An error occurred, please try again.";
                            header("Location: " . DIR_URL . "/sell");
                            exit;

                        }

                    } else {

                        $_SESSION['alert'] = "An error occurred, please try again.";
                        header("Location: " . DIR_URL . "/sell");
                        exit;

                    }

                } else {

                    $_SESSION['alert'] = "An error occurred, please try again.";
                    header("Location: " . DIR_URL . "/sell");
                    exit;

                }

            } else {

                $_SESSION['alert'] = $response->message;
                header("Location: " . DIR_URL . "/sell");
                exit;

            }

        } else {

            $_SESSION['alert'] = "The data you are trying to send is invalid";
            header("Location: " . DIR_URL . "/sell");
            exit;

        }

    }

    public function calc_max_sell($id_wallet) {
    
        if (!empty($_SESSION['id'])) {

            if (is_numeric($id_wallet)) {
                
                $ethwallet = ETHWallets::getEthWallet($id_wallet);

                if ($ethwallet['id_user'] == $_SESSION['id']) {

                    $commissions = Commissions::read(1);
                    $total = $_SESSION['price_ether'] * $ethwallet['wallet_balance'];
                    $fee = ($total * $commissions['sell_usdwallet']) / 100;
                    
                    echo json_encode(array(
                        "total_eth" => $ethwallet['wallet_balance'],
                        "total_usd" => number_format($total, 2),
                        "fee" => number_format($fee, 2),
                        "subtotal" => number_format($total, 2),
                        "total" => number_format(($total - $fee), 2)
                    ));

                }

            }

        }
    
    }

}
?>