<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Users,
    \App\Models\ETHWallets,
    \App\Models\Logs;

class Validate {

    public function email($token) {

        $user = Users::validateEmail($token);

        if ($user > 0) {
            $status = 1;
            $message = "Your email has been successfully validated!";
            Users::changeStatus($user['id'], 3);
            Users::updateToken($user['id'], md5($user['id'] . time()));

            $wallet = ETHWallets::getPrimaryWalletEthByUser($user['id']);
            $_SESSION['id'] = $user['id'];
            $_SESSION['name'] = $user['name'];
            $_SESSION['email'] = $user['email'];
            $_SESSION['avatar'] = DIR_URL . "/administrator/public/avatars/" . $user['avatar'];
            $_SESSION['status'] = $user['status'];
            $_SESSION['address'] = $wallet['address'];
            $_SESSION['wallet'] = $wallet['id'];
            $_SESSION['time'] = time();
            $_SESSION['validateToken'] = hash('sha256', md5(uniqid(mt_rand(), true)));
            $data = array();
            $data['ip'] =  $_SERVER['REMOTE_ADDR'];
            $data['browser'] =  $_SERVER['HTTP_USER_AGENT'];
            $data['date'] =  time();
            $data['user'] =  $_SESSION['id'];
            Logs::create($data);
            
            require_once LIBSPATH . '/jwt/init.php';
            
            $shared_secret = "A?Z8A1hy}{Fl!s)";
            $payload = array(
                'iat' => time(),
                'jti' => md5($shared_secret . ':' . time()),
                'email' => $_SESSION['email'],
                'name' => $_SESSION['name'],
                'picture' => $_SESSION['avatar']
                );
            
            $_SESSION['kayako_token'] = \Firebase\JWT\JWT::encode($payload, $shared_secret, 'HS256');
            
            if (array_key_exists('returnto', $_REQUEST)) {
            
                header('Location: ' . $_REQUEST['returnto'] . '&jwt=' . $_SESSION['kayako_token']);
                exit;
            
            } else {
            
                header("Location: " . DIR_URL . "/dashboard");
                exit;
            
            }

            $_SESSION['alert'] = $message;
            header("Location: " . DIR_URL . "/dashboard");
            exit;

        } else {
            $status = 0;
            $message = "An error occurred while validating your email.";
        }

        View::set("status", $status);
        View::set("message", $message);
        View::set("title", "Validate Email");
        View::render("validate-email");

    }
}
?>