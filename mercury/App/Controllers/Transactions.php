<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Etherscan,
    \App\Models\Request;

class Transactions {

    public function index() {

        if (!empty($_SESSION['id'])) {
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;

            View::set("ethusd", $ethusd);
            /* HEADER */

            $requests = Request::getRequestByPayer($_SESSION['id']);
            $unread = 0;
            for ($i=0; $i < count($requests); $i++) {
                if ($requests[$i]['viewed'] == 0) {
                    $unread = $unread + 1;
                }
            }

            $transactions = Etherscan::getlistTransactions($_SESSION['address']);
            $transactions = array_reverse($transactions);

            View::set("transactions", $transactions);
            View::set("title", "Transactions");
            View::set("requestsUnread", $unread);
            View::render("transactions");

        } else {
            header("Location: " . DIR_URL . "/login");
        }

    }

}
?>