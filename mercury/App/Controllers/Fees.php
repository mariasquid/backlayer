<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Etherscan;

class Fees {

    public function index() {

		$price = Etherscan::getLastPrice();
		$ethusd = $price->ethusd;

		View::set("ethusd", $ethusd);
        View::set("title", "Fees Mercury Cash");
        View::render("fees");

    }

}
?>