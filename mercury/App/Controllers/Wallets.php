<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Etherscan,
	\App\Models\Wallets as WalletsModel;

class Wallets {

    public function index() {

        if (!empty($_SESSION)) {
            $time = time() - $_SESSION['time'];
            if ($time >= 300) {
                header("Location: " . DIR_URL . "/login");
            } else {
                $_SESSION['time'] = time();
            }

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $ethusd = $price->ethusd;

            View::set("ethusd", "Buy 1 ETH = " . $ethusd);
            /* HEADER */

            $wallets = WalletsModel::getByUser($_SESSION['id']);

            View::set("wallets", $wallets);
            View::set("title", "My Wallets");
            View::render("user-wallets");

        } else {
            header("Location: " . DIR_URL . "/login");
        }

    }

    public function create() {

        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");

        if (!empty($_POST['json_wallet']) && !empty($_POST['private_key']) && !empty($_POST['public_key']) && !empty($_POST['address']) && !empty($_POST['user'])) {
            $result = WalletsModel::create($_POST);
            if ($result) {
                echo json_encode(array(
                    "status" => 1,
                    "description" => "You have successfully registered on our platform",
                    "redirect" => DIR_URL . "/dashboard",
                    "user" => $user,
                    "password" => $password
                    ));
            }
        }

    }

    public function editAlias() {

        if (!empty($_SESSION)) {
            header("Access-Control-Allow-Origin: *");
            header("Content-Type: application/json");

            $wallet = WalletsModel::read($_POST['id']);

            $data = array();

            $data['id'] = $_POST['id'];
            $data['json_wallet'] = $wallet['json_wallet'];
            $data['private_key'] = $wallet['private_key'];
            $data['public_key'] = $wallet['public_key'];
            $data['address'] = $wallet['address'];
            $data['alias'] = (!empty($_POST['alias'])) ? htmlspecialchars($_POST['alias'], ENT_QUOTES) : $data['alias'];
            $data['principal'] = $wallet['principal'];
            $data['user'] = $wallet['user'];

            $result = WalletsModel::update($data);
            if ($result) {
                echo json_encode(array(
                    "status" => 1,
                    "description" => "Alias successfully saved"
                    ));
            } else {
                echo json_encode(array(
                    "status" => 0,
                    "description" => "An error has occurred"
                    ));
            }
        } else {
            header("Location: " . DIR_URL . "/login");
        }

    }

    public function total() {

        if (!empty($_SESSION)) {
            
            $wallets = WalletsModel::getByUser($_SESSION['id']);

            if (count($wallets) <= 10) {
                echo json_encode(array(
                    "status" => 1,
                    "description" => "The wallet has been created successfully"
                ));
            } else {
                echo json_encode(array(
                    "status" => 0,
                    "description" => "Has reached the limit of wallets per user"
                    ));
            }

        } else {
            header("Location: " . DIR_URL . "/login");
        }

    }

}
?>