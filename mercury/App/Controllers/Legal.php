<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Etherscan;

class Legal {

    public function index() {

        session_destroy();

		$price = Etherscan::getLastPrice();
		$ethusd = $price->ethusd;

		View::set("ethusd", $ethusd);
        View::set("title", "Mercury Cash User Agreement");
        View::render("legal");

    }

}
?>