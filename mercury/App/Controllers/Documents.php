<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Documents as DocumentsModel,
	\App\Models\Users,
    \App\Controllers\Mailer;

class Documents {

	/*
    public function index() {

    	$pending = Documents::getByStatusAndUserStatus();

    	View::set("pending", $pending);
    	View::set("title", "Dashboard");
        View::render("dashboard");

    }
    */

    public function validate($id) {

        if (!empty($_SESSION)) {

        	$document = DocumentsModel::read($id);
        	$user = Users::read($document['user']);

        	View::set("document", $document);
        	View::set("user", $user);
        	View::set("title", "Validate document");
            View::render("validate_document");

        } else {
            header("Location: " . DIR_URL . "/home");
        }

    }

    public function evaluate($id, $user, $status) {

        if (!empty($_SESSION)) {

            $result = DocumentsModel::changeStatus($id, $status);

            $userData = Users::read($user);

            Mailer::validate_document($userData['email'], $status);

            if ($result) {
                if ($status == 1) {
                    $result = Users::changeStatus($user, 1);
                    if ($result) {
                        header("Location: " . DIR_URL . "/dashboard");
                    }
                }
                header("Location: " . DIR_URL . "/dashboard");
            }

        } else {
            header("Location: " . DIR_URL . "/home");
        }

    }

}
?>