<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Users,
    \App\Models\Limits,
    \App\Models\ETHWallets,
    \App\Models\USDWallets,
    \App\Models\Etherscan,
    \App\Models\Deposits,
    \App\Models\TransferValidation,
    \App\Models\Transfers,
    \App\Models\Favorites,
    \App\Models\Temporal,
    \App\Models\OldTransfer,
    \App\Models\Backup,
    \App\Controllers\Mailer;

class Test {

    public function statement() {
    
        require_once LIBSPATH . "/phpexcel/init.php";
        $usdwallets = USDWallets::getAll();

        $excel = \PHPExcel_IOFactory::load('template.xlsx');
        $excel->setActiveSheetIndex(0);
        
        $count = 3;
        foreach ($usdwallets as $usdwallet) {
            $excel->getActiveSheet()->insertNewRowBefore($count, 1);
            $excel->getActiveSheet()
                ->setCellValue('A' . $count, $usdwallet['id'])
                ->setCellValue('B' . $count, $usdwallet['name'])
                ->setCellValue('C' . $count, $usdwallet['last_name'])
                ->setCellValue('D' . $count, $usdwallet['balance']);
            $count++;
        }

        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

        // make table headers
        $excel->getActiveSheet()
            ->setCellValue('A2', 'ID')
            ->setCellValue('B2', 'NAME')
            ->setCellValue('C2', 'LAST NAME')
            ->setCellValue('D2', 'BALANCE');

        // marging the title
        $excel->getActiveSheet()->mergeCells('A1:D1');

        // aligning
        $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal('right');

        // styling
        $excel->getActiveSheet()->getStyle('A1')->applyFromArray(
            array(
                'font' => array(
                    'size' => 24
                )
            )
        );

        $excel->getActiveSheet()->getStyle('A2:D2')->applyFromArray(
            array(
                'font' => array(
                    'bold' => true
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );

        $date = date('Y_m_d');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="report_usdwallets_' . $date . '.xlsx"');  //File name extension was wrong
        header('Cache-Control: max-age=0');

        $file = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $file->save('php://output');

    
    }

    public function backup() {
    
        $data = file_get_contents("mining.csv");

        print_r($data);
    
    }

    public function oldtransfer() {
    
        $wallets = ETHWallets::getAll();
        foreach ($wallets as $wallet) {

            $transactions = Etherscan::getlistTransactions($wallet['address']);
            foreach ($transactions as $transaction) {
                
                $data = array();
                $data['value'] = $transaction->value / 1000000000000000000;
                $data['from_address'] = ($transaction->from == $wallet['address']) ? $transaction->from : $transaction->to;
                $data['to_address'] = ($transaction->to == $wallet['address']) ? $transaction->to : $transaction->to;
                $data['txhash'] = $transaction->hash;
                $data['start_date'] = date("Y-m-d H:i:s", $transaction->timeStamp);

                echo "<pre>";
                var_dump(OldTransfer::create($data));
                echo "</pre>";
                echo "<hr>";

            }
            
        }
    
    }

    public function balances() {
    
        $wallets = ETHWallets::getAll();
        foreach ($wallets as $wallet) {

            $balance = Etherscan::getBalance($wallet['address']);

            $data = array();
            $data['address'] = $wallet['address'];
            $data['balance'] = $balance;
            
            echo "<pre>";
            var_dump(Temporal::create($data));
            echo "</pre>";
            echo "<hr>";
            
        }
    
    }


    public function actualizar() {

        $url = "https://test.oppwa.com/v1/checkouts";
        $data = "authentication.userId=8a8294185f116e86015f255ae6d02a94" .
                "&authentication.password=2w5MEDz9tp" .
                "&authentication.entityId=8a8294185f116e86015f255b241b2a99" .
                "&amount=50.00" .
                "&currency=USD" .
                "&paymentType=DB";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);

        echo "<pre>";
        print_r($responseData);
        echo "</pre>";
    }

    public function checkout() {

        $data = '';
        //$url = 'http://167.114.81.219:8545';
        $url = 'http://192.95.5.106:8545';
        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $data);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);

    }

    public function respaldo(){
        $dbhost = 'localhost';
        $dbname = 'devmercu_devmercury';
        $dbuser = 'devmercu_devusr';
        $dbpass = 'Mercury2017';
        $date = date('Y-m-d');
        $time = date('H:i:s');
        $now = time();


         
        $backup_file = "backup/".$dbname."#".$date."T".$time. ".sql";
        echo $backup_file;
        // comandos a ejecutar
        $commands = array(
                "mysqldump --opt -h $dbhost -u $dbuser -p$dbpass -v $dbname > $backup_file",
              "bzip2 $backup_file"
        );
         
        // ejecución y salida de éxito o errores
        foreach ( $commands as $command ) {
            system($command,$output);
                //echo $output;
        }

        $ficheros = scandir("backup/", 1);
        foreach ($ficheros as $file) {
            $hora_archivo = filemtime("backup/".$file);
            $diferencia = $now - $hora_archivo;
            if ($diferencia > 432000) {
                unlink("backup/".$file);
            }
        }
        //var_dump($ficheros);
    }

}
?>