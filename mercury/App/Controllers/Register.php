<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Users,
    \App\Controllers\Mailer,
    \App\Models\Country,
    \App\Models\Cellphones,
    \App\Models\USDWallets,
    \App\Models\Limits,
    \App\Models\ETHWallets,
    \App\Models\Functions,
    \App\Models\CategoriesBusiness,
    \App\Models\SubcategoriesBusiness,
    \App\Models\Business,
    \App\Models\TransferValidation,
    \App\Models\Devices;

class Register {

    public function index() {

        $countrys = Country::getAll();

        View::set("title", "Register");
        View::set("countrys", $countrys);
        View::render("register");

    }

    public function business() {

        $countrys = Country::getAll();
        $categories = CategoriesBusiness::getAll();

        View::set("categories", $categories);
        View::set("countrys", $countrys);
        View::set("title", "Register");
        View::render("register_business");

    }

    public function subcategories($category) {

        if (is_numeric($category)) {
            
            $subcategories = SubcategoriesBusiness::getByCategory($category);
            echo json_encode($subcategories);

        }

    }


    public function create() {

        require_once LIBSPATH . "/recaptcha/recaptchalib.php";

        if (!empty($_POST['name']) && 
            !empty($_POST['email']) && 
            !empty($_POST['password']) &&
            !empty($_POST['type_user'])) {

            if (!empty($_POST['tems'])) {

                $response = NULL;
                $res = NULL;

                if (!empty($_POST['g-recaptcha-response'])) {

                    $secret = "6LftmSQUAAAAAM_9SAp4XlIKc_kBcEy6wr2eIoo7";
                    $reCaptcha = new \ReCaptcha($secret);
                    
                    $res = $reCaptcha->verifyResponse($_SERVER["REMOTE_ADDR"], $_POST["g-recaptcha-response"]);
                    
                    $response = ($res->success) ? $res->success : NULL;

                    if ($response) {

                        $validate = Users::readByEmail($_POST['email']);
                        
                        if (empty($validate)) {
                        
                            $password = $_POST['password'];

                            $data = array();
                            $data['name'] = ucwords(htmlspecialchars($_POST['name'], ENT_QUOTES));
                            $data['last_name'] = (!empty($_POST['last_name'])) ? ucwords(htmlspecialchars($_POST['last_name'], ENT_QUOTES)) : NULL;
                            $data['email'] = strtolower(htmlspecialchars($_POST['email'], ENT_QUOTES));
                            $data['phonecode'] = (!empty($_POST['code_country'])) ? htmlspecialchars($_POST['code_country'], ENT_QUOTES) : NULL;
                            $data['phone'] = (!empty($_POST['phone'])) ? str_replace("-", "", htmlspecialchars($_POST['phone'], ENT_QUOTES)) : NULL;
                            $data['password'] = md5($_POST['password']);
                            $data['country_id'] = (!empty($_POST['country']) && is_numeric($_POST['country'])) ? $_POST['country'] : 0;
                            $data['token'] = md5($_POST['email'] . time()); hash('sha256', md5($_POST['email'] . md5($_POST['password']) . time()));
                            $data['date_admission'] = date("Y-m-d H:i:s", time());
                            $data['type_user'] = ($_POST['type_user'] == 1) ? 1 : 2;
                            
                            if (!is_null($data['phone']) && !is_null($data['phonecode'])) {
                                
                                require_once LIBSPATH . "/authy/init.php";

                                $createAuthy = $authy_api->registerUser($data['email'], $data['phone'], $data['phonecode']);
                                
                                if ($createAuthy->ok()) {

                                    $data['authy_id'] = $createAuthy->id();

                                } else {

                                    setcookie("name", $data['name'], 0, "", "", FALSE, TRUE);
                                    setcookie("last_name", $data['last_name'], 0, "", "", FALSE, TRUE);
                                    setcookie("email", $data['email'], 0, "", "", FALSE, TRUE);
                                    setcookie("code_country", $data['code_country'], 0, "", "", FALSE, TRUE);
                                    setcookie("phone", $data['phone'], 0, "", "", FALSE, TRUE);
                                    
                                    foreach ($createAuthy->errors() as $field => $message) {

                                        $_SESSION['alert'] = $message;
                                        header("Location: " . DIR_URL . "/register");
                                        exit;
                                    }
                                    
                                }

                            }

                            $user = Users::create($data);

                            if ($user > 0) {

                                $data['trial_count'] = 0;
                                $data['amount'] = 0;
                                $data['user'] = $user;
                                TransferValidation::create($data);

                                $url = "https://node.mercury.cash/create";
                                $post = "user=" . $user . "&password=" . $password . "";
                                $con = curl_init();
                                curl_setopt($con, CURLOPT_URL, $url);
                                curl_setopt($con, CURLOPT_HTTPGET, FALSE);
                                curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
                                curl_setopt($con, CURLOPT_POST, 1);
                                curl_setopt($con, CURLOPT_POSTFIELDS, $post);
                                curl_setopt($con, CURLOPT_HEADER, FALSE);
                                curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
                                $response = curl_exec($con);
                                curl_close($con);
                                $response = json_decode($response);

                                if (!empty($response)) {

                                    $data['json_wallet'] = $response->json_wallet;
                                    $data['private_key'] = $response->private_key;
                                    $data['public_key'] = $response->public_key;
                                    $data['address'] = $response->address;
                                    $data['primary'] = 1;
                                    $data['user'] = $response->user;
                                    $ethwallet = ETHWallets::create($data);

                                }

                                $data['buy_eth_card'] = 200;
                                $data['buy_eth_ach'] = 500;
                                $data['buy_eth_usdwallet'] = 15000;
                                $data['sell'] = 15000;
                                $data['user'] = $user;

                                Limits::create($data);

                                setcookie("name", NULL, -1);
                                setcookie("last_name", NULL, -1);
                                setcookie("email", NULL, -1);
                                setcookie("code_country", NULL, -1);
                                setcookie("phone", NULL, -1);

                                $url = "http://newsletter.backlayer.com/api/v1/lists/594855c6230c9/subscribers/store?api_token=JxP8Rd2nV5UO1HTmQ3oETQXJK1DsklM2OMAsc5WN4tWTnjdvH5IPDFlbfx5w";
                                $dataCurl = 'EMAIL=' . $data['email'] . '&FIRST_NAME=' . $data['name'] . '&LAST_NAME=' . $data['last_name'];

                                $con = curl_init();
                                curl_setopt($con, CURLOPT_URL, $url);
                                curl_setopt($con, CURLOPT_HTTPGET, FALSE);
                                curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($dataCurl)));
                                curl_setopt($con, CURLOPT_POST, 1);
                                curl_setopt($con, CURLOPT_POSTFIELDS, $dataCurl);
                                curl_setopt($con, CURLOPT_HEADER, FALSE);
                                curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
                                $response = curl_exec($con);
                                curl_close($con);

                                $data['address'] = hash('sha256', md5($user . $data['email'] . time()));
                                $data['balance'] = 0.00;
                                $data['user'] = $user;

                                USDWallets::create($data);

                                $dataMail = array(
                                    "to" => $data['email'],
                                    "token" => $data['token']
                                    );

                                Mailer::signup($dataMail);

                                $details = json_decode(file_get_contents("http://freegeoip.net/json/" . $_SERVER['REMOTE_ADDR']));
                                $near = $details->country_name . ", " . $details->region_name;

                                $message = "<p>New Custumer in Mercury Cash</p>";
                                $message .= "<p>" . $data['name'] . " " . $data['last_name'] . "</p>";
                                $message .= "<p>" . $data['email'] . "</p>";
                                $message .= "<p><b>Near:</b> " . $near . "</p>";

                                Mailer::sendMail("registration@mercury.cash", "Mercury Cash - New Customer", $message, "Go to Mercury Cash", DIR_URL);

                                $time = 86400 * 15;
                                $date = time();
                                $data['disable_tfa'] = $date + $time;
                                $data['remember'] = 1;
                                $data['date'] = time();
                                $data['browser'] = $_SERVER['HTTP_USER_AGENT'];
                                $data['ip'] = $_SERVER['REMOTE_ADDR'];
                                $data['token'] = md5($data['browser'] . $data['ip'] . uniqid());
                                $data['status'] = 1;
                                $data['user'] = $user;

                                Devices::create($data);

                                $_SESSION['alert_success'] = "Thank you for register in Mercury Cash, The next step is validate your email, check your inbox and click on the validation link to continue.";
                                if ($_POST['type_user'] == 2) {
                                    header("Location: " . DIR_URL . "/register/business");
                                }else{
                                    header("Location: " . DIR_URL . "/register");
                                }
                                exit;

                            } else {

                                setcookie("name", $data['name'], 0, "", "", FALSE, TRUE);
                                setcookie("last_name", $data['last_name'], 0, "", "", FALSE, TRUE);
                                setcookie("email", $data['email'], 0, "", "", FALSE, TRUE);
                                setcookie("code_country", $data['code_country'], 0, "", "", FALSE, TRUE);
                                setcookie("phone", $data['phone'], 0, "", "", FALSE, TRUE);

                                $_SESSION['alert'] = "An error occurred while trying to register, please try again.";
                                header("Location: " . DIR_URL . "/register/business");
                                exit;
                            }

                        } else {

                            setcookie("name", $_POST['name'], 0, "", "", FALSE, TRUE);
                            //setcookie("last_name", $_POST['last_name'], 0, "", "", FALSE, TRUE);
                            setcookie("email", $_POST['email'], 0, "", "", FALSE, TRUE);
                            //setcookie("code_country", $_POST['code_country'], 0, "", "", FALSE, TRUE);
                            //setcookie("phone", $_POST['phone'], 0, "", "", FALSE, TRUE);

                            $_SESSION['alert'] = "E-mail already exists";
                            if ($_POST['type_user'] == 2) {
                                header("Location: " . DIR_URL . "/register/business");
                            }else{
                                header("Location: " . DIR_URL . "/register");
                            }
                            exit;

                        }

                    }

                } else {

                setcookie("name", $_POST['name'], 0, "", "", FALSE, TRUE);
                setcookie("email", $_POST['email'], 0, "", "", FALSE, TRUE);

                $_SESSION['alert'] = "Please select the captcha google to process your information";
                header("Location: " . DIR_URL . "/register");
                exit;

                }

            } else {

                setcookie("name", $_POST['name'], 0, "", "", FALSE, TRUE);
                setcookie("email", $_POST['email'], 0, "", "", FALSE, TRUE);

                $_SESSION['alert'] = "You must agree to Agreement and Privacy Policy";
                header("Location: " . DIR_URL . "/register");
                exit;

            }

        } else {

            setcookie("name", $_POST['name'], 0, "", "", FALSE, TRUE);
            setcookie("email", $_POST['email'], 0, "", "", FALSE, TRUE);

            if (empty($_POST['name'])) {

                setcookie("name", $_POST['name'], 0, "", "", FALSE, TRUE);
                setcookie("nameClass", TRUE, 0, "", "", FALSE, TRUE);

            } else {
                setcookie("nameClass", NULL, -1);
            }

            if (empty($_POST['email'])) {

                setcookie("email", $_POST['email'], 0, "", "", FALSE, TRUE);
                setcookie("emailClass", TRUE, 0, "", "", FALSE, TRUE);

            } else {
                setcookie("emailClass", NULL, -1);
            }

            if (empty($_POST['password'])) {

                setcookie("password", $_POST['password'], 0, "", "", FALSE, TRUE);
                setcookie("passClass", TRUE, 0, "", "", FALSE, TRUE);

            } else {
                setcookie("passClass", NULL, -1);
            }

            if (empty($_POST['confirmPassword'])) {

                setcookie("confirmPassword", $_POST['confirmPassword'], 0, "", "", FALSE, TRUE);
                setcookie("passClass", TRUE, 0, "", "", FALSE, TRUE);

            } else {
                setcookie("passClass", NULL, -1);
            }

            $_SESSION['alert'] = "There are empty fields that are required";
            if ($_POST['type_user'] == 2) {
                header("Location: " . DIR_URL . "/register/business");
            }else{
                header("Location: " . DIR_URL . "/register");
            }
            exit;

        }

    }    


    public function business_register() {

        require_once LIBSPATH . "/recaptcha/recaptchalib.php";

        if (!empty($_POST['name']) && 
            !empty($_POST['email']) && 
            !empty($_POST['password']) &&
            !empty($_POST['type_user']) &&
            !empty($_POST['business_name']) &&
            !empty($_POST['taxid_ein']) &&
            !empty($_POST['source_founds']) &&
            !empty($_POST['business_address'])) {

            if (!empty($_POST['tems'])) {

                $response = NULL;
                $res = NULL;

                if (!empty($_POST['g-recaptcha-response'])) {

                    $secret = "6LftmSQUAAAAAM_9SAp4XlIKc_kBcEy6wr2eIoo7";
                    $reCaptcha = new \ReCaptcha($secret);                    
                    $res = $reCaptcha->verifyResponse($_SERVER["REMOTE_ADDR"], $_POST["g-recaptcha-response"]);                   
                    $response = ($res->success) ? $res->success : NULL;

                    if ($response) {

                        $validate = Users::readByEmail($_POST['email']);
                        
                        if (empty($validate)) {
                        
                            $password = $_POST['password'];
                            $data = array();
                            $data['name'] = ucwords(htmlspecialchars($_POST['name'], ENT_QUOTES));
                            $data['last_name'] = (!empty($_POST['last_name'])) ? ucwords(htmlspecialchars($_POST['last_name'], ENT_QUOTES)) : NULL;
                            $data['email'] = strtolower(htmlspecialchars($_POST['email'], ENT_QUOTES));
                            $data['phonecode'] = (!empty($_POST['code_country'])) ? htmlspecialchars($_POST['code_country'], ENT_QUOTES) : NULL;
                            $data['phone'] = (!empty($_POST['phone'])) ? str_replace("-", "", htmlspecialchars($_POST['phone'], ENT_QUOTES)) : NULL;
                            $data['password'] = md5($_POST['password']);
                            $data['country_id'] = (!empty($_POST['country']) && is_numeric($_POST['country'])) ? $_POST['country'] : 0;
                            $data['token'] = md5($_POST['email'] . time()); hash('sha256', md5($_POST['email'] . md5($_POST['password']) . time()));
                            $data['date_admission'] = time();
                            $data['type_user'] = ($_POST['type_user'] == 1) ? 1 : 2;

                            $user = Users::create($data);

                            if ($user > 0) {

                                $data['trial_count'] = 0;
                                $data['amount'] = 0;
                                $data['user'] = $user;
                                TransferValidation::create($data);

                                $url = "https://node.mercury.cash/create";
                                $post = "user=" . $user . "&password=" . $password . "";
                                $con = curl_init();
                                curl_setopt($con, CURLOPT_URL, $url);
                                curl_setopt($con, CURLOPT_HTTPGET, FALSE);
                                curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
                                curl_setopt($con, CURLOPT_POST, 1);
                                curl_setopt($con, CURLOPT_POSTFIELDS, $post);
                                curl_setopt($con, CURLOPT_HEADER, FALSE);
                                curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
                                $response = curl_exec($con);
                                curl_close($con);
                                $response = json_decode($response);

                                if (!empty($response)) {

                                    $data['json_wallet'] = $response->json_wallet;
                                    $data['private_key'] = $response->private_key;
                                    $data['public_key'] = $response->public_key;
                                    $data['address'] = $response->address;
                                    $data['primary'] = 1;
                                    $data['user'] = $response->user;
                                    $ethwallet = ETHWallets::create($data);

                                }

                                $data['buy_eth_card'] = 200;
                                $data['buy_eth_ach'] = 500;
                                $data['buy_eth_usdwallet'] = 15000;
                                $data['sell'] = 15000;
                                $data['user'] = $user;

                                Limits::create($data);
                                        
                                $dataBusiness['name'] = htmlspecialchars($_POST['business_name'], ENT_QUOTES);
                                $dataBusiness['dba'] = htmlspecialchars($_POST['dba'], ENT_QUOTES);
                                $dataBusiness['taxid_ein'] = htmlspecialchars($_POST['taxid_ein'], ENT_QUOTES);
                                $dataBusiness['source_founds'] = htmlspecialchars($_POST['source_founds'], ENT_QUOTES);
                                $dataBusiness['address'] = htmlspecialchars($_POST['business_address'], ENT_QUOTES);
                                $dataBusiness['subcategory'] = "12";
                                $dataBusiness['user'] = $data['user'];

                                Business::create($dataBusiness);

                                $url = "http://newsletter.backlayer.com/api/v1/lists/594855c6230c9/subscribers/store?api_token=JxP8Rd2nV5UO1HTmQ3oETQXJK1DsklM2OMAsc5WN4tWTnjdvH5IPDFlbfx5w";
                                $dataCurl = 'EMAIL=' . $data['email'] . '&FIRST_NAME=' . $data['name'] . '&LAST_NAME=' . $data['last_name'];

                                $con = curl_init();
                                curl_setopt($con, CURLOPT_URL, $url);
                                curl_setopt($con, CURLOPT_HTTPGET, FALSE);
                                curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($dataCurl)));
                                curl_setopt($con, CURLOPT_POST, 1);
                                curl_setopt($con, CURLOPT_POSTFIELDS, $dataCurl);
                                curl_setopt($con, CURLOPT_HEADER, FALSE);
                                curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
                                $response = curl_exec($con);
                                curl_close($con);

                                $data['address'] = hash('sha256', md5($user . $data['email'] . time()));
                                $data['balance'] = 0.00;
                                $data['user'] = $user;

                                USDWallets::create($data);

                                $dataMail = array(
                                    "to" => $data['email'],
                                    "token" => $data['token']
                                    );

                                Mailer::signup($dataMail);

                                $details = json_decode(file_get_contents("http://freegeoip.net/json/" . $_SERVER['REMOTE_ADDR']));
                                $near = $details->country_name . ", " . $details->region_name;

                                $message = "<p>New Custumer in Mercury Cash</p>";
                                $message .= "<p>" . $data['name'] . " " . $data['last_name'] . "</p>";
                                $message .= "<p>" . $data['email'] . "</p>";
                                $message .= "<p><b>Near:</b> " . $near . "</p>";

                                Mailer::sendMail("registration@mercury.cash", "Mercury Cash - New Customer", $message, "Go to Mercury Cash", DIR_URL);

                                $time = 86400 * 15;
                                $date = time();
                                $data['disable_tfa'] = $date + $time;
                                $data['remember'] = 1;
                                $data['date'] = time();
                                $data['browser'] = $_SERVER['HTTP_USER_AGENT'];
                                $data['ip'] = $_SERVER['REMOTE_ADDR'];
                                $data['token'] = md5($data['browser'] . $data['ip'] . uniqid());
                                $data['status'] = 1;
                                $data['user'] = $user;

                                Devices::create($data);

                                $_SESSION['alert_success'] = "Thank you for register in Mercury Cash, The next step is validate your email, check your inbox and click on the validation link to continue.";
                                header("Location: " . DIR_URL . "/register/business");
                                exit;

                            } else {
                                $_SESSION['alert'] = "An error occurred while trying to register, please try again.";
                                header("Location: " . DIR_URL . "/register/business");
                                exit;
                            }
                        } else {
                            $_SESSION['alert'] = "E-mail already exists";
                            header("Location: " . DIR_URL . "/register/business");
                            exit;
                        }
                    }
                } else {
                    $_SESSION['alert'] = "Please select the captcha google to process your information";
                    header("Location: " . DIR_URL . "/register/business");
                    exit;
                }
            } else {
                $_SESSION['alert'] = "You must agree to Agreement and Privacy Policy";
                header("Location: " . DIR_URL . "/register/business");
                exit;
            }
        } else {
            $_SESSION['alert'] = "There are empty fields that are required";
            header("Location: " . DIR_URL . "/register/business");
            exit;
        }
    }


}
?>