<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\ETHWallets as ETHWalletsModel;

class Ethwallets {

    public function create() {

    	header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");

        if (!empty($_POST['json_wallet']) && !empty($_POST['private_key']) && !empty($_POST['public_key']) && !empty($_POST['address']) && !empty($_POST['user'])) {

            $data['json_wallet'] = $_POST['json_wallet'];
            $data['private_key'] = $_POST['private_key'];
            $data['public_key'] = $_POST['public_key'];
            $data['address'] = $_POST['address'];
            $data['primary'] = 1;
            $data['user'] = $_POST['user'];
            
            $result = ETHWalletsModel::create($data);
            
            if ($result) {
            
                echo json_encode(array(
                    "status" => 1,
                    "description" => "You have successfully registered on our platform",
                    "redirect" => DIR_URL . "/dashboard",
                    "user" => $user,
                    "password" => $password
                    ));
            
            }
            
        } else {

            echo json_encode(array(
                "status" => 0,
                "description" => "Error!"
                ));

        }

    }

}
?>