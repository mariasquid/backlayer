<?php
namespace App\Interfaces;
defined("APPPATH") OR die("Access denied");

interface Crud {
    public static function create($data);
    public static function read($id);
    public static function update($data);
    public static function delete($id);

    public static function getAll();
}
?>