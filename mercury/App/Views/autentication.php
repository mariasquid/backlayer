<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/login.js?v=<?php echo time(); ?>"></script>
	</head>
	<body style="background-color: #f5f5f5;">
		<div class="wrapper">
			<div class="main">
				<div class="content">
					<div class="container">
						<div class="row">
							<div class="col-md-4 col-md-offset-4">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<img src="<?php echo DIR_URL; ?>/public/img/logo-dark.png" alt="" class="img-responsive">
									</div>
								</div>
								<div class="card" style="margin-top: 50px;">
									<div class="header">
										<h4 class="title">Validation Code</h4>
									</div>
									<div class="content">
										<form method="POST" action="<?php echo DIR_URL; ?>/login/validate" id="login-form" autocomplete="off">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label>Verification code</label>
														<input type="text" class="form-control" id="code" name="code" placeholder="Verification code">
													</div>
												</div>
											</div>
											<button type="button" id="enter" class="btn btn-info btn-fill pull-right">Validate</button>
											<div class="clearfix"></div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>