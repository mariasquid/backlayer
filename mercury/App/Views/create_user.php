<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/media/css/dataTables.bootstrap.css">
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/media/css/dataTables.plugins.css">
	</head>
	<body class="dashboard-page sb-l-o sb-r-c">
		<div id="main">
			<?php include "inc/header.php"; ?>
			<?php include "inc/aside.php"; ?>
			<div id="content_wrapper">
				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<div class="panel">
								<div class="panel-heading">
									<span class="panel-title">Create User</span>
								</div>
								<div class="panel-body">
									<form class="form-horizontal" action="<?php echo DIR_URL; ?>/users/create" method="POST" id="register-form" role="form" autocomplete="off">
										<div class="form-group">
											<label for="name" class="col-lg-3 control-label">First Name</label>
											<div class="col-lg-8">
												<div class="bs-component">
													<input type="text" id="name" name="name" class="form-control" placeholder="First name...">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label for="last_name" class="col-lg-3 control-label">Last Name</label>
											<div class="col-lg-8">
												<div class="bs-component">
													<input type="text" id="last_name" name="last_name" class="form-control" placeholder="Last Name...">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label for="email" class="col-lg-3 control-label">Email</label>
											<div class="col-lg-8">
												<div class="bs-component">
													<input type="email" id="email" name="email" class="form-control" placeholder="Email...">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label for="password" class="col-lg-3 control-label">Temporay Password</label>
											<div class="col-lg-8">
												<div class="bs-component">
													<input type="password" id="password" name="password" class="form-control" placeholder="Password...">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-lg-4 col-lg-offset-4">
												<button type="submit" class="btn btn-system btn-block">Create user</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="https://node.mercury.cash/socket.io/socket.io.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/register.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				"use strict";
				Core.init();
			});
		</script>
	</body>
</html>