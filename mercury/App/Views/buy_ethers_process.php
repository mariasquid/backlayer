<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <?php include "inc/scripts.php"; ?>
    </head>
    <body>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <div class="content">
                    <div class="container-fluid">
                        <div class="alert alert-<?php echo ($status == "success") ? 'info' : 'danger'; ?> alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="fa fa-check pr10"></i>
                            <?php if ($status == "success") { ?>
                            <strong>In process!</strong> If purchase is being processed, you will soon receive an email with information on the status of your purchase
                            <?php } else { ?>
                            <strong>Error!</strong> To buy ethereum, the minimum amount is $1
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>