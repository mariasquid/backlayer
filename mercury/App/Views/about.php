<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title>1 ETH = $<?php echo $ethusd;?> Mercury Cash: Ethereum Wallet, ETH/USD Market</title>
        <meta name="description" content="Mercury cash is a specialized and secure platform that has its own ethereum wallet and market exchange for ETH/USD">
        <meta name="keywords" content="Ethereum Wallet">
        <meta name="robots" content="index, follow">
        <meta name="copyright" content="Adventurous Entertainment LLC">
        <meta name="language" content="EN">
        <meta name="author" content="Adventurous Entertainment LLC">
        <meta name="creationdate" content="May 2017">
        <meta name="distribution" content="global">
        <meta name="rating" content="general">
        <link rel="Publisher" href="https://www.adenter.io">
        <link rel="Canonical" href="<?php echo DIR_URL; ?>">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo DIR_URL; ?>/public/css/theme.css" rel="stylesheet">
        <link href="<?php echo DIR_URL; ?>/public/css/about.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700'>
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
        <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic'>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/fonts/elegant/style.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/css/social.css" rel="stylesheet">
        <!-- Multiple Favicon 14-5-2017 -->
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#f3c805">
        <meta name="apple-mobile-web-app-title" content="Mercury Cash">
        <meta name="application-name" content="Mercury Cash">
        <meta name="msapplication-TileColor" content="#2f3740">
        <meta name="msapplication-TileImage" content="/mstile-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Live chat -->
        <script type="text/javascript">
        window.__lo_site_id = 80707;
        (function() {
        var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
        wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
        })();
        </script>
        <!-- Google Analytics 14-5-2017 -->
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-98285209-1', 'auto');
        ga('send', 'pageview');
        </script>
    </head>
    <body id="page-top" class="index">
        <!-- Navigation -->
        <nav class="navbar navbar-default">
            <!-- Topbar Nav (hidden) -->
            <div class="container" style="max-width: 1050px;">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand page-scroll" href="<?php echo DIR_URL; ?>">
                        <img id="logo-navbar" src="<?php echo DIR_URL; ?>/public/img/logo-ligth.png" class="img-responsive" alt="">
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="<?php echo DIR_URL; ?>" class="page-scroll">1 ETH = $<?php echo $ethusd;?></a>
                        </li>
                        <li>
                            <a href="<?php echo DIR_URL; ?>">HOME</a> </li>
                            <li>
                                <a href="<?php echo DIR_URL; ?>/login" class="page-scroll">SIGN IN</a>
                            </li>
                            <li>
                                <a href="<?php echo DIR_URL; ?>/register" class="page-scroll">CREATE AN ACCOUNT</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
            <!-- Hero Content -->
            <header id="hero1">
                <div class="container">
                    <div class="intro-text">
                        <div class="intro-lead-in">ABOUT OUR COMPANY</div>
                        <div class="intro-heading">GET TO KNOW US BETTER</div>
                        
                        <!-- <a href="#services" class="page-scroll btn btn-xl mr30 btn-primary">Learn More</a>
                        <a href="#contact" class="page-scroll btn btn-xl btn-wire">Contact Us</a> -->
                    </div>
                </div>
            </header>
            <!-- Services Section -->
            <section id="services">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-left">
                            <h2 class="subtitles_tatement1">WHO WE ARE AND WHAT WE DO</h2>
                            <p class="statement1">&nbsp;&nbsp;&nbsp;&nbsp;Headquartered in Orlando, Florida since 2016, Mercury Cash is a trading platform for cryptocurrencies, aimed to create a regulated bank for the new digital generation.
                            <br><br>&nbsp;&nbsp;&nbsp;&nbsp;Mercury Cash is a project that will lead people out in the investment, adoption and usage of cryptocurrencies with a series of innovative strategies.  If you want to be an active part of the financial revolution the world will go through, then follow us and be prepared.</p>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-left">
                            <h2 class="subtitles_tatement1">OUR MISSION</h2>
                            <p class="statement1">&nbsp;&nbsp;&nbsp;&nbsp;Our Mission is to develop financial solutions for the common citizens to excel their financial capacities, promoting new and reliable ways for savings, faster and cheaper ways for trading in their daily basis and with more secure and safest ways of investment using cutting edge financial technologies.</p>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-left">
                            <h2 class="subtitles_tatement1">ABOUT THE TEAM</h2>
                            <p class="statement1">&nbsp;&nbsp;&nbsp;&nbsp;Mercury Cash´s shareholders comes from a large experience in the economic field. Giving birth to Mercury Cash as the expressed evolution of all of their ideas, innovations and willing to help improving world current situation, combining new technologies and vision of the future with business and services. Our staff is our most important asset, but our customers, clients and vendors are the core of all of our ideas and decisions. We are willing to generate confidence and security in every service.</p>
                        </div>
                    </div>
                </div>
            </section>                             
            <section id="values">
                <div class="info">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <h2>Our Values</h2>
                                <p>We understand values as everything that preserves the integrity of Mercury Cash family and enhancement of our services.</p>
                                <p class="info-text">That´s why we are identified with the following values:</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="slider" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#slider" data-slide-to="0" class="active"></li>
                        <li data-target="#slider" data-slide-to="1"></li>
                        <li data-target="#slider" data-slide-to="2"></li>
                        <li data-target="#slider" data-slide-to="3"></li>
                        <li data-target="#slider" data-slide-to="4"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="<?php echo DIR_URL; ?>/public/img/about/clock-1461689_1920.jpg" alt="...">
                            <div class="carousel-caption">
                                <h2>Commitment</h2>
                                <p class="hidden-xs hidden-sm">We like been a trusty mean of investment for our clients. That´s why we guarantee following rules and regulations that protects our customers and their assets.</p>
                                <p class="related hidden-xs hidden-sm"><b>Related Values:</b> Responsibility, efficiency, effectiveness, passion, discipline and perseverance.</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="<?php echo DIR_URL; ?>/public/img/about/electricity-1835546_1920.jpg" alt="...">
                            <div class="carousel-caption">
                                <h2>Innovation</h2>
                                <p class="hidden-xs hidden-sm">in a world where changes are the only constant, we are commit to our clients in offering updated and original tools for them to invest their money and be adapted to a whole new Cryptocurrency era.</p>
                                <p class="related hidden-xs hidden-sm"><b>Related Values:</b> Creativity, technology, challenge, ambition and strategic thinking.</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="<?php echo DIR_URL; ?>/public/img/about/lego-1044891_1920.jpg" alt="...">
                            <div class="carousel-caption">
                                <h2>Equality</h2>
                                <p class="hidden-xs hidden-sm">we believe in a fair world, where everyone has the same opportunities of success. Thus, we help people out being wealthy while becoming part of this excitement new era.</p>
                                <p class="related hidden-xs hidden-sm"><b>Related Values:</b> Opportunity, fairness, cooperation and inclusiveness.</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="<?php echo DIR_URL; ?>/public/img/about/office-594132_1920.jpg" alt="...">
                            <div class="carousel-caption">
                                <h2>Organization</h2>
                                <p class="hidden-xs hidden-sm">we believe synergy as the most powerful mean to achieve success as we become one with our clients. And this will be possible only by tracing plans, methods and gathering the best team to make our vision possible.</p>
                                <p class="related hidden-xs hidden-sm"><b>Related Values:</b> Teamwork, unity, strength, communication and quality service.</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="<?php echo DIR_URL; ?>/public/img/about/achieve-2203653_1920.jpg" alt="...">
                            <div class="carousel-caption">
                                <h2>Achievement</h2>
                                <p class="hidden-xs hidden-sm">the way we measure our values is as we accomplish all our goals.  Excel is the manifestation of our promises as our integrity and values are kept.</p>
                                <p class="related hidden-xs hidden-sm"><b>Related Values:</b> Courage, Enthusiasm and Excellence.</p>
                            </div>
                        </div>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#slider" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#slider" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </section>
            <div id="timeline" class="bg-light">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2 class="section-heading" style="color: #fff;"><strong> TIMELINE <strong> </h2>
                            <h3 class="section-subheading text-muted">OUR GROUP OF EXPERTS IS OUR MOST VALUABLE ASSET.</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="timeline">
                                <span class="spectre-highlight"></span>
                                <div class="timeline">
                                    <div class="container_timeline left_timeline">
                                        <div class="content_timeline">
                                            <h2>Sep 4th 2017</h2>
                                            <p>Looking next financial round seed capital</p>
                                        </div>
                                        <div class="first_timeline"></div>
                                    </div>
                                    <div class="container_timeline right_timeline">
                                        <div class="content_timeline">
                                            <h2>Aug 21th 2017</h2>
                                            <p>Mercury Cash Launch US</p>
                                        </div>
                                    </div>
                                    <div class="container_timeline left_timeline">
                                        <div class="content_timeline">
                                            <h2>Aug 17th 2017</h2>
                                            <p>Get Money transmitters License</p>
                                        </div>
                                    </div>
                                    <div class="container_timeline right_timeline">
                                        <div class="content_timeline">
                                            <h2>Aug 1st 2017</h2>
                                            <p>APP 1.0 Release for Android</p>
                                        </div>
                                    </div>
                                    <div class="container_timeline left_timeline">
                                        <div class="content_timeline">
                                            <h2>May 15th 2017</h2>
                                            <p>Mercury cash Open beta</p>
                                        </div>
                                    </div>
                                    <div class="container_timeline right_timeline">
                                        <div class="content_timeline">
                                            <h2>Feb 12th 2017</h2>
                                            <p>Run 8.2 Gh/s Ethereum Farm</p>
                                        </div>
                                    </div>
                                    <div class="container_timeline left_timeline">
                                        <div class="content_timeline">
                                            <h2>July 1st 2016</h2>
                                            <p>Raised $200,000.00 Seed Capital I</p>
                                        </div>
                                    </div>
                                    <div class="container_timeline right_timeline">
                                        <div class="content_timeline">
                                            <h2>Jun 28th 2016</h2>
                                            <p>Company founded</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section id="team" class="bg-light">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2 class="section-heading"><strong> THE TEAM YOU NEED <strong> </h2>
                            <h3 class="section-subheading text-muted">OUR GROUP OF EXPERTS IS OUR MOST VALUABLE ASSET.</h3>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="team-member">
                            <img src="<?php echo DIR_URL; ?>/public/img/team/2.jpg" class="img-responsive" alt="">
                            <h4>Victor Romero</h4>
                            <p class="text-muted">CCO</p>
                            <ul class="list-inline">
                                
                                <li><a href="https://www.linkedin.com/in/victorhugoromeromelendez/" target="_blank"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="team-member">
                        <img src="<?php echo DIR_URL; ?>/public/img/team/4.jpg" class="img-responsive" alt="">
                        <h4>Giordano Lugo</h4>
                        <p class="text-muted">Creative Director</p>
                        <ul class="list-inline">
                            
                            <li><a href="https://www.linkedin.com/in/giordano-lugo/" target="_blank"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="<?php echo DIR_URL; ?>/public/img/team/3.jpg" class="img-responsive" alt="">
                    <h4>Marco Pirrongelli</h4>
                    <p class="text-muted">CTO</p>
                    <ul class="list-inline">
                        
                        <li><a href="https://www.linkedin.com/in/mpirrongelli" target="_blank"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="<?php echo DIR_URL; ?>/public/img/team/1.jpg" class="img-responsive" alt="">
                    <h4>Amy Hunt</h4>
                    <p class="text-muted">Strategy Planner</p>
                    <ul class="list-inline">
                        
                        <li><a href="https://www.linkedin.com/in/-amy-hunt/" target="_blank"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="team-member">
                <img src="<?php echo DIR_URL; ?>/public/img/team/5.jpg" class="img-responsive" alt="">
                <h4>Ruben Velez</h4>
                <p class="text-muted">Advisor</p>
                <ul class="list-inline">
                    
                    <li><a href="https://www.linkedin.com/in/ruben-velez-72975a11a/" target="_blank"><i class="fa fa-linkedin"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="team-member">
            <img src="<?php echo DIR_URL; ?>/public/img/team/6.jpg" class="img-responsive" alt="" width="150">
            <h4>Carlos Arias</h4>
            <p class="text-muted">Executive Assistant</p>
            <ul class="list-inline">
                
                <li><a href="https://www.linkedin.com/in/carlosariasreggeti/" target="_blank"><i class="fa fa-linkedin"></i></a>
            </li>
        </ul>
    </div>
</div>
<div class="col-sm-4">
    <div class="team-member">
        <img src="<?php echo DIR_URL; ?>/public/img/team/7.jpg" class="img-responsive" alt="" width="150">
        <h4>Rainiero Montilva</h4>
        <p class="text-muted">Executive Assistant</p>
        <ul class="list-inline">
            
            <li><a href="https://www.linkedin.com/in/rainiero-montilva-castillo-2a4715146/" target="_blank"><i class="fa fa-linkedin"></i></a>
        </li>
    </ul>
</div>
</div>
</div>
</div>
</section>
<!-- Contact Section -->
<section id="contact">
<div class="container">
<div class="row">
<div class="col-lg-12 text-center">
<h2 class="section-heading">Get in touch and explore the possibilities!!</h2>
</div>
</div>
<form name="sentMessage" id="contactForm" class="mw800 center-block clearfix" novalidate>
<div class="form-group">
<input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
<p class="help-block text-danger"></p>
</div>
<div class="form-group">
<input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
<p class="help-block text-danger"></p>
</div>
<div class="form-group">
<button type="submit" class="btn btn-xl btn-block btn-wire">Subscribe</button>
</div>
</form>
<p> By subscribing you agree to our terms and conditions. </p>
</div>
</section>
<!-- Footer -->
<?php include "inc/footer-homes.php"; ?>
<!-- jQuery -->
<!-- <script src="js/vendor/jquery.js"></script> -- Local Version -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<!-- <script src="js/vendor/bootstrap.min.js"></script> -- Local Version -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- Contact Form JavaScript -->
<script src="<?php echo DIR_URL; ?>/public/js/vendor/jqBootstrapValidation.js"></script>
<script src="<?php echo DIR_URL; ?>/public/js/contact_me.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo DIR_URL; ?>/public/js/main.js"></script>
<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/js/about.js"></script>
<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/device.js"></script>
</body>
</html>