<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <?php include "inc/scripts.php"; ?>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jquery.numeric.min.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/send.js?v=<?php echo time(); ?>"></script>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            var ethusd = <?php echo $ethusd; ?>;
            var usd = 0;
            var eth = 0;
            $('#amount').keyup(function(event) {
                usd = ($(this).val() * ethusd);
                $('#amount_usd').val(usd.toFixed(2));
                $('#total').val((usd + 0.02).toFixed(2) + '$');
            });
            $('#amount_usd').keyup(function(event) {
                usd = Number($(this).val());
                eth = ($(this).val() / ethusd);
                $('#amount').val(eth);
                $('#total').val((usd + 0.02).toFixed(2) + '$');
            });
            if (location.pathname == "/send&1") {
                $('#myModal').modal();
                $('#myModal .modal-body').html("We have registered the new address, you can transfer your ETHs when you want");
                $('#myModal').on('hidden.bs.modal', function(e) {
                    location.href = dir_url + '/send';
                });
            }
        });
        </script>
    </head>
    <body>
        <?php include "inc/trialInfoModal.php"; ?>
        <!-- Modal -->
        <div class="modal fade" id="removeAddress" tabindex="-1" role="dialog" aria-labelledby="removeAddressLabel" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="removeAddressLabel">Attention</h4>
                    </div>
                    <div class="modal-body" style="text-align: center;">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="removeAddressButton" class="btn btn-default">Yes</button>
                        <button type="button" class="btn btn-default closeModal" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Attention</h4>
                    </div>
                    <div class="modal-body" style="text-align: center;">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default closeModal" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="save_address" tabindex="-1" role="dialog" aria-labelledby="save_addressLabel" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="<?php echo DIR_URL; ?>/send/saveAddress" id="saveAddress" method="POST" autocomplete="off">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="save_addressLabel">Add new address</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="address">Address or Email:</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="Address or Email" style="padding-right: 45px;">
                                <img src="public/img/MercuryLogo.png" id="ico_favorite" class="ico" alt="">
                            </div>
                            <div class="form-group">
                                <label for="description">Description:</label>
                                <input type="text" class="form-control" id="description" name="description" placeholder="Description">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="token" value="<?php echo $_SESSION['validateToken']; ?>">
                            <button type="submit" class="btn btn-default closeModal">Add</button>
                            <button type="button" class="btn btn-default closeModal" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="loading">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <div class="content">
                    <div class="container-fluid">
                        <div class="tabs-m">
                            <ul>
                                <li class="active"><a href="<?php echo DIR_URL; ?>/send">Send</a></li>
                                <li><a href="<?php echo DIR_URL; ?>/request">Request</a></li>
                            </ul>
                        </div>
                        <div class="block">
                            <div class="block-header">
                                <span class="text-yellow">Transfer ethereum</span>
                            </div>
                            <div class="block-body">
                                <?php if (!empty($_SESSION['alert'])) { ?>
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <div class="alertGrey">
                                            <?php echo $_SESSION['alert']; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php unset($_SESSION['alert']); ?>
                                <?php } ?>
                                <form action="<?php echo DIR_URL; ?>/send/createTransaction" id="send" method="POST" autocomplete="off">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-elements">
                                                <label>From</label>
                                                <!--<input type="text" name="from" id="from" value="Mercury Cash ETH Wallet | <?php // echo $ethwallet['balance']; ?> ETH" disabled="disabled" readonly="readonly">-->
                                                <select name="from" id="from">
                                                    <option value="">Select the address from transfer</option>
                                                    <?php foreach ($ethwallets as $ethwallet) { ?>
                                                    <option value="<?php echo $ethwallet['id']; ?>"><?php echo $ethwallet['alias']; ?> | <?php echo $ethwallet['wallet_balance']; ?> ETH</option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-elements">
                                                <label>To</label>
                                                <ul class="between_wallets">
                                                    <li class="active"><a class="active" id="a_to_favorites" data-select="to_favorites" href="#">Favorites</a></li>
                                                    <li><a id="a_to_myaddress" data-select="to_myaddress" href="#">My Wallets</a></li>
                                                </ul>
                                                <select name="to_favorites" id="to_favorites">
                                                    <option value="">Select the address to transfer</option>
                                                    <?php foreach ($favorites as $favorite) { ?>
                                                    <option value="<?php echo $favorite['id']; ?>"><?php echo $favorite['description']; ?> - <?php echo $favorite['address']; ?></option>
                                                    <?php } ?>
                                                    <option value="new">Add new address</option>
                                                </select>
                                                <select name="to_myaddress" id="to_myaddress">
                                                    <option value="">Select the address to transfer</option>
                                                    <?php foreach ($ethwallets as $ethwallet) { ?>
                                                    <option value="<?php echo $ethwallet['id']; ?>"><?php echo $ethwallet['alias']; ?> | <?php echo $ethwallet['wallet_balance']; ?> ETH</option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-elements">
                                                <label>ETH Amount</label>
                                                <input type="text" id="eth_amount" name="eth_amount" placeholder="ETH Amount">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-elements">
                                                <label>USD Amount</label>
                                                <input type="text" id="usd_amount" name="usd_amount" placeholder="USD Amount">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="content">
                                            <div class="col-md-2 col-md-offset-10">
                                                <div class="form-elements">
                                                    <span class="input-addon">
                                                        Total
                                                    </span>
                                                    <input type="text" id="total" name="total" readonly="readonly"  class="input-addon" value="$0.00" placeholder="Fee" aria-label="...">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="content">
                                            <div class="col-md-2 col-md-offset-10">
                                                <input type="hidden" name="token" value="<?php echo $_SESSION['validateToken']; ?>">
                                                <button type="submit" id="transfer" class="btn-m-l">Transfer</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row hidden-xs hidden-sm">
                            <div class="col-md-12">
                                <div class="block" style="margin-top: 40px;">
                                    <div class="block-header">
                                        <span class="text-yellow">My favorite wallets</span>
                                    </div>
                                    <div class="block-body">
                                        <table class="table-m">
                                            <thead>
                                                <tr>
                                                    <th>Description</th>
                                                    <th>Address</th>
                                                    <td></td>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($favorites as $favorite) { ?>
                                                <tr>
                                                    <td><?php echo $favorite['description']; ?></td>
                                                    <td><?php echo $favorite['address']; ?></td>
                                                    <td><?php echo ($favorite['is_mercury'] == 1) ? '<img src="public/img/MercuryLogo.png" width="32" alt="">' : '<img src="public/img/ETHEREUM-ICON_Black.png" width="32" alt="">'; ?></td>
                                                    <td><a href="<?php echo DIR_URL; ?>/send/deleteFavorite/<?php echo $favorite['id'] ?>" class="deleteAddress" data-description="<?php echo $favorite['description']; ?>" data-address="<?php echo $favorite['address']; ?>" class="use"><i class="pe-7s-trash" style="color: #d9534f; font-size: 2.8rem;"></i></a></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "inc/close.php"; ?>
    </body>
</html>