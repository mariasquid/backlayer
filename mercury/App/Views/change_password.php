<!DOCTYPE html>

<html lang="es">

    <head>

        <meta charset="UTF-8">

        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

        <title><?php echo $title; ?></title>

        <?php include "inc/styles.php"; ?>

        <?php include "inc/scripts.php"; ?>

        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/settings.js?v=<?php echo time(); ?>"></script>

    </head>

    <body>

        <?php if ($_SESSION['status'] == 3) { ?>

        <script type="text/javascript">

            $(document).ready(function() {

                $('#validateDocument').modal();

            });

        </script>

        <!-- Modal -->

        <div class="modal fade" id="validateDocument" tabindex="-1" role="dialog" aria-labelledby="validateDocumentLabel" data-target=".bs-example-modal-sm">

            <div class="modal-dialog" role="document">

                <div class="modal-content">

                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                        <h4 class="modal-title" id="validateDocumentLabel">Attention</h4>

                    </div>

                    <div class="modal-body" style="text-align: center;">

                        <p>In order to verify your account, please upload your Driver's License (US Citizens Only) or your Passport.</p>

                        <p>Example of Selfie's ID Verification</p>

                        <div class="row">

                            <div class="col-md-6">

                                <img src="<?php echo DIR_URL; ?>/public/img/do1.png" alt="" class="img-responsive">

                            </div>

                            <div class="col-md-6">

                                <img src="<?php echo DIR_URL; ?>/public/img/do2.png" alt="" class="img-responsive">

                            </div>

                        </div>

                    </div>

                    <div class="modal-footer">

                        <a href="<?php echo DIR_URL; ?>/settings/document" class="btn btn-default">Validate Now</a>

                    </div>

                </div>

            </div>

        </div>

        <?php } ?>

        <div class="wrapper">

            <?php include "inc/aside.php"; ?>

            <div class="main-panel">

                <?php include "inc/header.php"; ?>

                <div class="content">

                    <div class="container-fluid">

                        <div class="row">

                            <div class="col-lg-10 col-lg-offset-1">

                                <div class="tabs-m">

                                    <ul>

                                        <li><a href="<?php echo DIR_URL; ?>/settings">Personal information</a></li>

                                        <?php echo ($type_user == 2) ? '<li><a href="' . DIR_URL . '/settings/businessInfo">Business information</a></li>' : ''; ?>

                                        <li><a href="<?php echo DIR_URL; ?>/settings/document">Document</a></li>

                                        <li><a href="<?php echo DIR_URL; ?>/settings/bank">Your Financial Information</a></li>

                                        <li class="active"><a href="<?php echo DIR_URL; ?>/settings/password">Change Password</a></li>

                                        <li><a href="<?php echo DIR_URL; ?>/settings/security">Two Factor Authenticator</a></li>

                                    </ul>

                                </div>

                                <div class="block">

                                    <div class="block-header">

                                        <span class="text-yellow">Edit profile</span>

                                    </div>

                                    <div class="block-body">

                                        <form action="<?php echo DIR_URL; ?>/settings/changePassword" id="change-password" method="POST" autocomplete="off">

                                            <div class="row">

                                                <div class="col-md-4 col-md-offset-4">

                                                    <?php if (!empty($_SESSION['message'])) { ?>

                                                    <div class="row">

                                                        <div class="col-md-12">

                                                            <div class="alertGrey">

                                                                <?php echo $_SESSION['message']; ?>

                                                                <?php unset($_SESSION['message']) ?>

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <?php } ?>

                                                    <div class="row">

                                                        <div class="col-md-12">

                                                            <div class="form-elements">

                                                                <label for="name">Old password</label>

                                                                <input type="password" id="old_password" name="old_password">

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="row">

                                                        <div class="col-md-12">

                                                            <div class="form-elements">

                                                                <label for="last_name">New password</label>

                                                                <input type="password" id="new_password" name="new_password">

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="row">

                                                        <div class="col-md-12">

                                                            <div class="form-elements">

                                                                <label for="email">Repeat new password</label>

                                                                <input type="password" id="repeat_password" name="repeat_password">

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="row">

                                                        <div class="col-md-8 col-md-offset-3">

                                                            <input type="hidden" name="token" value="<?php echo $_SESSION['validateToken']; ?>">

                                                            <button type="submit" class="btn-mercury-yellow">UPDATE</button>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </form>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <?php include "inc/close.php"; ?>

    </body>

</html>