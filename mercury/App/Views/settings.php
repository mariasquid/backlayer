<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/assets/css/jasny-bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/assets/css/cropper.css">
        <?php include "inc/scripts.php"; ?>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jasny-bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jquery.maskedinput.min.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/cropper.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/settings.js?v=<?php echo time(); ?>"></script>
        <script type="text/javascript">
        $(document).ready(function() {
            if ($('#type_document_s').val() == 2) {
                $("#ssn").mask("***-**-****");
            } else {
                $("#ssn").mask();
            }

            $('#type_document_s').change(function(event) {
                if ($('#type_document_s').val() == 2) {
                    $("#ssn").mask("***-**-****");
                } else {
                    $("#ssn").mask();
                }
            });

            $("#birthdate").mask("9999-99-99", {
                placeholder:"YYYY-MM-DD"
            });
        });
        </script>
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="changeAvatar" tabindex="-1" role="dialog" aria-labelledby="changeAvatarLabel" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="changeAvatarLabel">Update Avatar</h4>
                    </div>
                    <div class="modal-body" style="text-align: center;">
                        <div id="alertModal"></div>
                        <div id="result" class="contentNewAvatar">
                            <!--<img src="folder/photo.jpg" id="image" style="width: 100%;" alt="Press Upload">-->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="file" name="avatar" id="inputavatar">
                        <button type="button" id="uploadBtn" class="btn btn-default">Upload</button>
                        <button type="button" id="button" class="btn btn-default" style="position: relative; z-index: 999;">Finish</button>
                    </div>
                </div>
            </div>
        </div>
        <?php include "inc/trialInfoModal.php"; ?>
        <div class="loading">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1">
                                <div class="tabs-m">
                                    <ul>
                                        <li class="active"><a href="<?php echo DIR_URL; ?>/settings">Personal information</a></li>
                                        <?php echo ($type_user == 2) ? '<li><a href="' . DIR_URL . '/settings/businessInfo">Business information</a></li>' : ''; ?>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/document">Document</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/bank">Your Financial Information</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/password">Change Password</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/security">Two Factor Authenticator</a></li>
                                    </ul>
                                </div>
                                <div class="block">
                                    <div class="block-header">
                                        <span class="text-yellow">Edit profile</span>
                                    </div>
                                    <div class="block-body">
                                        <?php if (!empty($_SESSION['alert'])) { ?>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="alertGrey">
                                                    <?php echo $_SESSION['alert']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php unset($_SESSION['alert']); ?>
                                        <?php } ?>
                                        <div class="row">
                                            <form action="<?php echo DIR_URL; ?>/settings/updateProfile" id="update-profile" method="POST" autocomplete="off">
                                               
                                                 <div class="col-md-2 col-md-push-10">
                                                    <fieldset>
                                                        <legend>Profile Picture</legend>
                                                        <img src="<?php echo $_SESSION['avatar']; ?>" style="width: 100%;" alt="<?php echo $_SESSION['name']; ?>">
                                                        <button type="button" class="btn-m-l" style="width: 100%;" data-toggle="modal" data-target="#changeAvatar">CHANGE</button>
                                                    </fieldset>
                                                </div>
                                                 <div class="col-md-10 col-md-pull-2">
                                                    <fieldset>

                                                        <legend>TIER I <span class="tag-fieldset">(required)</span></legend>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-elements">
                                                                    <label for="email">Email address</label>
                                                                    <input type="text" id="email" name="email" disabled="disabled" value="<?php echo $user['email']; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-elements">
                                                                    <label for="name">First name</label>
                                                                    <input type="text" id="name" name="name" required="required" value="<?php echo $user['name']; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-elements">
                                                                    <label for="last_name">Last name</label>
                                                                    <input type="text" id="last_name" name="last_name" required="required" value="<?php echo $user['last_name']; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="form-elements">
                                                                    <label for="ndocument">Driver License or Passport</label>
                                                                    <div class="row">
                                                                        <div class="col-md-5">
                                                                            <select name="type_document" id="type_document">                                                                               
                                                                                <?php if (!is_null($user['ndocument'])) { ?>
                                                                                <?php if (substr($user['ndocument'], 0, 1) == "P") { ?>
                                                                                <option value="1">Passport</option>
                                                                                <option value="2">Driver License</option>
                                                                                <?php } else { ?>
                                                                                <option value="2">Driver License</option>
                                                                                <option value="1">Passport</option>
                                                                                <?php } ?>
                                                                                <?php } else { ?>
                                                                                <option value="1">Passport</option>
                                                                                <option value="2">Driver License</option>
                                                                                <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-7">
                                                                            <input type="text" id="ndocument" name="ndocument" required="required" value="<?php echo substr($user['ndocument'], 2, strlen($user['ndocument'])); ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <div class="form-elements">
                                                                            <label for="country_id">Country</label>
                                                                            <select name="country_id" id="country_id" required="required">
                                                                                <?php echo (count($countryUser['id']) > 0) ? '<option value="' . $countryUser['id'] . '">' . $countryUser['nicename'] . '</option>' : ''; ?>
                                                                                <?php foreach ($countries as $country) { ?>
                                                                                <option value="<?php echo $country['id']; ?>"><?php echo $country['nicename']; ?></option>
                                                                                <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-elements">
                                                                            <label for="province_id" required="required">Province</label>
                                                                            <select name="province_id" id="province_id">
                                                                                <?php echo (count($provinceUser['id']) > 0) ? '<option value="' . $provinceUser['id'] . '">' . $provinceUser['exonimo_en'] . '</option>' : ''; ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-elements">
                                                                            <label for="city">City</label>
                                                                            <input type="text" id="city" name="city" required="required" value="<?php echo $user['city']; ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-elements">
                                                                            <label for="zip_code">ZIP Code</label>
                                                                            <input type="text" id="zip_code" name="zip_code" required="required" value="<?php echo $user['zip_code']; ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-elements">
                                                                    <label for="address">Address</label>
                                                                    <textarea name="address" id="address" required="required"><?php echo $user['address']; ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <fieldset>
                                                        <legend>TIER II</legend>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="form-elements">
                                                                    <label for="birtdate">Day of birth</label>
                                                                    <input type="text" id="birthdate" name="birthdate" value="<?php echo (is_null($user['birthdate'])) ? '' : $user['birthdate']; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-elements">
                                                                    <label for="occupation">Occupation</label>
                                                                    <input type="text" id="occupation" name="occupation" value="<?php echo $user['occupation']; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-elements">
                                                                    <label for="profession">Profession</label>
                                                                    <input type="text" id="profession" name="profession" value="<?php echo $user['profession']; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-elements">
                                                                    <label for="ssn">SSN or Passport</label>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <select name="type_document_s" id="type_document_s">                                                                               
                                                                                <?php if (!is_null($user['ssn'])) { ?>
                                                                                <?php if (substr($user['ssn'], 0, 1) == "P") { ?>
                                                                                <option value="1">Passport</option>
                                                                                <option value="2">SSN</option>
                                                                                <?php } else { ?>
                                                                                <option value="2">SSN</option>
                                                                                <option value="1">Passport</option>
                                                                                <?php } ?>
                                                                                <?php } else { ?>
                                                                                <option value="1">Passport</option>
                                                                                <option value="2">SSN</option>
                                                                                <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <input type="text" id="ssn" name="ssn" value="<?php echo (substr($user['ssn'], 0, 1) == "P") ? substr($user['ssn'], 2, strlen($user['ssn'])) : "XXX-XX-" . substr($user['ssn'], 6); ?>">
                                                                            <span id="view_ssn"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-elements">
                                                                    <label for="phonecode">Code</label>
                                                                    <select name="phonecode" id="phonecode">
                                                                        <?php echo (!is_null($user['phonecode'])) ? '<option value="' . $user['phonecode'] . '">+' . $user['phonecode'] . '</option>' : '<option value="">Country Code</option>'; ?>
                                                                        <?php foreach ($countries as $country) { ?>
                                                                        <option value="<?php echo $country['phonecode']; ?>"><?php echo $country['nicename'] . " (+" . $country['phonecode'] . ")"; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-elements">
                                                                    <label for="phone">Phone</label>
                                                                    <input type="text" id="phone" name="phone" value="<?php echo $user['phone']; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <div class="row">
                                                        <div class="col-md-4 col-md-offset-8 col-lg-3 col-lg-offset-9">
                                                            <input type="hidden" name="token" value="<?php echo $_SESSION['validateToken']; ?>">
                                                            <button type="submit" class="btn-m-l" style="margin-top: 57px; margin-bottom: 40px;">UPDATE</button>
                                                        </div>
                                                    </div>
                                                </div>
                                              
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "inc/close.php"; ?>
    </body>
</html>