<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Document</title>
	</head>
	<body>
		<table border="1">
			<thead>
				<tr>
					<th>Nro.</th>
					<th>ETH Price</th>
					<th>ETH Amount</th>
					<th>USD Amount</th>
					<th>eth_value %</th>
					<th>Commission</th>
					<th>Total</th>
					<th>Date</th>
					<th>Fullname</th>
					<th>Email</th>
				</tr>
			</thead>
			<tbody>
				<?php $count = 1; foreach ($purchases as $purchase) { ?>
				<tr>
					<td><?php echo $count; ?></td>
					<td>$<?php echo $purchase['eth_value']; ?></td>
					<td><?php echo $purchase['eth_amount']; ?></td>
					<td>$<?php echo $purchase['usd_amount']; ?></td>
					<?php if ($purchase['payment_method'] == 1) { ?>
					<td>1.99%</td>
					<td>$<?php echo number_format((($purchase['usd_amount'] * 1.99) / 100) + 0.02, 2); ?></td>
					<td>$<?php echo number_format(((($purchase['usd_amount'] * 1.99) / 100) + 0.02) + $purchase['usd_amount'], 2); ?></td>
					<?php } elseif ($purchase['payment_method'] == 2) { ?>
					<td>3.99%</td>
					<td>$<?php echo number_format((($purchase['usd_amount'] * 3.99) / 100) + 0.02, 2); ?></td>
					<td>$<?php echo number_format(((($purchase['usd_amount'] * 3.99) / 100) + 0.02) + $purchase['usd_amount'], 2); ?></td>
					<?php } ?>
					<td><?php echo date("d-m-Y H:i:s", $purchase['date']); ?></td>
					<td><?php echo $purchase['name'] . " " . $purchase['last_name']; ?></td>
					<td><?php echo $purchase['email']; ?></td>
					<?php $count++; ?>
				</tr>
				<?php } ?>
			</tbody>
		</table>		
	</body>
</html>