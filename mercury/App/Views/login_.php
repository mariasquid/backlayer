<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/login.js"></script>
	</head>
	<body style="background-color: #f5f5f5;">
		<div class="wrapper">
			<div class="main">
				<div class="content">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-md-offset-3">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<img src="<?php echo DIR_URL; ?>/public/img/logo-dark.png" alt="" class="img-responsive">
									</div>
								</div>
								<div class="card" style="margin-top: 50px;">
									<div class="header">
										<h4 class="title">Log in to Your Account</h4>
									</div>
									<div class="content">
										<form method="POST" action="<?php echo DIR_URL; ?>/login/sign" id="login-form" autocomplete="off">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label>Email</label>
														<input type="email" class="form-control" id="email" name="email"  placeholder="Email">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label>Password</label>
														<input type="password" class="form-control" id="password" name="password" placeholder="Password...">
													</div>
												</div>
											</div>
											<button type="submit" class="btn btn-info btn-fill pull-right">Log in</button>
											<div class="clearfix"></div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>