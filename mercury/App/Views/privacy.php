<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <title>1 ETH = $<?php echo $ethusd;?> Mercury Cash: Ethereum Wallet, ETH/USD Market</title>
<meta name="description" content="Mercury cash is a specialized and secure platform that has its own ethereum wallet and market exchange for ETH/USD">
<meta name="keywords" content="Ethereum Wallet">
<meta name="robots" content="index, follow">
<meta name="copyright" content="Adventurous Entertainment LLC">
<meta name="language" content="EN">
<meta name="author" content="Adventurous Entertainment LLC">
<meta name="creationdate" content="May 2017">
<meta name="distribution" content="global">
<meta name="rating" content="general">
<link rel="Publisher" href="https://www.adenter.io">
<link rel="Canonical" href="https://www.mercury.cash">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo DIR_URL; ?>/public/css/theme.css" rel="stylesheet">
    <link href="<?php echo DIR_URL; ?>/public/css/about.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700'>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic'>
    <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/fonts/elegant/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/css/social.css" rel="stylesheet">
    <!-- Multiple Favicon 14-5-2017 -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#f3c805">
        <meta name="apple-mobile-web-app-title" content="Mercury Cash">
        <meta name="application-name" content="Mercury Cash">
        <meta name="msapplication-TileColor" content="#2f3740">
        <meta name="msapplication-TileImage" content="/mstile-144x144.png">
        <meta name="theme-color" content="#ffffff">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
 <!-- Live chat --> 
  <script type="text/javascript">
  window.__lo_site_id = 80707;
  (function() {
      var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
      wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
    })();
  </script>
  <!-- Google Analytics 14-5-2017 -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-98285209-1', 'auto');
ga('send', 'pageview');

</script>
</head>
  <body id="page-top" class="index">
    <!-- Navigation -->
    <nav class="navbar navbar-default">
      <!-- Topbar Nav (hidden) -->
      <div class="topbar-nav clearfix">
        <div class="container">
          <ul class="topbar-left list-unstyled list-inline">
            <li> <span class="fa fa-phone"></span> 123-456-7890 </li>
            <li> <span class="fa fa-envelope"></span> info@yourdomain.com </li>
          </ul>
          <ul class="topbar-right list-unstyled list-inline topbar-social">
            <li> <a href="#"> <span class="fa fa-facebook"></span> </a></li>
            <li> <a href="#"> <span class="fa fa-twitter"></span> </a></li>
            <li> <a href="#"> <span class="fa fa-google-plus"></span> </a></li>
            <li> <a href="#"> <span class="fa fa-dribbble"></span> </a></li>
            <li> <a href="#"> <span class="fa fa-instagram"></span> </a></li>
          </ul>
        </div>
      </div>
      <div class="container" style="max-width: 1050px;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand page-scroll" href="<?php echo DIR_URL; ?>">
            <img id="logo-navbar" src="<?php echo DIR_URL; ?>/public/img/logo-ligth.png" class="img-responsive" alt="">
          </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
                        <li>
                <a href="<?php echo DIR_URL; ?>" class="page-scroll">1 ETH = $<?php echo $ethusd;?></a>
            </li>
                        <li>
                          <a href="<?php echo DIR_URL; ?>">HOME</a> </li>
            <li>
              <a href="<?php echo DIR_URL; ?>/login" class="page-scroll">SIGN IN</a>
            </li>
            <li>
              <a href="<?php echo DIR_URL; ?>/register" class="page-scroll">CREATE AN ACCOUNT</a>
            </li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
    </nav>

    <!-- Hero Content -->
    <header id="hero1">
        <div class="container">

            <div class="intro-text">
      <div class="intro-heading">MERCURY CASH</div>
      <div class="intro-lead-in">PRIVACY POLICY</div>
      <!-- <div class="intro-heading">GET TO KNOW US BETTER</div> -->
      <!-- <a href="#services" class="page-scroll btn btn-xl mr30 btn-primary">Learn More</a>
      <a href="#contact" class="page-scroll btn btn-xl btn-wire">Contact Us</a> -->
    </div>
  </div>
</header>


<!-- Services Section -->
<section id="services">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-left">
        <h3 class="subtitle">Last updated: April 25, 2017</h3>
      </div>
    </div>
    <br>
    <div align="justify">
      This Privacy Policy describes how Mercury Cash ("Mercury Cash") collects, uses, stores,
      shares, and protects your information whenever you use mercury.cash, any Mercury
      Cash or third party applications relying on such an API (the "Mercury Cash Site") or any
      Mercury Cash Services. By using the Mercury Cash Site and Mercury Cash Services, you
      consent to the data practices prescribed in this statement. We may periodically post
      changes to this Privacy Policy on this page, and it is your responsibility to review this
      Privacy Policy frequently and we encourage you to visit this page often. When required by
      law, we will notify you of any changes to this Privacy Policy.
      <hr>

      <div class="row">
        <div class="col-sm-12 text-left">
          <h3 class="subtitle">How we collect information about you</h3>
        </div>
      </div>
      When you use Mercury Cash Services, we collect information sent to us through your computer, mobile phone, or other access device. This information may include your IP address, device information including, but not limited to, identifier, device name and type,
      operating system, location, mobile network information, and standard web log information,
      such as your browser type, traffic to and from our site, and the pages you accessed on
      our website. Mercury Cash does not intentionally collect information from or about any
      individual who is under 13 years old.If you create an account or use Mercury Cash Services, we, or our affiliates vendors
      acting on our behalf may collect the following types of information:
      <br>
      <br>
      ● Contact information your
      name, address, phone, email, Skype ID, and other similar
      information and;
      <br>
      <br>
      ● Financial information the
      full bank account and routing numbers and/or credit card numbers
      that you link to your Mercury Cash Account or input when you use paid Mercury Cash
      Services.
      <br>
      <br>
      If you do not use the Mercury Cash Conversion Service, you may opt out of
      providing this information.

      If you seek permissions to raise Digital Currency buy and sell limits associated with your
      Mercury Cash Account, we may require you to provide additional information which we
      may use in collaboration with service providers acting on our behalf to verify your identity
      or address, and/or to manage risk. This information may include your date of birth,
      taxpayer or government identification number, a copy of your governmentissued
      identification, or other personal information. We may also obtain information about you
      from third parties such as credit bureaus and identity verification services.
      When you use Mercury Cash Services, we collect information about your transactions
      and/or your other activities on our website and we may continuously collect information
      about your computer, mobile device, or other access device for fraud prevention
      purposes, to monitor for possible breach of your Mercury Cash Account, and to identify
      any malicious software or other activity that may harm Mercury Cash or its users.
      You may choose to provide us with access to certain personal information stored by third
      parties such as social media sites (such as Facebook and Twitter). The information we
      have access to varies by site and is controlled by your privacy settings on that site and
      your authorization. By associating an account managed by a third party with your Mercury
      Cash account and authorizing Mercury Cash to have access to this information, you
      agree that Mercury Cash may collect, store, and use this information in accordance with
      this Privacy Policy.
      Finally, we may collect additional information you may disclose to our customer support
      team.
      <br> <hr>
      <div class="row">
        <div class="col-sm-12 text-left">
          <h3 class="subtitle">How we use cookies</h3>
        </div>
      </div>
      When you access our website or content or use our application or Mercury Cash
      Services, we or companies we work with may place small data files called cookies or pixel
      tags on your computer or other device. We use these technologies to:
      <br>
      <br>
      ● Recognize you as a Mercury Cash customer;
      <br>
      ● Customize Mercury Cash Services, content, and advertising;
      <br>
      ● Measure promotional effectiveness; and
      <br>
      ● Collect information about your computer or other access device to mitigate risk, help prevent
      fraud, and promote trust and safety.
      <br>
      <br>
      We use both session and persistent cookies when you access our website or content.
      Session cookies expire and no longer have any effect when you log out of your account
      or close your browser. Persistent cookies remain on your browser until you erase them or
      they expire.
      We also use Local Shared Objects, commonly referred to as "Flash cookies," to help
      ensure that your account security is not compromised, to spot irregularities in behavior to
      help prevent fraud, and to support our sites and services.
      We encode our cookies so that only we can interpret the information stored in them. You
      are free to decline our cookies if your browser or browser addon
      permits, but doing so
      may interfere with your use of Mercury Cash Services. The help section of most browsers
      or browser addons
      provides instructions on blocking, deleting, or disabling cookies.
      You may encounter Mercury Cash cookies or pixel tags on websites that we do not
      control. For example, if you view a web page created by a third party or use an application
      developed by a third party, there may be a cookie or pixel tag placed by the web page or
      application. Likewise, these third parties may place cookies or pixel tags that are not
      subject to our control and the Mercury Cash Privacy Policy does not cover their use.
      <br> <hr>
      <div class="row">
        <div class="col-sm-12 text-left">
          <h3 class="subtitle">How we protect and store personal information</h3>
        </div>
      </div>

      <br>
      Throughout this policy, we use the term "personal information" to describe information that
      can be associated with a specific person and can be used to identify that person. This
      Privacy Policy does not apply to personal information that has been anonymized so that it
      does not and cannot be used to identify a specific user. Mercury Cash takes reasonable
      precautions, as described herein, to protect your personal information from loss, misuse,
      unauthorized access, disclosure, alteration, and destruction.
      We store and process your personal and transactional information, including certain
      payment information, such as your encrypted bank account and/or routing numbers, on
      our computers in the United States and elsewhere in the world where Mercury Cash
      facilities or our service providers are located, and we protect it by maintaining physical,
      electronic, and procedural safeguards in compliance with applicable U.S. federal and state
      regulations. We use computer safeguards such as firewalls and data encryption, we
      enforce physical access controls to our buildings and files, and we authorize access to
      personal information only for those employees who require it to fulfill their job
      responsibilities. Full credit card data is securely transferred and hosted offsite
      by a
      payment vendor in compliance with Payment Card Industry Data Security Standards (PCI
      DSS). This information is not accessible to Mercury Cash staff.
      We store our customers' personal information securely throughout the life of the
      customer's Mercury Cash Account. Mercury Cash will retain your personal information for
      a minimum of five years or as necessary to comply with our legal obligations or to resolve
      disputes.

      <br> <hr>
      <div class="row">
        <div class="col-sm-12 text-left">
          <h3 class="subtitle">How we use the personal information we collect</h3>
        </div>
      </div>
      <br>
  <div align="justify">
      Our primary purpose in collecting personal information is to provide you with a secure,
      smooth, efficient, and customized experience. We may use your personal information to:
      <br>
      <br>
      ● Provide Mercury Cash Services and customer support you request;
      <br>
      <br>
      ● Process transactions and send notices about your transactions;
      <br>
      <br>
      ● Resolve disputes, collect fees, and troubleshoot problems;
      <br>
      <br>
      ● Prevent and investigate potentially prohibited or illegal activities, and/or violations of our posted
      user terms;
      <br>
      <br>
      ● Customize, measure, and improve Mercury Cash Services and the content and layout of our
      website and applications;
      <br>
      <br>
      ● Deliver targeted marketing, service update notices, and promotional offers based on your
      communication preferences; and
      <br>
      <br>
      ● Verify your identity by comparing your personal information against thirdparty
      databases.
      <br>
      <br>
      We will not use your personal information for purposes other than those purposes we
      have disclosed to you, without your permission. From time to time we may request your
      permission to allow us to share your personal information with third parties. You may opt
      out of having your personal information shared with third parties, or from allowing us to
      use your personal information for any purpose that is incompatible with the purposes for
      which we originally collected it or subsequently obtained your authorization. If you choose
      to so limit the use of your personal information, certain features or Mercury Cash Services
      may not be available to you.
</div>
      <br> <hr>
      <div class="row">
        <div class="col-sm-12 text-left">
          <h3 class="subtitle">Marketing</h3>
        </div>
      </div>
      <br>
      We will never sell or rent your personal information to third parties.

      <br> <hr>
      <div class="row">
        <div class="col-sm-12 text-left">
          <h3 class="subtitle">How personal information is shared with other Mercury Cash Users</h3>
        </div>
      </div>
      <br>
    </div>
  <div align="justify">
    If you use your Mercury Cash Account to transfer Digital Currency in connection with the
    purchase or sale of goods or services, we or you may also provide the seller with your
    shipping address, name, and/or email to help complete your transaction with the seller.
    The seller is not allowed to use this information to market their services to you unless you
    have agreed to it. If an attempt to transfer Digital Currency to your seller fails or is later
    invalidated, we may also provide your seller with details of the unsuccessful transfer. To
    facilitate dispute resolutions, we may provide a buyer with the seller's address so that
    goods can be returned to the seller.
    <br>
    <br>
    In connection with a Digital Currency transfer between you and a third party, including
    merchants, a third party may share information about you with us, such as your email
    address or mobile phone number which may be used to inform you that a transfer has
    been sent to or received from the third party. We may use this information in connection
    with such transfers to confirm that you are a Mercury Cash customer, that Digital Currency
    transfers are enabled, and/or to notify you that you have received Digital Currency. If you
    request that we validate your status as a Mercury Cash customer with a third party, we will
    do so. You may also choose to send Digital Currency to or request Digital Currency from
    an email address. In such cases, your username will be displayed in an email message
    notifying the user of the designated email address of your action. Please note that
    merchants you interact with may have their own privacy policies, and Mercury Cash is not
    responsible for their operations, including, but not limited to, their information practices.
    If you authorize one or more thirdparty
    applications to access your Mercury Cash
    account, the information you have provided to Mercury Cash may be shared with those
    third parties. Unless you provide further authorization, these third parties are not allowed
    to use this information for any purpose other than to facilitate your transactions using
    Mercury Cash services.
</div>
    <br> <hr>
    <div class="row">
      <div class="col-sm-12 text-left">
        <h3 class="subtitle">How we share personal information with other parties</h3>
      </div>
    </div>
    <br>

  <div align="justify">
    <br>
    We may share your personal information with:
    <br>
    <br>
    ● Third party identity verification services in order to prevent fraud. This allows Mercury Cash to
    confirm your identity by comparing the information you provide us to public records and other
    third party databases. These service providers may create derivative data based on your
    personal information that can be used solely in connection with provision of identity verification
    and fraud prevention services;
    <br>
    <br>
    ● Service providers under contract who help with parts of our business operations such as bill
    collection, marketing, and technology services. Our contracts require these service providers
    to only use your information in connection with the services they perform for us, and prohibit
    them from selling your information to anyone else;
    <br>
    <br>
    ● Financial institutions with which we partner;
    <br>
    <br>
    ● Companies or other entities that we plan to merge with or be acquired by. Should such a
    combination occur, we will require that the new combined entity follow this Privacy Policy with
    respect to your personal information. You will receive prior notice of any change in applicable
    policy;
    <br>
    <br>
    ● Companies or other entities that purchase Mercury Cash assets pursuant to a courtapproved
    sale under U.S. Bankruptcy law;
    <br>
    <br>
    ● Law enforcement, and officials, or other third parties when:
    ○ We are compelled to do so by a subpoena, court order, or similar legal procedure;
    or
    ○ We believe in good faith that the disclosure of personal information is necessary
    to prevent physical harm or financial loss, to report suspected illegal activity or to
    investigate violations of our User Agreement; and
    <br>
    <br>
    ● Other third parties with your consent or direction to do so.
    Mercury Cash may use Plaid Technologies, Inc. ("Plaid") to verify your bank account.
    Mercury Cash only shares your information with Plaid in accordance with this Privacy
    Policy. Information shared with Plaid is treated by Plaid in accordance with its Privacy
    Policy, available at https://plaid.com/legal/ privacypolicy.
    Before Mercury Cash shares your information with any third party that is not acting as an
    agent to perform tasks on behalf of and under the instructions of Mercury Cash, Mercury
    Cash will enter into a written agreement requiring that the third party to provide at least the
    same level of privacy protection as required hereunder.
    If you establish a Mercury Cash account indirectly on a third party website or via a third
    party application, any information that you enter on that website or application (and not
    directly on a Mercury Cash website) will be shared with the owner of the third party website
    or application and your information may be subject to their privacy policies.
    In general, we will notify you of material changes to this policy by updating the last
    updated date at the top of this page, and we will provide you with explicit notice of material
    changes as required by law. We recommend that you visit this page frequently to check
    for changes.
  </div>

    <br> <hr>
    <div class="row">
      <div class="col-sm-12 text-left">
        <h3 class="subtitle">How you can access or change your personal information</h3>
      </div>
    </div>
    <br>
    <br>
  <div align="justify">
    You are entitled to review, correct, or amend your personal information, or to delete that
    information where it is inaccurate, and you may do so at any time by logging into your
    account and clicking the Profile or My Account tab. This right shall only be limited where
    the burden or expense of providing access would be disproportionate to the risks to your
    privacy in the case in question, or where the rights of persons other than you would be
    violated. If you close your Mercury Cash account, we will mark your account in our
    database as "Closed," but will keep your account information in our database for a period
    of time described above. This is necessary in order to deter fraud, by ensuring that
    persons who try to commit fraud will not be able to avoid detection simply by closing their
    account and opening a new account. However, if you close your account, your personally
    identifiable information will not be used by us for any further purposes, nor sold or shared
    with third parties, except as necessary to prevent fraud and assist law enforcement, as
    required by law, or in accordance with this Privacy Policy.
</div>
    <br> <hr>
    <div class="row">
      <div class="col-sm-12 text-left">
        <h3 class="subtitle">How you can contact us about privacy questions</h3>
      </div>
    </div>
    <br>
    <br>
  <div align="justify">
    If you have questions or concerns regarding this policy, or if you have a complaint, you
    should first contact us on our support page or by writing to us at Mercury Cash, 6427
    Milner Blvd, Suite suite 4, Orlando, FL 32809, USA, or by calling us at 4074834057.
</div>
  </div>
</div>
</div>
</section>

<br>
<br>
<br>

<!-- Contact Section -->
<section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading">Get in touch and explore the possibilities!!</h2>
      </div>
    </div>
    <form name="sentMessage" id="contactForm" class="mw800 center-block clearfix" novalidate>
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
        <p class="help-block text-danger"></p>
      </div>
      <div class="form-group">
        <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
        <p class="help-block text-danger"></p>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-xl btn-block btn-wire">Subscribe</button>
      </div>
    </form>
    <p> By subscribing you agree to our terms and conditions. </p>

  </div>
</section>

<!-- Footer -->
		<?php include "inc/footer-homes.php"; ?>




<!-- jQuery -->
<!-- <script src="js/vendor/jquery.js"></script> -- Local Version -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<!-- <script src="js/vendor/bootstrap.min.js"></script> -- Local Version -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="js/vendor/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script>

<!-- Custom Theme JavaScript -->
<script src="js/main.js"></script>

</body>

</html>
