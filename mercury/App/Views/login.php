<!DOCTYPE html>
<html lang="es">
	<head>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<script src="<?php echo DIR_URL; ?>/public/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
		<script src="<?php echo DIR_URL; ?>/public/assets/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo DIR_URL; ?>/public/assets/js/bootstrap-notify.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/dir_url.js"></script>
	</head>
	<body style="background-color: #f5f5f5;">
		<div class="wrapper">
			<div class="main">
				<div class="content">
					<div class="container">
						<div class="row">
							<div class="col-sm-3 col-sm-offset-4">
								<div id="logo-register">
									<img src="<?php echo DIR_URL; ?>/public/img/logo-dark.png" alt="" class="img-responsive">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-md-offset-3">
								<?php if (!empty($_GET['error'])) { ?>
								<?php if ($_GET['error'] == 'blockuser') { ?>
								<div class="alert-m alert-m-danger">
									<p>Credential error</p>
								</div>
								<?php } elseif ($_GET['error'] == 'captchaempty') { ?>
								<div class="alert-m alert-m-danger">
									<p>Do not forget to pass the captcha test</p>
								</div>
								<?php } elseif ($_GET['error'] == 'errorblockuser') { ?>
								<div class="alert-m alert-m-danger">
									<p>Credential error, user has been blocked. Contact your administrator</p>
								</div>
								<?php } elseif ($_GET['error'] == 'credentialerror') { ?>
								<div class="alert-m alert-m-danger">
									<p>Credential error</p>
								</div>
								<?php } elseif ($_GET['error'] == 'empty') { ?>
								<div class="alert-m alert-m-danger">
									<p>Fields are empty</p>
								</div>
								<?php } ?>
								<?php } ?>
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Log in</span> Into your account
									</div>
									<div class="block-body">
										<form method="POST" action="<?php echo (!empty($_REQUEST['returnto'])) ? DIR_URL . '/login/sign?returnto=' . $_REQUEST['returnto'] : DIR_URL . '/login/sign'; ?>" id="login-form" autocomplete="off">
											<div class="row">
												<div class="col-md-12">
													<div class="form-elements">
														<label>Email</label>
														<input type="email" id="email" name="email"  placeholder="Email">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="form-elements">
														<label>Password</label>
														<input type="password" id="password" name="password" placeholder="Password...">
													</div>
												</div>
											</div>
											<?php if (!empty($_SESSION['tries'])) { ?>
											<div id="captcha">
												<hr>
												<div class="row">
													<div class="col-md-12" style="position: relative; height: 90px;">
														<div class="g-recaptcha" data-sitekey="6LftmSQUAAAAAG97w4Owc0iku3REEOYEmhXXczIl" style="width: 55%; margin: auto; position: absolute; left: 0; right: 0;"></div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-8 col-md-offset-2" style="text-align: center; margin-top: 10px;">
														<a href="https://support.mercury.cash/" target="_blank">Having problems logging in?</a>
													</div>
												</div>
												<hr>
											</div>
											<?php } ?>
											<div class="row">
												<div class="col-md-4" style="padding-top: 15px;">
													<a href="<?php echo DIR_URL; ?>/lostpassword">Forgot Password?</a>
												</div>
												<div class="col-md-4">
													<button type="submit" id="enter" class="btn-m-l">SIGN IN</button>
												</div>
												<div class="col-md-4" style="padding-top: 15px;">
													<a href="<?php echo DIR_URL; ?>/register">Create new account</a>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src='https://www.google.com/recaptcha/api.js'></script>
	</body>
</html>