<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/qrcode.min.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/moment.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/accounts.js?v=<?php echo time(); ?>"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/wallets.js?v=<?php echo time(); ?>"></script>
	</head>
	<body>
		<?php if ($_SESSION['status'] == 3) { ?>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#validateDocument').modal();
			});
		</script>
		<!-- Modal -->
		<div class="modal fade" id="validateDocument" tabindex="-1" role="dialog" aria-labelledby="validateDocumentLabel" data-target=".bs-example-modal-sm">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="validateDocumentLabel">Attention</h4>
					</div>
					<div class="modal-body" style="text-align: center;">
						<p>In order to verify your account, please upload your Driver's License (US Citizens Only) or your Passport.</p>
						<p>Example of Selfie's ID Verification</p>
						<div class="row">
							<div class="col-md-6">
								<img src="<?php echo DIR_URL; ?>/public/img/do1.png" alt="" class="img-responsive">
							</div>
							<div class="col-md-6">
								<img src="<?php echo DIR_URL; ?>/public/img/do2.png" alt="" class="img-responsive">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<a href="<?php echo DIR_URL; ?>/settings/document" class="btn btn-default">Validate Now</a>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-target=".bs-example-modal-sm">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Wallet</h4>
					</div>
					<div class="modal-body" style="text-align: center;">
					<div id="qrcode" style="display: inline-block;"></div>
					<br>
					<code id="addressView"></code>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default closeModal" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div class="loading">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">My accounts</span> Rafael Moreno
										<a href="#" class="btn-header">Add wallet</a>
									</div>
									<div class="block-body">
										<table class="table-m">
											<thead>
												<tr>
													<th>Name</th>
													<th>Currency</th>
													<th>Balance</th>
													<th>Address</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>USD Wallet</td>
													<td>USD</td>
													<td>0.00</td>
													<td>Address</td>
												</tr>
												<tr>
													<td>ETH Wallet 1</td>
													<td>USD</td>
													<td>0.00</td>
													<td>Address</td>
												</tr>
												<tr>
													<td>ETH Wallet 2</td>
													<td>USD</td>
													<td>0.00</td>
													<td>Address</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>