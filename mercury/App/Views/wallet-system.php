<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
	</head>
	<body class="dashboard-page sb-l-o sb-r-c">
		<div id="main">
			<?php include "inc/header.php"; ?>
			<?php include "inc/aside.php"; ?>
			<div id="content_wrapper">
				<section id="content" class="animated fadeIn">
					<div id="alert">
						
					</div>
					<div class="row">
						<div class="col-md-9">
							<div class="panel">
								<div class="panel-heading">
									<span class="panel-title">Wallet System</span>
								</div>
								<div class="panel-body">
									<form class="form-horizontal" action="<?php echo DIR_URL; ?>/settings/updateWallet" id="update" method="POST" role="form">
										<div class="form-group">
											<label for="private_key" class="col-lg-3 control-label">Private Key</label>
											<div class="col-lg-8">
												<div class="bs-component">
													<input type="text" id="private_key" name="private_key" class="form-control" value="<?php echo $wallet['private_key']; ?>">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-3 control-label" for="public_key">Public Key</label>
											<div class="col-lg-8">
												<div class="bs-component">
													<textarea class="form-control" id="public_key" name="public_key" rows="3"><?php echo $wallet['public_key']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label for="address" class="col-lg-3 control-label">Address</label>
											<div class="col-lg-8">
												<div class="bs-component">
													<input type="text" id="address" name="address" class="form-control" value="<?php echo $wallet['address']; ?>">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-lg-5 col-lg-offset-3">
												<div class="bs-component">
													<button type="submit" class="btn btn-hover btn-info btn-block">UPDATE</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/wallet-system.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				"use strict";
				Core.init();
				
			});
		</script>
	</body>
</html>