<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/assets/css/metricsgraphics.css">
		<?php include "inc/scripts.php"; ?>
		<script src="https://d3js.org/d3.v4.min.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/metricsgraphics.min.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/dashboard.js?v=<?php echo time(); ?>"></script>
	</head>
	<body>
		<?php if ($_SESSION['status'] == 3) { ?>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#validateDocument').modal();
			});
		</script>
		<!-- Modal -->
		<div class="modal fade" id="validateDocument" tabindex="-1" role="dialog" aria-labelledby="validateDocumentLabel" data-target=".bs-example-modal-sm">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="validateDocumentLabel">Attention</h4>
					</div>
					<div class="modal-body" style="text-align: center;">
						<p>In order to verify your account, please upload your Driver's License (US Citizens Only) or your Passport.</p>
						<p>Example of Selfie's ID Verification</p>
						<div class="row">
							<div class="col-md-6">
								<img src="<?php echo DIR_URL; ?>/public/img/do1.png" alt="" class="img-responsive">
							</div>
							<div class="col-md-6">
								<img src="<?php echo DIR_URL; ?>/public/img/do2.png" alt="" class="img-responsive">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<a href="<?php echo DIR_URL; ?>/settings/document" class="btn btn-default">Validate Now</a>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<div class="modal fade" id="infoTransfer" tabindex="-1" role="dialog" aria-labelledby="infoTransferLabel" data-target=".bs-example-modal-sm">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="infoTransferLabel"></h4>
					</div>
					<div class="modal-body" style="text-align: center;">
						<h2 id="eth_amount"></h2>
						<h4 id="usd_amount"></h4>
						<hr>
						<p id="address"></p>
						<hr>
						<hr>
						<p><a href="#" id="linkEth" target="_blank"><span id="confirmations"></span> confirmations</a></p>
						<hr>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Historical Price</span> Ethereum
									</div>
									<div class="block-body">
										<div id="chartContainer"></div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div id="balanceTotal" class="block">
									<div class="block-header">
										<span class="text-yellow">Your Total</span> Balance
									</div>
									<div class="block-body">
										<h1 style="text-align: center;">$<span id="total_balance"><?php echo number_format(($balance_eth * $ethusd) + $usd_wallet['balance'], 2); ?></span></h1>
										<h6 style="text-align: center;">Total Balance</h6>
										<br>
										<div class="table-full-width" style="margin: 0;">
											<table class="table">
												<tr>
													<td class="icons-mercury"><?php include PROJECTPATH . "/public/img/ethereum.svg"; ?></td>
													<td>ETH Wallet</td>
													<td style="text-align: right;"><span id="eth_balance"><?php echo $balance_eth; ?></span> ETH <br> $<span id="eth_to_usd_balance"><?php echo number_format($balance_eth * $ethusd, 2); ?></span></td>
												</tr>
												<tr>
													<td class="icons-mercury"><?php include PROJECTPATH . "/public/img/dollar.svg" ?></td>
													<td>USD Wallet</td>
													<td style="text-align: right;">$<span id="usd_balance"><?php echo number_format($usd_wallet['balance'], 2); ?></span></td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="block" style="margin-top: 40px;">
									<div class="block-header">
										<span class="text-yellow">Recent</span> activities
									</div>
									<div class="block-body">
										<table class="table table-hover table-striped">
											<tbody>
												<?php foreach ($transactions as $transaction) { ?>
												<tr <?php if ($transaction->type_transaction == 'Transfer') { ?> data-toggle="modal" data-target="#infoTransfer" data-confirmations="<?php echo $transaction->confirmations; ?>" data-eth="<?php echo $transaction->value; ?>" data-usd="<?php echo $transaction->usd_amount; ?>" data-txhash="<?php echo $transaction->hash; ?>" data-address="<?php echo ($transaction->from == $_SESSION['address']) ? '<b>To:</b> ' . $transaction->to : '<b>From:</b> ' . $transaction->from; ?>" data-title="<?php echo ($transaction->from == $_SESSION['address']) ? 'Transfer ethereum' : 'Received ethereum'; ?>" <?php } else { ?>  onclick="javascript:location.href='<?php echo DIR_URL; ?>/accounts/usdwallet'" <?php } ?> style="cursor: pointer;">
													<td><?php echo date("d/M", $transaction->date); ?></td>

													<?php //Purchase ?>
													<?php if ($transaction->type_transaction == 'Purchase') { ?>
													<?php if ($transaction->payment_method == 1) { ?>
													<td><i class="pe-7s-wallet" style="color: #02c501; font-size: 4rem;"></i></td>
													<?php } elseif ($transaction->payment_method == 2) { ?>
													<td><i class="pe-7s-credit" style="color: #02c501; font-size: 4rem;"></i></td>
													<?php } elseif ($transaction->payment_method == 3) { ?>
													<td><i class="pe-7s-culture" style="color: #02c501; font-size: 4rem;"></i></td>
													<?php } ?>
													<td>
														<?php if ($transaction->status == 0) { ?>
														<span class="label label-danger">Reject</span>
														<?php } elseif ($transaction->status == 1) { ?>
														<span class="label label-success">Approve</span>
														<?php } elseif ($transaction->status == 2) { ?>
														<span class="label label-warning">pending</span>
														<?php } ?>
													</td>
													<td><?php echo $transaction->type_transaction; ?></td>
													<td style="text-align: right;"><span style="color: #01c512;">+<?php echo $transaction->eth_amount; ?> ETH</span><br>+$<?php echo $transaction->usd_amount; ?> USD</td>
													
													<?php //Purchase ?>
													<?php } elseif ($transaction->type_transaction == 'Sell') { ?>
													<?php if ($transaction->payment_method == 1) { ?>
													<td><i class="pe-7s-wallet" style="color: #02c501; font-size: 4rem;"></i></td>
													<?php } elseif ($transaction->payment_method == 2) { ?>
													<td><i class="pe-7s-culture" style="color: #02c501; font-size: 4rem;"></i></td>
													<?php } elseif ($transaction->payment_method == 3) { ?>
													<td><i class="pe-7s-culture" style="color: #02c501; font-size: 4rem;"></i></td>
													<?php } ?>
													<td>
														<?php if ($transaction->status == 0) { ?>
														<span class="label label-danger">Reject</span>
														<?php } elseif ($transaction->status == 1) { ?>
														<span class="label label-success">Approve</span>
														<?php } elseif ($transaction->status == 2) { ?>
														<span class="label label-warning">pending</span>
														<?php } ?>
													</td>
													<td><?php echo $transaction->type_transaction; ?></td>
													<td style="text-align: right;"><span style="color: #01c512;">+<?php echo $transaction->eth_amount; ?> ETH</span><br>+$<?php echo $transaction->usd_amount; ?> USD</td>
													
													<?php //Withdraw ?>
													<?php } elseif ($transaction->type_transaction == 'Withdraw') { ?>
													<td><i class="pe-7s-angle-right-circle" style="color: #d9534f; font-size: 4rem;"></i></td>
													<td>
														<?php if ($transaction->status == 0) { ?>
														<span class="label label-danger">Reject</span>
														<?php } elseif ($transaction->status == 1) { ?>
														<span class="label label-success">Approve</span>
														<?php } elseif ($transaction->status == 2) { ?>
														<span class="label label-warning">pending</span>
														<?php } ?>
													</td>
													<td><?php echo $transaction->type_transaction; ?></td>
													<td style="text-align: right;"><span style="color: #01c512;"></span><br>$<?php echo $transaction->usd_amount; ?> USD</td>

													<?php //Deposits ?>
													<?php } elseif ($transaction->type_transaction == 'Deposits') { ?>
													<td><i class="pe-7s-angle-left-circle" style="color: #02c501; font-size: 4rem;"></i></td>
													<td>
														<?php if ($transaction->status == 0) { ?>
														<span class="label label-danger">Reject</span>
														<?php } elseif ($transaction->status == 1) { ?>
														<span class="label label-success">Approve</span>
														<?php } elseif ($transaction->status == 2) { ?>
														<span class="label label-warning">pending</span>
														<?php } ?>
													</td>
													<td><?php echo $transaction->type_transaction; ?></td>
													<td style="text-align: right;"><span style="color: #01c512;"></span><br>$<?php echo $transaction->usd_amount; ?> USD</td>

													<?php //Transfer ?>
													<?php } elseif ($transaction->type_transaction == 'Transfer') { ?>
													<?php if ($transaction->from == $_SESSION['address']) { ?>
													<td><i class="pe-7s-angle-right-circle" style="color: #d9534f; font-size: 4rem;"></i></td>
													<td></td>
													<td><?php echo $transaction->type_transaction; ?></td>
													<td style="text-align: right;"><span style="color: #d9534f;">-<?php echo $transaction->value; ?> ETH</span><br>-$<?php echo $transaction->usd_amount; ?> USD</td>
													<?php } elseif ($transaction->to == $_SESSION['address']) { ?>
													<td><i class="pe-7s-angle-left-circle" style="color: #02c501; font-size: 4rem;"></i></td>
													<td></td>
													<td><?php echo $transaction->type_transaction; ?></td>
													<td style="text-align: right;"><span style="color: #01c512;">+<?php echo $transaction->value; ?> ETH</span><br>+$<?php echo $transaction->usd_amount; ?> USD</td>
													<?php } ?>
													<?php } ?>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div id="app" class="block" style="margin-top: 40px; height: 431px;">
									<div class="block-header">
										<span class="text-yellow">Recommended</span>  For You
									</div>
									<div class="block-body">
										<div class="row">
											<div class="col-md-6">
												<p class="firts">Download <b>Mercury Cash</b> for your phone</p>
												<p>Access your <b>Mercury Cash</b> account anywhere with our mobile apps.</p>
												<div class="col-md-6">
													<a href="https://play.google.com/store/apps/details?id=com.adenter.mercurycash&hl=en">
						<img src="https://www.mercury.cash/public/img/app-android.png" alt="" class="img-responsive">
					 </a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>