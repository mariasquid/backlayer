<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <?php include "inc/scripts.php"; ?>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/settings.js?v=<?php echo time(); ?>"></script>
    </head>
    <body>
        <?php include "inc/trialInfoModal.php"; ?>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Attention</h4>
                    </div>
                    <div class="modal-body" style="text-align: center;">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default closeModal" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="loading">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1">
                                <div class="tabs-m">
                                    <ul>
                                        <li><a href="<?php echo DIR_URL; ?>/settings">Personal information</a></li>
                                        <?php echo ($type_user == 2) ? '<li><a href="' . DIR_URL . '/settings/businessInfo">Business information</a></li>' : ''; ?>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/document">Document</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/bank">Your Financial Information</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/password">Change Password</a></li>
                                        <li class="active"><a href="<?php echo DIR_URL; ?>/settings/security">Two Factor Authenticator</a></li>
                                    </ul>
                                </div>
                                <div class="block">
                                    <div class="block-header">
                                        <span class="text-yellow">Confirmed Devices</span> These devices are currently allowed to access your account.
                                    </div>
                                    <div class="block-body">
                                        <?php if (!empty($_SESSION['alert_device'])) { ?>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="alertGrey">
                                                    <?php echo $_SESSION['alert_device']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php unset($_SESSION['alert_device']); ?>
                                        <?php } ?>
                                        <table class="table-m">
                                            <thead>
                                                <tr>
                                                    <th>Comfirmed</th>
                                                    <th>Browser</th>
                                                    <th>IP Address</th>
                                                    <th class="hidden-xs">Near</th>
                                                    <th class="hidden-xs">Current</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($devices as $device) { ?>
                                                <tr>
                                                    <td><?php echo date("d/m/Y", $device['date']); ?></td>
                                                    <td><?php echo $device['browser']; ?></td>
                                                    <td><?php echo $device['ip']; ?></td>
                                                    <td class="hidden-xs"><?php echo $device['near']; ?></td>
                                                    <td class="hidden-xs"><?php echo ($device['ip'] == $_SERVER['REMOTE_ADDR']) ? "<i class='pe-7s-check' style='color: green; font-size: 3rem;'></i>" : ""; ?></td>
                                                    <td><a class="trashDevice" href="<?php echo DIR_URL; ?>/settings/trashDevice/<?php echo $device['id']; ?>/<?php echo $_SESSION['validateToken']; ?>"><i class="pe-7s-trash" style="color: red; font-size: 3rem;"></i></a></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "inc/close.php"; ?>
    </body>
</html>