<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/moment.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/transactions.js?v=<?php echo time(); ?>"></script>
	</head>
	<body>
		<?php include "inc/trialInfoModal.php"; ?>
		<div class="modal fade" id="infoTransfer" tabindex="-1" role="dialog" aria-labelledby="infoTransferLabel" data-target=".bs-example-modal-sm">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="infoTransferLabel"></h4>
					</div>
					<div class="modal-body" style="text-align: center;">
						<h2 id="eth_amount"></h2>
						<h4 id="usd_amount"></h4>
						<hr>
						<p id="address"></p>
						<hr>
						<hr>
						<p><a href="#" id="linkEth" target="_blank"><span id="confirmations"></span> confirmations</a></p>
						<hr>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="fiterDate" tabindex="-1" role="dialog" aria-labelledby="fiterDateLabel" data-target=".bs-example-modal-sm">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form action="<?php echo DIR_URL; ?>/accounts/genPdfReport" method="POST" target="_blank">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="fiterDateLabel">Attention</h4>
						</div>
						<div class="modal-body" style="text-align: center;">
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-elements">
                                        <label for="from">From</label>
                                        <input type="text" id="from" name="from" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-elements">
                                        <label for="to">To</label>
                                        <input type="text" id="to" name="to" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                            </div>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="eth_wallet" id="eth_wallet" value="<?php echo $wallet['wallet_address']; ?>">
							<button type="submit" class="btn btn-default">Search</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Transactions</span>
										<a href="#" data-toggle="modal" data-target="#fiterDate" class="btn-header">e-statement</a>
									</div>
									<div class="block-body">
										<table class="table-m">
											<tbody id="tbody-wallets">
												<?php foreach ($transactions as $transaction) { ?>
												<tr>
													<td><?php echo $transaction['start_date']; ?></td>
													<?php if ($transaction['from_address'] == $wallet['wallet_address']) { ?>
													<td><i class="pe-7s-angle-right-circle" style="color: #d9534f; font-size: 4rem;"></i></td>
													<td>To: <?php echo $transaction['to_address']; ?></td>
													<td style="text-align: right;"><span style="color: #d9534f;">-<?php echo $transaction['value']; ?> ETH</span><br>-<?php echo number_format($ethusd * $transaction['value'], 2); ?> USD</td>
													<?php } elseif ($transaction['to_address'] == $wallet['wallet_address']) { ?>
													<td><i class="pe-7s-angle-left-circle" style="color: #02c501; font-size: 4rem;"></i></td>
													<td>From: <?php echo $transaction['from_address']; ?></td>
													<td style="text-align: right;"><span style="color: #01c512;">+<?php echo $transaction['value']; ?> ETH</span><br>+<?php echo number_format($ethusd * $transaction['value'], 2); ?> USD</td>
													<?php } ?>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>