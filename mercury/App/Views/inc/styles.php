<link href="<?php echo DIR_URL; ?>/public/assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo DIR_URL; ?>/public/assets/css/animate.min.css" rel="stylesheet" />
<link href="<?php echo DIR_URL; ?>/public/assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" />
<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css' />
<link href="<?php echo DIR_URL; ?>/public/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
<link href="<?php echo DIR_URL; ?>/public/assets/css/generals.css" rel="stylesheet">
<!-- Multiple Favicon 14-5-2017 -->
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#f3c805">
<meta name="apple-mobile-web-app-title" content="Mercury Cash">
<meta name="application-name" content="Mercury Cash">
<meta name="msapplication-TileColor" content="#2f3740">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<meta name="theme-color" content="#ffffff">
<!-- Live chat --> 
  <script type="text/javascript">
  window.__lo_site_id = 80707;
  (function() {
      var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
      wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
    })();
  </script>
  <!-- Google Analytics 14-5-2017 -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-98285209-1', 'auto');
ga('send', 'pageview');

</script>