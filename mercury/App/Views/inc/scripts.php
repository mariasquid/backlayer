<script src="<?php echo DIR_URL; ?>/public/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="<?php //echo DIR_URL; ?>/public/assets/js/value.js"></script>-->
<script src="<?php echo DIR_URL; ?>/public/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo DIR_URL; ?>/public/assets/js/bootstrap-checkbox-radio-switch.js"></script>
<script src="<?php echo DIR_URL; ?>/public/assets/js/bootstrap-notify.js"></script>
<script src="<?php echo DIR_URL; ?>/public/assets/js/light-bootstrap-dashboard.js"></script>
<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/bower_components/push.js/push.min.js"></script>
<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/dir_url.js"></script>
<script type="text/javascript">
setTimeout(function() {
	Push.create("Hi <?php echo $_SESSION['name']; ?>", {
	    body: "We have not detected activity in the last 5 minutes",
	    icon: dir_url + "/public/img/marketplace.png",
	    timeout: 60000,
	    onClick: function () {
	        window.focus();
	        alert("We have not detected activity in the last 5 minutes");
	        this.close();
	    }
	});
}, 258000);
setTimeout(function() {
	location.href = dir_url + "/login/signout/<?php echo $_SESSION['validateToken']; ?>";
}, 300000);
</script>