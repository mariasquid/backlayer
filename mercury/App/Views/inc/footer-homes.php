<section id="certificates">
    <div class="container">
        <div class="row">
            <div class="col-md12">
                <h2>Licenses and Certificates</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2 col-sm-offset-1">
                <a href="<?php echo DIR_URL; ?>/public/img/certificates/trademark.pdf" target="_bank">
                    <img src="<?php echo DIR_URL; ?>/public/img/certificates/trademark.svg" class="img-responsive certificates_img">
                    <h5>Trademark</h5>
            </div>
            <div class="col-sm-2">
            <a href="<?php echo DIR_URL; ?>/public/img/certificates/company-no.pdf" target="_blank">
            <img src="<?php echo DIR_URL; ?>/public/img/certificates/company-no.svg" class="img-responsive certificates_img">
            <h5>Certificate of Incorporation</h5>
            </a>
            </div>
            <div class="col-sm-2">
                <a href="<?php echo DIR_URL; ?>/public/img/certificates/fincen-registration.pdf" target="_blank">
                    <img src="<?php echo DIR_URL; ?>/public/img/certificates/fincen-registration.svg" class="img-responsive certificates_img">
                    <h5>FinCEN Registration</h5>
                </a>
            </div>
            <div class="col-sm-2">
                <img src="<?php echo DIR_URL; ?>/public/img/certificates/pci-compliant.svg" class="img-responsive certificates_img">
                <h5>PCI DSS Compliant</h5>
            </div>
            <!--
                <div class="col-sm-2">
                    <img src="<?php //echo DIR_URL; ?>/public/img/certificates/ico-certificate.svg" class="img-responsive certificates_img">
                    <h5>ICO Certificate of Registration</h5>
                </div>
                -->
            <div class="col-sm-2">
                <a href="<?php echo DIR_URL; ?>/public/img/certificates/money-transmitter.pdf" target="_blank">
                    <img src="<?php echo DIR_URL; ?>/public/img/certificates/money-transmitter.svg" class="img-responsive certificates_img">
                    <h5>Money Transmitters License</h5>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="zn_section eluid57ded959     section-sidemargins    section--no " id="eluid57ded959">
    <div class="zn_section_size container zn-section-height--auto zn-section-content_algn--top ">
        <div class="row gutter-xs">
            <div class="eluidc1f31e9b      col-md-3 col-sm-3   znColumnElement" id="eluidc1f31e9b">
                <div class="znColumnElement-innerWrapper-eluidc1f31e9b znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">
                    <div class="znColumnElement-innerContent">
                        <div class="elm-custommenu clearfix eluid48603ba1  text-left elm-custommenu--v1">
                            <ul id="eluid48603ba1" class="elm-cmlist clearfix elm-cmlist--skin-light element-scheme--light zn_dummy_value elm-cmlist--v1 nav-with-smooth-scroll">
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-807"><a href="https://www.mercury.cash"><span>HOME</span></a></li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-808"><a href="https://www.mercury.cash/about"><span>ABOUT</span></a></li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-970"><a href="http://blog.mercury.cash"><span>BLOG</span></a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-417 current_page_item menu-item-806 active"><a href="http://press.mercury.cash/"><span>PRESS</span></a></li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-809"><a href="https://www.mercury.cash/login"><span>SIGN IN</span></a></li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-810"><a href="https://www.mercury.cash/register"><span>CREATE AN ACCOUNT</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="eluiddbb9ce88      col-md-3 col-sm-3   znColumnElement" id="eluiddbb9ce88">
                <div class="znColumnElement-innerWrapper-eluiddbb9ce88 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">
                    <div class="znColumnElement-innerContent">
                        <div class="elm-custommenu clearfix eluid3d7bba84  text-left elm-custommenu--v1">
                            <ul id="eluid3d7bba84" class="elm-cmlist clearfix elm-cmlist--skin-light element-scheme--light zn_dummy_value elm-cmlist--v1 nav-with-smooth-scroll">
                                <li id="menu-item-936" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-936"><a href="https://www.mercury.cash/legal"><span>LEGAL</span></a></li>
                                <li id="menu-item-937" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-937"><a href="https://www.mercury.cash/terms"><span>TERMS AND CONDITIONS</span></a></li>
                                <li id="menu-item-938" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-938"><a href="https://www.mercury.cash/privacy"><span>PRIVACY POLICY</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="eluid53c56454      col-md-3 col-sm-3   znColumnElement" id="eluid53c56454">
                <div class="znColumnElement-innerWrapper-eluid53c56454 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">
                    <div class="znColumnElement-innerContent">
                        <div class="elm-custommenu clearfix eluidefee58aa  text-left elm-custommenu--normal">
                            <ul id="eluidefee58aa" class="elm-cmlist clearfix elm-cmlist--skin-light element-scheme--light zn_dummy_value elm-cmlist--normal nav-with-smooth-scroll">
                                <li id="menu-item-939" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-939"><a href="https://support.mercury.cash"><span>SUPPORT</span></a></li>
                                <li id="menu-item-940" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-940"><a href="https://www.mercury.cash/fees"><span>FEES</span></a></li>
                                <li id="menu-item-941" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-941"><a href="https://www.mercury.cash/careers"><span>CAREERS</span></a></li>
                                <li id="menu-item-942" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-942"><a href="https://support.mercury.cash/"><span>FAQ</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="eluid304d7cb3      col-md-3 col-sm-3   znColumnElement" id="eluid304d7cb3">
                <div class="znColumnElement-innerWrapper-eluid304d7cb3 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-center ">
                    <div class="znColumnElement-innerContent">
                        <div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--  tbk-icon-pos--after-title eluida7829318 "></div>
                        <div class="elm-socialicons eluidcb26c4d6  text-center sc-icon--center elm-socialicons--light element-scheme--light">
                            <ul class="redes">
                                <li>
                                    <a href="http://slack.mercury.cash/"><span class="icon-social-slack"></span></a>
                                </li>
                                <li>
                                    <a href="http://faceboook.com/mercurycash"><span class="icon-social-facebook"></span></a>
                                </li>
                                <li>
                                    <a href="http://twitter.com/mercurycash"><span class="icon-social-twitter"></span></a>
                                </li>
                                <li>
                                    <a href="http://instagram.com/mercury.cash"><span class="icon-social-instagram"></span></a>
                                </li>
                                <li>
                                    <a href="http://youtube.com/c/mercurycash"><span class="icon-social-youtube"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer id="footer" class="site-footer" role="contentinfo" itemscope="itemscope" itemtype="https://schema.org/WPFooter">
    <div class="container">
        <div class="row">
            <!--<div class="col-sm-4 col-sm-offset-4 col-md-3 col-md-offset-4">-->
            <div class="col-xs-8 col-xs-offset-1 col-sm-4 col-sm-offset-4 col-md-3 col-md-offset-4">
                <div class="bottom site-footer-bottom clearfix">
                    <div class="copyright footer-copyright">
                        <a href="<?php echo DIR_URL; ?>/blog" class="footer-copyright-link"><img class="footer-copyright-img" src="<?php echo DIR_URL; ?>/blog/wp-content/uploads/2017/06/mercury-cash-logo.png" width="227" height="64" alt="Mercury Cash: Ethereum Wallet, ETH/USD Market"></a>                           
                    </div>
                    <!-- end copyright -->
                </div>
                <!-- end bottom -->
            </div>
        </div>
        <!-- end row -->
    </div>
</footer>
<div id="appsmobile">
    <span>x</span>
    <img src="<?php echo DIR_URL; ?>/public/img/micon.png" alt="">
    <div class="text">
        <h3>MERCURY.CASH</h3>
        <p>Get the mercury.cash app for your phone</p>
    </div>
    <a href="">GET</a>
</div>