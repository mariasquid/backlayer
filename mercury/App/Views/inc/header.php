<nav class="navbar navbar-default navbar-fixed">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <!--<a class="navbar-brand" href=""><b>My Address:</b> 0x3b8fa2811bb6c863c807da6d0d702c6a759eec12</a>-->
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li>
                    <a href="<?php echo DIR_URL; ?>/dashboard">
                        <b>My Primary Address:</b> <span class="mywalletheader-span" id="mywalletheader"><?php echo $_SESSION['address']; ?></span>
                    </a>
                </li>
                <li><a href="#" id="copymyaddress" title="copy address"><i class="pe-7s-copy-file"></i></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li id="values_now">1 ETH = <span class="text-yellow">$<span id="valueEthinUsd"><?php echo $ethusd; ?></span> USD</span></li>
                <li><span class="divider"></span></li>
                <li class="dropdown li-profile">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo $_SESSION['avatar']; ?>" id="avatar" class="img-responsive hidden-xs hidden-sm" width="44" height="44"> <?php echo $_SESSION['name']; ?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo DIR_URL; ?>/settings">Profile</a></li>
                        <li><a href="<?php echo DIR_URL; ?>/settings/security">Settings</a></li>
                        <li><a href="https://mercurycash.kayako.com/Base/SSO/JWT/?type=customer&action=/HelpCenter/Login/Index&jwt=<?php echo $_SESSION['kayako_token']; ?>" target="_blank">Support</a></li>
                        <li><a href="<?php echo DIR_URL; ?>/home">Log out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>