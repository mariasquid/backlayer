<?php if (!empty($_SESSION['trialInfo'])) { ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#validateDocument').modal();
    });
</script>
<!-- Modal -->
<div class="modal fade" id="validateDocument" tabindex="-1" role="dialog" aria-labelledby="validateDocumentLabel" data-target=".bs-example-modal-sm">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="validateDocumentLabel">Attention</h4>
            </div>
            <div class="modal-body" style="text-align: center;">
                <p>In order to verify your account, please upload your Driver's License (US Citizens Only) or your Passport.</p>
                <p>Example of Selfie's ID Verification</p>
                <div class="row">
                    <div class="col-md-6">
                        <img src="<?php echo DIR_URL; ?>/public/img/do1.png" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-6">
                        <img src="<?php echo DIR_URL; ?>/public/img/do2.png" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="<?php echo DIR_URL; ?>/settings/document" class="btn btn-default">Validate Now</a>
            </div>
        </div>
    </div>
</div>
<?php } ?>