<div class="sidebar" data-color="black" data-image="">
	<div class="sidebar-wrapper">
		<div class="logo">
			<a href="<?php echo DIR_URL; ?>/dashboard" class="simple-text">
				<img src="<?php echo DIR_URL; ?>/public/img/logo-ligth.png" alt="" class="img-responsive">
			</a>
		</div>
		<ul class="nav">
			<li <?php echo ($_SERVER['QUERY_STRING'] == "url=dashboard") ? 'class="active"' : ''; ?>>
				<a href="<?php echo DIR_URL; ?>/dashboard">
					<i class="pe-7s-graph"></i>
					<p>Dashboard</p>
				</a>
			</li>
			<li <?php echo ($_SERVER['QUERY_STRING'] == "url=buy") ? 'class="active"' : ''; ?>>
				<a href="<?php echo DIR_URL; ?>/buy">
					<i class="pe-7s-repeat"></i>
					<p>Buy / Sell</p>
				</a>
			</li>
			<li <?php echo ($_SERVER['QUERY_STRING'] == "url=send") ? 'class="active"' : ''; ?>>
				<a href="<?php echo DIR_URL; ?>/send">
					<i class="pe-7s-paper-plane"></i>
					<?php echo ($requestsUnread > 0) ? '<div id="cantRequest">' . $requestsUnread . '</div>' : ''; ?>
					<p>Send / Request</p>
				</a>
			</li>
			<li <?php echo ($_SERVER['QUERY_STRING'] == "url=accounts") ? 'class="active"' : ''; ?>>
				<a href="<?php echo DIR_URL; ?>/accounts">
					<i class="pe-7s-wallet"></i>
					<p>Accounts</p>
				</a>
			</li>
			<!--
			<li <?php //echo ($_SERVER['QUERY_STRING'] == "url=tools") ? 'class="active"' : ''; ?>>
				<a href="<?php //echo DIR_URL; ?>/tools">
					<i class="pe-7s-portfolio"></i>
					<p>Tools</p>
				</a>
			</li>
			-->
			<li <?php echo ($_SERVER['QUERY_STRING'] == "url=settings") ? 'class="active"' : ''; ?>>
				<a href="<?php echo DIR_URL; ?>/settings">
					<i class="pe-7s-config"></i>
					<p>Settings</p>
				</a>
			</li>
			<!--
			<li <?php //echo ($_SERVER['QUERY_STRING'] == "url=buy") ? 'class="active"' : ''; ?>>
				<a href="icons.html">
					<i class="pe-7s-science"></i>
					<p>Icons</p>
				</a>
			</li>
			<li <?php //echo ($_SERVER['QUERY_STRING'] == "url=buy") ? 'class="active"' : ''; ?>>
				<a href="maps.html">
					<i class="pe-7s-map-marker"></i>
					<p>Maps</p>
				</a>
			</li>
			<li <?php //echo ($_SERVER['QUERY_STRING'] == "url=buy") ? 'class="active"' : ''; ?>>
				<a href="notifications.html">
					<i class="pe-7s-bell"></i>
					<p>Notifications</p>
				</a>
			</li>
			<li class="active-pro">
				<a href="upgrade.html">
					<i class="pe-7s-rocket"></i>
					<p>Upgrade to PRO</p>
				</a>
			</li>
			-->
		</ul>
	</div>
</div>