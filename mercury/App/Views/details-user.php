<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
	</head>
	<body class="dashboard-page sb-l-o sb-r-c">
		<div id="main">
			<?php include "inc/header.php"; ?>
			<?php include "inc/aside.php"; ?>
			<div id="content_wrapper">
				<section id="content" class="animated fadeIn">
					<div class="tray tray-center">
						<div class="mw1000 center-block">
							<div class="admin-form">
								<div class="panel heading-border">
									<form action="<?php echo DIR_URL; ?>/users/update" id="update" method="POST">
										<div class="panel-body bg-light">
											<div class="section-divider mb40">
												<span>Details form <?php echo $user['name'] . " " . $user['last_name']; ?></span>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="section">
														<label class="field">
															<input type="text" name="walletAddress" id="walletAddress" class="gui-input" value="<?php echo $wallet['address']; ?>" readonly="readonly" placeholder="Name...">
														</label>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													<div class="section">
														<label class="field">
															<input type="text" name="name" id="name" class="gui-input" value="<?php echo $user['name']; ?>" placeholder="Name...">
														</label>
													</div>
												</div>
												<div class="col-md-4">
													<div class="section">
														<label class="field">
															<input type="text" name="last_name" id="last_name" class="gui-input" value="<?php echo $user['last_name']; ?>" placeholder="Last Name...">
														</label>
													</div>
												</div>
												<div class="col-md-4">
													<div class="section">
														<label class="field">
															<input type="text" name="birthdate" id="birthdate" class="gui-input" value="<?php echo date("d/m/Y", $user['birthdate']); ?>" placeholder="Birth Date...">
														</label>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="section">
														<label class="field">
															<input type="email" name="email" id="email" class="gui-input" value="<?php echo $user['email']; ?>" placeholder="Email...">
														</label>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="section">
														<label class="field">
															<textarea class="gui-textarea" id="address" name="address" placeholder="Address"><?php echo $user['address']; ?></textarea>
														</label>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													<div class="section">
														<label class="field">
															<input type="text" name="city" id="city" class="gui-input" value="<?php echo $user['city']; ?>" placeholder="City...">
														</label>
													</div>
												</div>
												<div class="col-md-4">
													<div class="section">
														<label class="field">
															<input type="text" name="country" id="country" class="gui-input" value="<?php echo $user['country']; ?>" placeholder="Country...">
														</label>
													</div>
												</div>
												<div class="col-md-4">
													<div class="section">
														<label class="field">
															<input type="text" name="zip_code" id="zip_code" class="gui-input" value="<?php echo $user['zip_code']; ?>" placeholder="Zip Code...">
														</label>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="section">
														<label class="field select">
															<select id="status" name="status">
															<?php if ($user['status'] == 0) { ?>
															<option value="0">Inactive</option>
															<option value="1">Active</option>
															<option value="2">Without validating the email</option>
															<option value="3">Without validating the identity document</option>
															<?php } elseif ($user['status'] == 1) { ?>
															<option value="1">Active</option>
															<option value="0">Inactive</option>
															<option value="2">Without validating the email</option>
															<option value="3">Without validating the identity document</option>
															<?php } elseif ($user['status'] == 2) { ?>
															<option value="2">Without validating the email</option>
															<option value="0">Inactive</option>
															<option value="1">Active</option>
															<option value="3">Without validating the identity document</option>
															<?php } elseif ($user['status'] == 3) { ?>
															<option value="3">Without validating the identity document</option>
															<option value="0">Inactive</option>
															<option value="1">Active</option>
															<option value="2">Without validating the email</option>
															<?php } ?>
															</select>
															<i class="arrow double"></i>
														</label>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="section">
														<label class="field">
															<textarea class="gui-textarea" id="suspension" name="suspension" placeholder="If you wish to suspend <?php echo $user['name'] . " " . $user['last_name']; ?>, please indicate the reason"><?php echo ($user['status'] == 0) ? $suspension['description'] : NULL; ?></textarea>
														</label>
													</div>
												</div>
											</div>
										</div>
										<div class="panel-footer text-right">
											<input type="hidden" id="id" name="id" value="<?php echo $user['id']; ?>">
											<button type="submit" class="button btn-primary">UPDATE</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<?php include "inc/scripts.php"; ?>
		<script src="<?php echo DIR_URL; ?>/public/assets/js/users.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				"use strict";
				Core.init();
				// Init DataTables
			});
		</script>
	</body>
</html>