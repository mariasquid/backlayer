<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/moment.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/transactions.js?v=<?php echo time(); ?>"></script>
	</head>
	<body>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="header">
										<h4 class="title">Transactions</h4>
										<!--<p class="category">Here is a subtitle for this table</p>-->
									</div>
									<div class="content table-responsive table-full-width">
										<table class="table table-hover table-striped">
											<tbody>
												<?php foreach ($transactions as $transaction) { ?>
												<tr <?php if ($transaction['type_transaction'] == 1 && $type == 2) { echo 'style="display: none;"'; } ?>>
													<td><?php echo date("d/M", $transaction['date']); ?></td>
													<td>
														<?php if ($transaction['type_transaction'] == 1) { ?>
														<?php echo ($transaction['type_wallet'] == 2) ? '<i class="pe-7s-angle-right-circle" style="color: #c50101; font-size: 4rem;"></i>' : '<i class="pe-7s-angle-left-circle" style="color: #01c512; font-size: 4rem;"></i>'; ?>
														<?php } elseif ($transaction['type_transaction'] == 2) { ?>
														<?php echo ($transaction['type_wallet'] == 2) ? '<i class="pe-7s-angle-right-circle" style="color: #c50101; font-size: 4rem;"></i>' : '<i class="pe-7s-angle-left-circle" style="color: #01c512; font-size: 4rem;"></i>'; ?>
														<?php } elseif ($transaction['type_transaction'] == 3) { ?>
														<?php echo ($transaction['type_wallet'] == 2) ? '<i class="pe-7s-angle-right-circle" style="color: #c50101; font-size: 4rem;"></i>' : '<i class="pe-7s-angle-left-circle" style="color: #01c512; font-size: 4rem;"></i>'; ?>
														<?php } elseif ($transaction['type_transaction'] == 4) { ?>
														<?php echo ($transaction['type_wallet'] == 2) ? '<i class="pe-7s-angle-right-circle" style="color: #c50101; font-size: 4rem;"></i>' : '<i class="pe-7s-angle-left-circle" style="color: #01c512; font-size: 4rem;"></i>'; ?>
														<?php } elseif ($transaction['type_transaction'] == 5) { ?>
														<?php echo ($transaction['type_wallet'] == 2) ? '<i class="pe-7s-angle-right-circle" style="color: #c50101; font-size: 4rem;"></i>' : '<i class="pe-7s-angle-left-circle" style="color: #01c512; font-size: 4rem;"></i>'; ?>
														<?php } ?>
													</td>
													<td>
														<?php if ($transaction['type_transaction'] == 1) { ?>
														<h5>ETH transfer</h5>
														<?php } elseif ($transaction['type_transaction'] == 2) { ?>
														<h5>ETH Purchase</h5>
														<?php } elseif ($transaction['type_transaction'] == 3) { ?>
														<h5>Deposit to USD Wallet</h5>
														<?php } elseif ($transaction['type_transaction'] == 4) { ?>
														<h5>Sale of ETH</h5>
														<?php } elseif ($transaction['type_transaction'] == 5) { ?>
														<h5>Withdrawal of money</h5>
														<?php } ?>
													</td>
													<td><span data-toggle="tooltip" data-placement="top" title="<?php echo $transaction['description']; ?>"><?php echo (strlen($transaction['description']) <= 50) ? $transaction['description'] : substr($transaction['description'], 0, 47) . '...'; ?></span></td>
													<td>
														<?php if ($transaction['status'] == 0) { ?>
														<span class="label label-danger">Reject</span>
														<?php } elseif ($transaction['status'] == 1) { ?>
														<span class="label label-success">Approve</span>
														<?php } elseif ($transaction['status'] == 2) { ?>
														<span class="label label-warning">Pending</span>
														<?php } ?>
													</td>
													<td style="text-align: right;">
														<?php if ($transaction['type_transaction'] == 1) { ?>
														<span <?php echo ($transaction['trans'] == 'in') ? 'style="color: #01c512;"' : ''; ?>><?php echo ($transaction['trans'] == 'in') ? '+' : '-'; ?><span class="eth_amount"><?php echo $transaction['eth_amount']; ?></span> ETH</span>
														<br>
														<?php echo ($transaction['trans'] == 'in') ? '+' : '-'; ?><span class="usd_amount"><?php echo number_format($transaction['usd_amount'], 2); ?></span> USD
														<?php } elseif ($transaction['type_transaction'] == 2) { ?>
														<span <?php echo ($transaction['type_wallet'] == $type) ? 'style="color: #01c512;"' : '' ?>><?php echo ($transaction['type_wallet'] == $type) ? '+' : '-' ?><?php echo $transaction['eth_amount']; ?> ETH</span>
														<br>
														<?php echo ($transaction['type_wallet'] == $type) ? '+' : '-' ?><?php echo number_format($transaction['usd_amount'], 2); ?> USD
														<?php } elseif ($transaction['type_transaction'] == 3) { ?>
														<span  <?php echo ($transaction['type_wallet'] == $type) ? 'style="color: #01c512;"' : '' ?>><?php echo ($transaction['type_wallet'] == $type) ? '+' : '-' ?><?php echo $transaction['eth_amount']; ?> ETH</span>
														<br>
														<?php echo ($transaction['type_wallet'] == $type) ? '+' : '-' ?><?php echo number_format($transaction['usd_amount'], 2); ?> USD
														<?php } elseif ($transaction['type_transaction'] == 4) { ?>
														<span  <?php echo ($transaction['type_wallet'] == $type) ? 'style="color: #01c512;"' : '' ?>><?php echo ($transaction['type_wallet'] == $type) ? '+' : '-' ?><?php echo $transaction['eth_amount']; ?> ETH</span>
														<br>
														<?php echo ($transaction['type_wallet'] == $type) ? '+' : '-' ?><?php echo number_format($transaction['usd_amount'], 2); ?> USD
														<?php } elseif ($transaction['type_transaction'] == 5) { ?>
														<span  <?php echo ($transaction['type_wallet'] == $type) ? 'style="color: #01c512;"' : '' ?>><?php echo ($transaction['type_wallet'] == $type) ? '+' : '-' ?><?php echo $transaction['eth_amount']; ?> ETH</span>
														<br>
														<?php echo ($transaction['type_wallet'] == $type) ? '+' : '-' ?><?php echo number_format($transaction['usd_amount'], 2); ?> USD
														<?php } ?>
													</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>