<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/media/css/dataTables.bootstrap.css">
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/media/css/dataTables.plugins.css">
	</head>
	<body class="dashboard-page sb-l-o sb-r-c">
		<div id="main">
			<?php include "inc/header.php"; ?>
			<?php include "inc/aside.php"; ?>
			<div id="content_wrapper">
				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-visible" id="spy1">
								<div class="panel-heading">
									<div class="panel-title hidden-xs">
										<span class="glyphicon glyphicon-tasks"></span> All Purchases
									</div>
								</div>
								<div class="panel-body pn">
									<table class="table table-striped table-hover" id="datatable" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>ID</th>
												<th>ETH Amount</th>
												<th>USD Amount</th>
												<th>Date</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($purchases as $purchase) { ?>
											<tr>
												<td><?php echo $purchase['transaction_id_stripe']; ?></td>
												<td><?php echo $purchase['eth_amount']; ?> ETH</td>
												<td>$<?php echo number_format($purchase['usd_amount'], 2); ?></td>
												<td><?php echo date("d/m/Y H:i:s", $purchase['date']); ?></td>
												<?php if ($purchase['status'] == 0) { ?>
												<td><span class="label label-danger mr5 mb10 ib lh15">Rejected</span></td>
												<?php } elseif ($purchase['status'] == 1) { ?>
												<td><span class="label label-success mr5 mb10 ib lh15">Approved</span></td>
												<?php } elseif ($purchase['status'] == 2) { ?>
												<td><span class="label label-warning mr5 mb10 ib lh15">pending</span></td>
												<?php } ?>
												<td><a href="<?php echo DIR_URL; ?>/purchases/details/<?php echo $purchase['id']; ?>">Details</a></td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<?php include "inc/scripts.php"; ?>
		<script src="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
		<script src="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				"use strict";
				Core.init();
				// Init DataTables
				$('#datatable').dataTable({
					"sDom": 't<"dt-panelfooter clearfix"ip>',
					"oTableTools": {
						"sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
					}
				});
			});
		</script>
	</body>
</html>