<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
		<style type="text/css">
		body {color: #8d8da7;}
		.container {text-align: center;}
		h3 {font-weight: 400;}
		#contenedor {background-color: #fff;padding: 30px 0 60px 0;box-shadow: 0 0 5px 3px rgba(0, 0, 0, 0.35);margin-top: 50px;}
		ol, p {font-size: 1.2rem; padding: 0;}
		li {list-style-type: none;}
		img {margin: auto; width: 50%;}
		</style>
	</head>
	<body style="background-color: #f5f5f5;">
		<div class="wrapper">
			<div class="main">
				<div class="content">
					<div class="container">
						<div class="row">
							<div id="contenedor" class="col-md-8 col-md-offset-2">
								<div class="col-md-8 col-md-offset-2">
									<h3>Enable <span style="color: #3a7cec;">G</span><span style="color: #e33e2b;">o</span><span style="color: #f1b500;">o</span><span style="color: #3a7cec;">g</span><span style="color: #2ba14b;">l</span><span style="color: #e33e2b;">e</span> Authenticator</h3>
									<?php if (is_null($key)) { ?>
									<ol>
										<li>1. Enable Google Authenticator on your phone.</li>
										<li>2. Open the Enable Google Authenticator App.</li>
										<li>3. Tap Menu, then tap "Set up account" then tap "Scan barcode"</li>
										<li>4. Your phone will now be in a "scanning" mode. When you in <br> this mode, scan the barcode below</li>
									</ol>
									<img src="<?php echo $codeqr; ?>" alt="">
									<p>If you can not see the QR code or you have problems to scan, you can enter the following code manually in the Google Authenticator App on your device:</p>
									<h4><?php echo $secret; ?></h4>
									<p>Once you have scanned the barcode, ether the 6 digit code below:</p>
									<?php } ?>
									<div class="col-md-8 col-md-offset-2">
										<form method="POST" action="<?php echo DIR_URL; ?>/login/google_validate" id="login-form" autocomplete="off">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<input type="text" class="form-control" id="code" name="code" placeholder="Verification code">
													</div>
												</div>
												<div class="col-md-12">
													<div class="form-group">
														<label for="" class="checkbox">
															<input type="checkbox" name="disable_tfa" id="disable_tfa" value="1" data-toggle="checkbox">
															Disable Google Authenticator for 15 days
														</label>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-<?php echo (is_null($key)) ? '4' : '6'; ?>">
													<button type="submit" id="enter" class="btn-mercury-yellow">SUBMIT</button>
												</div>
												<div class="col-md-<?php echo (is_null($key)) ? '4' : '6'; ?>">
													<button type="button" onclick="window.location='<?php echo DIR_URL; ?>'" class="btn-mercury-black">CANCEL</button>
												</div>
												<?php if (is_null($key)) { ?>
												<div class="col-md-4">
													<button type="button" onclick="window.location='<?php echo DIR_URL; ?>/login/skip_google'" class="btn-mercury-black">SKIP</button>
												</div>
												<?php } ?>
											</div>
											<div class="clearfix"></div>
											<div class="alert">
												<?php if (!empty($_GET['error'])) { ?>
												<?php if ($_GET['error'] == 'codeinvalid') { ?>
												<div class="alert alert-danger">
													<span><b> Error - </b> The code is invalid</span>
												</div>
												<?php } ?>
												<?php } ?>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>