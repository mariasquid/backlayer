<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link href="<?php echo DIR_URL; ?>/public/assets/css/register.css" rel="stylesheet">
		<script src="<?php echo DIR_URL; ?>/public/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
		<script src="<?php echo DIR_URL; ?>/public/assets/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo DIR_URL; ?>/public/assets/js/bootstrap-checkbox-radio-switch.js"></script>
		<script src="<?php echo DIR_URL; ?>/public/assets/js/bootstrap-notify.js"></script>
		<script src="<?php echo DIR_URL; ?>/public/assets/js/light-bootstrap-dashboard.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jquery.numeric.min.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/dir_url.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/register.js?v=<?php echo time(); ?>"></script>
		<style type="text/css">
			.centrado > div  {
				margin: 0px auto;
			}
			@media (max-width: 600px){
				.block-header {
					height: 100px !important;
				}

				.block .block-header .btn-header{
					width: 97%;
					top: 45px;
					left: 1.5%;
					height: 50px;
					line-height: 36px;
				}
			}
		</style>
	</head>
	<body style="background-color: #f5f5f5;">
		<?php if (!empty($_SESSION['alert_success'])) { ?>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#myModal').modal();
			});
		</script>
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-target=".bs-example-modal-sm">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Attention</h4>
					</div>
					<div class="modal-body" style="text-align: center;">
						<?php echo $_SESSION['alert_success']; ?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default closeModal" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<?php unset($_SESSION['alert_success']); ?>
		<?php } ?>
		<div class="wrapper">
			<div class="main">
				<div class="content">
					<div class="container">
						<div class="row">
							<div class="col-sm-3 col-sm-offset-4">
								<div id="logo-register">
									<img src="<?php echo DIR_URL; ?>/public/img/logo-dark.png" alt="" class="img-responsive">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Sign up</span> Business account
										<a href="<?php echo DIR_URL; ?>/register" class="btn-header">Create personal account</a>
									</div>
									<div class="block-body">
										<?php if (!empty($_SESSION['alert'])) { ?>
										<div class="row">
											<div class="col-md-12">
												<div class="alertGrey">
													<?php echo $_SESSION['alert']; ?>
												</div>
											</div>
										</div>
										<?php unset($_SESSION['alert']); ?>
										<?php } ?>
										<div class="row">
											<div class="col-md-12">
												<div class="alertGrey alertDanger" id="error_message" style="display:none">
													
												</div>
											</div>
										</div>
										<form method="POST" id="register_business" action="<?php echo DIR_URL; ?>/register/business_register" autocomplete="off">
											<div class="row">
												<div class="col-md-6">
													<div class="row">
														<div class="col-md-6">
															<div class="form-elements">
																<input type="text" id="name" name="name" placeholder="First Name" required>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-elements">
																<input type="text" id="last_name" name="last_name" placeholder="Last Name" required>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-elements">
																<select name="code_country" id="code_country" required>
																	<option value="">Country</option>
																	<?php foreach ($countrys as $country) { ?>
																	<option value="<?php echo $country['phonecode']; ?>"><?php echo $country['nicename'] . " (+" .  $country['phonecode'] . ")"; ?></option>
																	<?php } ?>
																</select>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-elements">
																<input type="text" id="phone" name="phone" placeholder="Phone" required>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="form-elements">
																<input type="email" id="email" name="email"  placeholder="E-mail" required>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="form-elements">
																<input type="password" id="pwd" name="password" placeholder="Password">
																<div id="pwd_strength_wrap">
																	<div id="passwordDescription">Password not entered</div>
																	<div id="passwordStrength" class="strength0"></div>
																	<div id="pswd_info">
																		<strong>Strong Password Tips:</strong>
																		<ul>
																			<li class="invalid" id="length">At least 8 characters</li>
																			<li class="invalid" id="pnum">At least one number</li>
																			<li class="invalid" id="capital">At least one lowercase &amp; one uppercase letter</li>
																			<li class="invalid" id="spchar">At least one special character (!@#%&*?_.)</li>
																		</ul>
																	</div><!-- END pswd_info -->
													        	</div><!-- END pwd_strength_wrap -->
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="row">
														<div class="col-md-12">
															<div class="form-elements">
																<input type="text" name="business_name" placeholder="Company Name" required>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="form-elements">
																<textarea name="business_address" placeholder="Address" required></textarea>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="form-elements">
																<select name="category" id="category">
																	<option value="">Category</option>
																	<?php foreach ($categories as $category) { ?>
																	<option value="<?php echo $category['id']; ?>"><?php echo $category['category']; ?></option>
																	<?php } ?>
																</select>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="form-elements">
																<select name="subcategory" id="subcategory">
																	<option value="">Subcategory</option>
																</select>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-elements">
																<input type="text" id="dba" name="dba" placeholder="dba">
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-elements">
																<input type="text" id="taxid_ein" name="taxid_ein" placeholder="Tax ID/EIN" required>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="form-elements">
																<textarea name="source_founds" placeholder="Source Founds" required></textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4 col-md-offset-4">
													<div id="policy">
														<label>
															<div id="check-view">
																<div id="checked-view"></div>
															</div>
															<input type="checkbox" id="policy-check" name="tems" value="ok">
															I Agree to the <a href="<?php echo DIR_URL; ?>/terms" target="_blank">User Agreement</a> and the <a href="<?php echo DIR_URL; ?>/privacy" target="_blank">Privacy Policy</a>
														</label>
													</div>
												</div>
											</div>
											<div id="captcha">
												<hr>
												<div class="row">
													<div class="col-md-12" style="position: relative; height: 90px;">
														<div class="g-recaptcha centrado" data-sitekey="6LftmSQUAAAAAG97w4Owc0iku3REEOYEmhXXczIl"></div>
													</div>
												</div>
												<hr>
											</div>
											<div class="row">
												<div class="col-md-12">
													<input type="hidden" id="type_user" name="type_user" value="2">
													<button type="submit" class="btn-m-l" style="display: block; margin: 0px auto; width: 35%">SIGN UP</button>
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 col-md-offset-3">
										<p style="font-size: 12px; text-align: center; margin-top: 20px;">Aready have an account? <a href="<?php echo DIR_URL; ?>/login">Sign in</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src='https://www.google.com/recaptcha/api.js'></script>
	</body>
</html>