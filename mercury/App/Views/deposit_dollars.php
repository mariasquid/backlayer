<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <?php include "inc/scripts.php"; ?>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jquery.numeric.min.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jquery.maskedinput.min.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/deposit.js?v=<?php echo time(); ?>"></script>
    </head>
    <body>
        <?php if (isset($_GET['checkout'])) { ?>
        <div id="pay_card">
            <script src="https://test.oppwa.com/v1/paymentWidgets.js?checkoutId=<?php echo $_GET['checkout']; ?>"></script>
            <script>
                var wpwlOptions = {
                    style: "card"
                }
            </script>
            <div id="pay_card_form">
                <form action="<?php echo DIR_URL; ?>/accounts/processDepositCard" class="paymentWidgets">VISA MASTER AMEX</form>
            </div>
        </div>
        <?php } ?>
        <?php include "inc/trialInfoModal.php"; ?>
        <?php if (!empty($_SESSION['alert_success'])) { ?>
        <script type="text/javascript">
        $(document).ready(function() {
        $('#alert').modal();
        });
        </script>
        <!-- Modal -->
        <div class="modal fade" id="alert" tabindex="-1" role="dialog" aria-labelledby="alertLabel" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="alertLabel">Attention</h4>
                    </div>
                    <div class="modal-body">
                        <?php echo $_SESSION['alert_success']; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default closeModal" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <?php unset($_SESSION['alert_success']); ?>
        <?php } ?>
        <!-- Modal -->
        <div class="modal fade" id="info" tabindex="-1" role="dialog" aria-labelledby="infoLabel" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="infoLabel">Attention</h4>
                    </div>
                    <div class="modal-body">
                        <p id="message_info"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default closeModal" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Attention</h4>
                    </div>
                    <div class="modal-body" style="text-align: center;">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default closeModal" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="loading">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <div class="content">
                    <div class="container-fluid">
                        <form action="<?php echo DIR_URL; ?>/accounts/depositProcess" method="POST" autocomplete="off">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tabs-m">
                                        <ul>
                                            <li><a href="<?php echo DIR_URL; ?>/buy">Buy</a></li>
                                            <li><a href="<?php echo DIR_URL; ?>/sell">Sell</a></li>
                                            <li class="active"><a href="<?php echo DIR_URL; ?>/accounts/deposit">Deposit to USD Wallet</a></li>
                                            <li><a href="<?php echo DIR_URL; ?>/buy/limits">Limits</a></li>
                                        </ul>
                                    </div>
                                    <div class="block last">
                                        <div class="block-header">
                                            <span class="text-yellow">Payment method</span>
                                        </div>
                                        <div class="block-body">
                                            <?php if (!empty($_SESSION['alert'])) { ?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="alertGrey">
                                                        <?php echo $_SESSION['alert']; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php unset($_SESSION['alert']); ?>
                                            <?php } ?>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h6>STEP 1 - Choose your payment method</h6>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="radio" id="card_check" name="payment_method" value="2">
                                                            <div class="payment_method" data-method="#card_check" data-name="Debit / Credit Card">
                                                                <i class="pe-7s-check icon-check"></i>
                                                                <i class="pe-7s-credit" style="font-size: 45px;margin-top: 10px;"></i>
                                                                <h3 style="margin: 0;">Card</h3>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="radio" id="bank_check" name="payment_method" value="3">
                                                            <div class="payment_method" data-method="#bank_check" data-name="Bank Account" style="opacity: 0.3;">
                                                                <i class="pe-7s-check icon-check"></i>
                                                                <i class="pe-7s-culture" style="font-size: 45px;margin-top: 10px;"></i>
                                                                <h3 style="margin: 0;">ACH</h3>
                                                                <p>Comming Soon</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="radio" id="wire_transfer" name="payment_method" value="4">
                                                            <div class="payment_method" data-method="#wire_transfer" data-name="Wire Transfer">
                                                                <i class="pe-7s-check icon-check"></i>
                                                                <i class="pe-7s-culture" style="font-size: 45px;margin-top: 10px;"></i>
                                                                <h3 style="margin: 0;">Wire Transfer</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-elements">
                                                                <div id="content_bank_accounts" style="display: none;">
                                                                    <?php if (count($accounts) <= 0) { ?>
                                                                    <div id="load_bank_accounts" class="alert alert-danger">
                                                                        <span>You have not registered any bank account</span>
                                                                    </div>
                                                                    <?php } else { ?>
                                                                    <div class="col-md-10">
                                                                        <label>Select Bank Account</label>
                                                                        <select name="bank_accounts" id="bank_accounts">
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="form-elements">
                                                                            <label>&nbsp;</label>
                                                                            <a href="#" id="delete_ach" class="btn btn-info btn-fill pull-right" style="display: block;">Remove ACH</a>
                                                                        </div>
                                                                    </div>
                                                                    <?php } ?>
                                                                </div>
                                                                <div id="select_wire" class="row" style="display: none;">
                                                                    <div class="col-md-12">
                                                                        <div class="form-elements">
                                                                            <label>Wire Transfer Type</label>
                                                                            <select name="wire" id="wire">
                                                                                <option value="1">Domestic</option>
                                                                                <option value="2">International</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- TARJETAS -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <hr>
                                                    <h6>Payment Method</h6>
                                                    <h4 id="payment_method">Please select a payment method.</h4>
                                                    <hr>
                                                    <h6>Step 2 - Set the purchase amount</h6>
                                                    <div class="form-elements">
                                                        <label>Amount to deposit (At least $<span id="least">1.00</span>)</label>
                                                        <input type="text" id="usd_amount" name="usd_amount" class="ach" placeholder="Amount to deposit">
                                                    </div>
                                                    <hr>
                                                    <div class="block-amounts">
                                                        <ul>
                                                            <li><span class="tags-amounts">Subtotal:</span> <span id="subtotal" class="amounts">$0.00</span></li>
                                                            <li><span class="tags-amounts"><a target="_blank" href="<?php echo DIR_URL; ?>/fees">Fee:</span> </a>  <span id="fee" class="amounts">$0.00</span></li>
                                                            <li><span class="tags-amounts">Total:</span> <span id="total" class="amounts">$0.00</span></li>
                                                        </ul>
                                                    </div>
                                                    <hr>
                                                    <div class="form-group">
                                                        <input type="hidden" id="eth_amount" value="0">
                                                        <input type="hidden" id="client_name" value="<?php echo $_SESSION['name']; ?>">
                                                        <div class="row">
                                                            <div id="button" class="col-md-4 col-md-offset-8 col-lg-3 col-lg-offset-9">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include "inc/close.php"; ?>
    </body>
</html>