<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<style type="text/css">
		body {color: #8d8da7;}
		.container {text-align: center;}
		h3 {font-weight: 400;}
		#contenedor {background-color: #fff;padding: 30px 0 60px 0;box-shadow: 0 0 5px 3px rgba(0, 0, 0, 0.35);margin-top: 50px;}
		ol, p {font-size: 1.2rem; padding: 0;}
		li {list-style-type: none;}
		img {margin: auto; width: 50%;}
		</style>
	</head>
	<body style="background-color: #f5f5f5;">
		<div class="wrapper">
			<div class="main">
				<div class="content">
					<div class="container">
						<div class="row">
							<div id="contenedor" class="col-md-8 col-md-offset-2">
								<div class="col-md-8 col-md-offset-2">
									<img src="<?php echo DIR_URL; ?>/public/img/logo-dark.png" alt="" class="img-responsive">
									<h3>Device does not match</h3>
									<p>We are detecting that the current device is not the same device that you want to authorize.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>