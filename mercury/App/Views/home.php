<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Mercury Cash: Ethereum Wallet, ETH/USD Market</title>
		<meta name="description" content="Mercury cash is a specialized and secure platform that has its own ethereum wallet and market exchange for ETH/USD">
		<meta name="keywords" content="Ethereum Wallet">
		<meta name="robots" content="index, follow">
		<meta name="copyright" content="Adventurous Entertainment LLC">
		<meta name="language" content="EN">
		<meta name="author" content="Adventurous Entertainment LLC">
		<meta name="creationdate" content="May 2017">
		<meta name="distribution" content="global">
		<meta name="rating" content="general">
		<!-- Open Graph data -->
		<meta property="og:title" content="Mercury Cash: Ethereum Wallet, ETH/USD Market" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content=" https://www.mercury.cash" />
		<meta property="og:image" content="https://www.mercury.cash/public/img/OpenGraph.jpg" />
		<meta property="og:description" content="Mercury cash is a specialized and secure platform that has its own ethereum wallet and market exchange for ETH/USD" />
		<meta property="og:site_name" content="Mercury Cash" />
		<link rel="Publisher" href="https://www.adenter.io">
		<link rel="Canonical" href="https://www.mercury.cash">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo DIR_URL; ?>/public/css/theme.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700'>
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
		<link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic'>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/fonts/elegant/style.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/css/social.css" rel="stylesheet">
		<!-- Multiple Favicon 14-5-2017 -->
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#f3c805">
		<meta name="apple-mobile-web-app-title" content="Mercury Cash">
		<meta name="application-name" content="Mercury Cash">
		<meta name="msapplication-TileColor" content="#2f3740">
		<meta name="msapplication-TileImage" content="/mstile-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!-- Live chat -->
		<script type="text/javascript">
		window.__lo_site_id = 80707;
		(function() {
		var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
		wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
		})();
		</script>
		<!-- Google Analytics 14-5-2017 -->
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-98285209-1', 'auto');
		ga('send', 'pageview');
		</script>
	</head>
	<body id="page-top" class="index">
		<!-- Navigation -->
		<nav class="navbar navbar-default">
			<!-- Topbar Nav (hidden) -->
			<div class="topbar-nav clearfix">
				<div class="container">
					<ul class="topbar-left list-unstyled list-inline">
						<li> <span class="fa fa-phone"></span> 123-456-7890 </li>
						<li> <span class="fa fa-envelope"></span> info@yourdomain.com </li>
					</ul>
					<ul class="topbar-right list-unstyled list-inline topbar-social">
						<li> <a href="#"> <span class="fa fa-facebook"></span> </a></li>
						<li> <a href="#"> <span class="fa fa-twitter"></span> </a></li>
						<li> <a href="#"> <span class="fa fa-google-plus"></span> </a></li>
						<li> <a href="#"> <span class="fa fa-dribbble"></span> </a></li>
						<li> <a href="#"> <span class="fa fa-instagram"></span> </a></li>
					</ul>
				</div>
			</div>
			<div class="container" style="max-width: 1050px;">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header page-scroll">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand page-scroll" href="<?php echo DIR_URL; ?>">
						<img id="logo-navbar" src="<?php echo DIR_URL; ?>/public/img/logo-ligth.png" class="img-responsive" alt="">
					</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="<?php echo DIR_URL; ?>" class="page-scroll">1 ETH = $<?php echo $ethusd; ?></a>
						</li>
						<li>
							<a href="<?php echo DIR_URL; ?>/about">ABOUT US</a>
						</li>
						<li>
							<a href="<?php echo DIR_URL; ?>/login" class="page-scroll">SIGN IN</a>
						</li>
						<li>
							<a href="<?php echo DIR_URL; ?>/register" class="page-scroll">START NEW ACCOUNT</a>
						</li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>
		<!-- Hero Content -->
		<header id="hero">
			<div class="container">
				<div class="intro-text">
					<div class="intro-lead-in hidden">This is a lead in statement!</div>
					<!--<div class="intro-heading"><strong>SMART MONEY</strong> <br> FOR <br> <strong>SMART PEOPLE</strong></div>-->
					<div class="slogan money"><span class="yellow">smart</span> money</div>
					<div class="slogan people">for smart <span class="yellow">people</span></div>
					<a href="<?php echo DIR_URL; ?>/register" class="page-scroll btn btn-xl btn-primary">Start New Account</a>
				</div>
			</div>
		</header>
		
		<!-- Clients Section -->
		<section id="clients">
			<div class="container">
				<div class="row">
					<div class="col-xs-6 col-sm-4 col-sm-4 col-sm-offset-1 col-md-2 col-md-offset-3">
						<a href="https://entethalliance.org/" target="_blank"><img src="/public/img/eea.png" alt="Enterprise Ethereum Aliance" class="img-responsive"></a>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-3">
						<a href="http://www.orlandosentinel.com/business/brinkmann-on-business/os-virtual-currency-startup-exchange-20170916-story.html" target="_blank"><img src="/public/img/orlando.png" alt="Orlando Sentinel" class="img-responsive" style="margin-top: 15px;"></a>
					</div>
				</div>
			</div>
		</section>
		<!-- Services Section -->
		<section id="services">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="col-xs-2">
							<span class="icon-phone icons"></span>
						</div>
						<div class="col-xs-10">
							<h3>MOBILE WALLET</h3>
							<p>Mercury Cash app works on Android or iPhone.</p>
							<br>
						</div>
					</div>
					<div class="col-md-6">
						<div class="col-xs-2">
							<span class="icon-lock icons"></span>
						</div>
						<div class="col-xs-10">
							<h3>SAFE STORAGE</h3>
							<p>Our infraescturcture allows and offer secure storage.</p>
							<br>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="col-xs-2">
							<span class="icon-shield icons"></span>
						</div>
						<div class="col-xs-10">
							<h3>PROTECTION INSURANCE</h3>
							<p>We are covered by an insurance policy.</p>
							<br>
						</div>
					</div>
					<div class="col-md-6">
						<div class="col-xs-2">
							<span id="icon-refresh" class="icon-refresh icons"></span>
						</div>
						<div class="col-xs-10">
							<h3>INSTANT CHANGE</h3>
							<p>Exchange to local currency your cryptocurrency.</p>
							<br>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Flat Features Section -->
		<section id="features-flat" class="bg-light">
			<div class="container">
				
				<div class="row">
					<div class="col-sm-12 col-md-6">
						<div class="row">
							<div class="col-md-10">
								<h3>CREATE AN ACCOUNT</h3>
								<p>Create a wallet in digital currency where you store digital currency securely.</p>
							</div>
							<div class="col-md-10">
								<h3>CONNECT</h3>
								<p>Link a bank account, credit or debit card to be able to convert digital currency to and from your local currency.</p>
							</div>
							<div class="col-md-10">
								<h3>BUY ETHEREUM</h3>
								<p>Buy some ethereum to begin using the future of money.</p>
							</div>
						</div>
					</div>
					<div class="hidden-sm hidden-xs col-md-6">
						<img src="<?php echo DIR_URL; ?>/public/img/signup.png" title="CREATE AN ACCOUNT" class="img-responsive pull-right">
					</div>
				</div>
				
			</div>
		</section>
		<div id="slider-apps" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#slider-apps" data-slide-to="0" class="active"></li>
				<li data-target="#slider-apps" data-slide-to="1"></li>
			</ol>
			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<section id="merchant" class="slider_home">
						<div class="container">
							<div class="row">
								<div class="col-md-7 col-lg-6" style=" text-align: center;">
									<h2><span class="yellow">MERCHANT</span> SERVICES</h2>
									<ul>
										<li> - Accept Ethereum Payments to your existing web checkout</li>
										<li> - Sell to customers in more than 200 markets around the globe</li>
										<li> - Access new customers who look for way to pay in cryptocurrency</li>
									</ul>
									<a href="#" class="page-scroll btn btn-xl btn-primary">Pre-Register Now</a>
								</div>
							</div>
						</div>
					</section>
				</div>
				<div class="item">
					<section id="apps" class="slider_home">
						<div class="container">
							<div class="row">
								<div class="col-md-12 col-lg-6">
									<h2>Get your<br>Ethereum Wallet</h2>
									<p>That's all you need to start using cryptocurrencies.</p>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 col-sm-3 col-sm-offset-3 col-md-2 col-md-offset-0">
									<a href="https://play.google.com/store/apps/details?id=com.adenter.mercurycash&hl=en" target="_blank">
										<img src="<?php echo DIR_URL; ?>/public/img/app-android.png" alt="" class="img-responsive">
									</a>
								</div>
								<div class="col-xs-6 col-sm-3 col-md-2">
									<a href="https://itunes.apple.com/us/app/mercury-cash/id1291394963?mt=8" target="_blank">
										<img src="<?php echo DIR_URL; ?>/public/img/app-ios.png" alt="" class="img-responsive">
									</a>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
			<!-- Controls -->
			<a class="left carousel-control" href="#slider-apps" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#slider-apps" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
		<!-- Contact Section -->
		<section id="contact">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<h2 class="section-heading">Get in touch and explore the possibilities!</h2>
					</div>
				</div>
				<form name="sentMessage" id="contactForm" class="mw800 center-block clearfix" novalidate>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
						<p class="help-block text-danger"></p>
					</div>
					<div class="form-group">
						<input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
						<p class="help-block text-danger"></p>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-xl btn-block btn-wire">Subscribe</button>
					</div>
				</form>
				<p> By subscribing you agree to our terms and conditions. </p>
			</div>
		</section>
		<!-- Footer -->
		<?php include "inc/footer-homes.php"; ?>
		<!-- jQuery -->
		<!-- <script src="js/vendor/jquery.js"></script> -- Local Version -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
		<!-- Bootstrap Core JavaScript -->
		<!-- <script src="js/vendor/bootstrap.min.js"></script> -- Local Version -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<!-- Contact Form JavaScript -->
		<script src="<?php echo DIR_URL; ?>/public/js/vendor/jqBootstrapValidation.js"></script>
		<script src="<?php echo DIR_URL; ?>/public/js/contact_me.js"></script>
		<!-- Custom Theme JavaScript -->
		<script src="<?php echo DIR_URL; ?>/public/js/main.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/device.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
		$('#myModal').modal();
		});
		</script>
	</body>
</html>