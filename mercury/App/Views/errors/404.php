<!DOCTYPE html>
<!--
* OOPS - Perfect 404 pages Pack
* Build Date: October 2016
* Last Update: October 2016
* Author: Madeon08 for ThemeHelite
* Copyright (C) 2016 ThemeHelite
* This is a premium product available exclusively here : http://themeforest.net/user/Madeon08/portfolio
* -->
<html lang="en-us" class="no-js">

	<head>
		<meta charset="utf-8">
		<title>We are doing maintenance work, come back soon</title>
		<meta name="description" content="Mercury cash is a specialized and secure platform that has its own ethereum wallet and market exchange for ETH/USD">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Adventurous Entertainment LLC">

        <!-- ================= Favicons ================== -->
        <!-- Multiple Favicon 14-5-2017 -->
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#f3c805">
        <meta name="apple-mobile-web-app-title" content="Mercury Cash">
        <meta name="application-name" content="Mercury Cash">
        <meta name="msapplication-TileColor" content="#2f3740">
        <meta name="msapplication-TileImage" content="/mstile-144x144.png">
        <meta name="theme-color" content="#ffffff">

		<!-- ============== Resources style ============== -->
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/css/style.css" />
		  <!-- Google Analytics 14-5-2017 -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-98285209-1', 'auto');
ga('send', 'pageview');

</script>
	</head>

	<body class="flat">

        <!-- Canvas for particles animation -->
        <div id="particles-js"></div>

        

        <div class="content">

            <div class="content-box">

                <div class="big-content">

                    <!-- Main squares for the content logo in the background -->
                    <div class="list-square">
                        <span class="square"></span>
                        <span class="square"></span>
                        <span class="square"></span>
                    </div>

                    <!-- Main lines for the content logo in the background -->
                    <div class="list-line">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>

                    <!-- The animated searching tool -->
                    <i class="fa fa-search" aria-hidden="true"></i>

                    <!-- div clearing the float -->
                    <div class="clear"></div>

                </div>

                <!-- Your text -->
                <h1>We are doing maintenance work, come back soon</h1>

                

            </div>

        </div>

        <footer>

            <ul>
                <li><a href="/">Home</a></li>

                
            </ul>

        </footer>

        <!-- ///////////////////\\\\\\\\\\\\\\\\\\\ -->
        <!-- ********** jQuery Resources ********** -->
        <!-- \\\\\\\\\\\\\\\\\\\/////////////////// -->

        <!-- * Libraries jQuery and Bootstrap - Be careful to not remove them * -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- Particles plugin -->
        <script src="<?php echo DIR_URL; ?>/public/js/particles.js"></script>

    </body>

</html>