<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/assets/css/jasny-bootstrap.min.css">
        <?php include "inc/scripts.php"; ?>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jasny-bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jquery.maskedinput.min.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/settings.js?v=<?php echo time(); ?>"></script>
        <script type="text/javascript">
        $(document).ready(function() {
            $("#ssn").mask("***-**-****");
            $("#birthdate").mask("9999-99-99", {
                placeholder:"YYYY-MM-DD"
            });
        });
        </script>
    </head>
    <body>
        <?php include "inc/trialInfoModal.php"; ?>
        <div class="loading">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1">
                                <div class="tabs-m">
                                    <ul>
                                        <li><a href="<?php echo DIR_URL; ?>/settings">Personal information</a></li>
                                        <?php echo ($type_user == 2) ? '<li class="active"><a href="' . DIR_URL . '/settings/businessInfo">Business information</a></li>' : ''; ?>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/document">Document</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/bank">Your Financial Information</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/password">Change Password</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/security">Two Factor Authenticator</a></li>
                                    </ul>
                                </div>
                                <div class="block">
                                    <div class="block-header">
                                        <span class="text-yellow">Edit profile</span>
                                    </div>
                                    <div class="block-body">
                                        <?php if (!empty($_SESSION['alert'])) { ?>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="alertGrey">
                                                    <?php echo $_SESSION['alert']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php unset($_SESSION['alert']); ?>
                                        <?php } ?>
                                        <div class="row">
                                            <form action="<?php echo DIR_URL; ?>/settings/updateBusinessInfo" id="update-profile" method="POST" autocomplete="off">
                                                <div class="col-md-8 col-md-offset-2">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-elements">
                                                                <label for="name">Name</label>
                                                                <input type="text" id="name" name="name" value="<?php echo $business['name']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-elements">
                                                                <label for="dba">DBA</label>
                                                                <input type="text" id="dba" name="dba" value="<?php echo $business['dba']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-elements">
                                                                <label for="taxid_ein">Tax ID/EIN</label>
                                                                <input type="text" id="taxid_ein" name="taxid_ein" value="<?php echo $business['taxid_ein']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-elements">
                                                                <label for="source_founds">Source Founds</label>
                                                                <input type="text" id="source_founds" name="source_founds" value="<?php echo $business['source_founds']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-elements">
                                                                <label for="address">Address</label>
                                                                <textarea name="address" id="address"><?php echo $business['address']; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-md-offset-3 col-lg-3 col-lg-offset-4">
                                                            <input type="hidden" name="token" value="<?php echo $_SESSION['validateToken']; ?>">
                                                            <button type="submit" class="btn-m-l" style="margin-top: 57px; margin-bottom: 40px;">UPDATE</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "inc/close.php"; ?>
    </body>
</html>