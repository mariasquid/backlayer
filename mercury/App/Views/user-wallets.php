<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="https://node.mercury.cash/socket.io/socket.io.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/bower_components/bignumber.js/bignumber.min.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/bower_components/web3/dist/web3-light.min.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/wallets.js?v=<?php echo time(); ?>"></script>
	</head>
	<body>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-2 col-md-offset-10">
								<a data-href="<?php echo DIR_URL; ?>/tools/total" id="new-wallet" data-user="<?php echo $_SESSION['id']; ?>" data-password="<?php echo time(); ?>" class="btn btn-info btn-fill pull-right">New Wallet</a>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="header">
										<h4 class="title">My Wallets</h4>
										<!--<p class="category">Here is a subtitle for this table</p>-->
									</div>
									<div class="content table-responsive table-full-width">
										<table class="table table-hover table-striped">
											<thead>
												<th>Alias</th>
												<th>Address</th>
												<th>Actions</th>
											</thead>
											<tbody>
												<?php foreach ($wallets as $wallet) { ?>
												<tr>
													<td>
														<div class="input-group">
															<input class="form-control edit-alias" id="wallet-<?php echo $wallet['id']; ?>" value="<?php echo $wallet['alias']; ?>">
															<span class="input-group-btn">
																<button class="btn btn-default button-edit" data-id="<?php echo $wallet['id']; ?>" data-input="#wallet-<?php echo $wallet['id']; ?>" type="button">save</button>
															</span>
														</div>
													</td>
													<td><?php echo $wallet['address']; ?></td>
													<td><?php echo ($wallet['principal'] == 1) ? '<span class="label label-success">Primary</span>' : ''; ?></td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>