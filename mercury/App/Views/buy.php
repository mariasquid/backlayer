<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <?php include "inc/scripts.php"; ?>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jquery.maskedinput.min.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jquery.numeric.min.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/buy.js?v=<?php echo time(); ?>"></script>
    </head>
    <body>
        <?php if (isset($_GET['checkout'])) { ?>
        <div id="pay_card">
            <script src="https://test.oppwa.com/v1/paymentWidgets.js?checkoutId=<?php echo $_GET['checkout']; ?>"></script>
            <script>
                var wpwlOptions = {
                    style: "card"
                }
            </script>
            <div id="pay_card_form">
                <form action="<?php echo DIR_URL; ?>/buy/processPayCard" class="paymentWidgets">VISA MASTER AMEX</form>
            </div>
        </div>
        <?php } ?>
        <?php if ($usd_wallet['balance'] == 0) { ?>
        <script type="text/javascript">
        $(document).ready(function() {
        $('#validateUsdWalletFunds').modal();
        });
        </script>
        <!-- Modal -->
        <div class="modal fade" id="validateUsdWalletFunds" tabindex="-1" role="dialog" aria-labelledby="validateUsdWalletFundsLabel" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="validateUsdWalletFundsLabel">Attention</h4>
                    </div>
                    <div class="modal-body" style="text-align: center;">
                        <p>Your USD Wallet has no funds, please add funds to your USD Wallet in order to buy ETH</p>
                    </div>
                    <div class="modal-footer">
                        <a href="<?php echo DIR_URL; ?>/accounts/deposit" class="btn btn-default">Deposit Now</a>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php include "inc/trialInfoModal.php"; ?>
        <div class="modal fade" id="paymentDisabled" tabindex="-1" role="dialog" aria-labelledby="paymentDisabledLabel" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="paymentDisabledLabel">Attention</h4>
                    </div>
                    <div class="modal-body" style="text-align: center;">
                        <p>This payment method is not available at the moment, we apologize for the inconvenience. Keep posted in <a href='www.mercury.cash/press'>www.mercury.cash/press</a> for news about the availability of this payment method.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="loading">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabs-m">
                                    <ul>
                                        <li class="active"><a href="<?php echo DIR_URL; ?>/buy">Buy</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/sell">Sell</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/accounts/deposit">Deposit to USD Wallet</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/buy/limits">Limits</a></li>
                                    </ul>
                                </div>
                                <div class="block">
                                    <div class="block-header">
                                        <span class="text-yellow">Buy ethereum</span>
                                    </div>
                                    <div class="block-body">
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="alertGrey alertDanger" id="error_message" style="display:none">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <?php if (!empty($_SESSION['alert'])) { ?>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="alertGrey">
                                                    <?php echo $_SESSION['alert']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php unset($_SESSION['alert']); ?>
                                        <?php } ?>
                                        <form action="<?php echo DIR_URL; ?>/buy/process" method="POST" autocomplete="off">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h6>STEP 1 - Choose your payment method</h6>
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <input type="radio" id="usd_wallet_check" name="payment_method" value="1">
                                                                    <div class="payment_method" data-method="#usd_wallet_check" data-name="USD Wallet">
                                                                        <i class="pe-7s-check icon-check"></i>
                                                                        <h3>USD Wallet</h3>
                                                                        <span>$<span id="balanceUsd"><?php echo number_format($usd_wallet['balance'], 2); ?></span></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <input type="radio" id="card_check" name="payment_method" value="2">
                                                                    <div class="payment_method" data-method="#card_check" data-name="Credit Card">
                                                                        <i class="pe-7s-check icon-check"></i>
                                                                        <i class="pe-7s-credit" style="font-size: 45px;margin-top: 10px;"></i>
                                                                        <h3 style="margin: 0;">Card</h3>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <input type="radio" name="payment_method" value="3">
                                                                    <div class="payment_method_disabled">
                                                                        <i class="pe-7s-check icon-check"></i>
                                                                        <i class="pe-7s-culture" style="font-size: 45px;margin-top: 10px;"></i>
                                                                        <h3 style="margin: 0;">Bank Account</h3>
                                                                        <p>Comming Soon</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-elements">
                                                                <div id="content_bank_accounts" style="display: none;">
                                                                    <?php if (count($accounts) <= 0) { ?>
                                                                    <div id="load_bank_accounts" class="alert alert-danger">
                                                                        <span>You have not registered any bank account</span>
                                                                    </div>
                                                                    <?php } else { ?>
                                                                    <div class="col-md-9">
                                                                        <div class="form-elements">
                                                                            <label>Select Bank Account</label>
                                                                            <select name="bank_accounts" id="bank_accounts">
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-elements">
                                                                            <label>&nbsp;</label>
                                                                            <a href="#" id="delete_ach" class="btn-m-l" style="display: block;">Remove ACH</a>
                                                                        </div>
                                                                    </div>
                                                                    <?php } ?>
                                                                </div>
                                                                <!-- TARJETAS -->
                                                                <div id="content_card_accounts" style="display: none;">
                                                                    <?php if (count($cards) <= 0) { ?>
                                                                    <div id="load_bank_accounts" class="alert alert-danger">
                                                                        <span>You have not registered any card</span>
                                                                    </div>
                                                                    <?php } else { ?>
                                                                    <div class="col-md-9">
                                                                        <div class="form-elements">
                                                                            <label>Select Card</label>
                                                                            <select name="cards" id="cards">
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-elements">
                                                                            <label>&nbsp;</label>
                                                                            <a href="#" id="delete_card" class="btn-m-l" style="display: block;">Remove card</a>
                                                                        </div>
                                                                    </div>
                                                                    <?php } ?>
                                                                </div>
                                                                <br>
                                                                <!-- AGREGAR NUEVA TARJETA -->
                                                                <!--
                                                                <div class="row">
                                                                    <div class="col-md-3 col-md-offset-9">
                                                                        <a href="#" class="btn-m-l" data-toggle="modal" data-target="#paymentDisabled" style="margin-top: 15px; text-align: center;">Add Card</a>
                                                                        <a href="#" class="btn-m-l" data-toggle="modal" data-target="#paymentDisabled" style="margin-top: 15px; text-align: center;">Add ACH</a>
                                                                    </div>
                                                                </div>
                                                                -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="content">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-elements">
                                                                            <h6>Step 2 - Set the purchase amount</h6>
                                                                            <br>
                                                                            <label>Select Recipient Address</label>
                                                                            <select name="eth_wallet" id="eth_wallet">
                                                                                <?php foreach ($ethwallets as $ethwallet) { ?>
                                                                                <option value="<?php echo $ethwallet['id']; ?>"><?php echo (is_null($ethwallet['alias']) ? $ethwallet['address'] : $ethwallet['alias']); ?> (<?php echo $ethwallet['wallet_balance']; ?> ETH)</option>
                                                                                <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-elements">
                                                                            <div class="form-elements">
                                                                                <label>Eth amount</label>
                                                                                <input type="text" id="eth_amount" name="eth_amount" value="0" disabled="disabled" placeholder="ethereums you want to buy">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-elements">
                                                                            <div class="form-elements">
                                                                                <label>Dollars amount (At least $1)</label>
                                                                                <input type="text" id="usd_amount" name="usd_amount" value="0" disabled="disabled" placeholder="Amount of dollars to pay">
                                                                                <button id="usd_max_buy" class="max_buy">MAX BUY</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div id="progress-bar">
                                                                            <p>Your available <span id="type_method">method</span> limit</p>
                                                                            <div id="limit-progress" class="progress">
                                                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                                                                    100%
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" id="client_name" value="<?php echo $_SESSION['name']; ?>">
                                                                        <div class="row">
                                                                            <div class="col-md-3 col-md-offset-4">
                                                                                <button id="button-pay-usd-wallet" type="submit" class="btn-m-l buttons-buy">Buy</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <hr>
                                                    <h6>Payment Method</h6>
                                                    <h4 id="payment_method">Please select a payment method.</h4>
                                                    <hr>
                                                    <h6>Deposit To</h6>
                                                    <h4>ETH Wallet</h4>
                                                    <hr>
                                                    <div class="block-amounts">
                                                        <ul>
                                                            <li><span class="tags-amounts">Subtotal:</span> <span id="subtotal" class="amounts">$0.00</span></li>
                                                            <li><span class="tags-amounts"><a target="_blank" href="<?php echo DIR_URL; ?>/fees">Fee:</span> </a> <span id="fee" class="amounts">$0.00</span></li>
                                                            <li><span class="tags-amounts">Total:</span> <span id="total" class="amounts">$0.00</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "inc/close.php"; ?>
    </body>
</html>