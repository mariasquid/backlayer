<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <title>Mercury Cash: Ethereum Wallet, ETH/USD Market</title>
<meta name="description" content="Mercury cash is a specialized and secure platform that has its own ethereum wallet and market exchange for ETH/USD">
<meta name="keywords" content="Ethereum Wallet">
<meta name="robots" content="index, follow">
<meta name="copyright" content="Adventurous Entertainment LLC">
<meta name="language" content="EN">
<meta name="author" content="Adventurous Entertainment LLC">
<meta name="creationdate" content="May 2017">
<meta name="distribution" content="global">
<meta name="rating" content="general">
<link rel="Publisher" href="https://www.adenter.io">
<link rel="Canonical" href="https://www.mercury.cash">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo DIR_URL; ?>/public/css/theme.css" rel="stylesheet">
    <link href="<?php echo DIR_URL; ?>/public/css/about.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700'>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic'>
    <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/fonts/elegant/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/css/social.css" rel="stylesheet">
    <!-- Multiple Favicon 14-5-2017 -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#f3c805">
        <meta name="apple-mobile-web-app-title" content="Mercury Cash">
        <meta name="application-name" content="Mercury Cash">
        <meta name="msapplication-TileColor" content="#2f3740">
        <meta name="msapplication-TileImage" content="/mstile-144x144.png">
        <meta name="theme-color" content="#ffffff">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
 <!-- Live chat --> 
  <script type="text/javascript">
  window.__lo_site_id = 80707;
  (function() {
      var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
      wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
    })();
  </script>
  <!-- Google Analytics 14-5-2017 -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-98285209-1', 'auto');
ga('send', 'pageview');

</script>
</head>
  <body id="page-top" class="index">
    <!-- Navigation -->
    <nav class="navbar navbar-default">
      <!-- Topbar Nav (hidden) -->
      <div class="topbar-nav clearfix">
        <div class="container">
          <ul class="topbar-left list-unstyled list-inline">
            <li> <span class="fa fa-phone"></span> 123-456-7890 </li>
            <li> <span class="fa fa-envelope"></span> info@yourdomain.com </li>
          </ul>
          <ul class="topbar-right list-unstyled list-inline topbar-social">
            <li> <a href="#"> <span class="fa fa-facebook"></span> </a></li>
            <li> <a href="#"> <span class="fa fa-twitter"></span> </a></li>
            <li> <a href="#"> <span class="fa fa-google-plus"></span> </a></li>
            <li> <a href="#"> <span class="fa fa-dribbble"></span> </a></li>
            <li> <a href="#"> <span class="fa fa-instagram"></span> </a></li>
          </ul>
        </div>
      </div>
      <div class="container" style="max-width: 1050px;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand page-scroll" href="<?php echo DIR_URL; ?>">
            <img id="logo-navbar" src="<?php echo DIR_URL; ?>/public/img/logo-ligth.png" class="img-responsive" alt="">
          </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
                        <li>
                <a href="<?php echo DIR_URL; ?>" class="page-scroll">1 ETH = $<?php echo $ethusd;?></a>
            </li>
                        <li>
                          <a href="<?php echo DIR_URL; ?>">HOME</a> </li>
            <li>
              <a href="<?php echo DIR_URL; ?>/login" class="page-scroll">SIGN IN</a>
            </li>
            <li>
              <a href="<?php echo DIR_URL; ?>/register" class="page-scroll">CREATE AN ACCOUNT</a>
            </li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
    </nav>

    <!-- Hero Content -->
    <header id="hero1">
        <div class="container">

            <div class="intro-text">
      <div class="intro-heading">MERCURY CASH</div>
      <div class="intro-lead-in">User Agreement</div>
      <!-- <div class="intro-heading">GET TO KNOW US BETTER</div> -->
      <!-- <a href="#services" class="page-scroll btn btn-xl mr30 btn-primary">Learn More</a>
      <a href="#contact" class="page-scroll btn btn-xl btn-wire">Contact Us</a> -->
    </div>
  </div>
</header>


<!-- Services Section -->
<section id="services">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-left">
        <h3 class="subtitle">Last updated: April 25, 2017</h3>
      </div>
    </div>
    <br>
    <div align="justify">
This is a contract between you,  Adventurous Enterntainment, LLC and Mercury Cash, Ltd. ("Mercury Cash"). By signing up to use an account through mercury.cash, or any associated websites, APIs, or mobile applications (collectively the "Mercury Cash Site"), you agree that you have read, understood, and accept all of the terms and conditions contained in this Agreement, as well as our Privacy Policy and E-Sign Consent.

      <hr>

      <div class="row">
        <div class="col-sm-12 text-left">
          <h3 class="subtitle">PART 1: GENERAL USE</h3>
        </div>
      </div>
      <h4 style="margin-top: 20px; margin-bottom: 20px;">1. Basic Mercury Cash Services.</h4>
      <p><b>1.1. Eligibility.</b> To be eligible to use the Mercury Cash, you must be at least 13 years old. In
order to link a funding method or to purchase or sell digital currency, you must be at least
18 years old (or the applicable age of majority and contractual capacity). If you are under
the age of 18 and you wish to engage in digital currency purchase or sale activity, please
contact us at support@mercury.cash</p>
<p><b>1.2. Mercury Cash Services.</b> Your Mercury Cash account ("MercuryCash Account")
encompasses the following basic Mercury Cash services: One or more hosted Digital
Currency wallets that allow users to store certain supported digital currencies, like Ethereum
or other ("Digital Currency"), and to track, transfer, and manage supported Digital
Currencies (the "Hosted Digital Currency Wallet"); Digital Currency conversion services
through which users can buy and sell Digital Currencies in transactions with Mercury Cash
(the "Conversion Services"); and a U.S. Dollar account for use in connection with other
Mercury Cash Services (a "USD Wallet" or "Currency Wallet")</p>
<p><b>The risk of loss in trading or holding Digital Currency can be substantial. You should
therefore carefully consider whether trading or holding Digital Currency is suitable
for you in light of your financial condition.
</b></p>
      <h4 style="margin-top: 20px; margin-bottom: 20px;">2. Creating a Mercury Cash Account.</h4>
      <p><b>2.1. Registration of Mercury Cash Account.</b> In order to use any of the Mercury Cash
Services, you must first register by providing your name, email address, password, and
affirming your acceptance of this Agreement. Mercury Cash may, in our sole discretion,
refuse to allow you to establish a Mercury Cash Account, or limit the number of Mercury
Cash Accounts that a single user may establish and maintain at any time.
</p>
      <p><b>2.2. Identity Verification.</b> In order to use certain features of the Mercury Cash Services,
including certain transfers of Digital Currency and/or government-issued currency ("Fiat
Currency"), you may be required to <b>provide Mercury Cash with certain personal information,
  including, but not limited to, your name, address, telephone number, e-mail address, date of
  birth, social security number (for U.S. Citizens), taxpayer identification number (for Non-U.S.
  Citizens), government identification number, information about your profession or business
  activity, and information regarding your bank account (e.g., financial institution, account
  type, routing number, and account number).</b> In submitting this or any other personal
information as may be required, you verify that the information is accurate and authentic,
and you agree to update Mercury Cash if any information changes. <b>You hereby authorize
  Mercury Cash to, directly or through third parties make any inquiries we consider
  necessary to verify your identity and/or protect against fraud, including to query
  identity information contained in public reports (e.g., your name, address, past
  addresses, or date of birth), to query account information associated with your linked
  bank account (e.g., name or account balance), and to take action we reasonably
  deem necessary based on the results of such inquiries and reports. You further
  authorize any and all third parties to which such inquiries or requests may be
  directed to fully respond to such inquiries or requests.</b>
</p>
      <h4 style="margin-top: 20px; margin-bottom: 20px;">3. Hosted Digital Currency Wallet.</h4>
      <p><b>3.1. In General.</b> The Hosted Digital Currency Wallet services allow you to send supported
Digital Currency to, and request, receive, and store supported Digital Currency from, third
parties pursuant to instructions you provide through the Mercury Cash Site (each such
transaction is a "Digital Currency Transaction"). <b>Mercury Cash reserves the right to 
  refuse to process or to cancel any pending Digital Currency Transaction as required
  by law or in response to a subpoena, court order, or other binding government order
  or to enforce transaction limits. Mercury Cash cannot reverse a Digital Currency
  Transaction which has been broadcast to a Digital Currency network. The Hosted
  Digital Currency Wallet services are available only in connection with those Digital
  Currencies that Mercury Cash, in its sole discretion, decides to support. The Digital
  Currencies that Mercury Cash supports may change from time to time. If you have
  any questions about which Digital Currencies Mercury Cash currently supports,
  please contact support@mercury.cash. Under no circumstances should you attempt
  to use your Hosted Digital Currency Wallet services to store, send, request, or
  receive digital currencies in any form that are not supported by Mercury Cash.
  Mercury Cash assumes no responsibility or liability in connection with any attempt to
  use Mercury Cash Services for digital currencies that Mercury Cash does not
  support.</b></p>
      <p><b>3.2. Digital Currency Transactions.</b> Mercury Cash processes supported Digital Currency
according to the instructions received from its users and we do not guarantee the identity of
any user, receiver, requestee or other party. You should verify all transaction information
prior to submitting instructions to Mercury Cash. In the event you initiate a Digital Currency
Transaction by entering the recipient's email address and the recipient does not have an
existing Mercury Cash Account, Mercury Cash will email the recipient and invite them to
open a Mercury Cash Account. If the designated recipient does not open a Mercury Cash
Account within 30 days, Mercury Cash will return the supported Digital Currency associated
with the transaction to your Mercury Cash Account. Once submitted to a Digital Currency
network, a Digital Currency Transaction will be unconfirmed for a period of time pending
sufficient confirmation of the transaction by the Digital Currency network. A transaction is
not complete while it is in a pending state. Funds associated with transactions that are in a
pending state will be designated accordingly, and will not be included in your Mercury Cash
Account balance or be available to conduct transactions. Mercury Cash may charge
network fees (miner fees) to process a Digital Currency transaction on your behalf. Mercury
Cash will calculate the network fee in its discretion, although Mercury Cash will always
notify you of the network fee at or before the time you authorize the transaction.</p>
      <p><b>3.3. Digital Currency Storage & Transmission Delays.</b> Mercury Cash securely stores all
Digital Currency private keys in our control in a combination of online and offline storage. As
a result, it may be necessary for Mercury Cash to retrieve certain information from offline
storage in order to facilitate a Digital Currency Transaction in accordance with your
instructions, which may delay the initiation or crediting of such Digital Currency Transaction
for 48 hours or more. You acknowledge and agree that a Digital Currency Transaction
facilitated by Mercury Cash may be delayed.
      <p><b>3.4. Third Party Payments.</b> Mercury Cash has no control over, or liability for, the delivery,
        quality, safety, legality or any other aspect of any goods or services that you may purchase
        or sell to or from a third party (including other users of Mercury Cash Services). Mercury
        Cash is not responsible for ensuring that a buyer or a seller you may transact with will
        actually complete the transaction or is authorized to do so. If you experience a problem with
        any goods or services purchased from, or sold to, a third party in connection with Digital
        Currency transferred using the Mercury Cash Services, or if you have a dispute with such
        third party, you must resolve the dispute directly with that third party. If you believe a third
        party has behaved in a fraudulent, misleading, or inappropriate manner, or if you cannot
        adequately resolve a dispute with a third party, you may notify Mercury Cash Support at
        support@Mercury.cash so that we may consider what action to take, if any.</p>
<p><b>3.5 Advanced Protocols.</b> Unless specifically announced on our website or through some
  other official public statement of Mercury Cash, we do not support metacoins, colored coins,
  side chains, or other derivative, enhanced, or forked protocols, tokens, or coins which
  supplement or interact with a Digital Currency supported by Mercury Cash (collectively,
  “Advanced Protocols”). Do not use your Mercury Cash Account to attempt to receive,
  request, send, store, or engage in any other type of transaction involving an Advanced
  Protocol. The Mercury Cash platform is not configured to detect and/or secure Advanced
  Protocol transactions and Mercury Cash assumes absolutely no responsibility whatsoever
  in respect to Advanced Protocols.</p>
<p><b>3.6 Operation of Digital Currency Protocols.</b> Mercury Cash does not own or control the
  underlying software protocols which govern the operation of Digital Currencies supported on
  our platform. In general, the underlying protocols are open source and anyone can use, 
  copy, modify, and distribute them. By using the Mercury Cash, you acknowledge and agree
  (i) that Mercury Cash is not responsible for operation of the underlying protocols and that
  Mercury Cash makes no guarantee of their functionality, security, or availability; and (ii) that
  the underlying protocols are subject to sudden changes in operating rules (a/k/a “forks”),
  and that such forks may materially affect the value, function, and/or even the name of the
  Digital Currency you store in the Mercury Cash platform. In the event of a fork, you agree
  that Mercury Cash may temporarily suspend Mercury Cash operations (with or without
  advance notice to you) and that Mercury Cash may, in its sole discretion, (a) configure or
  reconfigure its systems or (b) decide not to support (or cease supporting) the forked
  protocol entirely, provided, however, that you will have an opportunity to withdraw funds
  from the platform. You acknowledge and agree that Mercury Cash assumes absolutely no
  responsibility whatsoever in respect of an unsupported branch of a forked protocol.</p>
  <h4 style="margin-top: 20px; margin-bottom: 20px;">4. Conversion Services.</h4>
  <p><b>4.1. In General.</b> Eligible users in certain jurisdictions may buy or sell supported Digital
    Currency through the Conversion Services. The Conversion Services are subject to the
    Mercury Cash "Conversion Rate" for the given transaction. "Conversion Rate" means the
    price of a given supported Digital Currency amount in terms of Fiat Currency or other Digital
    Currency as quoted on the Mercury Cash Site. The Conversion Rate is stated either as a
    "Buy Price" or as a "Sell Price," which is the price in terms of Fiat Currency or Digital
    Currency at which you may buy or sell supported Digital Currency to Mercury Cash. You
    acknowledge that the quoted Buy Price Conversion Rate may not be the same as the Sell
    Price Conversion Rate at any given time, and that Mercury Cash may add a margin or
    “spread” to the quoted Conversion Rate. You agree, as a condition of using any Mercury
    Cash Conversion Services, to accept the Conversion Rate as the sole conversion metric.
    Mercury Cash reserves the right to delay any Conversion Service transaction if it perceives
    a risk of fraud or illegal activity. Mercury Cash does not guarantee the availability of its
    Conversion Service, and the act of purchasing supported Digital Currency from Mercury
    Cash does not result in a guarantee that you may sell your supported Digital Currency to
    Mercury Cash.</p>
<p><b>4.2. Purchase Transactions.</b> After successfully completing the Verification Procedures,
  you may purchase supported Digital Currency by linking a valid payment method. You
  authorize Mercury Cash to initiate debits from your selected payment method(s) in
  settlement of purchase transactions. A Conversion Service Fee (defined below) applies to
  all purchase transactions. Although Mercury Cash will attempt to deliver supported Digital
  Currency to you as promptly as possible, funds may be debited from your selected payment
  method before Digital Currency is delivered to your Mercury Cash Account. We will make
  best efforts to fulfill all transactions, but in the rare circumstance where Mercury Cash
  cannot fulfill your purchase order, we will notify you and seek your approval to fulfill the
  purchase order at the contemporaneous Buy Price Conversion Rate.
  To secure the performance of your obligations under this Agreement, you grant to Mercury
  Cash a lien on and security interest in and to the balances in your account.</p>
<p><b>4.3. Sale Transactions.</b> After successfully completing the verification procedures, you may
  sell supported Digital Currency by linking a valid payment method. You authorize Mercury
  Cash to debit your Mercury Cash Account(s) and initiate payments to your selected
  payment method(s) in settlement of sell transactions. An applicable Conversion Fee
  (defined below) applies to all sale transactions. Your receipt of funds will depend on the
  payment type, and may take up to three or more business days.</p>
<p><b>4.4. Service Fees.</b> Each Conversion Service transaction is subject to a fee (a "Service
  Fee," also referred to as a "Conversion Fee"). The applicable Service Fee is displayed to
  you on the Mercury Cash Site prior to you completing a Conversion Service transaction.
  Service Fees and Conversion Service transactions may depend on the following factors:</p>
  <ul>
    <li>Mercury Cash will not process a conversion transaction if the Service Fee exceeds the value
of your transaction.</li>
    <li>Payments using other methods not described below, such as wire (if permitted), are subject
to different transaction fees disclosed to you before you authorize the transaction.</li>
  </ul>
  <p>Mercury Cash may waive some portion of the Service Fee depending on the payment
method you select. The availability of each Payment Method depends on a number of
factors, including but not limited to your location, the identification information you have
provided to us, and limitations imposed by third party payment processors.
You can view the current fee applicable to your location and payment method at our Fees
page.
Mercury Cash reserves the right to adjust its Service Fees and any applicable waivers. We
will always notify you of the Service Fee which applies to your transaction, both at the time
of the transaction and in each receipt we issue to you.
</p>
<p><b>4.5. Reversals; Cancellations.</b> You cannot cancel, reverse, or change any transaction
  marked as complete or pending. If your payment is not successful or if your payment
  method has insufficient funds, you authorize Mercury Cash, in its sole discretion, either to
  cancel the transaction or to debit your other payment methods, including Mercury Cash
  balances or other linked accounts, in any amount necessary to complete the transaction.
  You are responsible for maintaining an adequate balance and/or sufficient credit limits in
  order to avoid overdraft, NSF, or similar fees charged by your payment provider. Mercury
  Cash reserves the right to refuse to process, or to cancel or reverse, any purchases or
  sales of Digital Currency in its sole discretion, even after funds have been debited from your
  account(s), if Mercury Cash suspects the transaction involves (or has a high risk of
  involvement in) money laundering, terrorist financing, fraud, or any other type of financial
  crime; in response to a subpoena, court order, or other government order; or if Mercury
  Cash suspects the transaction relates to Prohibited Use or a Prohibited Business as set
  forth below. In such instances, Mercury Cash will reverse the transaction and we are under
  no obligation to allow you to reinstate a purchase or sale order at the same price or on the
  same terms as the cancelled transaction.</p>
<p><b>4.6. Recurring Transactions.</b> If you initiate recurring Conversion Service transactions, you
  authorize Mercury Cash to initiate recurring electronic payments in accordance with your
  selected Conversion Service and any corresponding payment accounts, such as recurring
  automated clearing house (ACH) debit or credit entries from or to your linked bank account.
  Your recurring transactions will occur in identical, periodic installments, based on your 
  period selection (e.g., daily, weekly, monthly), until either you or Mercury Cash cancels the
  recurring order. If you select a U.S. Bank Account as your payment method for a recurring
  transaction, and such transaction falls on a weekend or holiday, or after bank business
  hours, the ACH credit or debit will be executed on the next business day, although the
  Digital Currency Conversion Rate at the time of the regularly-scheduled transaction will
  apply. If your Bank is unable to process any electronic ACH debit entry, Mercury Cash will
  notify you of cancellation of the transaction and may avail itself of remedies set forth in this
  User Agreement to recover any amount owed to Mercury Cash. This authorization will
  remain in full force and effect until you change your recurring transaction settings or until
  you provide us written notification at support@Mercury.cash. You agree to notify Mercury
  Cash in writing of any changes in your linked bank account information prior to a recurring
  transaction. Mercury Cash may, at any time, terminate recurring transactions by providing
  notice to you.</p>
<p><b>4.7. Payment Services Partners.</b> Mercury Cash may use a third party payment processor
  to process any US Dollar payment between you and Mercury Cash, including but not
  limited to payments in relation to your use of the Conversion Service or deposits or
  withdrawals from your USD Wallet.</p>
  <h4 style="margin-top: 20px; margin-bottom: 20px;">5. USD Wallet.</h4>
  <p><b>5.1. USD Wallets.</b> Certain approved users may establish and fund a U.S. Dollar balance
    ("USD Wallet") to facilitate transactions on the Mercury Cash. You are the owner of the
    balance of your USD Wallet. Mercury Cash holds your USD balance in dedicated custodial
    accounts with a financial institution.</p>
<p><b>5.2. Deposits and Withdrawals.</b> You may initiate a transfer from your linked bank account
  to fund your USD Wallet. Mercury Cash will not charge a fee for you to transfer funds to or
  from Mercury Cash, but bank transfer fees may apply. Funds sent via bank wire (if
  permitted by Mercury Cash) are subject to additional wire fees. For deposits, Mercury Cash
  will credit your USD Wallet a corresponding amount of dollars after funds are delivered to 
  Mercury Cash, typically within two to three business days after you authorize a deposit. For
  withdrawals, Mercury Cash will immediately debit your USD Wallet when you authorize a
  withdrawal and funds will typically settle to you within two to three business days. Bank fees
  are netted out of transfers to or from Mercury Cash. We will not process a transfer if
  associated bank fees exceed the value of the transfer.</p>
  <h4 style="margin-top: 20px; margin-bottom: 20px;">6. General Use, Prohibited Use, and Termination.</h4>
  <p><b>6.1. Limited License.</b> We grant you a limited, nonexclusive, nontransferable license,
    subject to the terms of this Agreement, to access and use the Mercury Cash Site, and
    related content, materials, information (collectively, the "Content") solely for approved
    purposes as permitted by Mercury Cash from time to time. Any other use of the Mercury
    Cash Site or Content is expressly prohibited and all other right, title, and interest in the
    Mercury Cash Site or Content is exclusively the property of Mercury Cash and its licensors.
    You agree you will not copy, transmit, distribute, sell, license, reverse engineer, modify,
    publish, or participate in the transfer or sale of, create derivative works from, or in any other
    way exploit any of the Content, in whole or in part. "www.mercury.cash", "Mercury Cash"
    and all logos related to the Mercury Cash Services or displayed on the Mercury Cash Site
    are either trademarks or registered marks of Mercury Cash or its licensors. You may not
    copy, imitate or use them without Mercury Cash's prior written consent.</p>
<p><b>6.2. Website Accuracy.</b> Although we intend to provide accurate and timely information on
  the Mercury Cash Site, the Mercury Cash Site (including, without limitation, the Content)
  may not always be entirely accurate, complete or current and may also include technical
  inaccuracies or typographical errors. In an effort to continue to provide you with as complete
  and accurate information as possible, information may be changed or updated from time to
  time without notice, including without limitation information regarding our policies, products
  and services. Accordingly, you should verify all information before relying on it, and all
  decisions based on information contained on the Mercury Cash Site are your sole
  responsibility and we shall have no liability for such decisions. Links to third-party materials
  (including without limitation websites) may be provided as a convenience but are not 
  controlled by us. You acknowledge and agree that we are not responsible for any aspect of
  the information, content, or services contained in any third-party materials or on any third
  party sites accessible or linked to the Mercury Cash Site,</p>
<p><b>6.3. Third-Party Applications.</b> If, to the extent permitted by Mercury Cash from time to
  time, you grant express permission to a third party to access or connect to your Mercury
  Cash Account, either through the third party's product or service or through the Mercury
  Cash Site, you acknowledge that granting permission to a third party to take specific actions
  on your behalf does not relieve you of any of your responsibilities under this Agreement.
  You are fully responsible for all acts or omissions of any third party with access to your
  Mercury Cash Account. Further, you acknowledge and agree that you will not hold Mercury
  Cash responsible for, and will indemnify Mercury Cash from, any liability arising out of or
  related to any act or omission of any third party with access to your Mercury Cash Account.
  You may change or remove permissions granted by you to third parties with respect to your
  Mercury Cash Account at any time through the Account Settings (Integrations) page on the
  Mercury Cash Site.</p>
<p><b>6.4. Prohibited Use.</b> In connection with your use of the Mercury Cash Services, and your
  interactions with other users, and third parties you agree and represent you will not engage
  in any Prohibited Business or Prohibited Use defined herein. We reserve the right at all
  times to monitor, review, retain and/or disclose any information as necessary to satisfy any
  applicable law, regulation, sanctions programs, legal process or governmental request. We
  reserve the right to cancel and/or suspend your Mercury Cash Account and/or block
  transactions or freeze funds immediately and without notice if we determine, in our sole
  discretion, that your Account is associated with a Prohibited Use and/or a Prohibited
  Business.</p>
<p><b>6.5. Transactions Limits.</b> The use of all Mercury Cash Services is subject to a limit on the
  amount of volume, stated in U.S. Dollar terms, you may transact or transfer in a given
  period (e.g., daily). To view your limits, login to your Mercury Cash Account and visit
  settings. Your transaction limits may vary depending on your payment method, verification
  steps you have completed, and other factors. Mercury Cash reserves the right to change
  applicable limits as we deem necessary in our sole discretion. If you wish to raise your limits 
  beyond the posted amounts, you may submit a request to support@mercury.cash. We may
  require you to submit additional information about yourself or your business, provide
  records, and arrange for meetings with Mercury Cash staff (such process, "Enhanced Due
  Diligence"). Mercury Cash reserves the right to charge you costs and fees associated with
  Enhanced Due Diligence, provided that we notify you in advance of any such charges
  accruing. In our sole discretion, we may refuse to raise your limits or we may lower your
  limits at a subsequent time even if you have completed Enhanced Due Diligence.</p>
<p><b>6.6. Suspension, Termination, and Cancellation.</b> Mercury Cash may: (a) suspend,
  restrict, or terminate your access to any or all of the Mercury Cash Services, and/or (b)
  deactivate or cancel your Mercury Cash Account if:</p>
  <ul>
    <li>We are so required by a facially valid subpoena, court order, or binding order of a
government authority; or</li>
<li>We reasonably suspect you of using your Mercury Cash Account in connection with a
  Prohibited Use or Business; or</li>
<li>Use of your Mercury Cash Account is subject to any pending litigation, investigation, or
  government proceeding and/or we perceive a heightened risk of legal or regulatory noncompliance
  associated with your Account activity; or</li>
<li>Our service partners are unable to support your use; or</li>
<li>You take any action that Mercury Cash deems as circumventing Mercury Cash's controls,
  including, but not limited to, opening multiple Mercury Cash Accounts or abusing promotions
  which Mercury Cash may offer from time to time.</li>
  </ul>
  <p>If Mercury Cash suspends or closes your account, or terminates your use of Mercury Cash
Services for any reason, we will provide you with notice of our actions unless a court order
or other legal process prohibits Mercury Cash from providing you with such notice.
You acknowledge that Mercury Cash's decision to take certain actions, including limiting
access to, suspending, or closing your account, may be based on confidential criteria that
are essential to Mercury Cash's risk management and security protocols. You agree that
Mercury Cash is under no obligation to disclose the details of its risk management and
security procedures to you.
You will be permitted to transfer Digital Currency or funds associated with your Hosted
Digital Currency Wallet(s) and/or your USD Wallet(s) for ninety (90) days after Account
deactivation or cancellation unless such transfer is otherwise prohibited (i) under the law,
including but not limited to applicable sanctions programs, or (ii) by a facially valid subpoena
or court order. You may cancel your Mercury Cash Account at any time by withdrawing all
balances. You will not be charged for canceling your Mercury Cash Account, although you
will be required to pay any outstanding amounts owed to Mercury Cash. You authorize us to
cancel or suspend any pending transactions at the time of cancellation.
</p>
<p><b>6.7. Relationship of the Parties.</b> Mercury Cash is an independent contractor for all
  purposes. Nothing in this Agreement shall be deemed or is intended to be deemed, nor
  shall it cause, you and Mercury Cash to be treated as partners, joint ventures, or otherwise
  as joint associates for profit, or either you or Mercury Cash to be treated as the agent of the
  other.</p>
<p><b>6.8. Privacy of Others; Marketing.</b> If you receive information about another user through
  the Mercury Cash Services, you must keep the information confidential and only use it in
  connection with the Mercury Cash Services. You may not disclose or distribute a user's
  information to a third party or use the information except as reasonably necessary to
  effectuate a transaction and other functions reasonably incidental thereto such as support,
  reconciliation and accounting unless you receive the user's express consent to do so. You
  may not send unsolicited email to a user through the Mercury Cash Services.</p>
<p><b>6.9. Password Security; Contact Information.</b> You are responsible for maintaining
  adequate security and control of any and all IDs, passwords, hints, personal identification
  numbers (PINs), API keys or any other codes that you use to access the Mercury Cash
  Services. Any loss or compromise of the foregoing information and/or your personal
  information may result in unauthorized access to your Mercury Cash Account by thirdparties
  and the loss or theft of any Digital Currency and/or funds held in your Mercury Cash
  Account and any associated accounts, including your linked bank account(s) and credit
  card(s). You are responsible for keeping your email address and telephone number up to
  date in your Account Profile in order to receive any notices or alerts that we may send you.
  <b>We assume no responsibility for any loss that you may sustain due to compromise of 
     account login credentials due to no fault of Mercury Cash and/or failure to follow or
     act on any notices or alerts that we may send to you.</b> In the event you believe your
  Mercury Cash Account information has been compromised, contact Mercury Cash Support
  immediately at support@mercury.cash, or report your claim by phone at (407) 483-4057.</p>
<p><b>6.10. Taxes.</b> It is your sole responsibility to determine whether, and to what extent, any
  taxes apply to any transactions you conduct through the Mercury Cash Services, and to
  withhold, collect, report and remit the correct amounts of taxes to the appropriate tax
  authorities. Your transaction history is available through your Mercury Cash Account.</p>
<p><b>6.11. Unclaimed Property.</b> If Mercury Cash is holding funds in your account, and Mercury
  Cash is unable to contact you and has no record of your use of the Services for several
  years, applicable law may require Mercury Cash to report these funds as unclaimed
  property to the applicable jurisdiction. If this occurs, Mercury Cash will try to locate you at
  the address shown in our records, but if Mercury Cash is unable to locate you, it may be
  required to deliver any such funds to the applicable state or jurisdiction as unclaimed
  property. Mercury Cash reserves the right to deduct a dormancy fee or other administrative
  charges from such unclaimed funds, as permitted by applicable law.</p>
  <h4 style="margin-top: 20px; margin-bottom: 20px;">7. Customer Feedback, Queries, Complaints, and Dispute Resolution.</h4>
  <p><b>7.1. Contact Mercury Cash.</b> If you have any feedback, questions, or complaints, contact us
    via our Customer Support web page or write to us at Mercury Cash Customer Support,
    6427 Milner Blvd, unit 4, Orlando, FL 32809, USA. When you contact us please provide us
    with your name, address, and any other information we may need to identify you, your
    Mercury Cash Account, and the transaction on which you have feedback, questions, or
    complaints. If you believe your account has been compromised, you may also report your
    claim by calling (407) 483-4057.</p>
<p><b>7.2. Arbitration; Waiver of Class Action.</b> If you have a dispute with Mercury Cash, we will
  attempt to resolve any such disputes through our support team. <b>If we cannot resolve the
    dispute through our support team, you and we agree that any dispute arising under
    this Agreement shall be finally settled in binding arbitration, on an individual basis, in
    accordance with the American Arbitration Association's rules for arbitration of
    consumer-related disputes (accessible at (https://www.adr.org/Rules) and you and
    Mercury Cash hereby expressly waive trial by jury and right to participate in a class
    action lawsuit or class-wide arbitration</b>. The arbitration will be conducted by a single,
  neutral arbitrator and shall take place in the county or parish in which you reside, or another
  mutually agreeable location, in the English language. The arbitrator may award any relief
  that a court of competent jurisdiction could award, including attorneys' fees when authorized
  by law, and the arbitral decision may be enforced in any court. At your request, hearings
  may be conducted in person or by telephone and the arbitrator may provide for submitting
  and determining motions on briefs, without oral hearings. The prevailing party in any action
  or proceeding to enforce this agreement shall be entitled to costs and attorney's' fees.</p>
  <p>If the arbitrator(s) or arbitration administrator would impose filing fees or other administrative
  costs on you, we will reimburse you, upon request, to the extent such fees or costs would
  exceed those that you would otherwise have to pay if you were proceeding instead in a
  court. We will also pay additional fees or costs if required to do so by the arbitration
  administrator's rules or applicable law. Apart from the foregoing, each Party will be
  responsible for any other fees or costs, such as attorney fees that the Party may incur. If a
  court decides that any provision of this section 8.2 is invalid or unenforceable, that provision
  shall be severed and the other parts of this section 8.2 shall still apply. In any case, the
  remainder of this User Agreement, will continue to apply.</p>
  <h4 style="margin-top: 20px; margin-bottom: 20px;">8. General Provisions.</h4>
  <p><b>8.1. Computer Viruses.</b> We shall not bear any liability, whatsoever, for any damage or
    interruptions caused by any computer viruses, spyware, scareware, Trojan horses, worms
    or other malware that may affect your computer or other equipment, or any phishing, 
    spoofing or other attack. We advise the regular use of a reputable and readily available
    virus screening and prevention software. You should also be aware that SMS and email
    services are vulnerable to spoofing and phishing attacks and should use care in reviewing
    messages purporting to originate from Mercury Cash. Always log into your Mercury Cash
    Account through the Mercury Cash Site to review any transactions or required actions if you
    have any uncertainty regarding the authenticity of any communication or notice.</p>
    <p><b>8.2. Release of Mercury Cash; Indemnification.</b> If you have a dispute with one or more
      users of the Mercury Cash services, you release Mercury Cash, its affiliates and service
      providers, and each of their respective officers, directors, agents, joint venturers, employees
      and representatives from any and all claims, demands and damages (actual and
      consequential) of every kind and nature arising out of or in any way connected with such
      disputes. You agree to indemnify and hold Mercury Cash, its affiliates and Service
      Providers, and each of its or their respective officers, directors, agents, joint venturers,
      employees and representatives, harmless from any claim or demand (including attorneys'
      fees and any fines, fees or penalties imposed by any regulatory authority) arising out of or
      related to your breach of this Agreement or your violation of any law, rule or regulation, or
      the rights of any third party.</p>
<p><b>8.3. Limitation of Liability; No Warranty.</b> IN NO EVENT SHALL Mercury Cash, ITS
  AFFILIATES AND SERVICE PROVIDERS, OR ANY OF THEIR RESPECTIVE OFFICERS,
  DIRECTORS, AGENTS, JOINT VENTURERS, EMPLOYEES OR REPRESENTATIVES,
  BE LIABLE (A) FOR ANY AMOUNT GREATER THAN THE VALUE OF THE SUPPORTED
  DIGITAL CURRENCY ON DEPOSIT IN YOUR Mercury Cash ACCOUNT OR (B) FOR ANY
  LOST PROFITS OR ANY SPECIAL, INCIDENTAL, INDIRECT, INTANGIBLE, OR
  CONSEQUENTIAL DAMAGES, WHETHER BASED IN CONTRACT, TORT,
  NEGLIGENCE, STRICT LIABILITY, OR OTHERWISE, ARISING OUT OF OR IN
  CONNECTION WITH AUTHORIZED OR UNAUTHORIZED USE OF THE Mercury Cash
  SITE OR THE Mercury Cash SERVICES, OR THIS AGREEMENT, EVEN IF AN
  AUTHORIZED REPRESENTATIVE OF Mercury Cash HAS BEEN ADVISED OF OR KNEW
  OR SHOULD HAVE KNOWN OF THE POSSIBILITY OF SUCH DAMAGES. THIS MEANS,
  BY WAY OF EXAMPLE ONLY (AND WITHOUT LIMITING THE SCOPE OF THE
  PRECEDING SENTENCE), THAT IF YOU CLAIM THAT Mercury Cash FAILED TO 
  PROCESS A BUY OR SELL TRANSACTION PROPERLY, YOUR DAMAGES ARE
  LIMITED TO NO MORE THAN THE VALUE OF THE SUPPORTED DIGITAL CURRENCY
  AT ISSUE IN THE TRANSACTION, AND THAT YOU MAY NOT RECOVER FOR LOST
  PROFITS, LOST BUSINESS OPPORTUNITIES, OR OTHER TYPES OF SPECIAL,
  INCIDENTAL, INDIRECT, INTANGIBLE, OR CONSEQUENTIAL DAMAGES IN EXCESS
  OF THE VALUE OF THE SUPPORTED DIGITAL CURRENCY AT ISSUE IN THE
  TRANSACTION. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR
  LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES SO THE ABOVE
  LIMITATION MAY NOT APPLY TO YOU.</p>
<p>THE Mercury Cash SERVICES ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE"
  BASIS WITHOUT ANY REPRESENTATION OR WARRANTY, WHETHER EXPRESS,
  IMPLIED OR STATUTORY. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE
  LAW, Mercury Cash SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTIES OF TITLE,
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND/OR NONINFRINGEMENT.
  Mercury Cash DOES NOT MAKE ANY REPRESENTATIONS OR
  WARRANTIES THAT ACCESS TO THE SITE, ANY PART OF THE Mercury Cash
  SERVICES, OR ANY OF THE MATERIALS CONTAINED THEREIN, WILL BE
  CONTINUOUS, UNINTERRUPTED, TIMELY, OR ERROR-FREE.</p>
<p>Mercury Cash makes no representations about the accuracy or completeness of historical
  Digital Currency price data available on the Site. Mercury Cash will make reasonable efforts
  to ensure that requests for electronic debits and credits involving bank accounts, credit
  cards, and check issuances are processed in a timely manner but Mercury Cash makes no
  representations or warranties regarding the amount of time needed to complete processing
  which is dependent upon many factors outside of our control.</p>
  <p><b>8.4. Entire Agreement.</b> This Agreement, the Privacy Policy, E-Sign Consent, and
    Appendices incorporated by reference herein comprise the entire understanding and
    agreement between you and Mercury Cash as to the subject matter hereof, and supersedes
    any and all prior discussions, agreements and understandings of any kind (including without 
    limitation any prior versions of this Agreement), and every nature between and among you
    and Mercury Cash. Section headings in this Agreement are for convenience only, and shall
    not govern the meaning or interpretation of any provision of this Agreement.
    8.5. Amendments. We may amend or modify this Agreement by posting on the Mercury
    Cash Site or emailing to you the revised Agreement, and the revised Agreement shall be
    effective at such time. If you do not agree with any such modification, your sole and
    exclusive remedy is to terminate your use of the Services and close your account. You
    agree that we shall not be liable to you or any third party for any modification or termination
    of the Mercury Cash Services, or suspension or termination of your access to the Mercury
    Cash Services, except to the extent otherwise expressly set forth herein. If the revised
    Agreement includes a material change, we will endeavor to provide you advanced notice via
    our website and/or email before the material change becomes effective.</p>
<p><b>8.6. Assignment.</b> You may not assign any rights and/or licenses granted under this
  Agreement. We reserve the right to assign our rights without restriction, including without
  limitation to any Mercury Cash affiliates or subsidiaries, or to any successor in interest of
  any business associated with the Mercury Cash Services. Any attempted transfer or
  assignment in violation hereof shall be null and void. Subject to the foregoing, this
  Agreement will bind and inure to the benefit of the parties, their successors and permitted
  assigns.</p>
<p><b>8.7. Severability.</b> If any provision of this Agreement shall be determined to be invalid or
  unenforceable under any rule, law or regulation or any governmental agency, local, state, or
  federal, such provision will be changed and interpreted to accomplish the objectives of the
  provision to the greatest extent possible under any applicable law and the validity or
  enforceability of any other provision of this Agreement shall not be affected.</p>
<p><b>8.8. Change of Control.</b> In the event that Mercury Cash is acquired by or merged with a
  third party entity, we reserve the right, in any of these circumstances, to transfer or assign
  the information we have collected from you as part of such merger, acquisition, sale, or
  other change of control.</p>
<p><b>8.9. Survival.</b> All provisions of this Agreement which by their nature extend beyond the
  expiration or termination of this Agreement, including, without limitation, sections pertaining
  to suspension or termination, Mercury Cash Account cancellation, debts owed to Mercury
  Cash, general use of the Mercury Cash Site, disputes with Mercury Cash, and general
  provisions, shall survive the termination or expiration of this Agreement.</p>
<p><b>8.10. Governing Law.</b> For U.S. Citizens the laws of the State of Florida will govern this Agreement.  For Non-U.S. Citizens this agreement will be govern by the Financial Conduct Authority of UK Government.   Any claim or dispute that has arisen or may arise between you and Adventurous Entertainment, LLC or Mercury Cash, Ltd will be taken to a court of respective jurisdiction, except to the extent governed by United States Federal Law. </p>
<p><b>8.11. Force Majeure.</b> We shall not be liable for delays, failure in performance or interruption
  of service which result directly or indirectly from any cause or condition beyond our
  reasonable control, including but not limited to, any delay or failure due to any act of God,
  act of civil or military authorities, act of terrorists, civil disturbance, war, strike or other labor
  dispute, fire, interruption in telecommunications or Internet services or network provider
  services, failure of equipment and/or software, other catastrophe or any other occurrence
  which is beyond our reasonable control and shall not affect the validity and enforceability of
  any remaining provisions.</p>
<p><b>8.12. English Language Controls.</b> Notwithstanding any other provision of this Agreement,
  any translation of this Agreement is provided for your convenience. The meanings of terms,
  conditions and representations herein are subject to definitions and interpretations in the
  English language. Any translation provided may not accurately represent the information in
  the original English.</p>
<p><b>8.13. Non-Waiver of Rights.</b> This agreement shall not be construed to waive rights that
  cannot be waived under applicable state money transmission laws in the state where you
  are located.</p>

      <div class="row">
      <div class="col-sm-12 text-left">
        <h3 class="subtitle">APPENDIX 1: PROHIBITED USE, PROHIBITED BUSINESSES AND CONDITIONAL USE</h3>
      </div>
    </div>

    <h4 style="margin-top: 20px; margin-bottom: 20px;">Prohibited Use</h4>
    <p>You may not use your Mercury Cash Account to engage in the following categories of
activity ("Prohibited Uses"). The specific types of use listed below are representative, but
not exhaustive. If you are uncertain as to whether or not your use of Mercury Cash Services
involves a Prohibited Use, or have questions about how these requirements apply to you,
please contact us at support@mercury.cash. By opening a Mercury Cash Account, you
confirm that you will not use your Account to do any of the following:
</p>
<ul>
  <li><b>Unlawful Activity:</b> Activity which would violate, or assist in violation of, any law,
statute, ordinance, or regulation, sanctions programs administered in the
countries where Mercury Cash conducts business, including but not limited to the
U.S. Department of Treasury's Office of Foreign Assets Control ("OFAC"), or
which would involve proceeds of any unlawful activity; publish, distribute or
disseminate any unlawful material or information.</li>
<li><b>Abusive Activity:</b> Actions which impose an unreasonable or disproportionately
large load on our infrastructure, or detrimentally interfere with, intercept, or
expropriate any system, data, or information; transmit or upload any material to
the Mercury Cash Site that contains viruses, trojan horses, worms, or any other
harmful or deleterious programs; attempt to gain unauthorized access to the
Mercury Cash Site, other Mercury Cash Accounts, computer systems or
networks connected to the Mercury Cash Site, through password mining or any
other means; use Mercury Cash Account information of another party to access
or use the Mercury Cash Site, except in the case of specific Merchants and/or
applications which are specifically authorized by a user to access such user's
Mercury Cash Account and information; or transfer your account access or rights
to your account to a third party, unless by operation of law or with the express
permission of Mercury Cash</li>
<li><b>Abuse Other Users:</b> Interfere with another individual's or entity's access to or
use of any Mercury Cash Services; defame, abuse, extort, harass, stalk, threaten
or otherwise violate or infringe the legal rights (such as, but not limited to, rights 
of privacy, publicity and intellectual property) of others; incite, threaten, facilitate,
promote, or encourage violent acts against others; harvest or otherwise collect
information from the Mercury Cash Site about others, including without limitation
email addresses, without proper consent</li>
<li><b>Fraud:</b> Activity which operates to defraud Mercury Cash, Mercury Cash users, or
  any other person; provide any false, inaccurate, or misleading information to
  Mercury Cash</li>
<li><b>Gambling:</b> Lotteries; bidding fee auctions; sports forecasting or odds making;
  fantasy sports leagues with cash prizes; internet gaming; contests; sweepstakes;
  games of chance</li>
<li><b>Intellectual Property Infringement:</b> Engage in transactions involving items that
  infringe or violate any copyright, trademark, right of publicity or privacy or any
  other proprietary right under the law, including but not limited to sales,
  distribution, or access to counterfeit music, movies, software, or other licensed
  materials without the appropriate authorization from the rights holder; use of
  Mercury Cash intellectual property, name, or logo, including use of Mercury Cash
  trade or service marks, without express consent from Mercury Cash or in a
  manner that otherwise harms Mercury Cash or the Mercury Cash brand; any
  action that implies an untrue endorsement by or affiliation with Mercury Cash</li>
</ul>

<div class="row">
      <div class="col-sm-12 text-left">
        <h3 class="subtitle">APPENDIX 2: VERIFICATION PROCEDURES AND LIMITS</h3>
      </div>
    </div>

<p>Mercury Cash uses multi-level systems and procedures to collect and verify information
  about you in order to protect Mercury Cash and the community from fraudulent users, and
  to keep appropriate records of Mercury Cash's customers. Your daily or weekly Conversion
  limits, withdrawal limits, Instant Buy limits, USD Wallet transfer limits, and limits on
  transactions from a linked payment method are based on the identifying information and/or
  proof of identity you provide to Mercury Cash.</p>
<p>All users who wish to buy Digital Currency using Mercury Cash's Standard Conversion
  Service, at minimum, must:</p>
<ul>
<li>Establish a Mercury Cash Account by providing your name, authenticating your email
    address, and accepting the Mercury Cash User Terms</li>
<li>Add and verify a phone number</li>
<li>Add and verify a bank account</li>
<li>Upload an Id document issued by your government to get verified under US laws and
    regulations.</li>
    </ul>
<p>Notwithstanding these minimum verification procedures for the referenced Mercury Cash
  Services, Mercury Cash may require you to provide or verify additional information, or to
  wait some amount of time after completion of a transaction, before permitting you to use
  any Mercury Cash Services and/or before permitting you to engage in transactions beyond
  certain volume limits. You may determine the volume limits associated with your level of
  identity verification by visiting your account's Limits page.</p>
<p>You may contact support@mercury.cash to request larger limits. Mercury Cash will require
  you to submit to Enhanced Due Diligence. Additional fees and costs may apply, and
  Mercury Cash does not guarantee that we will raise your limits.</p>

  <div class="row">
      <div class="col-sm-12 text-left">
        <h3 class="subtitle">APPENDIX 3: E-SIGN DISCLOSURE AND CONSENT</h3>
      </div>
    </div>

<p>This policy describes how Mercury Cash delivers communications to you electronically. We
  may amend this policy at any time by providing a revised version on our website. The
  revised version will be effective at the time we post it. We will provide you with prior notice
  of any material changes via our website.</p>
<h4 style="margin-top: 20px; margin-bottom: 20px;">Electronic Delivery of Communications</h4>
<p>You agree and consent to receive electronically all communications, agreements,
  documents, notices and disclosures (collectively, "Communications") that we provide in
  connection with your Mercury Cash Account and your use of Mercury Cash Services.
  Communications include:</p>
<ul>
<li>Terms of use and policies you agree to (e.g., the Mercury Cash User Agreement and Privacy
    Policy), including updates to these agreements or policies;</li>
<li>Account details, history, transaction receipts, confirmations, and any other Account or
    transaction information;</li>
<li>Legal, regulatory, and tax disclosures or statements we may be required to make available to
    you; and</li>
</ul>
<p>Responses to claims or customer support inquiries filed in connection with your Account.
    We will provide these Communications to you by posting them on the Mercury Cash
    website, emailing them to you at the primary email address listed in your Mercury Cash
    profile, communicating to you via instant chat, and/or through other electronic
    communication such as text message or mobile push notification.</p>
  <h4 style="margin-top: 20px; margin-bottom: 20px;">Hardware and Software Requirements</h4>
  <p>In order to access and retain electronic Communications, you will need the following
    computer hardware and software:</p>
  <ul>
    <li>A device with an Internet connection;</li>
    <li>A current web browser that includes 128-bit encryption (e.g. Internet Explorer version 9.0 and
      above, Firefox version 3.6 and above, Chrome version 31.0 and above, or Safari 7.0 and
      above) with cookies enabled;</li>
    <li>A valid email address (your primary email address on file with Mercury Cash); and</li>
    <li>Sufficient storage space to save past Communications or an installed printer to print them.
      Updating your Information</li>
    </ul>
<p>It is your responsibility to provide us with a true, accurate and complete e-mail address and
  your contact information, and to keep such information up to date. You understand and
  agree that if Mercury Cash sends you an electronic Communication but you do not receive it
  because your primary email address on file is incorrect, out of date, blocked by your service
  provider, or you are otherwise unable to receive electronic Communications, Mercury Cash
  will be deemed to have provided the Communication to you.
  You may update your information by logging into your account and visiting settings or by
  contacting our support team via email at support@mercury.cash</p>

  </div>
</div>
</div>
</section>

<br>
<br>
<br>

<!-- Contact Section -->
<section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading">Get in touch and explore the possibilities!!</h2>
      </div>
    </div>
    <form name="sentMessage" id="contactForm" class="mw800 center-block clearfix" novalidate>
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
        <p class="help-block text-danger"></p>
      </div>
      <div class="form-group">
        <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
        <p class="help-block text-danger"></p>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-xl btn-block btn-wire">Subscribe</button>
      </div>
    </form>
    <p> By subscribing you agree to our terms and conditions. </p>

  </div>
</section>

<!-- Footer -->
		<?php include "inc/footer-homes.php"; ?>




<!-- jQuery -->
<!-- <script src="js/vendor/jquery.js"></script> -- Local Version -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<!-- <script src="js/vendor/bootstrap.min.js"></script> -- Local Version -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="js/vendor/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script>

<!-- Custom Theme JavaScript -->
<script src="js/main.js"></script>

</body>

</html>
