<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <?php include "inc/scripts.php"; ?>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jquery.numeric.min.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/request.js?v=<?php echo time(); ?>"></script>
    </head>
    <body>
        <?php include "inc/trialInfoModal.php"; ?>
        <?php if (!empty($_SESSION['alert_add_address'])) { ?>
        <script type="text/javascript">
        $(document).ready(function() {
        $('#newAddressFavorite').modal();
        });
        </script>
        <!-- Modal -->
        <div class="modal fade" id="newAddressFavorite" tabindex="-1" role="dialog" aria-labelledby="newAddressFavoriteLabel" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="<?php echo DIR_URL; ?>/request/saveAddress" id="saveAddress" method="POST" autocomplete="off">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="save_addressLabel">Add new address</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="address">Address:</label>
                                <input type="text" class="form-control" id="address" name="address" value="<?php echo $_SESSION['address_applicant']; ?>" placeholder="Address">
                            </div>
                            <div class="form-group">
                                <label for="description">Description:</label>
                                <input type="text" class="form-control" id="description" name="description" placeholder="Description">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="token" value="<?php echo $_SESSION['validateToken']; ?>">
                            <button type="submit" class="btn btn-default closeModal">Add</button>
                            <button type="button" class="btn btn-default closeModal" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php unset($_SESSION['address_applicant']); ?>
        <?php unset($_SESSION['alert_add_address']); ?>
        <?php } ?>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Attention</h4>
                    </div>
                    <div class="modal-body" style="text-align: center;">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default closeModal" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="loading">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <div class="content">
                    <div class="container-fluid">
                        <div class="tabs-m">
                            <ul>
                                <li><a href="<?php echo DIR_URL; ?>/send">Send</a></li>
                                <li class="active"><a href="<?php echo DIR_URL; ?>/request">Request</a></li>
                            </ul>
                        </div>
                        <div class="block">
                            <div class="block-header">
                                <span class="text-yellow">Request ethereum</span>
                            </div>
                            <div class="block-body">
                                <?php if (!empty($_SESSION['alert'])) { ?>
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <div class="alertGrey">
                                            <?php echo $_SESSION['alert']; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php unset($_SESSION['alert']); ?>
                                <?php } ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <form action="<?php echo DIR_URL; ?>/request/request" method="POST" autocomplete="off">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-elements">
                                                        <label>Address to request</label>
                                                        <input type="text" id="email" name="email" placeholder="Friend's email...">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-elements">
                                                        <label>Your address</label>
                                                        <select name="eth_wallet" id="eth_wallet">
                                                            <?php foreach ($ethwallets as $ethwallet) { ?>
                                                            <option value="<?php echo $ethwallet['id']; ?>"><?php echo (is_null($ethwallet['alias']) ? $ethwallet['address'] : $ethwallet['alias']); ?> (<?php echo $ethwallet['balance']; ?> ETH | $<?php echo $ethwallet['usd']; ?> USD)</option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-elements">
                                                        <label>ETH Amount</label>
                                                        <input type="text" id="eth_amount" name="eth_amount" placeholder="0.00">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-elements">
                                                        <label>USD Amount</label>
                                                        <input type="text" id="usd_amount" name="usd_amount" placeholder="0.00">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-md-offset-8 col-lg-3 col-lg-offset-9">
                                                    <button type="submit" class="btn-m-l">Request</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <table class="table-m">
                                            <tbody>
                                                <?php foreach ($requests as $request) { ?>
                                                <tr <?php echo ($request['viewed'] == 0) ? 'class="unread"' : ''; ?>>
                                                    <td><?php echo $request['fullname']; ?> has requested <?php echo $request['eth_amount']; ?> ETH</td>
                                                    <td><?php echo ($request['status'] == 1) ? '<span class="label label-success">Approve</span>' : ''; ?></td>
                                                    <td class="td-actions text-right">
                                                        <a href="<?php echo DIR_URL; ?>/request/approve/<?php echo $request['id']; ?>" rel="tooltip" title="" class="btn btn-info btn-simple btn-xs approve" data-original-title="Approve"><i class="fa fa-edit"></i></a>
                                                        <a href="<?php echo DIR_URL; ?>/request/reject/<?php echo $request['id']; ?>" rel="tooltip" title="" class="btn btn-info btn-simple btn-xs reject" data-original-title="Reject"><i class="fa fa-times"></i></a>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "inc/close.php"; ?>
    </body>
</html>