<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/assets/css/jasny-bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/css/bank.css">
        <?php include "inc/scripts.php"; ?>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jasny-bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jquery.maskedinput.min.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/settings.js?v=<?php echo time(); ?>"></script>
        <script type="text/javascript">
        $(document).ready(function() {
            //$("#numberDomestic, #numberInternational").mask("99999999999");
            $("#routing").mask("999999999");
        });
        </script>
    </head>
    <body>
        <?php if ($_SESSION['status'] == 3) { ?>
        <script type="text/javascript">
        $(document).ready(function() {
            $('#validateDocument').modal();
        });
        </script>
        <!-- Modal -->
        <div class="modal fade" id="validateDocument" tabindex="-1" role="dialog" aria-labelledby="validateDocumentLabel" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="validateDocumentLabel">Attention</h4>
                    </div>
                    <div class="modal-body" style="text-align: center;">
                        <p>In order to verify your account, please upload your Driver's License (US Citizens Only) or your Passport.</p>
                        <p>Example of Selfie's ID Verification</p>
                        <div class="row">
                            <div class="col-md-6">
                                <img src="<?php echo DIR_URL; ?>/public/img/do1.png" alt="" class="img-responsive">
                            </div>
                            <div class="col-md-6">
                                <img src="<?php echo DIR_URL; ?>/public/img/do2.png" alt="" class="img-responsive">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="<?php echo DIR_URL; ?>/settings/document" class="btn btn-default">Validate Now</a>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="modal fade" id="modalNewAccount" tabindex="-1" role="dialog" aria-labelledby="newBankAccount" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="newBankAccount">Select the type of account</h4>
                    </div>
                    <div class="modal-body" style="text-align: center;">
                        <div class="row">
                            <div class="col-sm-6">
                                <div id="buttonDomestic" class="contentButtonsBank" data-formmodal="#modalNewAccountDomestic">
                                    <span class="icon-library"></span>
                                    <p><b>Domestic</b></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div id="buttonInternational" class="contentButtonsBank" data-formmodal="#modalNewAccountInternational">
                                    <span id="worldBank" class="icon-world"></span>
                                    <span class="icon-library"></span>
                                    <p><b>International</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" id="openModalNewAccount" class="btn btn-default">Add</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalNewAccountDomestic" tabindex="-1" role="dialog" aria-labelledby="newBankAccount" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="<?php echo DIR_URL; ?>/settings/AddBankAccount" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="newBankAccount">Add New Bank Account Domestic</h4>
                        </div>
                        <div class="modal-body" style="text-align: center;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-elements">
                                        <label for="beneficiary_name">Beneficiary Name</label>
                                        <input type="text" id="beneficiary_name" name="beneficiary_name" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-elements">
                                        <label for="numberDomestic">Account Number</label>
                                        <input type="text" id="numberDomestic" name="number" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-elements">
                                        <label for="routing">Routing Number</label>
                                        <input type="text" id="routing" name="routing" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-elements">
                                        <label for="city">City</label>
                                        <input type="text" id="city" name="city" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-elements">
                                        <label for="state">State</label>
                                        <input type="text" id="state" name="state" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-elements">
                                        <label for="zip_code">Zip Code</label>
                                        <input type="text" id="zip_code" name="zip_code" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-elements">
                                        <label for="address">Address</label>
                                        <textarea name="address" id="address" style="border: solid 1px #cccccc;"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="type" value="1">
                            <input type="hidden" name="token" value="<?php echo $_SESSION['validateToken']; ?>">
                            <button type="submit" class="btn btn-default">Add Account</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalNewAccountInternational" tabindex="-1" role="dialog" aria-labelledby="newBankAccount" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="<?php echo DIR_URL; ?>/settings/AddBankAccount" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="newBankAccount">Add New Bank Account International</h4>
                        </div>
                        <div class="modal-body" style="text-align: center;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-elements">
                                        <label for="beneficiary_name">Beneficiary Name</label>
                                        <input type="text" id="beneficiary_name" name="beneficiary_name" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-elements">
                                        <label for="numberInternational">Account Number</label>
                                        <input type="text" id="numberInternational" name="number" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-elements">
                                        <label for="swif_bic">SWIFT BIC</label>
                                        <input type="text" id="swif_bic" name="swif_bic" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-elements">
                                        <label for="city">City</label>
                                        <input type="text" id="city" name="city" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-elements">
                                        <label for="state">State</label>
                                        <input type="text" id="state" name="state" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-elements">
                                        <label for="zip_code">Zip Code</label>
                                        <input type="text" id="zip_code" name="zip_code" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-elements">
                                        <label for="address">Address</label>
                                        <textarea name="address" id="address" style="border: solid 1px #cccccc;"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-elements">
                                        <label for="account_number_intermediary">Account Number Intermediary</label>
                                        <input type="text" id="account_number_intermediary" name="account_number_intermediary" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="type" value="2">
                            <input type="hidden" name="token" value="<?php echo $_SESSION['validateToken']; ?>">
                            <button type="submit" class="btn btn-default">Add Account</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="loading">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1">
                                <div class="tabs-m">
                                    <ul>
                                        <li><a href="<?php echo DIR_URL; ?>/settings">Personal information</a></li>
                                        <?php echo ($type_user == 2) ? '<li><a href="' . DIR_URL . '/settings/businessInfo">Business information</a></li>' : ''; ?>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/document">Document</a></li>
                                        <li class="active"><a href="<?php echo DIR_URL; ?>/settings/bank">Your Financial Information</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/password">Change Password</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/security">Two Factor Authenticator</a></li>
                                    </ul>
                                </div>
                                <div class="block">
                                    <div class="block-header">
                                        <span class="text-yellow">Your Financial Information</span>
                                        <a href="#"  data-toggle="modal" data-target="#modalNewAccount" class="btn-header">Add New Bank Account</a>
                                    </div>
                                    <div class="block-body">
                                        <?php if (!empty($_SESSION['alert'])) { ?>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="alertGrey">
                                                    <?php echo $_SESSION['alert']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php unset($_SESSION['alert']); ?>
                                        <?php } ?>
                                        <?php if (count($accounts) > 0) { ?>
                                        <table class="table-m">
                                            <thead>
                                                <tr>
                                                    <th>Type</th>
                                                    <th>Account Number</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($accounts as $account) { ?>
                                                <tr>
                                                    <td>
                                                        <?php
                                                        if ($account['type'] == 1) {
                                                        echo "Domestic";
                                                        } elseif ($account['type'] == 2) {
                                                        echo "International";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><?php echo $account['number']; ?></td>
                                                    <td>
                                                        <?php
                                                        if ($account['status'] == 0) {
                                                        echo "Reject";
                                                        } elseif ($account['status'] == 1) {
                                                        echo "Active";
                                                        } elseif ($account['status'] == 2) {
                                                        echo "Pending to validate";
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        <?php } else {?>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="alertGrey">
                                                    You have not added any bank account
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "inc/close.php"; ?>
    </body>
</html>