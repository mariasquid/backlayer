<!DOCTYPE html>
<html lang="es">
	<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
		
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
	</head>
	<body style="background-color: #f5f5f5;">
		<div class="wrapper">
			<div class="main">
				<div class="content">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-md-offset-3">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<img src="<?php echo DIR_URL; ?>/public/img/logo-dark.png" alt="" class="img-responsive">
									</div>
								</div>
								<div class="card" style="margin-top: 50px;">
									<div class="header">
										<h4 class="title">Error!</h4>
									</div>
									<div class="content">
										<div class="alert alert-danger">
											<span>The link you are using is invalid</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>