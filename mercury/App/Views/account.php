<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/assets/css/jasny-bootstrap.min.css">
        <?php include "inc/scripts.php"; ?>
    </head>
    <body>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left">
                                <li>
                                    <a href="<?php echo DIR_URL; ?>/account">
                                        Account
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo DIR_URL; ?>/account/transactions">
                                        Transactions
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo DIR_URL; ?>/account/deposit">
                                        To Deposit
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="content">
                    <div class="container-fluid">
                        <div id="alert">
                            <?php if ($_SESSION['status'] == 3) { ?>
                            <div class="alert alert-danger">
                                <span>No identity document has yet been validated</span>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="row">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "inc/close.php"; ?>
    </body>
</html>