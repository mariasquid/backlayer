<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <?php include "inc/scripts.php"; ?>
        <script type="text/javascript" src="https://node.mercury.cash/socket.io/socket.io.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/sell.js?v=<?php echo time(); ?>"></script>
    </head>
    <body>
        <?php if ($_SESSION['status'] == 3) { ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#validateDocument').modal();
            });
        </script>
        <!-- Modal -->
        <div class="modal fade" id="validateDocument" tabindex="-1" role="dialog" aria-labelledby="validateDocumentLabel" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="validateDocumentLabel">Attention</h4>
                    </div>
                    <div class="modal-body" style="text-align: center;">
                        <p>In order to verify your account, please upload your Driver's License (US Citizens Only) or your Passport.</p>
                        <p>Example of Selfie's ID Verification</p>
                        <div class="row">
                            <div class="col-md-6">
                                <img src="<?php echo DIR_URL; ?>/public/img/do1.png" alt="" class="img-responsive">
                            </div>
                            <div class="col-md-6">
                                <img src="<?php echo DIR_URL; ?>/public/img/do2.png" alt="" class="img-responsive">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="<?php echo DIR_URL; ?>/settings/document" class="btn btn-default">Validate Now</a>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left sub-menu">
                                <li>
                                    <a href="<?php echo DIR_URL; ?>/buy">
                                        Buy
                                    </a>
                                </li>
                                <li class="sub-active">
                                    <a href="<?php echo DIR_URL; ?>/sell">
                                        Sell
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card buy-panel">
                                    <div class="header">
                                        <h4 class="title"><span id="ethers-pay"><?php echo $data['eth_amount']; ?></span> ETH</h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="content">
                                                <hr>
                                                <div class="block">
                                                    <h6>Withdraw</h6>
                                                    <h4 id="payment_method">ETH Wallet</h4>
                                                </div>
                                                <hr>
                                                <div class="block">
                                                    <h6>Deposit To</h6>
                                                    <h4><?php echo ($data['deposit_to'] == 1) ? 'USD Wallet (instantly)' : 'Bank Account'; ?></h4>
                                                </div>
                                                <hr>
                                                <div class="block">
                                                    <ul>
                                                        <li><span class="tags-amounts">Subtotal:</span> <span id="subtotal" class="amounts">$<?php echo number_format($data['more'], 2); ?></span></li>
                                                        <li><span class="tags-amounts">Fee:</span> <span id="fee" class="amounts">$<?php echo number_format($data['fee'], 2); ?></span></li>
                                                        <li><span class="tags-amounts">Total:</span> <span id="total" class="amounts">$<?php echo number_format($data['usd_amount'], 2); ?></span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <form action="<?php echo DIR_URL; ?>/sell/process" id="sell" method="POST" autocomplete="off">
                                    <div class="card buy-panel">
                                        <div class="header">
                                            <h4 class="title">CONFIRM YOUR TRANSACTION</h4>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="content">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="hidden" id="sell_from" name="sell_from" value="<?php echo $data['sell_from']; ?>">
                                                            <input type="hidden" id="deposit_to" name="deposit_to" value="<?php echo $_POST['deposit_to']; ?>">
                                                            <input type="hidden" id="usd_amount" name="usd_amount" value="<?php echo $data['usd_amount']; ?>">
                                                            <input type="hidden" id="eth_amount" name="eth_amount" value="<?php echo $data['eth_amount']; ?>">
                                                            <button type="submit" class="btn btn-info btn-fill pull-right">Confirm Sell</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>