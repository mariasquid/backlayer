<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/media/css/dataTables.bootstrap.css">
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/media/css/dataTables.plugins.css">
	</head>
	<body class="dashboard-page sb-l-o sb-r-c">
		<div id="main">
			<?php include "inc/header.php"; ?>
			<?php include "inc/aside.php"; ?>
			<div id="content_wrapper">
				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-visible">
								<div class="panel-heading">
									<div class="panel-title-hidden-xs">
										<span class="glyphicon glyphicon-tasks"></span> Validate document
									</div>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-md-6">
											<blockquote class="blockquote-primary">
												<p>Full name:</p>
												<small><?php echo $user['name'] . " " . $user['last_name']; ?></small>
											</blockquote>
											<blockquote class="blockquote-primary">
												<p>Email:</p>
												<small></b> <?php echo $user['email']; ?></small>
											</blockquote>
										</div>
										<div class="col-md-4">
											<figure>
												<img src="<?php echo DIR_URL; ?>/public/identifications/<?php echo $document['document']; ?>.jpg" alt="" class="img-responsive">
											</figure>
										</div>
									</div>
								</div>
								<div class="panel-footer">
									<div class="row">
										<div class="col-md-2 col-md-offset-10">
											<a href="<?php echo DIR_URL; ?>/documents/evaluate/<?php echo $document['id']; ?>/<?php echo $document['user']; ?>/1" class="btn btn-system">APPROVE</a>
											<a href="<?php echo DIR_URL; ?>/documents/evaluate/<?php echo $document['id']; ?>/<?php echo $document['user']; ?>/0" class="btn btn-danger">REJECT</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<?php include "inc/scripts.php"; ?>
		<script src="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
		<script src="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				"use strict";
				Core.init();
				// Init DataTables
				$('#datatable').dataTable({
					"sDom": 't<"dt-panelfooter clearfix"ip>',
					"oTableTools": {
						"sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
					}
				});
			});
		</script>
	</body>
</html>