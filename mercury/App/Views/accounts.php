<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/qrcode.min.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/moment.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/accounts.js?v=<?php echo time(); ?>"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/wallets.js?v=<?php echo time(); ?>"></script>
	</head>
	<body>
		<?php include "inc/trialInfoModal.php"; ?>
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-target=".bs-example-modal-sm">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Wallet</h4>
					</div>
					<div class="modal-body" style="text-align: center;">
					<div id="qrcode" style="display: inline-block;"></div>
					<br>
					<code id="addressView"></code>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn-m-l closeModal" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div class="loading">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-lg-8 col-lg-offset-2">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">My accounts</span> <?php echo $_SESSION['name']; ?>
										<a href="<?php echo DIR_URL; ?>/tools/create" id="new-wallet" class="btn-header">Add wallet</a>
									</div>
									<div class="block-myaccount">
										<?php if (!empty($_SESSION['alert'])) { ?>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="alertGrey">
                                                    <?php echo $_SESSION['alert']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php unset($_SESSION['alert']); ?>
                                        <?php } ?>
										<table class="table-m tbody-wallets">
											<thead>
												<tr>
													<th>Name</th>
													<th>Currency</th>
													<th>Balance</th>
													<th>Address</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody id="tbody-wallets" >
												<tr style="cursor: pointer;">
													<td onclick="javascript:location.href='<?php echo DIR_URL; ?>/accounts/usdwallet'">USD Wallet</td>
													<td onclick="javascript:location.href='<?php echo DIR_URL; ?>/accounts/usdwallet'">USD</td>
													<td onclick="javascript:location.href='<?php echo DIR_URL; ?>/accounts/usdwallet'"><?php echo $usdwallet['balance']; ?></td>
													<td><a href="#" class="show_address viewAddress" data-address="<?php echo $usdwallet['address']; ?>" data-toggle="modal" data-target="#myModal">(Show Address)</a></td>
													<td><a href="<?php echo DIR_URL; ?>/accounts/deposit">Deposit to</a> | <a href="<?php echo DIR_URL; ?>/accounts/withdraw">Withdraw</a></td>
												</tr>
												<?php foreach ($ethwallets as $ethwallet) { ?>
												<tr style="cursor: pointer;">
													<td><?php echo ($ethwallet['primary'] == 1) ? '<div class="getPrimary"><span class="glyphicon glyphicon-star"></span></div>' : '<a href="' . DIR_URL . '/accounts/changePrimary/' . $ethwallet['id'] . '" class="changePrimary"  data-toggle="tooltip" data-placement="top" title="Set as Primary"><span class="glyphicon glyphicon-star"></span></a>'; ?> <span class="nameAlias" data-id="<?php echo $ethwallet['id']; ?>" contenteditable="false"><?php echo $ethwallet['alias']; ?></span> <a href="#" class="editNameAlias" style="color: #000;"><span class="pe-7s-pen"></span></a> <span class="pressToSave"></span></td>
													<td onclick="javascript:location.href='<?php echo DIR_URL; ?>/accounts/ethwallet/<?php echo $ethwallet['id']; ?>'">ETH</td>
													<td onclick="javascript:location.href='<?php echo DIR_URL; ?>/accounts/ethwallet/<?php echo $ethwallet['id']; ?>'"><?php echo $ethwallet['wallet_balance']; ?></td>
													<!--<td><?php //echo $ethwallet['address']; ?></td>-->
													<td>
														<a href="#" class="show_address viewAddress" data-address="<?php echo $ethwallet['address']; ?>" data-toggle="modal" data-target="#myModal">(Show Address)</a>
													</td>
													<td></td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>