<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/assets/skin/default_skin/css/theme.css">
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/assets/admin-tools/admin-forms/css/admin-forms.css">
        <link rel="shortcut icon" href="<?php echo DIR_URL; ?>/public/assets/img/favicon.ico">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="external-page external-alt sb-l-c sb-r-c">
        <div id="main" class="animated fadeIn">
            <section id="content_wrapper">
                <section id="content">
                    <div class="admin-form theme-info mw500" id="login">
                        <div class="row table-layout">
                            <a href="dashboard.html" title="Return to Dashboard">
                                <img src="<?php echo DIR_URL; ?>/public/assets/img/logos/logo.png" title="AdminDesigns Logo" class="center-block img-responsive" style="max-width: 275px;">
                            </a>
                        </div>
                        <div id="alert">
                            
                        </div>
                        <div class="panel mt30 mb25">
                            <form method="POST" action="<?php echo DIR_URL; ?>/login/sign" id="login-form">
                                <div class="panel-body bg-light p25 pb15">
                                    <div class="section">
                                        <label for="email" class="field-label text-muted fs18 mb10">Correo Electrónico</label>
                                        <label for="email" class="field prepend-icon">
                                            <input type="text" name="email" id="email" class="gui-input" placeholder="Correo Electrónico...">
                                            <label for="email" class="field-icon">
                                                <i class="fa fa-user"></i>
                                            </label>
                                        </label>
                                    </div>
                                    <div class="section">
                                        <label for="password" class="field-label text-muted fs18 mb10">Contraseña</label>
                                        <label for="password" class="field prepend-icon">
                                            <input type="password" name="password" id="password" class="gui-input" placeholder="Contraseña...">
                                            <label for="password" class="field-icon">
                                                <i class="fa fa-lock"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>
                                <div class="panel-footer clearfix">
                                    <button type="submit" class="button btn-primary mr10 pull-right">ENTRAR</button>
                                    <label class="switch ib switch-primary mt10">
                                        <input type="checkbox" name="remember" id="remember" checked>
                                        <label for="remember" data-on="SI" data-off="NO"></label>
                                        <span>Recordarme</span>
                                    </label>
                                </div>
                            </form>
                        </div>
                        <div class="login-links">
                            <p>
                                <a href="pages_login-alt.html" class="active" title="Sign In">Forgot Password?</a>
                            </p>
                            <p>Haven't yet Registered?
                                <a href="pages_login-alt.html" title="Sign In">Sign up here</a>
                            </p>
                        </div>
                    </div>
                </section>
            </section>
        </div>
        <script src="<?php echo DIR_URL; ?>/public/vendor/jquery/jquery-1.11.1.min.js"></script>
        <script src="<?php echo DIR_URL; ?>/public/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
        <script src="<?php echo DIR_URL; ?>/public/assets/js/utility/utility.js"></script>
        <script src="<?php echo DIR_URL; ?>/public/assets/js/main.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/login.js"></script>
        <script type="text/javascript">
        jQuery(document).ready(function() {
        "use strict";
        // Init Theme Core
        Core.init();
        });
        </script>
    </body>
</html>