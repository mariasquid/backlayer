<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/media/css/dataTables.bootstrap.css">
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/media/css/dataTables.plugins.css">
	</head>
	<body class="dashboard-page sb-l-o sb-r-c">
		<div id="main">
			<?php include "inc/header.php"; ?>
			<?php include "inc/aside.php"; ?>
			<div id="content_wrapper">
				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-visible" id="spy1">
								<div class="panel-heading">
									<div class="panel-title hidden-xs">
										<span class="glyphicon glyphicon-tasks"></span> All Users
									</div>
								</div>
								<div class="panel-body pn">
									<table class="table table-striped table-hover" id="datatable" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>Name</th>
												<th>Last Name</th>
												<th>email</th>
												<th>Satus</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($users as $user) { ?>
											<tr>
												<td><?php echo $user['name']; ?></td>
												<td><?php echo $user['last_name']; ?></td>
												<td><?php echo $user['email']; ?></td>
												<?php if ($user['status'] == 0) { ?>
												<td><span class="label label-danger mr5 mb10 ib lh15">Inactive</span></td>
												<?php } elseif ($user['status'] == 1) { ?>
												<td><span class="label label-success mr5 mb10 ib lh15">Active</span></td>
												<?php } elseif ($user['status'] == 2) { ?>
												<td><span class="label label-warning mr5 mb10 ib lh15">Without validating email</span></td>
												<?php } elseif ($user['status'] == 3) { ?>
												<td><span class="label label-warning mr5 mb10 ib lh15">Without validating identity document</span></td>
												<?php } ?>
												<td>
												<div class="btn-group">
														<a href="<?php echo DIR_URL; ?>/users/details/<?php echo $user['id']; ?>" class="btn btn-sm btn-system">Details</a>
														<a data-href="<?php echo DIR_URL; ?>/users/delete/<?php echo $user['id']; ?>" class="btn btn-sm btn-danger delete-user">Delete</a>
														<a href="<?php echo DIR_URL; ?>/transactions/transactions/<?php echo $user['id']; ?>" class="btn btn-sm btn-info">Transactions</a>
													</div>
												</td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<?php include "inc/scripts.php"; ?>
		<script src="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
		<script src="<?php echo DIR_URL; ?>/public/vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
		<script src="<?php echo DIR_URL; ?>/public/assets/js/delete.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				"use strict";
				Core.init();
				// Init DataTables
				$('#datatable').dataTable({
					"sDom": 't<"dt-panelfooter clearfix"ip>',
					"oTableTools": {
						"sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
					}
				});
			});
		</script>
	</body>
</html>