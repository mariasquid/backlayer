<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <title>1 ETH = $<?php echo $ethusd;?> Mercury Cash: Ethereum Wallet, ETH/USD Market</title>
<meta name="description" content="Mercury cash is a specialized and secure platform that has its own ethereum wallet and market exchange for ETH/USD">
<meta name="keywords" content="Ethereum Wallet">
<meta name="robots" content="index, follow">
<meta name="copyright" content="Adventurous Entertainment LLC">
<meta name="language" content="EN">
<meta name="author" content="Adventurous Entertainment LLC">
<meta name="creationdate" content="May 2017">
<meta name="distribution" content="global">
<meta name="rating" content="general">
<link rel="Publisher" href="https://www.adenter.io">
<link rel="Canonical" href="https://www.mercury.cash">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo DIR_URL; ?>/public/css/theme.css" rel="stylesheet">
    <link href="<?php echo DIR_URL; ?>/public/css/about.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700'>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic'>
    <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/fonts/elegant/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/css/social.css" rel="stylesheet">
    <!-- Multiple Favicon 14-5-2017 -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#f3c805">
        <meta name="apple-mobile-web-app-title" content="Mercury Cash">
        <meta name="application-name" content="Mercury Cash">
        <meta name="msapplication-TileColor" content="#2f3740">
        <meta name="msapplication-TileImage" content="/mstile-144x144.png">
        <meta name="theme-color" content="#ffffff">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
 <!-- Live chat --> 
  <script type="text/javascript">
  window.__lo_site_id = 80707;
  (function() {
      var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
      wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
    })();
  </script>
  <!-- Google Analytics 14-5-2017 -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-98285209-1', 'auto');
ga('send', 'pageview');

</script>
</head>
  <body id="page-top" class="index">
    <!-- Navigation -->
    <nav class="navbar navbar-default">
      <!-- Topbar Nav (hidden) -->
      <div class="topbar-nav clearfix">
        <div class="container">
          <ul class="topbar-left list-unstyled list-inline">
            <li> <span class="fa fa-phone"></span> 123-456-7890 </li>
            <li> <span class="fa fa-envelope"></span> info@yourdomain.com </li>
          </ul>
          <ul class="topbar-right list-unstyled list-inline topbar-social">
            <li> <a href="#"> <span class="fa fa-facebook"></span> </a></li>
            <li> <a href="#"> <span class="fa fa-twitter"></span> </a></li>
            <li> <a href="#"> <span class="fa fa-google-plus"></span> </a></li>
            <li> <a href="#"> <span class="fa fa-dribbble"></span> </a></li>
            <li> <a href="#"> <span class="fa fa-instagram"></span> </a></li>
          </ul>
        </div>
      </div>
      <div class="container" style="max-width: 1050px;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand page-scroll" href="<?php echo DIR_URL; ?>">
            <img id="logo-navbar" src="<?php echo DIR_URL; ?>/public/img/logo-dark.png" class="img-responsive" alt="">
          </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
                        <li>
                <a href="<?php echo DIR_URL; ?>" class="page-scroll">1 ETH = $<?php echo $ethusd;?></a>
            </li>
                        <li>
                          <a href="<?php echo DIR_URL; ?>">HOME</a> </li>
            <li>
              <a href="<?php echo DIR_URL; ?>/login" class="page-scroll">SIGN IN</a>
            </li>
            <li>
              <a href="<?php echo DIR_URL; ?>/register" class="page-scroll">CREATE AN ACCOUNT</a>
            </li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
    </nav>

    <!-- Hero Content -->
    <header id="hero1">
        <div class="container">

            <div class="intro-text">
             <div class="intro-heading">MERCURY CASH</div>
              <div class="intro-lead-in">TRANSACTION FEES</div>
                    <!-- <div class="intro-heading">GET TO KNOW US BETTER</div> -->
                <!-- <a href="#services" class="page-scroll btn btn-xl mr30 btn-primary">Learn More</a>
                <a href="#contact" class="page-scroll btn btn-xl btn-wire">Contact Us</a> -->
            </div>
        </div>
    </header>



    <section id="fees">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-left">
                    <h3 class="subtitle">United States</h3>
                </div>
            </div>

            <div class="container">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Form of payment for purchase</th>
                    <th>Effective service fee **</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>ACH DIRECT DEPOSIT</td>
                    <td>1.99%</td>
                  </tr>
                  <tr>
                    <td>USD WALLET MERCURY CASH</td>
                    <td>1.99%</td>
                  </tr>
                  <tr>
                    <td>CREDIT CARD</td>
                    <td>4.99%</td>
                  </tr>
                </tbody>
              </table>


                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th  width="646px">Payment method for sale</th>
                            <th>service Fee</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>ACH DIRECT PAYMENT</td>
                            <td>1.99%</td>
                          </tr>
                          <tr>
                            <td>USD WALLET MERCURY CASH</td>
                            <td>1.99%</td>
                          </tr>
                          <tr>
                            <td>PAYPAL</td>
                            <td>4.99%</td>
                          </tr>
                        </tbody>
                      </table>

                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th  width="646px">USD Deposit method</th>
                            <th>Fee</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>ACH TRANSFER</td>
                            <td>FREE</td>
                          </tr>
                          <tr>
                            <td>DOMESTIC WIRE TRANSFER</td>
                            <td>$ 30 USD</td>
                          </tr>
                          <tr>
                            <td>INTERNATIONAL WIRE TRANSFER</td>
                            <td>$ 60 USD</td>
                          </tr>
                          <tr>
                            <td>PAYPAL</td>
                            <td>$ 45 USD</td>
                          </tr>
                        </tbody>
                      </table>
  </div>

  <div class="row">
      <div class="col-sm-12 text-left">
        <font size="3" >*** In Some cases, bank may charge aditional fees for bank transfer between your personal account and your Mercurycash account. </font>
      </div>
  </div>



    </section>







    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Get in touch and explore the possibilities!!</h2>
                </div>
            </div>
			<form name="sentMessage" id="contactForm" class="mw800 center-block clearfix" novalidate>
        	   <div class="form-group">
        	  	 <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
                 <p class="help-block text-danger"></p>
        	  </div>
        	   <div class="form-group">
        	  	 <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
                 <p class="help-block text-danger"></p>
        	  </div>
            <div class="form-group">
  						<button type="submit" class="btn btn-xl btn-block btn-wire">Subscribe</button>
  					</div>
	        </form>
	        <p> By subscribing you agree to our terms and conditions. </p>

        </div>
    </section>

<!-- Footer -->
		<?php include "inc/footer-homes.php"; ?>

    <!-- jQuery -->
    <!-- <script src="js/vendor/jquery.js"></script> -- Local Version -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <!-- <script src="js/vendor/bootstrap.min.js"></script> -- Local Version -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/vendor/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/main.js"></script>

</body>

</html>
