<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
	</head>
	<body class="dashboard-page sb-l-o sb-r-c">
		<div id="main">
			<?php include "inc/header.php"; ?>
			<?php include "inc/aside.php"; ?>
			<div id="content_wrapper">
				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-visible">
								<div class="panel-heading">
									<div class="panel-title hidden-xs">
										<span class="glyphicon glyphicon-tasks"></span> User data
									</div>
								</div>
								<div class="panel-body">
									<blockquote class="blockquote-primary">
										<p>Full name:</p>
										<small><?php echo $user['name'] . " " . $user['last_name']; ?></small>
									</blockquote>
									<blockquote class="blockquote-primary">
										<p>Email:</p>
										<small></b> <?php echo $user['email']; ?></small>
									</blockquote>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-visible">
								<div class="panel-heading">
									<div class="panel-title hidden-xs">
										<span class="glyphicon glyphicon-tasks"></span> Wallet data
									</div>
								</div>
								<div class="panel-body">
									<blockquote class="blockquote-primary">
										<p>Address:</p>
										<small><?php echo $wallet['address']; ?></small>
									</blockquote>
									<blockquote class="blockquote-primary">
										<p>Balance:</p>
										<small></b> <?php echo $balance; ?> ETH</small>
									</blockquote>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<div class="panel panel-visible">
								<div class="panel-heading">
									<div class="panel-title hidden-xs">
										<span class="glyphicon glyphicon-tasks"></span> Suspend user
									</div>
								</div>
								<div class="panel-body">
									<form action="<?php echo DIR_URL; ?>/users/suspendUser" id="suspend-user" method="POST" class="form-horizontal" role="form">
										<div class="form-group">
											<label for="description" class="col-lg-3 control-label">Description</label>
											<div class="col-lg-8">
												<textarea name="description" id="description" required="required" class="form-control textarea-grow"></textarea>
											</div>
										</div>
										<div class="form-group">
											<div class="col-lg-6 col-lg-offset-4">
											<input type="hidden" id="user" name="user" value="<?php echo $user['id']; ?>">
												<button type="submit" class="btn btn-system btn-block">Suspend</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				"use strict";
				Core.init();
			});
		</script>
	</body>
</html>