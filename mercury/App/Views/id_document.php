<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <?php include "inc/scripts.php"; ?>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/id_document.js?v=<?php echo time(); ?>"></script>
    </head>
    <body>
        <div class="loading">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1">
                                <div class="tabs-m">
                                    <ul>
                                        <li><a href="<?php echo DIR_URL; ?>/settings">Personal information</a></li>
                                        <?php echo ($type_user == 2) ? '<li><a href="' . DIR_URL . '/settings/businessInfo">Business information</a></li>' : ''; ?>
                                        <li class="active"><a href="<?php echo DIR_URL; ?>/settings/document">Document</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/bank">Your Financial Information</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/password">Change Password</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/settings/security">Two Factor Authenticator</a></li>
                                    </ul>
                                </div>
                                <div class="block">
                                    <div class="block-header">
                                        <span class="text-yellow">Edit profile</span>
                                    </div>
                                    <div class="block-body">
                                        <?php if (!empty($_SESSION['alert'])) { ?>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="alertGrey">
                                                    <?php echo $_SESSION['alert']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php unset($_SESSION['alert']); ?>
                                        <?php } ?>
                                        <form action="<?php echo DIR_URL; ?>/settings/uploadIdDocument" method="POST" enctype="multipart/form-data" id="document_file">
                                            <div class="row">
                                                <div id="alert_upload_file" class="col-md-8">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="fileContentManual">
                                                        <div id="buttonUploadDocument">
                                                            <span>Choose file</span>
                                                            <input type="file" id="manual_document" name="document_manual">
                                                        </div>
                                                        <span id="namefileManual">No file chosen</span>
                                                        <button id="btnSearch" type="button" class="btn-mercury-black">Browse</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span>Document Type: </span>
                                                    <select name="document_type" id="document_type">
                                                        <option value="1">Passport</option>
                                                        <option value="2">Bank statement</option>
                                                        <option value="3">Driver's licence</option>
                                                        <option value="4">ID Confirmation Photo</option>
                                                    </select>
                                                    <br>
                                                    <p>Or Drag and Drop Below (Please upload one document at the time)</p>
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="fileContent">
                                                                <p>(Drag and drop here)</p>
                                                                <input type="file" id="document" name="document">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div-alert"><!--Loading document--></div>
                                                            <div id="contentbar">
                                                                <div id="statusbar"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="name_file">No file chosen</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="preview" class="col-md-6">
                                                    <h5>Example:</h5>
                                                    <img src="<?php echo DIR_URL; ?>/public/img/example-document.jpg" class="img-responsive img-thumbnail">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2 col-md-offset-4">
                                                    <input type="hidden" name="token" value="<?php echo $_SESSION['validateToken']; ?>">
                                                    <button type="submit" class="btn-m-l">UPLOAD</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="block hidden-xs" style="margin-top: 10px;">
                                    <div class="block-header">
                                        <span class="text-yellow">Uploaded documents</span>
                                    </div>
                                    <div class="block-body">
                                        <table class="table-m">
                                            <thead>
                                                <tr>
                                                    <th>Filename</th>
                                                    <th>Document</th>
                                                    <th>Document Type</th>
                                                    <th>Size</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($documents as $document) { ?>
                                                <tr>
                                                    <td><?php echo $document['original_name']; ?></td>
                                                    <td>
                                                        <?php
                                                        if ($document['type'] == 1) {
                                                        echo "Passport";
                                                        } elseif ($document['type'] == 2) {
                                                        echo "Bank statement";
                                                        } elseif ($document['type'] == 3) {
                                                        echo "Driver's Licence";
                                                        } elseif ($document['type'] == 4) {
                                                        echo "ID confirmation photo";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($document['type'] == 1) {
                                                        echo "Identity Document";
                                                        } elseif ($document['type'] == 2) {
                                                        echo "Proof of residence";
                                                        } elseif ($document['type'] == 3) {
                                                        echo "Identity Document";
                                                        } elseif ($document['type'] == 4) {
                                                        echo "ID confirmation";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><?php echo $document['size']; ?></td>
                                                    <?php if ($document['status'] == 0) { ?>
                                                    <td><span class="status-reject">Deny</span></td>
                                                    <?php } elseif ($document['status'] == 1) { ?>
                                                    <td><span class="status-success">Approved</span></td>
                                                    <?php } elseif ($document['status'] == 2) { ?>
                                                    <td><span class="status-pending">Pending</span></td>
                                                    <?php } ?>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "inc/close.php"; ?>
    </body>
</html>