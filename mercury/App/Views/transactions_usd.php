<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/assets/css/jasny-bootstrap.min.css">
        <?php include "inc/scripts.php"; ?>
    </head>
    <body>
        <?php include "inc/trialInfoModal.php"; ?>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left">
                                <li>
                                    <a href="<?php echo DIR_URL; ?>/account">
                                        Account
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo DIR_URL; ?>/account/transactions">
                                        Transactions
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo DIR_URL; ?>/account/deposit">
                                        To Deposit
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title">Transactions</h4>
                                        <!--<p class="category">Here is a subtitle for this table</p>-->
                                    </div>
                                    <div class="content table-responsive table-full-width">
                                        <table class="table table-hover table-striped">
                                            <thead>
                                                <th>Description</th>
                                                <th>Type</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Amount</th>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($transactions as $transaction) { ?>
                                                <tr>
                                                    <td><?php echo $transaction['description']; ?></td>
                                                    <td><?php echo $transaction['type']; ?></td>
                                                    <td><?php echo date("d/m/Y H:i:s", $transaction['date']); ?></td>
                                                    <td><?php echo $transaction['status']; ?></td>
                                                    <td><?php echo "$" . $transaction['usd_amount']; ?></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "inc/close.php"; ?>
    </body>
</html>