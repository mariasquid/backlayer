<!DOCTYPE html>
<html lang="es">
	<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
		
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
	</head>
	<body style="background-color: #f5f5f5;">
		<div class="wrapper">
			<div class="main">
				<div class="content">
					<div class="container">
						<div class="row">
							<div class="col-sm-3 col-sm-offset-4">
								<div id="logo-register">
									<img src="<?php echo DIR_URL; ?>/public/img/logo-dark.png" alt="" class="img-responsive">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-md-offset-3">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">I lost my password</span>
									</div>
									<div class="block-body">
										<form method="POST" action="<?php echo DIR_URL; ?>/lostpassword/validateMail" id="login-form" autocomplete="off">
											<div class="row">
												<div class="col-md-9">
													<div class="form-elements">
														<label>Email</label>
														<input type="email" id="email" name="email"  placeholder="Email">
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-elements">
														<label>&nbsp;</label>
														<button type="submit" class="btn-m-l">Send mail</button>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12" style="text-align: right; padding-top: 15px;">
													<a href="<?php echo DIR_URL; ?>/register">Create new account</a>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>