<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/usdwallets.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	</head>
	<body>
		<?php include "inc/trialInfoModal.php"; ?>
		<!-- Modal -->
		<div class="modal fade" id="fiterDate" tabindex="-1" role="dialog" aria-labelledby="fiterDateLabel" data-target=".bs-example-modal-sm">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form action="<?php echo DIR_URL; ?>/accounts/genPdfReportUsdWallet" method="POST" target="_blank">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="fiterDateLabel">Attention</h4>
						</div>
						<div class="modal-body" style="text-align: center;">
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-elements">
                                        <label for="from">From</label>
                                        <input type="text" id="from" name="from" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-elements">
                                        <label for="to">To</label>
                                        <input type="text" id="to" name="to" style="border: solid 1px #cccccc;">
                                    </div>
                                </div>
                            </div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-default">Search</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Transactions</span>
										<a href="#" data-toggle="modal" data-target="#fiterDate" class="btn-header">e-statement</a>
									</div>
									<div class="block-body">
										<?php if (!empty($_SESSION['alert'])) { ?>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="alertGrey">
                                                    <?php echo $_SESSION['alert']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php unset($_SESSION['alert']); ?>
                                        <?php } ?>
										<table class="table-m">
											<tbody id="tbody-wallets">
												<thead>
													<tr>
														<th>Date</th>
														<th>Wire Code</th>
														<th>Rate ETH</th>
														<th>ETH QTY.</th>
														<th>USD Amount</th>
														<th>Fee</th>
														<th>Status</th>
														<th>TX Type</th>
													</tr>
												</thead>
												<tbody>
													<?php foreach ($transactions as $transaction) { ?>
													<tr>
														<td><?php echo date("Y-m-d H:i:s", $transaction['date']); ?></td>
														<td><?php echo (!empty($transaction['wire_code'])) ? $transaction['wire_code'] : ""; ?></td>
														<td><?php echo (!empty($transaction['eth_value'])) ? "$" . number_format($transaction['eth_value'], 2). " USD" : ""; ?></td>
														<td><?php echo (!empty($transaction['eth_amount'])) ? number_format($transaction['eth_amount'], 2). " ETH" : ""; ?></td>
														<td><?php echo (!empty($transaction['usd_amount'])) ? "$" . number_format($transaction['usd_amount'], 2). " USD" : ""; ?></td>
														<td><?php echo (!empty($transaction['commission'])) ? "$" . number_format($transaction['commission'], 2). " USD" : ""; ?></td>
														<?php if ($transaction['status'] == 0) { ?>
														<td><span class="label label-danger mr5 mb10 ib lh15">Rejected</span></td>
														<?php } elseif ($transaction['status'] == 1) { ?>
														<td><span class="label label-success mr5 mb10 ib lh15">Approved</span></td>
														<?php } elseif ($transaction['status'] == 2) { ?>
														<td><span class="label label-warning mr5 mb10 ib lh15">Pending</span></td>
														<?php } ?>
														<?php if ($transaction['type_transaction'] == 1) { ?>
														<td>Deposit</td>
														<?php } elseif ($transaction['type_transaction'] == 2) { ?>
														<td>Buy</td>
														<?php } elseif ($transaction['type_transaction'] == 3) { ?>
														<td>Sell</td>
														<?php } elseif ($transaction['type_transaction'] == 4) { ?>
														<td>Withdraws</td>
														<?php } ?>
													</tr>
													<?php } ?>
												</tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>