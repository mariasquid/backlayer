<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <?php include "inc/scripts.php"; ?>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jquery.maskedinput.min.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jquery.numeric.min.js"></script>
        <!--<script type="text/javascript" src="<?php //echo DIR_URL; ?>/public/assets/js/withdraw.js"></script>-->
        <script type="text/javascript">
        $(document).ready(function() {
        $('#usd_amount').numeric('.');
        $('#eth_amount').numeric('.');
        });
        </script>
    </head>
    <body>
        <?php include "inc/trialInfoModal.php"; ?>
        <div class="loading">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="block">
                                    <div class="block-header">
                                        <span class="text-yellow">Withdraw</span>
                                        <a href="<?php echo DIR_URL; ?>/settings/bank" class="btn-header">Add Bank Account</a>
                                    </div>
                                    <div class="block-body">
                                        <?php if (!empty($_SESSION['alert'])) { ?>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="alertGrey">
                                                    <?php echo $_SESSION['alert']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php unset($_SESSION['alert']); ?>
                                        <?php } ?>
                                        <form action="<?php echo DIR_URL; ?>/accounts/newRequestsWithdraw" method="POST" autocomplete="off">
                                            <div class="form-elements">
                                                <label for="usdwallet">USD Wallet</label>
                                                <input type="text" id="usdwallet" name="usdwallet" disabled="disabled" value="<?php echo number_format($usdwallet['balance'], 2); ?>">
                                            </div>
                                            <div class="form-elements">
                                                <label for="bank_account">Bank Account</label>
                                                <select name="bank_account" id="bank_account">
                                                    <?php foreach ($accounts as $account) { ?>
                                                    <option value="<?php echo $account['id']; ?>"><?php echo $account['number']; ?> - <?php echo ($account['type'] == 1) ? "Domestic" : "International"; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-elements">
                                                <label for="usd_amount">USD Amount</label>
                                                <input type="text" id="usd_amount" name="usd_amount" value="0">
                                            </div>
                                            <div class="block-amounts">
                                                    <ul>
                                                        <li><span class="tags-amounts">Subtotal:</span> <span id="subtotal" class="amounts">$0.00</span></li>
                                                        <li><span class="tags-amounts"><a target="_blank" href="<?php echo DIR_URL; ?>/fees">Fee:</span> </a> <span id="fee" class="amounts">$0.00</span></li>
                                                        <li><span class="tags-amounts">Total:</span> <span id="total" class="amounts">$0.00</span></li>
                                                    </ul>
                                                </div>
                                            <div class="row">
                                                <div class="col-md-6 col-md-offset-3">
                                                    <input type="hidden" name="type" value="1">
                                                    <!--<button type="submit" class="btn-m-l">Request</button>-->
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "inc/close.php"; ?>
    </body>
</html>