<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/assets/css/jasny-bootstrap.min.css">
        <?php include "inc/scripts.php"; ?>
    </head>
    <body>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left sub-menu">
                                <li>
                                    <a href="<?php echo DIR_URL; ?>/accounts">
                                        Accounts
                                    </a>
                                </li>
                                <li class="sub-active">
                                    <a href="<?php echo DIR_URL; ?>/accounts/deposit">
                                        Deposit to USD Wallet
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="alert alert-<?php echo $alert; ?>">
                                <span><?php echo $message; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>