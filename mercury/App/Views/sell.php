<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php include "inc/styles.php"; ?>
        <?php include "inc/scripts.php"; ?>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jquery.numeric.min.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/sell.js"></script>
    </head>
    <body>
        <?php include "inc/trialInfoModal.php"; ?>
        <div class="loading">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
        <div class="wrapper">
            <?php include "inc/aside.php"; ?>
            <div class="main-panel">
                <?php include "inc/header.php"; ?>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <nav class="tabs-m">
                                    <ul>
                                        <li><a href="<?php echo DIR_URL; ?>/buy">Buy</a></li>
                                        <li class="active"><a href="<?php echo DIR_URL; ?>/sell">Sell</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/accounts/deposit">Deposit to USD Wallet</a></li>
                                        <li><a href="<?php echo DIR_URL; ?>/buy/limits">Limits</a></li>
                                    </ul>
                                </nav>
                                <div class="block">
                                    <div class="block-header">
                                        <span class="text-yellow">Sell ethereum</span>
                                    </div>
                                    <form action="<?php echo DIR_URL; ?>/sell/createTransaction" method="POST" autocomplete="off">
                                        <div class="block-body">
                                            <?php if (!empty($_SESSION['alert'])) { ?>
                                            <div class="row">
                                                <div class="col-md-6 col-md-offset-3">
                                                    <div class="alertGrey">
                                                        <?php echo $_SESSION['alert']; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php unset($_SESSION['alert']); ?>
                                            <?php } ?>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-elements">
                                                                <label>Balance</label>
                                                                <select name="eth_wallet" id="eth_wallet">
                                                                    <?php foreach ($ethwallets as $ethwallet) { ?>
                                                                    <option value="<?php echo $ethwallet['id']; ?>"><?php echo (is_null($ethwallet['alias']) ? $ethwallet['address'] : $ethwallet['alias']); ?> (<?php echo $ethwallet['wallet_balance']; ?> ETH)</option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-elements">
                                                                <label>Deposit to</label>
                                                                <select name="bank_account" id="bank_account">
                                                                    <option value="usdwallet">USD Wallet (instantly)</option>
                                                                    <?php foreach ($accounts as $account) { ?>
                                                                    <option value="<?php echo $account['id']; ?>"><?php echo $account['number']; ?> - <?php echo ($account['type'] == 1) ? "Domestic" : "International"; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-elements">
                                                                <label>Eth amount</label>
                                                                <input type="text" id="eth_amount" name="eth_amount" placeholder="ethereums you want to sell" style="padding-right: 85px;">
                                                                <button id="usd_max_sell_eth" class="max_buy" style="display: inline-block;">MAX SELL</button>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-elements">
                                                                <label>Dollars amount (At least $1)</label>
                                                                <input type="text" id="usd_amount" name="usd_amount" placeholder="Amount of dollars to receiver" style="padding-right: 85px;">
                                                                <button id="usd_max_sell_usd" class="max_buy" style="display: inline-block;">MAX SELL</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-md-offset-8 col-lg-3 col-lg-offset-9" style="margin-top: 50px">
                                                            <input type="hidden" id="commission" name="commission" value="<?php echo $commission; ?>">
                                                            <button type="submit" class="btn-m-l">Sell</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="">
                                                        <h6>Payment Method</h6>
                                                        <h4 id="payment_method">Without selecting</h4>
                                                    </div>
                                                    <hr>
                                                    <div class="">
                                                        <h6>Deposit To</h6>
                                                        <h4>ETH Wallet</h4>
                                                    </div>
                                                    <hr>
                                                    <div class="block-amounts">
                                                        <ul>
                                                            <li><span class="tags-amounts">Subtotal:</span> <span id="subtotal" class="amounts">$0.00</span></li>
                                                            <li><span class="tags-amounts"><a target="_blank" href="<?php echo DIR_URL; ?>/fees">Fee:</span> </a> <span id="fee" class="amounts">$0.00</span></li>
                                                            <li><span class="tags-amounts">Total:</span> <span id="total" class="amounts">$0.00</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "inc/close.php"; ?>
    </body>
</html>