<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
	</head>
	<body class="dashboard-page sb-l-o sb-r-c">
		<div id="main">
			<?php include "inc/header.php"; ?>
			<?php include "inc/aside.php"; ?>
			<div id="content_wrapper">
				<section id="content" class="animated fadeIn">
					<div id="alert">
						
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-visible" id="spy1">
								<div class="panel-heading">
									<div class="panel-title hidden-xs">
										<span class="glyphicon glyphicon-tasks"></span> Purchase information
									</div>
								</div>
								<div class="panel-body">
									<blockquote class="blockquote-warning">
										<p>TRANSACTION ID</p>
										<small><?php echo $purchase['transaction_id_stripe']; ?></small>
									</blockquote>
									<div class="row">
										<div class="col-md-6">
											<blockquote class="blockquote-warning">
												<p>ETHEREUMS</p>
												<small><?php echo $purchase['eth_amount']; ?> ETH</small>
											</blockquote>
										</div>
										<div class="col-md-6">
											<blockquote class="blockquote-warning">
												<p>DOLLARS</p>
												<small><?php echo number_format($purchase['usd_amount'], 2); ?>$</small>
											</blockquote>
										</div>
									</div>
									<blockquote class="blockquote-warning">
										<p>TRANSACTION STATUS</p>
										<?php if ($purchase['status'] == 0) { ?>
										<small><span class="label label-danger mr5 mb10 ib lh15">Rejected</span></small>
										<?php } elseif ($purchase['status'] == 1) { ?>
										<small><span class="label label-success mr5 mb10 ib lh15">Approved</span></small>
										<?php } elseif ($purchase['status'] == 2) { ?>
										<small><span class="label label-warning mr5 mb10 ib lh15">pending</span></small>
										<?php } ?>
									</blockquote>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-visible" id="spy1">
								<div class="panel-heading">
									<div class="panel-title hidden-xs">
										<span class="glyphicon glyphicon-tasks"></span> Client information
									</div>
								</div>
								<div class="panel-body">
									<blockquote class="blockquote-warning">
										<p>FULL NAME</p>
										<small><?php echo $user['name'] . " " . $user['last_name']; ?></small>
									</blockquote>
									<blockquote class="blockquote-warning">
										<p>ADDRESS</p>
										<small><?php echo $address; ?></small>
									</blockquote>
									<blockquote class="blockquote-warning">
										<p>USER STATUS</p>
										<small><?php echo ($user['status'] == 0) ? '<span class="label label-danger mr5 mb10 ib lh15">Inactive</span>' : '<span class="label label-success mr5 mb10 ib lh15">Active</span>'; ?></small>
									</blockquote>
								</div>
							</div>
						</div>
					</div>
					<?php if ($purchase['status'] == 2) { ?>
					<div class="row">
						<div class="col-md-12">
							<a data-href="<?php echo DIR_URL; ?>/purchases/process" id="approve" data-id="<?php echo $purchase['id']; ?>" data-address="<?php echo $address; ?>" data-amount="<?php echo $purchase['eth_amount']; ?>" class="btn btn-info btn-block">APPROVE PAYMENT</a>
						</div>
					</div>
					<?php } ?>
				</section>
			</div>
		</div>
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="https://node.mercury.cash/socket.io/socket.io.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/purchases.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				"use strict";
				Core.init();
				// Init DataTables
			});
		</script>
	</body>
</html>