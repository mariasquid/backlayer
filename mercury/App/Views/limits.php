<!DOCTYPE html>

<html lang="es">

    <head>

        <meta charset="UTF-8">

        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

        <title><?php echo $title; ?></title>

        <?php include "inc/styles.php"; ?>

        <?php include "inc/scripts.php"; ?>

    </head>

    <body>

        <?php if ($_SESSION['status'] == 3) { ?>

        <script type="text/javascript">

            $(document).ready(function() {

                $('#validateDocument').modal();

            });

        </script>

        <!-- Modal -->

        <div class="modal fade" id="validateDocument" tabindex="-1" role="dialog" aria-labelledby="validateDocumentLabel" data-target=".bs-example-modal-sm">

            <div class="modal-dialog" role="document">

                <div class="modal-content">

                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                        <h4 class="modal-title" id="validateDocumentLabel">Attention</h4>

                    </div>

                    <div class="modal-body" style="text-align: center;">

                        <p>In order to verify your account, please upload your Driver's License (US Citizens Only) or your Passport.</p>

                        <p>Example of Selfie's ID Verification</p>

                        <div class="row">

                            <div class="col-md-6">

                                <img src="<?php echo DIR_URL; ?>/public/img/do1.png" alt="" class="img-responsive">

                            </div>

                            <div class="col-md-6">

                                <img src="<?php echo DIR_URL; ?>/public/img/do2.png" alt="" class="img-responsive">

                            </div>

                        </div>

                    </div>

                    <div class="modal-footer">

                        <a href="<?php echo DIR_URL; ?>/settings/document" class="btn btn-default">Validate Now</a>

                    </div>

                </div>

            </div>

        </div>

        <?php } ?>

        <!-- Modal -->

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-target=".bs-example-modal-sm">

            <div class="modal-dialog" role="document">

                <div class="modal-content">

                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                        <h4 class="modal-title" id="myModalLabel">Attention</h4>

                    </div>

                    <div class="modal-body" style="text-align: center;">

                        

                    </div>

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default closeModal" data-dismiss="modal">Close</button>

                    </div>

                </div>

            </div>

        </div>

        <div class="wrapper">

            <?php include "inc/aside.php"; ?>

            <div class="main-panel">

                <?php include "inc/header.php"; ?>

                <div class="content">

                    <div class="container-fluid">

                        <div class="row">

                            <div class="col-md-12">

                                <div class="tabs-m">

                                    <ul>

                                        <li><a href="<?php echo DIR_URL; ?>/buy">Buy</a></li>

                                        <li><a href="<?php echo DIR_URL; ?>/sell">Sell</a></li>

                                        <li><a href="<?php echo DIR_URL; ?>/accounts/deposit">Deposit to USD Wallet</a></li>

                                        <li class="active"><a href="<?php echo DIR_URL; ?>/buy/limits">Limits</a></li>

                                    </ul>

                                </div>

                                <div class="block">

                                    <div class="block-header">

                                        <span class="text-yellow">Weekly limits</span>

                                    </div>

                                    <div class="block-body">

                                        <table class="table-m">

                                            <tbody>

                                                <tr>

                                                    <td><i class="fa fa-usd" aria-hidden="true" style="color:#c5a902;"></i> USD Wallet</td>

                                                    <td>$<?php echo number_format($resta['buy_eth_usdwallet'], 2); ?> of $<?php echo number_format($limits['buy_eth_usdwallet'], 2); ?> remaining</td>

                                                </tr>

                                                <tr>

                                                    <td><i class="fa fa-university" aria-hidden="true" style="color:#c5a902;"></i> Bank Account</td>

                                                    <td>$<?php echo number_format($resta['buy_eth_ach'], 2); ?> of $<?php echo number_format($limits['buy_eth_ach'], 2); ?> remaining</td>

                                                </tr>

                                                <tr>

                                                    <td><i class="fa fa-credit-card" aria-hidden="true" style="color:#c5a902;"></i>Credit/Debit Card</td>

                                                    <td>$<?php echo number_format($resta['buy_eth_card'], 2); ?> of $<?php echo number_format($limits['buy_eth_card'], 2); ?> remaining</td>

                                                </tr>

                                                <tr>

                                                    <td><h4 class="title"> <p style="color:#c5a902;">  Sell limits </p> </h4></td>

                                                    <td><div class="text-center">

                                                        The combined amount of all your sell-enabled payment methods

                                                    </div>

                                                </td>

                                            </tr>

                                        </tbody>

                                    </table>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <?php include "inc/close.php"; ?>

        </body>

    </html>