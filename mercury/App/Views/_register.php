<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title>Document</title>
        <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/assets/skin/default_skin/css/theme.css">
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/public/assets/admin-tools/admin-forms/css/admin-forms.css">
        <link rel="shortcut icon" href="<?php echo DIR_URL; ?>/public/assets/img/favicon.ico">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="external-page external-alt sb-l-c sb-r-c">
        <div id="main" class="animated fadeIn">
            <section id="content_wrapper">
                <section id="content">
                    <div class="admin-form theme-primary mw600" style="margin-top: 3%;" id="register">
                        <div class="row table-layout">
                            <a href="dashboard.html" title="Return to Dashboard">
                                <img src="<?php echo DIR_URL; ?>/public/assets/img/logos/logo.png" title="AdminDesigns Logo" class="center-block img-responsive" style="max-width: 275px;">
                            </a>
                        </div>
                        <div class="panel mt40">
                            <form method="POST" action="<?php echo DIR_URL; ?>/register/create" id="register-form" autocomplete="off">
                                <div class="panel-body bg-light p25 pb15">
                                    <div class="section row">
                                        <div class="col-md-6">
                                            <label for="name" class="field prepend-icon">
                                                <input type="text" name="name" id="name" class="gui-input" placeholder="Nombre...">
                                                <label for="name" class="field-icon">
                                                    <i class="fa fa-user"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="last_name" class="field prepend-icon">
                                                <input type="text" name="last_name" id="last_name" class="gui-input" placeholder="Apellido...">
                                                <label for="last_name" class="field-icon">
                                                    <i class="fa fa-user"></i>
                                                </label>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="section">
                                        <label for="email" class="field prepend-icon">
                                            <input type="email" name="email" id="email" class="gui-input" placeholder="Correo Electrónico...">
                                            <label for="email" class="field-icon">
                                                <i class="fa fa-envelope"></i>
                                            </label>
                                        </label>
                                    </div>
                                    <hr class="alt short">
                                    <div class="section">
                                        <label for="password" class="field prepend-icon">
                                            <input type="password" name="password" id="password" class="gui-input" placeholder="Contraseña...">
                                            <label for="password" class="field-icon">
                                                <i class="fa fa-unlock-alt"></i>
                                            </label>
                                        </label>
                                    </div>
                                    <div class="section">
                                        <label for="confirmPassword" class="field prepend-icon">
                                            <input type="password" name="confirmPassword" id="confirmPassword" class="gui-input" placeholder="Repita la contraseña...">
                                            <label for="confirmPassword" class="field-icon">
                                                <i class="fa fa-lock"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>
                                <div class="panel-footer clearfix">
                                    <button type="submit" class="button btn-primary mr10 pull-right">REGISTRARME</button>
                                    <!--
                                    <label class="option block mt10">
                                        <input type="checkbox" name="terms">
                                        <span class="checkbox"></span>Acepto los
                                        <a href="#" class="theme-link"> terminos y condiciones. </a>
                                    </label>
                                    -->
                                </div>
                            </form>
                        </div>
                        <hr class="alt mt40 mb30 mh70">
                        <div class="login-links">
                            <p>Already registered?
                                <a href="pages_login(alt).html" class="active" title="Sign In">Sign in here</a>
                            </p>
                        </div>
                    </div>
                </section>
            </section>
        </div>
        <script type="text/javascript" src="http://167.114.81.219:8080/socket.io/socket.io.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/dir_url.js"></script>
        <script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/register.js"></script>
        <script type="text/javascript">
        </script>
    </body>
</html>