<!DOCTYPE html>
<html lang="es">
	<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
		
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
	</head>
	<body style="background-color: #f5f5f5;">
		<div class="wrapper">
			<div class="main">
				<div class="content">
					<div class="container">
						<div class="row">
							<div class="col-sm-2 col-sm-offset-5">
								<img src="<?php echo DIR_URL; ?>/public/img/logo-dark.png" alt="" class="img-responsive" style="margin-top: 100px;">
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 col-md-offset-4">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Reset</span> your password
									</div>
									<div class="block-body">
										<?php if (!empty($_SESSION['alert'])) { ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="alertGrey">
                                                    <?php echo $_SESSION['alert']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php unset($_SESSION['alert']); ?>
                                        <?php } ?>
										<form method="POST" action="<?php echo DIR_URL; ?>/lostpassword/newPass" autocomplete="off">
											<div class="row">
												<div class="col-md-12">
													<div class="form-elements">
														<label>Email</label>
														<input type="email" id="email" name="email" value="<?php echo $email; ?>">
													</div>
													<div class="form-elements">
														<label>Password</label>
														<input type="password" id="password" name="password" placeholder="Password...">
													</div>
													<div class="form-elements">
														<label>Repeat Password</label>
														<input type="password" id="password_rp" name="password_rp" placeholder="Repeat Password...">
													</div>
												</div>
											</div>
											<div id="captcha">
												<div class="row">
													<div class="col-md-12" style="position: relative; height: 90px;">
														<div class="g-recaptcha" data-sitekey="6LftmSQUAAAAAG97w4Owc0iku3REEOYEmhXXczIl" style="width: 80%;margin: auto;position: absolute;left: 0;right: 0;"></div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-8 col-md-offset-4">
													<input type="hidden" id="token" name="token" value="<?php echo $token; ?>">
													<button type="submit" id="enter" class="btn-mercury-yellow">Reset</button>
												</div>
											</div>
											<div class="clearfix"></div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src='https://www.google.com/recaptcha/api.js'></script>
	</body>
</html>