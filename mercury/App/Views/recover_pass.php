<!DOCTYPE html>
<html lang="es">
	<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
		
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
		<style type="text/css">
		.box-alert { text-align: center; background-color: #fff; padding: 20px 10px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.4); }
		.box-alert h1 { color: #f6c401; }
		.box-alert h5 { font-size: 14px; color: #ccc; }
		.text-yellow { color: #f6c401; font-weight: bold; }
		</style>
	</head>
	<body style="background-color: #f5f5f5;">
		<div class="wrapper">
			<div class="main">
				<div class="content">
					<div class="container">
						<div class="row">
							<div class="col-sm-2 col-sm-offset-5">
								<img src="<?php echo DIR_URL; ?>/public/img/logo-dark.png" alt="" class="img-responsive" style="margin-top: 100px;">
							</div>
							<div class="col-md-6 col-md-offset-3" style="margin-top: 30px;">
								<div class="box-alert">
									<h1><?php echo ($status == 1) ? "Success!" : "Error!"; ?></h1>
									<?php echo $description; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>