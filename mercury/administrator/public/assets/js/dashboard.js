$(document).ready(function() {

    $.get(dir_url + '/dashboard/transactionsDay', function(data) {
        $('#transactionsTotal').text(data);
    });

    $('#lastDay').click(function(event) {
        event.preventDefault();
        $.get(dir_url + '/dashboard/transactionsDay', function(data) {
            $('#transactionsTotal').text(data);
        });
    });

    $('#lastWeek').click(function(event) {
        event.preventDefault();
        $.get(dir_url + '/dashboard/transactionsWeek', function(data) {
            $('#transactionsTotal').text(data);
        });
    });

    $('#lastMonth').click(function(event) {
        event.preventDefault();
        $.get(dir_url + '/dashboard/transactionsMonth', function(data) {
            $('#transactionsTotal').text(data);
        });
    });

    $('#lastYear').click(function(event) {
        event.preventDefault();
        $.get(dir_url + '/dashboard/transactionsYear', function(data) {
            $('#transactionsTotal').text(data);
        });
    });

    setInterval(function() {
        $.getJSON(dir_url + '/dashboard/price', function(json, textStatus) {
            $('#price_eth').text(json.price);
        });
    }, 3000);

});