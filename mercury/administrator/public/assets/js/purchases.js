var socket = io.connect("https://node.mercury.cash");

var sender;
var receiver;
var amount;
var privateKey;
var id;

$('#approve').click(function(event) {
    event.preventDefault();
    var url = $(this).data('href');
    id = $(this).data('id');
    receiver = $(this).data('address');
    amount = $(this).data('amount');

    $('#alert').html('<div class="alert alert-primary alert-dismissable"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="fa fa-trophy pr10"></i> <strong>Processing...</strong> Please wait while we process the transfer.</div>');

    $.getJSON(url, function(json, textStatus) {
        sender = json.address;
        privateKey = json.private_key;

        socket.emit('transaction', {
            sender: sender,
            receiver: receiver,
            amount: amount,
            privateKey: privateKey
        });
    });
});

socket.on('transaction-response', function(data) {

	console.log("ID: " + id);

    if (data.status == 1) {

        $.post(dir_url + '/Purchases/changeStatus', {
            id: id
        }, function(data, textStatus, xhr) {
            console.log(data);
            console.log(textStatus);
            console.log(xhr);
        });

        $('#alert').html('<div class="alert alert-success alert-dismissable"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="fa fa-trophy pr10"></i> <strong>Success!</strong> The transfer has performed successfully.</div>');

    } else {

        $('#alert').html('<div class="alert alert-success alert-dismissable"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="fa fa-trophy pr10"></i> <strong>Error!</strong> ' + data.message + '.</div>');

    }

});