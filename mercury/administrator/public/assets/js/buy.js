$(document).ready(function() {

    $('#usd_amount').numeric('.');
    $('#eth_amount').numeric('.');

    $("#expiration").mask("99/9999", {
        placeholder: "MM/YYYY"
    });

    var limit_init = 0;
    var limit_rest = 0;
    var typeMethod = 0;
    var least = '1.00';

    $('#linkPlaid').hide();
    $('#linkCards').hide();
    $('#linkPlaid').click(function(event) {
        event.preventDefault();
        var linkHandler = Plaid.create({
            env: 'sandbox',
            clientName: $('#client_name').val(),
            key: 'c13f45250f5c110e315a300814d3e7',
            product: ['auth'],
            selectAccount: true,
            onSuccess: function(public_token, metadata) {

                console.log(public_token, metadata);

                $.ajax({
                    url: dir_url + "/buy/authBank",
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        public_token: public_token,
                        account_id: metadata.account_id
                    },
                    beforeSend: function() {
                        $.notify({
                            icon: 'pe-7s-info',
                            message: "We are processing your request"
                        }, {
                            type: 'info',
                            timer: 2500
                        });
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.status == 1) {
                            $('#myModal').modal();
                            $('#myModal .modal-body').html(data.description);
                            $('#myModal').on('hidden.bs.modal', function(e) {
                                location.reload(true);
                            });
                        } else {
                            $.notify({
                                icon: 'pe-7s-info',
                                message: data.description
                            }, {
                                type: 'danger',
                                timer: 2500
                            });
                        };
                    },
                    timeout: 30000,
                    error: function(err) {
                        console.log(err);
                    }
                });
            },
            onExit: function(err, metadata) {
                // The user exited the Link flow.
                if (err != null) {
                    // The user encountered a Plaid API error prior to exiting.
                }
            },
        });

        linkHandler.open();
    });

    $('#progress-bar').hide();
    var price = parseFloat($('#valueEthinUsd').text());

    $('.payment_method').click(function(event) {
        $('.payment_method').removeClass('pay-active');
        $('input').removeAttr('disabled');
        $('#alert_bank').html('');
        $('#content_bank_accounts').hide();
        $('#content_card_accounts').hide();
        $('#linkPlaid').hide();
        $('#progress-bar').hide();
        $('#button-pay-usd-wallet').hide();
        $('#button-script').hide();
        $('#linkCards').hide();
        $('#addCard').hide();
        least = '1.00';
        $('#least').text(least);
        $('#usd_max_buy').show();
        $('#usd_amount').val('');
        $('#eth_amount').val('');

        check = $(this).data('method');

        $(check).prop('checked', true);
        $(this).addClass('pay-active');
        $('.icon-check').removeClass('icon-active');
        $(this).children('.icon-check').addClass('icon-active');

        if (check == "#eth_wallet_check") {
            $('#fee').text('$0.00');
            $('#button-pay-usd-wallet').show();
            $('#progress-bar').show();

        } else if (check == "#usd_wallet_check") {

            typeMethod = '1';

            $('#type_method').text('USD Wallet');

            calculations(0, typeMethod, '2');

            $('.buttons-buy').hide();
            $('#button-pay-usd-wallet').show();
            $('#progress-bar').show();

        } else if (check == "#card_check") {

            $('#type_method').text('Credit/Debit Card');

            typeMethod = '2';

            calculations(0, typeMethod, '2');

            $('.buttons-buy').hide();
            $('#button-script').show();
            $('#linkCards').show();

            $('.stripe-button-el span').text('add new card and pay');

            $('#button-pay-usd-wallet').show();
            $('#progress-bar').show();
            $('#content_card_accounts').show();

        } else if (check == "#bank_check") {

            typeMethod = '3';

            $('#type_method').text('ACH');

            calculations(0, typeMethod, '2');

            $('#button-pay-usd-wallet').show();
            $('#progress-bar').show();
            $('#content_bank_accounts').show();
            $('#linkPlaid').show();

        }

    });

    var percentage = 0;
    var value = 0;

    $('#eth_amount').keyup(function(event) {

        calculations($(this).val(), typeMethod, '1');

    });

    $('#usd_amount').keyup(function(event) {

        calculations($(this).val(), typeMethod, '2');

    });

    $('#delete_card').click(function(event) {
        event.preventDefault();

        $.ajax({
            url: dir_url + '/buy/removeCard',
            type: 'POST',
            dataType: 'json',
            data: {
                card: $('#cards').val()
            },
            beforeSend: function() {
                $('.loading').show();
            },
            success: function(data) {
                $('.loading').hide();
                $('#myModal .modal-body').html(data.description);
                $('#myModal').modal();
                $('#myModal').on('hidden.bs.modal', function(e) {
                    location.reload(true);
                });
            },
            timeout: 30000,
            error: function(err) {
                //console.log(err);
            }
        });

    });


    $('#delete_ach').click(function(event) {
        event.preventDefault();

        $.ajax({
            url: dir_url + '/buy/removeAch',
            type: 'POST',
            dataType: 'json',
            data: {
                ach: $('#bank_accounts').val()
            },
            beforeSend: function() {
                $('.loading').show();
            },
            success: function(data) {
                $('.loading').hide();
                $('#myModal .modal-body').html(data.description);
                $('#myModal').modal();
                $('#myModal').on('hidden.bs.modal', function(e) {
                    location.reload(true);
                });
            },
            timeout: 30000,
            error: function(err) {
                //console.log(err);
            }
        });

    });

    function calculations(amount, method, currency) {

        $.getJSON(dir_url + '/buy/calculations/' + amount + '/' + method + '/' + currency, function(json, textStatus) {

            $('#subtotal').text('$' + json.subtotal);
            $('#fee').text('$' + json.fee);
            $('#total').text('$' + json.total);

            if (currency == '1') {
                $('#usd_amount').val(json.subtotal);
            } else if (currency == '2') {
                $('#eth_amount').val(json.ethers);
            }

            if (json.modal) {
                $('#myModal .modal-body').html(json.description);
                $('#myModal').modal();
            }

            $('#limit-progress > div[role="progressbar"]').attr('aria-valuenow', (100 - json.percentage));
            $('#limit-progress > div[role="progressbar"]').css('width', (100 - json.percentage) + '%');
            $('#limit-progress > div[role="progressbar"]').text('$' + (json.limit - json.total).toFixed(2));

            if (parseFloat(json.balance) < parseFloat(json.total)) {
                $('#usd_amount').val('');
                $('.progress-bar-success').css('background-color', '#c5a902');
            } else if (parseFloat(json.balance) > parseFloat(json.total)) {
                $('.progress-bar-success').css('background-color', '#5cb85c');
            }

        });

    }

    $('#usd_max_buy').click(function(event) {

        event.preventDefault();

        $.getJSON(dir_url + '/buy/calc_max/' + typeMethod, function(json, textStatus) {

            $('#subtotal').text('$' + json.subtotal);
            $('#usd_amount').val(json.subtotal);
            $('#eth_amount').val(json.ethers);
            $('#fee').text('$' + json.fee);
            $('#total').text('$' + json.total);
            //calc_usd_amount();

            $('#limit-progress > div[role="progressbar"]').attr('aria-valuenow', 0);
            $('#limit-progress > div[role="progressbar"]').css('width', 0 + '%');
            $('#limit-progress > div[role="progressbar"]').text('$0.00');

            if (parseFloat(json.total) < 0) {
                $('.progress-bar-success').css('background-color', '#c5a902');
            } else if (parseFloat(json.total) > 0) {
                $('.progress-bar-success').css('background-color', '#5cb85c');
            }

        });

    });


    $('#button-pay-usd-wallet').click(function(event) {
        event.preventDefault();

        if (check == "#eth_wallet_check") {

            if ($('#usd_amount').val() < 1) {

                $.notify({
                    icon: 'pe-7s-close-circle',
                    message: 'To buy ethereum, the minimum amount is $1'

                }, {
                    type: 'danger',
                    timer: 2500
                });

            } else {

                amount = ethers;

                $.getJSON(dir_url + "/sell/process", function(json, textStatus) {

                    $.notify({
                        icon: 'pe-7s-close-circle',
                        message: 'We are processing the transfer'

                    }, {
                        type: 'info',
                        timer: 2500
                    });
                });

            }

        } else if (check == "#usd_wallet_check") {

            if ($('#usd_amount').val() < 1) {

                $.notify({
                    icon: 'pe-7s-close-circle',
                    message: 'To buy ethereum, the minimum amount is $1'

                }, {
                    type: 'danger',
                    timer: 2500
                });

            } else {

                var usd_amount = $('#usd_amount').val();
                usd_amount = parseFloat(usd_amount);

                if (usd_amount > 0) {
                    submitBuy();
                } else {
                    $.notify({
                        icon: 'pe-7s-info',
                        message: 'You do not have enough funds in your USD Wallet to make this purchase, try a smaller amount'
                    }, {
                        type: 'danger',
                        timer: 4000
                    });
                }

            }

        } else {

            submitBuy();

        }

    });

    $('#linkCards').click(function(event) {
        event.preventDefault();
        $('#addCard').show();
    });


    var submitBuy = function() {

        var values = $('#send').serialize();
        var url = $('#send').attr('action');

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: values,
            beforeSend: function() {

                $('#button-pay-usd-wallet').hide();
                $.notify({
                    icon: 'pe-7s-info',
                    message: "We are processing your purchase"
                }, {
                    type: 'info',
                    timer: 2500
                });

            },
            success: function(data) {
                if (data.status == 1) {
                    $('input').val('0');
                    $('#ethers-pay').text('0.000');
                    $('#subtotal').text('$0.00');
                    $('#total').text('$0.00');
                    $('#fee').text('$0.00');
                    $('.payment_method').removeClass('pay-active');
                    $('#myModal').modal();
                    $('#myModal .modal-body').html(data.description);
                    $('#button-pay-usd-wallet').show();
                    $('#myModal').on('hidden.bs.modal', function(e) {
                        location.href = dir_url + "/dashboard";
                    });

                } else {

                    $.notify({
                        icon: 'pe-7s-info',
                        message: data.description
                    }, {
                        type: 'danger',
                        timer: 2500

                    });
                };
            },
            timeout: 30000,
            error: function(err) {
                //console.log(err);
            }
        });

    }

    $('#add_card').click(function(event) {
        event.preventDefault();

        var expiration = $('#expiration').val();
        var month = expiration.substr(0, 2);
        var year = expiration.substr(3, 4);

        var date = new Date();
        var nowMonth = date.getMonth();
        var nowYear = date.getFullYear();

        if (month > 12) {
            $('#expiration').focus();
            $.notify({
                icon: 'pe-7s-info',
                message: "You must select a number between 01 and 12 for the expiration date of your card"
            }, {
                type: 'danger',
                timer: 2500
            });
        } else if (month < nowMonth && year < nowYear) {
            $('#expiration').focus();
            $.notify({
                icon: 'pe-7s-info',
                message: "The expiration date of the card can not be less than the current date"
            }, {
                type: 'danger',
                timer: 2500
            });
        } else {
            Stripe.setPublishableKey('pk_test_mN1rB9hfrx4YpeZy5IV8ji9x');
            Stripe.card.createToken({
                number: $('#number_card').val(),
                cvc: parseInt($('#cvc').val()),
                exp_month: parseInt(month),
                exp_year: parseInt(year)
            }, stripeResponseHandler);
        }


    });


    function stripeResponseHandler(status, response) {

        if (response.error) {
            $.notify({
                icon: 'pe-7s-info',
                message: response.error.message
            }, {
                type: 'danger',
                timer: 2500
            });
        } else {
            var token = response.id;
            $.ajax({
                url: dir_url + "/buy/addCard",
                type: 'POST',
                dataType: 'json',
                data: {
                    token: token
                },
                beforeSend: function() {
                    $('.loading').show();
                },
                success: function(data) {
                    if (data.status == 1) {
                        $('.loading').hide();
                        $('#myModal').modal();
                        $('#myModal .modal-body').html(data.description);
                        $('#myModal').on('hidden.bs.modal', function(e) {
                            location.reload(true);
                        });
                    } else {
                        $('.loading').show();
                        $.notify({
                            icon: 'pe-7s-info',
                            message: data.description
                        }, {
                            type: 'danger',
                            timer: 2500
                        });
                    };
                },
                timeout: 30000,
                error: function(err) {
                    //console.log(err);
                }
            });
        }
    }

});