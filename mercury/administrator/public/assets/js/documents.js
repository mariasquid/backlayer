$(document).ready(function() {

    $('.btn-mercury-yellow, .btn-mercury-black').click(function(event) {
        $('.loading').show();
    });

    $('#documentImage').elevateZoom({
        zoomType: "lens",
        lensShape: "round",
        cursor: "crosshair",
        zoomWindowFadeIn: 500,
        zoomWindowFadeOut: 750,
        zoomWindowWidth: 200,
        zoomWindowHeight: 200
    });

});