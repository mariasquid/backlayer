$(document).ready(function() {

    $('#usd_amount').numeric('.');
    $('#eth_amount').numeric('.');

    var price = $('#valueEthinUsd').text();

    $('#eth_amount').keyup(function(event) {
        event.preventDefault();
        $('#usd_amount').val(($(this).val() * price).toFixed(2));
    });

    $('#usd_amount').keyup(function(event) {
        event.preventDefault();
        $('#eth_amount').val(($(this).val() / price).toFixed(8));
    });


    $('#request').submit(function(event) {
        event.preventDefault();

        var values = $('#request').serialize();
        var url = $(this).attr('action');
        var type;

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: values,
            beforeSend: function() {
                $.notify({
                    icon: 'pe-7s-info',
                    message: 'We are processing your data'

                }, {
                    type: 'info',
                    timer: 2500
                });
            },
            success: function(data) {
                console.log(data);
                if (data.status == 1) {
                    type = 'success';
                } else {
                    type = 'danger';
                };

                $.notify({
                    icon: 'pe-7s-info',
                    message: data.description

                }, {
                    type: type,
                    timer: 2500
                });
            },
            timeout: 30000,
            error: function(err) {
                console.log(err);
            }
        });
    });


    var sender;
    var receiver;
    var eth_amount;
    var usd_amount;
    var privateKey;
    var save = "false";
    var description = "";

    var id = null;

    $('.approve').click(function(event) {

        eth_amount = $(this).data('amount');
        receiver = $(this).data('address');

        $.ajax({
            url: dir_url + '/send/createTransaction',
            type: 'POST',
            dataType: 'json',
            data: {
                eth_amount: eth_amount,
                receiver: receiver
            },
            beforeSend: function() {
                $('.loading').show();
            },
            success: function(data) {
                console.log(data);
                $('.loading').hide();
                if (data.status == 1) {
                    $('#myModal').modal();
                    $('#myModal .modal-body').html(data.description);
                } else {
                    $.notify({
                        icon: 'pe-7s-close-circle',
                        message: data.description

                    }, {
                        type: 'danger',
                        timer: 2500
                    });
                }
            },
            timeout: 30000,
            error: function(err) {
                console.log(err);
            }
        });

    });


    $('.reject').click(function(event) {
    	id = $(this).data('id');
    	$.get(dir_url + '/request/updateStatus/0/' + id, function(data) {
    		console.log(data);
    		location.reload(true);
    	});
    });

});