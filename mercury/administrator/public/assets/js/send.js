$(document).ready(function() {

    $('#usd_amount').numeric('.');
    $('#eth_amount').numeric('.');

    $('#select_address').change(function(event) {
        var value = $(this).val();
        if (value == "new") {
            $('#save_address').modal();
        }
    });


    $('#saveAddress').submit(function(event) {
        event.preventDefault();
        var data = $(this).serialize();
        var url = $(this).attr('action');

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            beforeSend: function() {
                $('.loading').show();
            },
            success: function(data) {
                console.log(data);
                if (data.status == 1) {
                    $('.loading').hide();
                    $('#save_address').modal('hide');
                    $('#myModal').modal();
                    $('#myModal .modal-body').html(data.description);
                    $('#myModal').on('hidden.bs.modal', function(e) {
                        location.reload(true);
                    });
                } else {
                    $('.loading').hide();
                    $('#save_address').modal('hide');
                    $.notify({
                        icon: 'pe-7s-close-circle',
                        message: data.description

                    }, {
                        type: 'danger',
                        timer: 2500
                    });
                }
            },
            timeout: 30000,
            error: function(err) {
                console.log(err);
            }
        });

    });


    $('#eth_amount').keyup(function(event) {
        calc_amount($(this).val(), 1);
    });

    $('#usd_amount').keyup(function(event) {
        calc_amount($(this).val(), 2);
    });

    function calc_amount(amount, currency) {

        $.getJSON(dir_url + '/send/calc_amount/' + amount + '/' + currency, function(json, textStatus) {
            console.log(dir_url + '/send/calc_amount/' + amount + '/' + currency);
            if (currency == '1') {
                $('#usd_amount').val(json.usd_amount);
            } else if (currency == '2') {
                $('#eth_amount').val(json.eth_amount);
            }
            $('#total').val('$' + json.usd_total);

        });

    }


    function deleteAddress(url) {

        $('.loading').show();
        $.getJSON(url, function(json, textStatus) {
            if (json.status == 1) {
                $('.loading').hide();
                $('#myModal').modal();
                $('#myModal .modal-body').html(json.description);
                $('#myModal').on('hidden.bs.modal', function(e) {
                    location.reload(true);
                });
            } else {
                $('.loading').hide();
                $('#myModal').modal('hide');
                $.notify({
                    icon: 'pe-7s-close-circle',
                    message: data.description
                }, {
                    type: 'danger',
                    timer: 2500
                });
            }
        });

    }

    var url;
    var description;
    var address;
    $('.deleteAddress').click(function(event) {
        event.preventDefault();
        url = $(this).attr('href');
        description = $(this).data('description');
        address = $(this).data('address');

        $('#removeAddress').modal();
        $('#removeAddress .modal-body').html("Do you want to delete the address: " + address + " with the description: " + description + "?");
    });

    $('#removeAddressButton').click(function(event) {
        deleteAddress(url);
    });


    $('#transfer').click(function(event) {

        $.ajax({
            url: dir_url + '/send/createTransaction',
            type: 'POST',
            dataType: 'json',
            data: {
                eth_amount: $('#eth_amount').val(),
                receiver: $('#select_address').val()
            },
            beforeSend: function() {
                $('.loading').show();
            },
            success: function(data) {
                console.log(data);
                $('.loading').hide();
                if (data.status == 1) {
                    $('#myModal').modal();
                    $('#myModal .modal-body').html(data.description);
                } else {
                    $.notify({
                        icon: 'pe-7s-close-circle',
                        message: data.description

                    }, {
                        type: 'danger',
                        timer: 2500
                    });
                }
            },
            timeout: 30000,
            error: function(err) {
                console.log(err);
            }
        });

    });

});