$(document).ready(function() {

    earningsCalc($('#buy').val(), 1);
    earningsCalc($('#sell').val(), 2);

    $('#buy').keyup(function(event) {
        earningsCalc($(this).val(), 1);
    });

    $('#sell').keyup(function(event) {
        earningsCalc($(this).val(), 2);
    });

    function earningsCalc(percentage, method) {
        $.getJSON(dir_url + '/settings/earningsCalc/' + percentage + '/' + method, function(json, textStatus) {
            if (method == 1) {
                $('#price_buy').val('$' + json.total + ' USD');
            } else if (method == 2) {
                $('#price_sell').val('$' + json.total + ' USD');
            }
        });

    }

});