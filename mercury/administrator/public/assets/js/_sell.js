$(document).ready(function() {

    var fee = 0.02;
    var ethers = 0;
    var dollars = 0;

    var price = Number($('#valueEthinUsd').text());

    $('#eth_amount').keyup(function(event) {
        ethers = $(this).val();
        dollars = ethers * price;
        $('#usd_amount').val(dollars.toFixed(2));

        $('#ethers-pay').text($(this).val());
        $('#subtotal').text('$' + dollars);
        $('#total').text('$' + (dollars + fee).toFixed(2));
    });

    $('#usd_amount').keyup(function(event) {
        dollars = Number($(this).val());
        ethers = ($(this).val() / price);
        $('#eth_amount').val(ethers);

        $('#ethers-pay').text($('#eth_amount').val());
        $('#subtotal').text('$' + $(this).val());
        $('#total').text('$' + (dollars + fee).toFixed(2));
    });


    var balanceUsd = $('#balanceUsd').text();
    balanceUsd = Number(balanceUsd);


    var socket = io.connect("https://node.mercury.cash");

    var sender, receiver, amount, privateKey, eth_amount;
    var save = "false";
    var description = "";
    var usd_amount = dollars;

    $('#sell').submit(function(event) {

        event.preventDefault();

        var url = $(this).attr('action');

        sender = $('#sell_from').val();

        $.getJSON(url, function(json, textStatus) {
            console.log(json);
            console.log(textStatus);

            receiver = json.receiver;
            privateKey = json.privateKey;
            amount = eth_amount;

            socket.emit('transaction', {
                sender: sender,
                receiver: receiver,
                amount: amount,
                privateKey: privateKey,
                save: save
            });

            $.notify({
                icon: 'pe-7s-close-circle',
                message: 'We are processing the transfer'

            }, {
                type: 'info',
                timer: 2500
            });
        });

    });

    socket.on('transaction-response', function(data) {

        console.log(data);

        if (data.status == 1) {
            icon = 'pe-7s-check';
            type = 'success';

            console.log($('#usd_amount').val(), $('#eth_amount').val(), $('#deposit_to').val());

            $.post(dir_url + '/sell/createTransaction', {
                usd_amount: $('#usd_amount').val(),
                eth_amount: $('#eth_amount').val(),
                deposit_to: $('#deposit_to').val()
            }, function(data1, textStatus, xhr) {
                console.log(data1);
                console.log(textStatus);
                console.log(xhr);
            });

        } else {
            icon = 'pe-7s-close-circle';
            type = 'danger';
        }

        $.notify({
            icon: icon,
            message: data.message

        }, {
            type: type,
            timer: 2500
        });
    });

    /*
        var values = $('#send').serialize();
        var url = $('#send').attr('action');

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: values,
            beforeSend: function() {
                console.log("Cargando...");
            },
            success: function(data) {
                console.log(data);

                
            },
            timeout: 30000,
            error: function(err) {
                console.log(err);
            }
        });
        */

});