jQuery(document).ready(function() {

    //#footerTable thead th

    // Setup - add a text input to each footer cell
    $('#datatable thead th').each(function() {
        var title = $(this).text();
        $(this).html('<input type="text" placeholder="' + title + '" />');
    });

    $('#footerTable thead th').each(function() {
        var title = $(this).text();
        $(this).html('<input type="text" placeholder="' + title + '" disabled="disabled" />');
    });

    // DataTable
    $('#footerTable').DataTable({
        "ordering": false,
        "paging": false,
        "searching": false
    });
    var table = $('#datatable').DataTable({
        "ordering": false,
        "iDisplayLength": 100,
        "aLengthMenu": [
            [5, 10, 25, 50, 100, -1],
            [5, 10, 25, 50, 100, "All"]
        ],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    totalizar();

    $('.block-body').on('keyup', 'input[type="search"]', function(event) {
        totalizar();
    });

    // Apply the search
    table.columns().every(function() {
        var that = this;

        $('input', this.header()).on('keyup change', function() {
            totalizar();
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    function totalizar() {
        var totaleth = 0;
        var totalusd = 0;
        var totalcommission = 0;
        var totalprice = 0;
        var totalfee = 0;
        var totalgross_income = 0;

        $('.totaleth').each(function(index, el) {
            if ($(el).text() != "") {
                var valor = $(el).text().replace(',', '');
                totaleth = totaleth + parseFloat(valor);
            }
        });
        $('#totaleth').html("<b style='font-size: 1.6rem;'>" + totaleth.toFixed(5) + "</b>");

        $('.totalusd').each(function(index, el) {
            if ($(el).text() != "") {
                var valor = $(el).text().replace(',', '');
                totalusd = totalusd + parseFloat(valor);
            }
        });
        $('#totalusd').html("<b style='font-size: 1.6rem;'>$" + totalusd.toFixed(2) + "</b>");

        $('.totalcommission').each(function(index, el) {
            if ($(el).text() != "") {
                var valor = $(el).text().replace(',', '');
                totalcommission = totalcommission + parseFloat(valor);
            }
        });
        $('#totalcommission').html("<b style='font-size: 1.6rem;'>$" + totalcommission.toFixed(2) + "</b>");

        $('.totalprice').each(function(index, el) {
            if ($(el).text() != "") {
                var valor = $(el).text().replace(',', '');
                totalprice = totalprice + parseFloat(valor);
            }
        });
        $('#totalprice').html("<b style='font-size: 1.6rem;'>$" + totalprice.toFixed(2) + "</b>");

        $('.totalfee').each(function(index, el) {
            if ($(el).text() != "") {
                var valor = $(el).text().replace(',', '');
                totalfee = totalfee + parseFloat(valor);
            }
        });
        $('#totalfee').html("<b style='font-size: 1.6rem;'>$" + totalfee.toFixed(2) + "</b>");

        $('.totalgross_income').each(function(index, el) {
            if ($(el).text() != "") {
                var valor = $(el).text().replace(',', '');
                totalgross_income = totalgross_income + parseFloat(valor);
            }
        });
        $('#totalgross_income').html("<b style='font-size: 1.6rem;'>$" + totalgross_income.toFixed(2) + "</b>");
    }



    /*
    $('#datatable').dataTable({
        "ordering": false,
        "aoColumnDefs": [{
            'bSortable': false,
            'aTargets': [-1]
        }],
        "oLanguage": {
            "oPaginate": {
               "sPrevious": "",
                "sNext": ""
            }
        },
        "iDisplayLength": 100,
        "aLengthMenu": [
            [5, 10, 25, 50, 100, -1],
            [5, 10, 25, 50, 100, "All"]
        ],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    */

    //$('#example2').DataTable({
    //  'paging'      : true,
    //  'lengthChange': false,
    //  'searching'   : false,
    //  'ordering'    : true,
    //  'info'        : true,
    //  'autoWidth'   : false
    //})

});