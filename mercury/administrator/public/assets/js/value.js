$(window).load(function() {
    if (typeof history.pushState === "function") {
        history.pushState("jibberish", null, null);
        window.onpopstate = function () {
            history.pushState('newjibberish', null, null);
            // Handle the back (or forward) buttons here
            // Will NOT handle refresh, use onbeforeunload for this.
        };
    }
    else {
        var ignoreHashChange = true;
        window.onhashchange = function () {
            if (!ignoreHashChange) {
                ignoreHashChange = true;
                window.location.hash = Math.random();
                // Detect and redirect change here
                // Works in older FF and IE9
                // * it does mess with your hash symbol (anchor?) pound sign
                // delimiter on the end of the URL
            }
            else {
                ignoreHashChange = false;   
            }
        };
    }
});


var NS = ( navigator.appName == "Netscape" && parseInt(navigator.appVersion, 10) >= 5 ) ? true : false;
var FF = ( navigator.appName == "Netscape" ) ? true : false;
var IE = ( navigator.appVersion.search(/MSIE/) != -1)? true : false;

/**
 * Función que restringe el menú contextual en Internet Explorer
 */
function clickIE() {
	if (document.all)
		return false;
}

/**
 * Función que restringe el menú contextual en Netscape
 */
function clickNS(e) {
	if (document.layers || (document.getElementById && !document.all)) {
		if (e.which == 2 || e.which == 3) {
			return false;
		}
 	}
}

/**
 * Código para capturar los eventos del mouse
 */
if (document.layers) {
	document.captureEvents(Event.MOUSEDOWN);
	document.onmousedown = clickNS;
} else {
	document.onmouseup = clickNS;
	document.oncontextmenu = clickIE;
}

function whichKey(km) {
	
	if (IE) {
                
		if (window.event && window.event.keyCode >= 112 && window.event.keyCode <= 125)
			window.event.keyCode = 505;
	
		if (window.event && window.event.keyCode == 116)
			window.event.keyCode = 505;
			
	 	if (window.event && window.event.keyCode == 505) {
			window.event.cancelBubble = true;
			window.event.returnValue = false;
			return false;
		}
				
		//ALT, CTRL
		if (window.focusObjeto == "OFF") {
			if (event.altLeft || event.altKey)
				return false;
 		}

 		//CTRL + TECLA
		if ( window.event.ctrlKey ) {
                        switch(window.event.keyCode) {
                            case 67: //C
                            case 70: //F
                            case 80: //P
                            case 86: //V
                                break;
                            default:
                                return false;
                        }
		}

		//ALT + FLECHA (IZQ, DER)
 		if (window.focusObjeto == "OFF" || event.altLeft) {
 			if (window.event && event.altLeft && (window.event.keyCode == 37 || window.event.keyCode == 39))
				return false;
 		}

		//RETROCESO
		if (window.focusObjeto == "OFF") {
			if (window.event && window.event.keyCode == 8) {
				window.event.cancelBubble = true;
				window.event.returnValue = false;
				return false;
			}
		}
	}
}

function make(event) {
	
	if (NS) {
		if (window.focusObjeto == "ON") {
			// Teclas de Retroceso
			if (event.keyCode == 8) {
				var newEvent = document.createEvent("KeyEvents");
				newEvent.initKeyEvent("onkeypress", true, true, document.defaultView, event.ctrlKey, event.altKey, event.shiftKey, event.metaKey, 0, 0);
				event.preventDefault();
				event.target.dispatchEvent(newEvent);
			}
		}
		
		
		// Teclas de Funcion F1 - F12 !F5
		
		if ((event.keyCode > 110 && event.keyCode < 116) || (event.keyCode > 116 && event.keyCode < 124)) {
			var newEvent = document.createEvent("KeyEvents");
			newEvent.initKeyEvent("onkeypress", true, true, document.defaultView, event.ctrlKey, event.altKey, event.shiftKey, event.metaKey, 0, 0);
			event.preventDefault();
			event.target.dispatchEvent(newEvent);
		}
	
		// Teclas de Flecha Adelante o Atras
		if (window.focusObjeto == "ON" || event.altKey) {
			if (event.keyCode == 37 || event.keyCode == 39) {
				var newEvent = document.createEvent("KeyEvents");
				newEvent.initKeyEvent("onkeydown", true, true, document.defaultView, event.ctrlKey, event.altKey, event.shiftKey, event.metaKey, 0, 0);
				event.preventDefault();
				event.target.dispatchEvent(newEvent);
			}
		}
	
		if (window.focusObjeto == "OFF") {
			if (event.ctrlKey) {
				// 100 >>> CTRL - D 110 >>> CTRL - N 111 >>> CTRL - O 112 >>> CTRL - P 114 >>> CTRL - R 116 >>> CTRL - T 117 >>> CTRL - U
                                
				if (event.which >= 100 && event.which <= 120 ){
					var newEvent = document.createEvent("KeyEvents");
					if (window.focusObjeto == "OFF")
						newEvent.initKeyEvent("onkeydown", true, true, document.defaultView, event.ctrlKey, event.altKey, event.shiftKey, event.metaKey, 0, 0);
					else
						newEvent.initKeyEvent("onkeydown", true, true, document.defaultView, event.ctrlKey, event.altKey, event.shiftKey, event.metaKey, event.which, 0);
					event.preventDefault();
					event.target.dispatchEvent(newEvent);
				}
			}
		}
	}
}

document.onkeypress = make;
document.onkeydown = whichKey;
document.oncontextmenu = new Function("return false");
window.focusObjeto = "OFF";

window.focus();
