$(document).ready(function() {

    $('#usd_amount').numeric('.');

    $("#expiration").mask("99/9999", {
        placeholder: "MM/YYYY"
    });

    $('#linkPlaid').hide();
    $('#linkCards').hide();

    var typeMethod = 0;
    var wire_method = 0;

    $('.payment_method').click(function(event) {
        $('.payment_method').removeClass('pay-active');
        $('input').removeAttr('disabled');
        $('#alert_bank').html('');
        $('#content_bank_accounts').hide();
        $('#content_card_accounts').hide();
        $('#linkPlaid').hide();
        $('#progress-bar').hide();
        $('#button-pay-usd-wallet').hide();
        $('#button-script').hide();
        $('#linkCards').hide();
        $('#addCard').hide();
        least = '1.00';
        $('#least').text(least);
        $('#usd_max_buy').show();
        $('#usd_amount').val('');
        $('#select_wire').hide();

        $('#subtotal').text('$0.00');
        $('#fee').text('$0.00');
        $('#total').text('$0.00');

        check = $(this).data('method');
        wire_method = 0;

        $(check).prop('checked', true);
        $(this).addClass('pay-active');
        $('.icon-check').removeClass('icon-active');
        $(this).children('.icon-check').addClass('icon-active');

        if (check == "#eth_wallet_check") {
            $('#fee').text('$0.00');
            $('#button-pay-usd-wallet').show();
            $('#progress-bar').show();

        } else if (check == "#usd_wallet_check") {

            typeMethod = '1';

            $('.buttons-buy').hide();
            $('#button-pay-usd-wallet').show();
            $('#progress-bar').show();

        } else if (check == "#card_check") {

            typeMethod = '2';

            $('.buttons-buy').hide();
            $('#button-script').show();
            $('#linkCards').show();

            $('.stripe-button-el span').text('add new card and pay');

            $('#button-pay-usd-wallet').show();
            $('#progress-bar').show();
            $('#content_card_accounts').show();

        } else if (check == "#bank_check") {

            typeMethod = '3';

            $('#button-pay-usd-wallet').show();
            $('#progress-bar').show();
            $('#content_bank_accounts').show();
            $('#linkPlaid').show();

        } else if (check == "#wire_transfer") {

            typeMethod = '4';

            $('#select_wire').show();
            $('#button-pay-usd-wallet').show();
            wire_method = parseInt($('#wire').val());

        }

    });

    $('.stripe-button-el').click(function(event) {
        if ($('#amount').val() < 1) {
            event.preventDefault();
            $('iframe').hide();
            $.notify({
                icon: 'pe-7s-info',
                message: 'To buy ethereum, the minimum amount is $1'
            }, {
                type: 'danger',
                timer: 2500
            });
        }
    });


    var total = 0;
    var commission = $('#commission').val();
    var discount = 0;
    $('#amount').keyup(function(event) {
        total = $(this).val();
        discount = (total * commission) / 100;
        total = Number(total) + Number(discount);
        $('#total').val(total.toFixed(2));
    });


    $('#delete_card').click(function(event) {
        event.preventDefault();

        $.ajax({
            url: dir_url + '/buy/removeCard',
            type: 'POST',
            dataType: 'json',
            data: {
                card: $('#cards').val()
            },
            beforeSend: function() {
                $('.loading').show();
            },
            success: function(data) {
                $('.loading').hide();
                $('#myModal .modal-body').html(data.description);
                $('#myModal').modal();
                $('#myModal').on('hidden.bs.modal', function(e) {
                    location.reload(true);
                });
            },
            timeout: 30000,
            error: function(err) {
                //console.log(err);
            }
        });

    });

    $('#delete_ach').click(function(event) {
        event.preventDefault();

        $.ajax({
            url: dir_url + '/buy/removeAch',
            type: 'POST',
            dataType: 'json',
            data: {
                ach: $('#bank_accounts').val()
            },
            beforeSend: function() {
                $('.loading').show();
            },
            success: function(data) {
                $('.loading').hide();
                $('#myModal .modal-body').html(data.description);
                $('#myModal').modal();
                $('#myModal').on('hidden.bs.modal', function(e) {
                    location.reload(true);
                });
            },
            timeout: 30000,
            error: function(err) {
                //console.log(err);
            }
        });

    });

    $('#wire').change(function(event) {
        $('#subtotal').text('$0.00');
        $('#fee').text('$0.00');
        $('#total').text('$0.00');
        $('#usd_amount').val('');
        wire_method = parseInt($(this).val());
    });

    $('#usd_amount').keyup(function(event) {
        $.getJSON(dir_url + '/accounts/calculations/' + $(this).val() + '/' + typeMethod + '/' + wire_method, function(json, textStatus) {

            $('#subtotal').text('$' + json.subtotal);
            $('#fee').text('$' + json.fee);
            $('#total').text('$' + json.total);

        });

    });


    $('#deposit_form').submit(function(event) {
        event.preventDefault();
        var data = $(this).serialize();
        url = $(this).attr('action');
        $.ajax({
            url: dir_url + '/accounts/depositProcess',
            type: 'POST',
            dataType: 'json',
            data: data,
            beforeSend: function() {
                console.log("Cargando...");
                $('.loading').show();
            },
            success: function(data) {
                console.log(data);
                if (data.status == 1) {
                    $('.loading').hide();
                    $('#myModal .modal-body').html(data.description);
                    $('#myModal').modal();
                    $('#myModal').on('hidden.bs.modal', function(e) {
                        location.reload(true);
                    });
                } else {
                    $.notify({
                        icon: 'pe-7s-info',
                        message: data.description
                    }, {
                        type: 'info',
                        timer: 2500
                    });
                }
            },
            timeout: 30000,
            error: function(err) {
                console.log(err);
            }
        });

    });


    $('#linkPlaid').hide();
    $('#linkCards').hide();
    $('#linkPlaid').click(function(event) {
        event.preventDefault();
        var linkHandler = Plaid.create({
            env: 'sandbox',
            clientName: $('#client_name').val(),
            key: 'c13f45250f5c110e315a300814d3e7',
            product: ['auth'],
            selectAccount: true,
            onSuccess: function(public_token, metadata) {

                $.ajax({
                    url: dir_url + "/buy/authBank",
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        public_token: public_token,
                        account_id: metadata.account_id
                    },
                    beforeSend: function() {
                        $.notify({
                            icon: 'pe-7s-info',
                            message: "We are processing your request"
                        }, {
                            type: 'info',
                            timer: 2500
                        });
                    },
                    success: function(data) {
                        var alert;
                        if (data.status == 1) {
                            $('#myModal').modal();
                            $('#myModal .modal-body').html(data.description);
                            $('#myModal').on('hidden.bs.modal', function(e) {
                                location.reload(true);
                            });
                        } else {
                            $.notify({
                                icon: 'pe-7s-info',
                                message: data.description
                            }, {
                                type: 'danger',
                                timer: 2500
                            });
                        };
                    },
                    timeout: 30000,
                    error: function(err) {
                        //console.log(err);
                    }
                });
            },
            onExit: function(err, metadata) {
                // The user exited the Link flow.
                if (err != null) {
                    // The user encountered a Plaid API error prior to exiting.
                }
            },
        });

        linkHandler.open();
    });

    $('#linkCards').click(function(event) {
        event.preventDefault();
        $('#addCard').show();
    });

    $('#add_card').click(function(event) {
        event.preventDefault();

        var expiration = $('#expiration').val();
        var month = expiration.substr(0, 2);
        var year = expiration.substr(3, 4);

        var date = new Date();
        var nowMonth = date.getMonth();
        var nowYear = date.getFullYear();

        if (month > 12) {
            $('#expiration').focus();
            $.notify({
                icon: 'pe-7s-info',
                message: "You must select a number between 01 and 12 for the expiration date of your card"
            }, {
                type: 'danger',
                timer: 2500
            });
        } else if (month < nowMonth && year < nowYear) {
            $('#expiration').focus();
            $.notify({
                icon: 'pe-7s-info',
                message: "The expiration date of the card can not be less than the current date"
            }, {
                type: 'danger',
                timer: 2500
            });
        } else {
            Stripe.setPublishableKey('pk_test_mN1rB9hfrx4YpeZy5IV8ji9x');
            Stripe.card.createToken({
                number: $('#number_card').val(),
                cvc: parseInt($('#cvc').val()),
                exp_month: parseInt(month),
                exp_year: parseInt(year)
            }, stripeResponseHandler);
        }

    });


    function stripeResponseHandler(status, response) {

        if (response.error) {
            $.notify({
                icon: 'pe-7s-info',
                message: response.error.message
            }, {
                type: 'danger',
                timer: 2500
            });
        } else {
            var token = response.id;
            $.ajax({
                url: dir_url + "/buy/addCard",
                type: 'POST',
                dataType: 'json',
                data: {
                    token: token
                },
                beforeSend: function() {
                    $('.loading').show();
                },
                success: function(data) {
                    if (data.status == 1) {
                        $('.loading').hide();
                        $('#myModal').modal();
                        $('#myModal .modal-body').html(data.description);
                        $('#myModal').on('hidden.bs.modal', function(e) {
                            location.reload(true);
                        });
                    } else {
                        $('.loading').show();
                        $.notify({
                            icon: 'pe-7s-info',
                            message: data.description
                        }, {
                            type: 'danger',
                            timer: 2500
                        });
                    };
                },
                timeout: 30000,
                error: function(err) {
                    //console.log(err);
                }
            });
        }
    }
});