$(document).ready(function() {

    var last_activity, date, year, month, day, time;
    $('.last_activity').each(function(index, el) {

        if ($(el).text() != 'No activities') {
            last_activity = $(el).text();
            time = moment(last_activity, "YYYYMMDD").fromNow();
            $(el).text(time);
        }

    });



    $('.changePrimary').click(function(event) {
        event.preventDefault();

        $('.loading').show();
        var url = $(this).attr('href');
        console.log(url);

        $.getJSON(url, function(json, textStatus) {
            console.log(json);
            console.log(textStatus);
            if (json.status == 1) {
                $('.loading').hide();
                $('#myModal').modal();
                $('#myModal .modal-body').html(json.description);
                $('#myModal').on('hidden.bs.modal', function(e) {
                    location.reload(true);
                });
            } else {
                $('.loading').hide();
                $.notify({
                    icon: 'pe-7s-close-circle',
                    message: json.description

                }, {
                    type: 'danger',
                    timer: 2500
                });
            }

        });

    });

    $('.viewAddress').click(function(event) {
        event.preventDefault();
        $('#qrcode').html('');
        var address = $(this).data('address');
        $('#addressView').text(address);
        var qrcode = new QRCode(document.getElementById("qrcode"), {
            text: address,
            width: 256,
            height: 256,
            colorDark: "#000000",
            colorLight: "#ffffff",
            correctLevel: QRCode.CorrectLevel.H
        });
    });


    $('.nameAlias').keyup(function(event) {
        if (event.keyCode == 13) {
            var id = $(this).data('id');
            var alias = $(this).val();
            var span = $(this).siblings('span.info');
            $(span).text('Press enter to save');
            $.ajax({
                url: dir_url + '/accounts/changeAlias',
                type: 'POST',
                dataType: 'json',
                data: {
                    alias: alias,
                    id: id
                },
                beforeSend: function() {
                    $(span).text('Saved...');
                },
                success: function(data) {
                    if (data.status == 1) {
                        $(span).text('Click to edit the name of your wallet');
                        $('.nameAlias').attr('readonly', 'readonly');
                    };
                },
                timeout: 30000,
                error: function(err) {
                    console.log(err);
                }
            });

        }
    });


    $('.nameAlias').click(function(event) {
        $(this).removeAttr('readonly');
        $(this).siblings('span.info').text('Press enter to save');
    });

});