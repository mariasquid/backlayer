$('.delete-user').click(function(event) {
    event.preventDefault();
    var url = $(this).data('href');
    bootbox.confirm("<h3>Are you sure?</h3><p>If you delete the user you delete all the information that is related to it, including the wallet and the transaction log</p>", function(result) {
        if (result) {
            $.getJSON(url, function(json, textStatus) {
                console.log(json);
                console.log(textStatus);
                bootbox.confirm("<h3>" + json.description + "</h3>", function(result) {
                    if (result) {
                        if (json.status == 1) {
                            location.reload();
                        }
                    }
                });
            });
        }
    });
});