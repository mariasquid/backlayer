$(document).ready(function() {
    /*
    $('#document').change(function(event) {
        event.preventDefault();

        $('#preview').html('');

        var file = document.getElementById('document').files;
        var browser = window.URL || window.webkitURL;

        if (file[0].type != 'image/jpeg' && file[0].type != 'image/jpg' && file[0].type != 'image/png') {
            $.notify({
                icon: 'pe-7s-close-circle',
                message: 'The file type is invalid, the file must be JPEG, JPG or PNG'

            }, {
                type: 'danger',
                timer: 4000
            });
        } else {
            var objectUrl = browser.createObjectURL(file[0]);
            $('#preview').html('<img src=" ' + objectUrl + '" width="100%" class="img-responsive img-thumbnail">');
        }

    });
    */
    $('#document').change(function(event) {

        $('#preview').html('');
        var file = document.getElementById('document').files;
        var browser = window.URL || window.webkitURL;

        $('#statusbar').stop().animate({
                width: '100%'
            },
            1000,
            function() {
                console.log("adadaasd");
                $('#div-alert').text('Upload Completed');
                $('#name_file').html($('#document').val() + ' <span id="success_file">Success</span>');
                $('#fileContent p').text($('#document').val());
                if (file[0].type != 'image/jpeg' && file[0].type != 'image/jpg' && file[0].type != 'image/png') {
                    $.notify({
                        icon: 'pe-7s-close-circle',
                        message: 'The file type is invalid, the file must be JPEG, JPG or PNG'

                    }, {
                        type: 'danger',
                        timer: 4000
                    });
                } else {
                    var objectUrl = browser.createObjectURL(file[0]);
                    $('#preview').html('<img src=" ' + objectUrl + '" width="100%" class="img-responsive img-thumbnail">');
                }
            });

    });

    /*
    $('#upload').submit(function(event) {
        event.preventDefault();

        $('#btn-upload').hide();

        var data = new FormData($("#upload")[0]);
        var url = $(this).attr('action');

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            contentType: false,
            processData: false,
            beforeSend: function() {
                console.log("Cargando...");
            },
            success: function(data) {
                if (data.status == 1) {
                    icon = 'pe-7s-check';
                    type = 'success';
                    $('#document').val();
                    $('#preview').html('');
                } else {
                    $('#btn-upload').show();
                    icon = 'pe-7s-close-circle';
                    type = 'danger';
                }

                $.notify({
                    icon: icon,
                    message: data.description

                }, {
                    type: type,
                    timer: 4000
                });
            },
            timeout: 30000,
            error: function(err) {
                console.log(err);
            }
        });
    });
    */

});