/**
 *  Wallet
 *
 *  A wallet takes in a hexidecimal seed to generate and manage a single
 *  Ethereum address.
 *
 */

// Stop-gap while address generation in ethereumjs-tx is broken
// We can remove elliptic from our deps once this is fixed
/*
var ECC = thirdparty.elliptic.ec('secp256k1');
function inplacePad32Array(a) {
    while (a.length < 32) {
        a.unshift(0);
    }
}
function privateToAddress(privateKey) {
    var secexp = new ethUtil.BN(privateKey);
    var publicKey = ECC.keyFromPrivate(secexp).getPublic();
    var x = publicKey.x.toArray(), y = publicKey.y.toArray();
    inplacePad32Array(x);
    inplacePad32Array(y);
    var hash = new ethUtil.sha3(Buffer.concat([new Buffer(x), new Buffer(y)]));
    return hash.toString('hex').slice(-40);
}
*/
function Wallet(hexSeed, noBootstrap) {
    if (typeof(hexSeed) !== 'string' || !hexSeed.match(/^[0-9a-fA-F]{64}$/)) {
        console.log("ERROR: hexSeed must be a 32 byte string (64 hex characters) [got: " + hexSeed + "]");
        return null;
    }

    this._private = new Buffer(hexSeed, 'hex');
    this._address = '0x' + ethUtil.privateToAddress(this._private).toString('hex')

    this._balance = 0;
    this._transactions = {};
    this._bestBlock = null;

    this._listeners = [];

    if (!noBootstrap) {
        this._update();

        var self = this;
        this._poll = setInterval(function() { self._update(); }, 60000);
    }
};

Wallet.fromJSON = function(password, json, callback) {
    return new Wallet(Wallet.hexSeedFromJSON(password, json, callback));
}

Wallet.hexSeedFromJSON = function(password, json, callback) {
    var payload = JSON.parse(json);

    var version = parseInt(payload.version);
    if (payload.version && parseInt(payload.version) === 3) {
        // @TODO: Check here that the address in the wallet is equal to the address in the file
        return Wallet._hexSeedFromJSONVersion3(password, payload, callback);
    } else if (payload.encseed && payload.ethaddr) {
        return Wallet._hexSeedFromJSONEthSale(password, payload, callback);
    }

    throw new Error("Unsupported JSON wallet version");
}

// https://github.com/ethereum/pyethsaletool/blob/master/pyethsaletool.py
Wallet._hexSeedFromJSONEthSale = function(password, payload, callback) {

    var encseed = new Buffer(payload.encseed, 'hex');

    var key = thirdparty.pbkdf2.pbkdf2Sync(password, password, 2000, 32, 'sha256').slice(0, 16);

    var iv = encseed.slice(0, 16);
    var encryptedSeed = encseed.slice(16);

    var decryption = new thirdparty.aes.ModeOfOperation.cbc(key, iv);
    var seed = [];
    for (var i = 0; i < encryptedSeed.length; i += 16) {
        var bytes = decryption.decrypt(encryptedSeed.slice(i, i + 16));
        for (var j = 0; j < 16; j++) {
            seed.push(bytes[j]);
        }
    }

    // Strip PKCS#7 padding
    var pad = seed[seed.length - 1];
    if (pad > 16 || pad > seed.length) {
        return null;
    }

    // Check PKCS#7 padding... 64 bytes is multiple of 16, so if fills the block with 16
    for (var i = seed.length - pad; i < seed.length; i++) {
        if (seed[i] !== pad) {
            return null;
        }
    }

    // Convert seed from binary encoding of hex into hex... Yes, this is a strange way to use entropy...
    var seedHex = '';
    for (var i = 0; i < seed.length - pad; i++) {
        seedHex += String.fromCharCode(seed[i]);
    }
    seed = new Buffer(ethUtil.sha3(new Buffer(seedHex)));

    //var address = ethUtil.publicToAddress(ethUtil.privateToPublic(seed)).toString('hex')
    var address = ethUtil.privateToAddress(seed).toString('hex')
    if (address !== payload.ethaddr) {
        return null;
    }

    return seed.toString('hex');

}

// See: https://github.com/ethereum/wiki/wiki/Web3-Secret-Storage-Definition
Wallet._hexSeedFromJSONVersion3 = function(password, payload, callback) {
    // Get a value from a dictionary given a path, ignoring case
    var getValue = function(path) {
        var current = payload;

        var parts = path.split('/');
        for (var i = 0; i < parts.length; i++) {
            var search = parts[i].toLowerCase();
            var found = null;
            for (var key in current) {
                if (key.toLowerCase() === search) {
                    found = key;
                    break;
                }
            }
            if (found === null) {
                return null;
            }
            current = current[found];
        }

        return current;
    }

    var ciphertext = new Buffer(getValue("crypto/ciphertext"), 'hex');

    var key = null;

    // Derive the key
    var kdf = getValue("crypto/kdf");
    if (kdf && kdf.toLowerCase() === "scrypt") {

        // Scrypt parameters
        var salt = new Buffer(getValue('crypto/kdfparams/salt'), 'hex');
        var N = getValue('crypto/kdfparams/n');
        var r = getValue('crypto/kdfparams/r');
        var p = getValue('crypto/kdfparams/p');
        if (!N || !r || !p) {
            throw new Error("Invalid JSON Wallet (bad kdfparams)");
        }

        // We need exactly 32 bytes of derived key
        var dkLen = getValue('crypto/kdfparams/dklen');
        if (dkLen !== 32) {
            throw new Error("Invalid JSON Wallet (dkLen != 32)");
        }

        // Derive the key, calling the callback periodically with progress updates
        var derivedKey = thirdparty.scryptsy(new Buffer(password), salt, N, r, p, dkLen, function(progress) {
            if (callback) {
                callback(progress.percent);
            }
        });

        // Check the password is correct
        var mac = ethUtil.sha3(Buffer.concat([derivedKey.slice(16, 32), ciphertext])).toString('hex')
        if (mac.toLowerCase() !== getValue('crypto/mac').toLowerCase()) {
            console.log("Message Authentication Code mismatch (wrong password)");
            return null;
        }
        key = derivedKey.slice(0, 16);

    } else {
        throw new Error("Unsupported key derivation function");
    }


    var seed = null;

    var cipher = getValue('crypto/cipher');
    if (cipher === 'aes-128-ctr') {
        var counter = new thirdparty.aes.Counter(new Buffer(getValue('crypto/cipherparams/iv'), 'hex'));

        var aes = new thirdparty.aes.ModeOfOperation.ctr(key, counter);

        seed = aes.decrypt(ciphertext);

    } else {
        throw new Error("Unsupported cipher algorithm");
    }

    return seed.toString('hex');
};

Wallet.prototype.suppotsExport = function(password) {
    if (window.crypto) {
        return true;
    }
    return false;
}

Wallet.prototype.exportJSON = function(password, callback, randomBytes) {
    if (password === undefined || password === null) {
        throw new Error('no password');
    }

    if (!randomBytes) {
        randomBytes = new Buffer(crypto.getRandomValues(new Uint8Array(32 + 16 + 16)))
    } else {
        randomBytes = new Buffer(randomBytes);
    }

    var nextRandomIndex = 0;
    var getRandomValues = function(count) {
        var result = randomBytes.slice(nextRandomIndex, nextRandomIndex + count);
        nextRandomIndex += count;
        if (result.length != count) {
            throw new Error('not enough random data');
        }
        return result;
    }

    var secret = this._private

    var salt = getRandomValues(32);
    var iv = getRandomValues(16);

    var derivedKey = thirdparty.scryptsy(new Buffer(password), salt, 262144, 1, 8, 32, function(progress) {
        if (callback) {
            callback(progress.percent);
        }
    });

    var counter = new thirdparty.aes.Counter(iv);
    //counter._counter = iv;

    var aes = new thirdparty.aes.ModeOfOperation.ctr(derivedKey.slice(0, 16), counter);

    var ciphertext = aes.encrypt(secret);

    var result = {
        address: this.getAddress().substring(2),
        Crypto: {
            cipher: "aes-128-ctr",
            cipherparams: {
                iv: iv.toString('hex'),
            },
            ciphertext: ciphertext.toString('hex'),
            kdf: "scrypt",
            kdfparams: {
                dklen: 32,
                n: 262144,
                r: 1,
                p: 8,
                salt: salt.toString('hex'),
            },
            mac: ethUtil.sha3(Buffer.concat([derivedKey.slice(16, 32), ciphertext])).toString('hex'),
        },
        id: thirdparty.uuid.v4({random: getRandomValues(16)}),
        version: 3,
    }

    return JSON.stringify(result);
}

Wallet.prototype.exportFilename = function() {
    return 'UTC--' + thirdparty.strftime.utc()('%Y-%m-%dT%H-%M-%S') + '.0--' + this.getAddress().substring(2);
}

/**
 *  @TODO: rename to stop?
 */
Wallet.prototype.destroy = function() {
    clearInterval(this._poll);
}

/**
 *  The address this wallet represents.
 */
Wallet.prototype.getAddress = function () {
    return this._address;
};

// Convert ICAP addresses (IBAN/BBAN)
// https://github.com/ethereum/wiki/wiki/ICAP:-Inter-exchange-Client-Address-Protocol
(function() {
    function zeroPadLeft(text, length) {
        while(text.length < length) {
            text = '0' + text;
        }
        return text;
    }

    // @TODO: File a PR to expose addSpecification; for now, hijack
    thirdparty.iban.countries.XE30 = thirdparty.iban.countries.UA;
    delete thirdparty.iban.countries.UA;
    thirdparty.iban.countries.XE30.countryCode = 'XE';
    thirdparty.iban.countries.XE30.length = 34;
    thirdparty.iban.countries.XE30.structure = 'B30';

    thirdparty.iban.countries.XE31 = thirdparty.iban.countries.BE;
    delete thirdparty.iban.countries.BE;
    thirdparty.iban.countries.XE31.countryCode = 'XE';
    thirdparty.iban.countries.XE31.length = 35;
    thirdparty.iban.countries.XE31.structure = 'B31';

    Wallet.getICAPAddress = function(data, forceBasic) {
        thirdparty.iban.countries.XE = thirdparty.iban.countries.XE30;
        if (thirdparty.iban.isValid(data)) {
            return data;
        }

        thirdparty.iban.countries.XE = thirdparty.iban.countries.XE31;
        if (thirdparty.iban.isValid(data)) {
            return data;
        }

        // Get the raw hex
        if (data.substring(0, 2) === '0x' && data.length === 42) {
            data = data.substring(2);
        }

        // Make sure it is a valid address
        if (!data.match(/^[0-9a-fA-F]{40}$/)) { return null; }

        // 0 prefixed can fit in 30 bytes (otherwise, we require 31)
        var length = 31;
        if (data.substring(0, 2) === '00' && !forceBasic) {
            data = data.substring(2);
            length = 30
            thirdparty.iban.countries.XE = thirdparty.iban.countries.XE30;
        } else {
            thirdparty.iban.countries.XE = thirdparty.iban.countries.XE31;
        }

        // Encode as base36 and add the checksum
        var encoded = (new thirdparty.bigi(data, 16)).toString(36).toUpperCase();
        encoded = zeroPadLeft(encoded, length);

        return thirdparty.iban.fromBBAN('XE', encoded);
    }

    Wallet.getAddress = function(data) {

        // Standard address, we're done
        if (data.match(/^0x[0-9a-fA-F]{40}$/)) {
            return data;
        }

        // ICAP...
        if (data.substring(0, 2) === 'XE') {

            // Check the checksum
            var validICAP = false;
            thirdparty.iban.countries.XE = thirdparty.iban.countries.XE31;
            if (thirdparty.iban.isValid(data)) {
                validICAP = true;
            } else {
                thirdparty.iban.countries.XE = thirdparty.iban.countries.XE30;
                if (thirdparty.iban.isValid(data)) {
                    validICAP = true;
                }
            }

            if (validICAP) {
                var encoded = data.substring(4);

                // Direct or Basic encoded
                if (encoded.match(/^[A-Za-z0-9]+$/)) {

                    // Decode the base36 encoded address
                    var hexAddress = (new thirdparty.bigi(encoded, 36)).toString(16);

                    // Something terrible happened...
                    if (hexAddress.length > 40) { throw new Error("Badness; this shouldn't happen"); }

                    // zero-pad
                    hexAddress = zeroPadLeft(hexAddress, 40);

                    // prepend the prefix
                    return '0x' + hexAddress;

                // Indirect encoded... Not supported yet (no namereg)
                } else if (encoded.substring(0, 7) === 'ETHXREG') {
                    return null;
                }
            }
        }

        return null;
    }
})();



/**
 *  The current balance.
 */
Wallet.prototype.getBalance = function () {
    return this._balance;
};


/**
 *  All transactions, sorted by date ascending (the API call provides sorted-ness).
 */
Wallet.prototype.getTransactions = function () {
    var transactions = [];

    for (var txid in this._transactions) {
        transactions.push(this._transactions[txid]);
    }

    return transactions;
};


/**
 *  Create a transaction to address for amount in wei.
 */
Wallet.prototype.createTransaction = function (address, amountWei) {
    if (address.substring(0, 2) != '0x') {
        address = '0x' + address;
    }

    if (address.length != 42) {
        console.log('Invalid address');
        return null;
    }

    function hexify(value) {
        if (typeof(value) === 'number' || typeof(value) === 'string') {
            value = thirdparty.web3.toBigNumber(value);
        }

        var hex = value.toString(16);
        if (hex.length % 2) {
            hex = '0' + hex;
        }

        return new Buffer(hex, 'hex');
    }

    var gasPrice = thirdparty.web3.toWei(50, 'shannon');

    var nonce = 0;
    for (var txid in this._transactions) {
        var tx = this._transactions[txid];
        if (tx.from === this._address) {
            nonce++;
        }
    }

    var rawTx = {
        nonce: hexify(nonce),
        gasPrice: hexify(thirdparty.web3.toBigNumber(gasPrice).plus(1000000000).toDigits(1)),
        gasLimit: hexify(21000),
        to: address,
        value: hexify(amountWei),
        //data: '',
    };

    var transaction = new thirdparty.ethereum.tx(rawTx);
    transaction.sign(this._private);
    transaction._mockTx = {
        blockNumber: null,
        confirmations: 0,
        from: this._address,
        hash: ('0x' + transaction.hash().toString('hex')),
        timeStamp: (new Date()).getTime() / 1000,
        to: address,
        nonce: nonce,
        value: amountWei,
    };

    return transaction;
}


/**
 *  Sends a transaction previously created with wallet.createTransaction().
 */
Wallet.prototype.sendTransaction = function(transaction, callback) {
    var hex = '0x' + transaction.serialize().toString('hex');

    var self = this;
    $.getJSON('https://api.etherscan.io/api?module=proxy&action=eth_sendRawTransaction&hex=' + hex, function (data) {
        if (!data || !data.result || data.result.length !== 66) {
            if (callback) {
                var message = 'An error occurred';
                if (data && data.error && data.error.message) {
                    message = data.error.message;
                }
                callback(new Error(message));
            }
            console.log('Error sending', data);
            return;
        }
        if (callback) {
            callback(null, data.result);
        }

        self._transactions[transaction._mockTx.hash] = transaction._mockTx;

        self._notify();
    });
}


/**
 *  Add a listener for balance and transaction changes.
 */
Wallet.prototype.addListener = function(callback) {
    this._listeners.push(callback);
}


/**
 *  Removes the first reference to callback.
 */
Wallet.prototype.removeListener = function(callback) {
    for (var i = this._listeners.length - 1; i >= 0; i--) {
        if (callback == this._listeners[i]) {
            this._listeners = this._listeners.splice(i, 1);
            break;
        }
    }
}

// Internal goop

// Notifies listeners of changes
Wallet.prototype._notify = function() {
    for (var i = 0; i < this._listeners.length; i++) {
        this._listeners[i]();
    }
}

// Called by a polling interval to update balance and transaction history
Wallet.prototype._update = function(callback) {
    console.log('Updating...');

    var self = this;
    var populateConfirmations = function() {
        var bestBlock = self._bestBlock;

        for (var txid in self._transactions) {
            var tx = self._transactions[txid];
            if (tx.blockNumber !== null && tx.blockNumber !== undefined && bestBlock !== null) {
                tx.confirmations = bestBlock - tx.blockNumber + 1;
            } else {
                tx.confirmations = 0;
            }
        }
    }

    $.getJSON('https://api.etherscan.io/api?module=account&action=txlist&address=' + this._address + '&sort=asc', function (data) {
        if (data.status != 1) { return; }
        for (var i = 0; i < data.result.length; i++) {
            var tx = data.result[i];
            self._transactions[tx.hash] = tx;
        }

        populateConfirmations();
        self._notify();

        if (callback) {
            callback({transactions: self.getTransactions()});
        }
    });
    $.getJSON('https://api.etherscan.io/api?module=account&action=balance&address=' + this._address + '&tag=latest', function (data) {
        if (data.status != 1) { return; }
        self._balance = data.result;
        self._notify();
        if (callback) {
            callback({balance: self._balance});
        }
    });
    $.getJSON('https://api.etherscan.io/api?module=proxy&action=eth_blockNumber', function (data) {
        if (!data || !data.result) { return; }
        self._bestBlock = parseInt(data.result, 16);
        populateConfirmations();
        self._notify();

        if (callback) {
            callback({bestBlock: self._bestBlock});
        }
    });
};

// @TODO: provide the option to provide a callback?
Wallet.prototype.refresh = function(callback) {
    this._update(callback);
};
