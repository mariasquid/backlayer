jQuery(document).ready(function() {

    $('#datatable').dataTable({
        "ordering": false,
        "aoColumnDefs": [{
            'bSortable': false,
            'aTargets': [-1]
        }],
        "oLanguage": {
            "oPaginate": {
               "sPrevious": "",
                "sNext": ""
            }
        },
        "iDisplayLength": 100,
        "aLengthMenu": [
            [5, 10, 25, 50, 100, -1],
            [5, 10, 25, 50, 100, "All"]
        ],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        'ordering': true
    });

    //$('#example2').DataTable({
    //  'paging'      : true,
    //  'lengthChange': false,
    //  'searching'   : false,
    //  'ordering'    : true,
    //  'info'        : true,
    //  'autoWidth'   : false
    //})

});