$('#update').submit(function(event) {

    event.preventDefault();

    var values = $('#update').serialize();
    var url = $(this).attr('action');

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: values,
        beforeSend: function() {
            $('#alert').html('<div class="alert alert-primary alert-dismissable"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="fa fa-trophy pr10"></i> <strong>Espere por favor!</strong> Estamos procesando tus datos...</div>');
        },
        success: function(data) {
            if (data.status == 1) {
                $('#alert').html('<div class="alert alert-success alert-dismissable"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="fa fa-remove pr10"></i> <strong>Exito!</strong> ' + data.description + '.</div>');
            } else {
                $('#alert').html('<div class="alert alert-danger alert-dismissable"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="fa fa-remove pr10"></i> <strong>Error!</strong> ' + data.description + '.</div>');
            };
        },
        timeout: 30000,
        error: function(err) {
            console.log(err);
        }
    });

});