$('.disable').click(function(event) {
    event.preventDefault();

    var url = $(this).attr('href');

    bootbox.confirm("<h3>Do you want to disable this device?</h3>", function(result) {
        if (result) {
            $.getJSON(url, function(json, textStatus) {
                console.log(json);
                console.log(textStatus);
                bootbox.confirm("<h3>" + json.description + "</h3>", function(result) {
                    if (result) {
                        if (json.status == 1) {
                            location.reload();
                        }
                    }
                });
            });
        }
    });
});