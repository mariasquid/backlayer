$(document).ready(function() {

    $('#update-profile').submit(function(event) {
        event.preventDefault();
        var values = $('#update-profile').serialize();
        var url = $(this).attr('action');

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: values,
            beforeSend: function() {
                console.log("Cargando...");
            },
            success: function(data) {
                if (data.status == 1) {
                    $.notify({
                        icon: 'pe-7s-check',
                        message: data.description

                    }, {
                        type: 'success',
                        timer: 2500
                    });
                    setTimeout(function() {
                        location.reload();
                    }, 2500);
                } else {
                    $.notify({
                        icon: 'pe-7s-close-circle',
                        message: data.description

                    }, {
                        type: 'danger',
                        timer: 2500
                    });
                }

            },
            timeout: 30000,
            error: function(err) {
                console.log(err);
            }
        });
    });


    $('#avatar_photo_user').change(function(event) {
        var data = new FormData($('#change-avatar')[0]);
        var url = $('#change-avatar').attr('action');

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            contentType: false,
            processData: false,
            beforeSend: function() {
                console.log("Cargando...");
            },
            success: function(data) {
                if (data.status == 1) {
                    $.notify({
                        icon: 'pe-7s-check',
                        message: data.description

                    }, {
                        type: 'success',
                        timer: 2500
                    });
                    setTimeout(function() {
                        location.reload(true);
                    }, 2500);
                } else {
                    $.notify({
                        icon: 'pe-7s-close-circle',
                        message: data.description

                    }, {
                        type: 'danger',
                        timer: 2500
                    });
                }

            },
            timeout: 30000,
            error: function(err) {
                //console.log(err);
            }
        });
    });




    /*VERIFICAR TELEFONO*/

    $('#verify_phone').click(function(event) {
        event.preventDefault();

        $.notify({
            icon: 'pe-7s-info',
            message: "We are processing the request"

        }, {
            type: 'info',
            timer: 2500
        });

        $.getJSON(dir_url + '/settings/verify_phone', function(json, textStatus) {
            console.log(json);
            console.log(textStatus);
            if (json.status == 1) {
                $('#confirm_phone').hide();
                $('#confirm_code').show();
                $.notify({
                    icon: 'pe-7s-info',
                    message: json.description

                }, {
                    type: 'info',
                    timer: 4000
                });
            }
        });

    });

    $('#verify_code').submit(function(event) {
        event.preventDefault();

        var values = $('#verify_code').serialize();
        var url = $(this).attr('action');

        var type;

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: values,
            beforeSend: function() {
                console.log("Cargando...");
            },
            success: function(data) {
                console.log(data);
                if (data.status == 1) {
                    $('input').val('');
                    type = 'success';
                    location.reload(true);
                } else {
                    type = 'danger';
                }

                $.notify({
                    icon: 'pe-7s-info',
                    message: data.description

                }, {
                    type: type,
                    timer: 2500
                });
            },
            timeout: 30000,
            error: function(err) {
                console.log(err);
            }
        });

    });


    $('#authy').click(function(event) {
        change_tfa("authy");
    });

    $('#google').click(function(event) {
        change_tfa("google");
    });

    function change_tfa(method) {

        $.ajax({
            url: dir_url + '/settings/changeTFA',
            type: 'POST',
            dataType: 'json',
            data: {
                method: method
            },
            beforeSend: function() {
                console.log("Cargando...");
            },
            success: function(data) {
                console.log(data);
                if (data.status == 1) {
                    $.notify({
                        icon: 'pe-7s-info',
                        message: data.description

                    }, {
                        type: 'success',
                        timer: 2500
                    });
                    location.reload(true);
                } else {
                    $.notify({
                        icon: 'pe-7s-info',
                        message: data.description

                    }, {
                        type: 'danger',
                        timer: 2500
                    });
                }
            },
            timeout: 30000,
            error: function(err) {
                console.log(err);
            }
        });

    }



    $('.trashDevice').click(function(event) {
        event.preventDefault();
        var url = $(this).attr('href');
        $('.loading').show();
        $.getJSON(url, function(json, textStatus) {
            $('.loading').hide();
            if (json.status == 1) {
                $('#myModal .modal-body').html(json.description);
                $('#myModal').modal();
                $('#myModal').on('hidden.bs.modal', function(e) {
                    location.reload(true);
                });
            } else {
                $.notify({
                    icon: 'pe-7s-info',
                    message: json.description
                }, {
                    type: 'danger',
                    timer: 2500

                });
            }
        });

    });

});