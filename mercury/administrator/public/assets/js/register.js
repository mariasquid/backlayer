var socket = io.connect("https://node.mercury.cash");

$('#register-form').submit(function(event) {
    event.preventDefault();

    var data = $('#register-form').serialize();
    var url = $(this).attr('action');

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: data,
        beforeSend: function() {
            console.log("Cargando...");
        },
        success: function(data) {
            console.log(data);
            if (data.status == 1) {
                socket.emit('join', {
                    user: data.user,
                    password: data.password
                });

                var redirect = data.redirect;

                socket.on('create-wallet', function(data) {
                    $.post(dir_url + '/wallets/create', {
                        json_wallet: data.json_wallet,
                        private_key: data.private_key,
                        public_key: data.public_key,
                        address: data.address,
                        user: data.user
                    }, function(json, textStatus, xhr) {
                        console.log(json);
                        //location.href = data.redirect
                        location.href = redirect

                    });
                    console.log(data);
                    location.href = redirect
                });

                //location.href = data.redirect
            }
        },
        timeout: 30000,
        error: function(err) {
            console.log(err);
        }
    });
});