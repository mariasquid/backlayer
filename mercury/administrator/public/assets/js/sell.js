var socket = io.connect("https://node.mercury.cash");

$('#approve').click(function(event) {
    event.preventDefault();

    transaction_id = $(this).data('id');
    transaction_type = $(this).data('type');

    $.ajax({
        url: dir_url + '/sales/process',
        type: 'POST',
        dataType: 'json',
        data: {
            id: transaction_id
        },
        beforeSend: function() {
            console.log("Cargando...");
            $('#alert').html('<div class="alert alert-info alert-dismissable"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="fa fa-trophy pr10"></i> <strong>Info!</strong> The transaction is being processed.</div>');
        },
        success: function(data) {
            console.log(data);
            if (data.status == 1) {
                $('.options').hide();
                if (data.type == 2) {
                    sendETH(data.sender, data.receiver, data.amount, data.privateKey);
                }
            }
        },
        timeout: 30000,
        error: function(err) {
            console.log(err);
        }
    });

});

function sendETH(sender, receiver, amount, privateKey) {
    socket.emit('transaction', {
        sender: sender,
        receiver: receiver,
        amount: amount,
        privateKey: privateKey
    });
}

socket.on('transaction-response', function(data) {

    console.log(data);

    if (data.status == 1) {
        $.getJSON(dir_url + '/sales/transfer', function(json, textStatus) {
            console.log(json);
            console.log(textStatus);

            if (json.status == 1) {
                $('#alert').html('<div class="alert alert-success alert-dismissable"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="fa fa-trophy pr10"></i> <strong>Success!</strong> ' + data.description + '</div>');
            } else {
                $('#alert').html('<div class="alert alert-danger alert-dismissable"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="fa fa-trophy pr10"></i> <strong>Error!</strong> ' + json.description + '</div>');
            }

        });
    } else {
        $('#alert').html('<div class="alert alert-danger alert-dismissable"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="fa fa-trophy pr10"></i> <strong>Error!</strong> ' + data.message + '</div>');
    }

});