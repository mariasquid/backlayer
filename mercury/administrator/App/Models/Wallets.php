<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Wallets implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `wallets`(`json_wallet`, `private_key`, `public_key`, `address`, `user`) VALUES (?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['json_wallet'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['private_key'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['public_key'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['user'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `users` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

			$connection = Database::instance();
			$sql = "SELECT * FROM usuarios";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();

		} catch(\PDOException $e) {

			print "Error!: " . $e->getMessage();

		}

    }

    public static function getByUser($user) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `wallets` WHERE `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function deleteByUser($user) {

        try {

            $connection = Database::instance();
            $sql = "DELETE FROM `wallets` WHERE `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            return $query->execute();
            
        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

}
?>