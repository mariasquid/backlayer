<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class BankAccounts implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `bank_accounts`(`type`, `number`, `address`, `city`, `state`, `zip_code`, `routing`, `beneficiary_name`, `swif_bic`, `account_number_intermediary`, `token`, `user`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['type'], \PDO::PARAM_INT);
            $query->bindParam(2, $data['number'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['city'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['state'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['zip_code'], \PDO::PARAM_STR);
            $query->bindParam(7, $data['routing'], \PDO::PARAM_STR);
            $query->bindParam(8, $data['beneficiary_name'], \PDO::PARAM_STR);
            $query->bindParam(9, $data['swif_bic'], \PDO::PARAM_STR);
            $query->bindParam(10, $data['account_number_intermediary'], \PDO::PARAM_STR);
            $query->bindParam(11, $data['token'], \PDO::PARAM_STR);
            $query->bindParam(12, $data['user'], \PDO::PARAM_INT);
            $query->execute();
            return $connection->lastInsertId();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `bank_accounts` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {

    }


    public static function getPrimaryWalletEthByUser($user) {

        try {

            $connection = Database::instance();
            $sql = "SELECT `wallets`.*, MAX(`transactions`.`date`) as `last_date` FROM `wallets` INNER JOIN `transactions` ON `transactions`.`wallet` = `wallets`.`id` WHERE `wallets`.`principal` = 1 AND `wallets`.`user` = ? LIMIT 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function getBankAccountByUser($user) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `bank_accounts` WHERE `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function changeStatus($status, $id) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `bank_accounts` SET `status`= ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $status, \PDO::PARAM_INT);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }
}
?>