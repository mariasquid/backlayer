<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class USDWalletAdmin implements Crud {

    public static function create($data) {

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `usd_wallet_admin` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            return "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `usd_wallet_admin` SET `balance` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['balance'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['id'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            return "Error!: " . $e->getMessage();

        }

    }

    public static function delete($id) {

    }

    public static function getAll() {

    }

    public static function getByUser($user) {

    }

}
?>