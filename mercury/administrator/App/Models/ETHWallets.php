<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class ETHWallets implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `eth_wallets`(`json_wallet`, `private_key`, `public_key`, `address`, `user`) VALUES (?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['json_wallet'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['private_key'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['public_key'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['user'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `eth_wallets` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `eth_wallets` SET `json_wallet` = ?, `private_key` = ?, `public_key` = ?, `address` = ?, `alias` = ?, `principal` = ?, `user` = ?  WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['json_wallet'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['private_key'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['public_key'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['alias'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['principal'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['user'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['id'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

			$connection = Database::instance();
			$sql = "SELECT * FROM `eth_wallets`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();

		} catch(\PDOException $e) {

			print "Error!: " . $e->getMessage();

		}

    }

    public static function getByUser($user) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `eth_wallets` WHERE `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function getPrimaryByUser($user) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `eth_wallets` WHERE `primary` = 1 AND `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function getTotalBalance() {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT SUM(`wallet_balance`) AS `total` FROM `vw_available_eth`";
            $query = $connection->prepare($sql);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    public static function getBalanceByUser($user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT `wallet_balance` FROM `vw_available_eth` WHERE `id_user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

}
?>