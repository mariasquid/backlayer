<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class ResetG2faLogs implements Crud {

    /**
    * @param array  Crea un nuevo registro en la tabla
    * @return bool devuelve TRUE si el registro es exitoso!
    */
    
    public static function create($data) {
    
        try {
    
            $connection = Database::instance();
            $sql = "INSERT INTO `reset_g2fa_logs`(`reason`, `date`, `customer`, `administrator`) VALUES (?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['reason'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['date'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['customer'], \PDO::PARAM_INT);
            $query->bindParam(4, $data['administrator'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    /**
    * @param int  lee un registro
    * @return bool devuelve un objeto de la tabla
    */
    
    public static function read($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `reset_g2fa_logs` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    /**
    * @param array  actualiza un registro
    * @return bool Devuelve TRUE si la actualización es exitoso!
    */
    
    public static function update($data) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `table` SET `field1` = ?, `field2` = ?, `field3` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['field1'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['field2'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['field3'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['id'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    /**
    * @param int  elimina un registro
    * @return bool Devuelve TRUE si se elimina con exitoso!
    */
    
    public static function delete($id) {

    try {
    
            $connection = Database::instance();
            $sql = "DELETE FROM `table` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(4, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
    
    }

    /**
    * @param void  obtener registros
    * @return array Devuelve todos los registros de la tabla!
    */    

    public static function getAll() {
    
        try {
	
    		$connection = Database::instance();
			$sql = "SELECT * FROM `table`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll(\PDO::FETCH_ASSOC);
		
        } catch(\PDOException $e) {
		
        	return "Error!: " . $e->getMessage();
		
        }
        
    }

    public static function getAllByUser($customer) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `reset_g2fa_logs` WHERE `customer` = ? ORDER BY `date` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $customer, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

}
?>