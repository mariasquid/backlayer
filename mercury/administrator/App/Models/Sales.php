<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Sales implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `sales`(`stripe_id`, `eth_amount`, `usd_amount`, `commission`, `eth_value`, `payment_method`, `ach_token`, `date`, `status`, `user`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['stripe_id'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['eth_amount'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['commission'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['eth_value'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['payment_method'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['ach_token'], \PDO::PARAM_STR);
            $query->bindParam(8, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(9, $data['status'], \PDO::PARAM_INT);
            $query->bindParam(10, $data['user'], \PDO::PARAM_INT);
            $query->execute();
            return $connection->lastInsertId();

        } catch(\PDOException $e) {

            return "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `sales` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            return "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `sales` SET `txhash` = ?, `eth_amount` = ?, `usd_amount` = ?, `commission` = ?, `eth_value` = ?, `payment_method` = ?, `date` = ?, `status` = ?, `bank_account` = ?, `eth_wallet` = ?, `provider` = ?, `price` = ?, `fee` = ?, `gross_income` = ?, `user` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['txhash'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['eth_amount'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['commission'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['eth_value'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['payment_method'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['status'], \PDO::PARAM_INT);
            $query->bindParam(9, $data['bank_account'], \PDO::PARAM_INT);
            $query->bindParam(10, $data['eth_wallet'], \PDO::PARAM_INT);
            $query->bindParam(11, $data['provider'], \PDO::PARAM_STR);
            $query->bindParam(12, $data['price'], \PDO::PARAM_STR);
            $query->bindParam(13, $data['fee'], \PDO::PARAM_STR);
            $query->bindParam(14, $data['gross_income'], \PDO::PARAM_STR);
            $query->bindParam(15, $data['user'], \PDO::PARAM_INT);
            $query->bindParam(16, $data['id'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            return "Error!: " . $e->getMessage();

        }

    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

            $connection = Database::instance();
            $sql = "SELECT `sales`.*, `users`.`name`, `users`.`last_name` FROM `sales` INNER JOIN `users` ON `users`.`id` = `sales`.`user`";
            $query = $connection->prepare($sql);
            $query->execute();
            return $query->fetchAll();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }


    public static function changeStatus($id, $status) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `sales` SET `status` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $status, \PDO::PARAM_INT);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            return "Error!: " . $e->getMessage();

        }

    }



    public static function getLastFive($user) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `sales` WHERE `user` = ? ORDER BY `date` DESC LIMIT 5";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll();

        } catch(\PDOException $e) {

            return "Error!: " . $e->getMessage();

        }

    }


    public static function getByUser($user, $range) {

        try {

            $end = time();
            $start = $end - 86400;

            switch ($range) {
                case 'day':
                    $start = $end - 86400;
                    break;
                case 'week':
                    $start = $end - 604800;
                    break;
                case 'month':
                    $start = $end - 2629743;
                    break;
                case 'year':
                    $start = $end - 31556926;
                    break;  
                default:
                    $start = $end - 86400;
                    break;
            }
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `sales` WHERE `user` = ? AND `date` BETWEEN ? AND ? ORDER BY `date` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->bindParam(2, $start, \PDO::PARAM_INT);
            $query->bindParam(3, $end, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }

    public static function getByRange($range) {
    
        try {

            $end = time();
            $start = $end - 86400;

            switch ($range) {
                case 'day':
                    $start = $end - 86400;
                    break;
                case 'week':
                    $start = $end - 604800;
                    break;
                case 'month':
                    $start = $end - 2629743;
                    break;
                case 'year':
                    $start = $end - 31556926;
                    break;  
                default:
                    $start = $end - 86400;
                    break;
            }
    
            $connection = Database::instance();
            $sql = "SELECT `sales`.*, `users`.`name`, `users`.`last_name` FROM `sales` INNER JOIN `users` ON `users`.`id` = `sales`.`user` WHERE `sales`.`date` BETWEEN ? AND ? ORDER BY `sales`.`date` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $start, \PDO::PARAM_INT);
            $query->bindParam(2, $end, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }

    public static function getTotalsByRange($range) {
    
        try {

            $end = time();
            $start = $end - 86400;

            switch ($range) {
                case 'day':
                    $start = $end - 86400;
                    break;
                case 'week':
                    $start = $end - 604800;
                    break;
                case 'month':
                    $start = $end - 2629743;
                    break;
                case 'year':
                    $start = $end - 31556926;
                    break;  
                default:
                    $start = $end - 86400;
                    break;
            }
    
            $connection = Database::instance();
            $sql = "SELECT SUM(`eth_amount`) AS `totaleth`, SUM(`usd_amount`) AS `totalusd`, SUM(`commission`) AS `totalcommission`, SUM(`price`) AS `totalprice`, SUM(`fee`) AS `totalfee`, SUM(`gross_income`) AS `totalgross_income` FROM `sales` WHERE `date` BETWEEN ? AND ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $start, \PDO::PARAM_INT);
            $query->bindParam(2, $end, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }

    public static function getDataSell($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT t.*, u.id AS id_user, u.name, u.last_name, u.email, u.ndocument, u.address, u.birthdate, u.occupation, ba.number, c.nicename FROM sales t INNER JOIN users u ON u.id = t.user LEFT JOIN bank_accounts ba ON ba.user = t.user LEFT JOIN country c ON c.id = u.country_id WHERE t.id = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

}
?>