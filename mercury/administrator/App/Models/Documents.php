<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Documents implements Crud {
    
    public static function create($data) {
    
        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `documents`(`name`, `original_name`, `date`, `type`, `status`, `description`, `size`, `user`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['name'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['original_name'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(4, $data['type'], \PDO::PARAM_INT);
            $query->bindParam(5, $data['status'], \PDO::PARAM_INT);
            $query->bindParam(6, $data['description'], \PDO::PARAM_STR);
            $query->bindParam(7, $data['size'], \PDO::PARAM_STR);
            $query->bindParam(8, $data['user'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }
    
    public static function read($id) {
    
        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `documents` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }
    
    public static function update($data) {
    
    }
    
    public static function delete($id) {
    
    }
    
    public static function getAll() {
    
        try {

			$connection = Database::instance();
			$sql = "SELECT * FROM `users`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();
		
        } catch(\PDOException $e) {
		
        	print "Error!: " . $e->getMessage();
		
        }
    }
    
    public static function getByUser($user) {
    
        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `documents` WHERE `user` = ? ORDER BY `id` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }
    
    public static function changeStatus($id, $status) {
    
        try {

            $connection = Database::instance();
            $sql = "UPDATE `documents` SET `status` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $status, \PDO::PARAM_INT);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
    }
    
    public static function addDescription($description, $id) {
    
        try {

            $connection = Database::instance();
            $sql = "UPDATE `documents` SET `description` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $description, \PDO::PARAM_STR);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function documentsPending() {
    
        try {

            $connection = Database::instance();
            $sql = "SELECT `documents`.*, `users`.`name` AS `nameUser`, `users`.`last_name` AS `lastnameUser` FROM `documents` INNER JOIN `users` ON `users`.`id` = `documents`.`user` WHERE `documents`.`status` = 2 ORDER BY `documents`.`date` DESC";
            $query = $connection->prepare($sql);
            $query->execute();
            return $query->fetchAll();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

}
?>