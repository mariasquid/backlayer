<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Withdraw implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `withdraw`(`wire_code`, `usd_amount`, `commission`, `eth_value`, `date`, `usd_wallet`, `bank_account`, `user`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['wire_code'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['commission'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['eth_value'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(6, $data['usd_wallet'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['bank_account'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['user'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            return "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `withdraw` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            return "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `withdraw` SET `wire_code`= ?, `usd_amount`= ?, `commission`= ?, `eth_value`= ?, `date`= ?, `status`= ?, `usd_wallet`= ?, `bank_account`= ?, `user`= ?  WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['wire_code'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['commission'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['eth_value'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(6, $data['usd_wallet'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['bank_account'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['user'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            return "Error!: " . $e->getMessage();

        }

    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

			$connection = Database::instance();
			$sql = "SELECT `withdraw`.*, `users`.`name`, `users`.`last_name` FROM `withdraw` INNER JOIN `users` ON `users`.`id` = `withdraw`.`user`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();

		} catch(\PDOException $e) {

			return "Error!: " . $e->getMessage();

		}

    }

    public static function getByUser($user, $range) {

        try {

            $end = time();
            $start = $end - 86400;

            switch ($range) {
                case 'day':
                    $start = $end - 86400;
                    break;
                case 'week':
                    $start = $end - 604800;
                    break;
                case 'month':
                    $start = $end - 2629743;
                    break;
                case 'year':
                    $start = $end - 31556926;
                    break;  
                default:
                    $start = $end - 86400;
                    break;
            }
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `withdraw` WHERE `user` = ? AND `date` BETWEEN ? AND ? ORDER BY `date` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->bindParam(2, $start, \PDO::PARAM_INT);
            $query->bindParam(3, $end, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }

    public static function changeStatus($status, $id) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `withdraw` SET `status`= ?  WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $status, \PDO::PARAM_INT);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            return "Error!: " . $e->getMessage();

        }

    }

    public static function getByRange($range) {
    
        try {

            $end = time();
            $start = $end - 86400;

            switch ($range) {
                case 'day':
                    $start = $end - 86400;
                    break;
                case 'week':
                    $start = $end - 604800;
                    break;
                case 'month':
                    $start = $end - 2629743;
                    break;
                case 'year':
                    $start = $end - 31556926;
                    break;  
                default:
                    $start = $end - 86400;
                    break;
            }
    
            $connection = Database::instance();
            $sql = "SELECT `withdraw`.*, `users`.`name`, `users`.`last_name` FROM `withdraw` INNER JOIN `users` ON `users`.`id` = `withdraw`.`user` WHERE `withdraw`.`date` BETWEEN ? AND ? ORDER BY `withdraw`.`date` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $start, \PDO::PARAM_INT);
            $query->bindParam(2, $end, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }

    public static function getTotalsByRange($range) {
    
        try {

            $end = time();
            $start = $end - 86400;

            switch ($range) {
                case 'day':
                    $start = $end - 86400;
                    break;
                case 'week':
                    $start = $end - 604800;
                    break;
                case 'month':
                    $start = $end - 2629743;
                    break;
                case 'year':
                    $start = $end - 31556926;
                    break;  
                default:
                    $start = $end - 86400;
                    break;
            }
    
            $connection = Database::instance();
            $sql = "SELECT SUM(`usd_amount`) AS `totalusd`, SUM(`commission`) AS `totalcommission` FROM `withdraw` WHERE `date` BETWEEN ? AND ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $start, \PDO::PARAM_INT);
            $query->bindParam(2, $end, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }

    public static function getDataWithdraw($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT t.*, u.id AS id_user, u.name, u.last_name, u.email, u.ndocument, u.address, u.birthdate, u.occupation, ba.number, c.nicename FROM withdraw t INNER JOIN users u ON u.id = t.user LEFT JOIN bank_accounts ba ON ba.user = t.user LEFT JOIN country c ON c.id = u.country_id WHERE t.id = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

}
?>