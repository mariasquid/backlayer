<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class ETHWalletAdmin implements Crud {

    public static function create($data) {

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `eth_wallet_admin` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `eth_wallet_admin` SET `private_key` = ?, `public_key` = ?, `address` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['private_key'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['public_key'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['id'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }
    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

			$connection = Database::instance();
			$sql = "SELECT * FROM usuarios";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();

		} catch(\PDOException $e) {

			print "Error!: " . $e->getMessage();

		}

    }

    public static function getBalance() {
    
        $data = '{"jsonrpc":"2.0","method":"eth_getBalance","params":["0x19fae4cd77328e0a12b301dc97e12f8dafa2004a", "latest"],"id":1}';
        $url = 'http://167.114.81.219:8545';
        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $data);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        $balance = hexdec($response->result);
        $balance = $balance / 1000000000000000000;
        return $balance;

    }

}
?>