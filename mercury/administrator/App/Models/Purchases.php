<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Purchases implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `purchases`(`transaction_id`, `eth_amount`, `usd_amount`, `payment_status`, `date`, `user`) VALUES (?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['transaction_id'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['eth_amount'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['payment_status'], \PDO::PARAM_INT);
            $query->bindParam(5, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(6, $data['user'], \PDO::PARAM_INT);
            $query->execute();
            return $connection->lastInsertId();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `purchases` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

        try {

            $connection = Database::instance();
            //$sql = "UPDATE `purchases` SET `stripe_id` = ?, `eth_amount` = ?, `usd_amount` = ?, `commission` = ?, `eth_value` = ?, `payment_method` = ?, `date` = ?, `status` = ?, `eth_wallet` = ?, `user` = ? WHERE `id` = ?";
            $sql = "UPDATE `purchases` SET `stripe_id` = ?, `eth_amount` = ?, `usd_amount` = ?, `commission` = ?, `eth_value` = ?, `payment_method` = ?, `date` = ?, `status` = ?, `eth_wallet` = ?, `provider` = ?, `price` = ?, `fee` = ?, `gross_income` = ?, `user` = ?  WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['stripe_id'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['eth_amount'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['commission'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['eth_value'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['payment_method'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['status'], \PDO::PARAM_INT);
            $query->bindParam(9, $data['eth_wallet'], \PDO::PARAM_INT);
            $query->bindParam(10, $data['provider'], \PDO::PARAM_STR);
            $query->bindParam(11, $data['price'], \PDO::PARAM_STR);
            $query->bindParam(12, $data['fee'], \PDO::PARAM_STR);
            $query->bindParam(13, $data['gross_income'], \PDO::PARAM_STR);
            $query->bindParam(14, $data['user'], \PDO::PARAM_INT);
            $query->bindParam(15, $data['id'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

            $connection = Database::instance();
            $sql = "SELECT `purchases`.*, `users`.`name`, `users`.`last_name` FROM `purchases` INNER JOIN `users` ON `users`.`id` = `purchases`.`user`";
            $query = $connection->prepare($sql);
            $query->execute();
            return $query->fetchAll();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }


    public static function changeStatus($id, $status) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `purchases` SET `status` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $status, \PDO::PARAM_INT);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }


    public static function getByUser($user, $range) {

        try {

            $end = time();
            $start = $end - 86400;

            switch ($range) {
                case 'day':
                    $start = $end - 86400;
                    break;
                case 'week':
                    $start = $end - 604800;
                    break;
                case 'month':
                    $start = $end - 2629743;
                    break;
                case 'year':
                    $start = $end - 31556926;
                    break;  
                default:
                    $start = $end - 86400;
                    break;
            }
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `purchases` WHERE `user` = ? AND `date` BETWEEN ? AND ? ORDER BY `date` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->bindParam(2, $start, \PDO::PARAM_INT);
            $query->bindParam(3, $end, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }

    public static function getByRange($range) {
    
        try {

            $end = time();
            $start = $end - 86400;

            switch ($range) {
                case 'day':
                    $start = $end - 86400;
                    break;
                case 'week':
                    $start = $end - 604800;
                    break;
                case 'month':
                    $start = $end - 2629743;
                    break;
                case 'year':
                    $start = $end - 31556926;
                    break;  
                default:
                    $start = $end - 86400;
                    break;
            }
    
            $connection = Database::instance();
            $sql = "SELECT `purchases`.*, `users`.`name`, `users`.`last_name` FROM `purchases` INNER JOIN `users` ON `users`.`id` = `purchases`.`user` WHERE `purchases`.`date` BETWEEN ? AND ? ORDER BY `purchases`.`date` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $start, \PDO::PARAM_INT);
            $query->bindParam(2, $end, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }

    public static function getTotalsByRange($range) {
    
        try {

            $end = time();
            $start = $end - 86400;

            switch ($range) {
                case 'day':
                    $start = $end - 86400;
                    break;
                case 'week':
                    $start = $end - 604800;
                    break;
                case 'month':
                    $start = $end - 2629743;
                    break;
                case 'year':
                    $start = $end - 31556926;
                    break;  
                default:
                    $start = $end - 86400;
                    break;
            }
    
            $connection = Database::instance();
            $sql = "SELECT SUM(`eth_amount`) AS `totaleth`, SUM(`usd_amount`) AS `totalusd`, SUM(`commission`) AS `totalcommission`, SUM(`price`) AS `totalprice`, SUM(`fee`) AS `totalfee`, SUM(`gross_income`) AS `totalgross_income` FROM `purchases` WHERE `date` BETWEEN ? AND ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $start, \PDO::PARAM_INT);
            $query->bindParam(2, $end, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }

    public static function getDataPurchase($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT t.*, u.id AS id_user, u.name, u.last_name, u.email, u.ndocument, u.address, u.birthdate, u.occupation, ba.number, c.nicename FROM purchases t INNER JOIN users u ON u.id = t.user LEFT JOIN bank_accounts ba ON ba.user = t.user LEFT JOIN country c ON c.id = u.country_id WHERE t.id = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

}
?>