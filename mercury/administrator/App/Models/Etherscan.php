<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;
use \App\Models\ETHWallets;

class Etherscan implements Crud {
    
    public static function create($data) {
    
    }
    
    public static function read($id) {
    
    }
    
    public static function update($data) {
    
    }
    
    public static function delete($id) {
    
    }
    
    public static function getAll() {
    
    }
    
    public static function getLastPrice() {
    
        $url = "https://api.etherscan.io/api?module=stats&action=ethprice&apikey=DP3WZBUEQA1FFPW4B2XJE6HPFPDURWXZI3";
        $json = file_get_contents($url);
        $obj = json_decode($json);
        
        return $obj->result;
        /*
        ethbtc: "0.036",
        ethbtc_timestamp: "1491805323",
        ethusd: "43.72",
        ethusd_timestamp: "1491805299"
        */
    }
    
    public static function getBalance($address) {
    
        $url = "https://api.etherscan.io/api?module=account&action=balance&address=" . $address . "&tag=latest&apikey=DP3WZBUEQA1FFPW4B2XJE6HPFPDURWXZI3";
        $json = file_get_contents($url);
        $obj = json_decode($json);
        
        return $obj->result;
    }
    
    public static function getlistTransactions($address) {
    
        $url = "http://api.etherscan.io/api?module=account&action=txlist&address=" . $address . "&startblock=0&endblock=99999999&sort=asc&apikey=DP3WZBUEQA1FFPW4B2XJE6HPFPDURWXZI3";
        $json = file_get_contents($url);
        $obj = json_decode($json);
        
        return $obj->result;
        /*
        [
            {
                blockNumber: "3502775",
                timeStamp: "1491708621",
                hash: "0xe8b62c8ef8c2832991e65e3550fde2d5d21aeaa7de61cfa0f9b34ffdde08590e",
                nonce: "0",
                blockHash: "0x7448917a3072a9b24d30b5764058161a6ecc615a0ecb903561709c9b30cba8d9",
                transactionIndex: "17",
                from: "0xf67b8d0ca4d45fad7b30261211387229ad39abe2",
                to: "0x3b8fa2811bb6c863c807da6d0d702c6a759eec12",
                value: "22737610000000000",
                gas: "90000",
                gasPrice: "20000000000",
                isError: "0",
                input: "0x",
                contractAddress: "",
                cumulativeGasUsed: "710652",
                gasUsed: "21000",
                confirmations: "6911"
            }
        ]
        */
    }
    
    public static function getTotalAmountByUser($user) {
    
        $wallets = ETHWallets::getByUser($user);
        $address = array();

        $totalAddress = count($wallets);
        for ($i=0; $i < $totalAddress; $i++) { 
            array_push($address, $wallets[$i]['address']);
        }

        $data = '';
        for ($i=0; $i < $totalAddress; $i++) { 
            $data = $data . '{"jsonrpc":"2.0","method":"eth_getBalance","params":["' . $address[$i] . '", "latest"],"id":1},';
        }

        $data = substr($data, 0, -1);
        $data = '[' . $data . ']';
        $url = 'http://167.114.81.219:8545';
        
        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $data);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        $total = 0;

        if (is_array($response) || is_object($response)) {
            foreach ($response as $key) {
                $eth = hexdec($key->result);
                $eth = $eth / 1000000000000000000;
                $total = $total + $eth;
            }
        }

        return $total;
    }
    
    public static function getTotalAmountMercury() {
    
        $wallets = ETHWallets::getAll();
        $address = array();
        $totalAddress = count($wallets);

        for ($i=0; $i < $totalAddress; $i++) { 
            array_push($address, $wallets[$i]['address']);
        }

        $data = '';
        for ($i=0; $i < $totalAddress; $i++) { 
            $data = $data . '{"jsonrpc":"2.0","method":"eth_getBalance","params":["' . $address[$i] . '", "latest"],"id":1},';
        }
        
        $data = substr($data, 0, -1);
        $data = '[' . $data . ']';
        $url = 'http://167.114.81.219:8545';
        
        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $data);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        $total = 0;

        foreach ($response as $r) {
            $eth = hexdec($r->result);
            $eth = $eth / 1000000000000000000;
            $total = $total + $eth;
        }

        return $total;
    }
    
    public static function fromWei($eth_amount) {
    
        $eth_amount = $eth_amount / 1000000000000000000;
        return number_format($eth_amount, 5);
    }
}
?>