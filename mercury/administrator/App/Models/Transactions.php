<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Transactions implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `transactions`(`address_from`, `address_to`, `amount`, `date`, `user`) VALUES (?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['address_from'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['address_to'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['amount'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(5, $data['user'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            return "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `transactions` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

			$connection = Database::instance();
			$sql = "SELECT * FROM `transactions` ORDER BY `date` DESC";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();

		} catch(\PDOException $e) {

			print "Error!: " . $e->getMessage();

		}

    }

    public static function getPurchases() {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `transactions` WHERE `type_transaction` = 2 ORDER BY `date` DESC";
            $query = $connection->prepare($sql);
            $query->execute();
            return $query->fetchAll();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }


    public static function changeStatus($id, $status) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `transactions` SET `status` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $status, \PDO::PARAM_INT);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return$query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }
    }

    public static function getByUser($user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `vw_transactions` WHERE `user` = ? ORDER BY `date` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_STR);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

}
?>