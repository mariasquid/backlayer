<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Dashboard implements Crud {

    public static function create($data) {

    }

    public static function read($id) {

    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {

    }

    public static function transactionsRange($start, $end) {

        try {

            //$end = time();
            //$start = $end - 86400;

            $connection = Database::instance();
            $sql = "SELECT ((SELECT COUNT(`purchases`.`id`) FROM `purchases` WHERE `purchases`.`date` BETWEEN ? AND ?) + (SELECT COUNT(`sales`.`id`) FROM `sales` WHERE `sales`.`date` BETWEEN ? AND ?) + (SELECT COUNT(`transfers`.`id`) FROM `transfers` WHERE `transfers`.`date` BETWEEN ? AND ?) + (SELECT COUNT(`withdraw`.`id`) FROM `withdraw` WHERE `withdraw`.`date` BETWEEN ? AND ?)) as `total`";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $start, \PDO::PARAM_INT);
            $query->bindParam(2, $end, \PDO::PARAM_INT);
            $query->bindParam(3, $start, \PDO::PARAM_INT);
            $query->bindParam(4, $end, \PDO::PARAM_INT);
            $query->bindParam(5, $start, \PDO::PARAM_INT);
            $query->bindParam(6, $end, \PDO::PARAM_INT);
            $query->bindParam(7, $start, \PDO::PARAM_INT);
            $query->bindParam(8, $end, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

}
?>