<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Account implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `transactions_usd`(`transaction_id`, `usd_amount`, `type`, `description`, `date`, `bank_account`) VALUES (?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['transaction_id'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['type'], \PDO::PARAM_INT);
            $query->bindParam(4, $data['description'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(6, $data['bank_account'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `transactions_usd` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

			$connection = Database::instance();
			$sql = "SELECT * FROM transactions_usd";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();

		} catch(\PDOException $e) {

			print "Error!: " . $e->getMessage();

		}

    }


    public static function getByAccount($bank) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `transactions_usd` WHERE `bank_account` = ? ORDER BY `id` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $bank, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }
}
?>