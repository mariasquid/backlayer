<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;
class Users implements Crud {

    public static function create($data) {
        
        try {
        
            $connection = Database::instance();
            $sql = "INSERT INTO `users`(`name`, `last_name`, `email`, `phonecode`, `phone`, `authy_id`, `password`, `token`, `date_admission`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['name'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['last_name'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['email'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['phonecode'], \PDO::PARAM_INT);
            $query->bindParam(5, $data['phone'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['authy_id'], \PDO::PARAM_STR);
            $query->bindParam(7, $data['password'], \PDO::PARAM_STR);
            $query->bindParam(8, $data['token'], \PDO::PARAM_STR);
            $query->bindParam(9, $data['date_admission'], \PDO::PARAM_INT);
            $query->execute();
            return $connection->lastInsertId();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function read($id) {
        
        try {
        
            $connection = Database::instance();
            $sql = "SELECT * FROM `users` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function update($data) {
        
        try {
        
            $connection = Database::instance();
            //$sql = "UPDATE `users` SET `name` = ?, `last_name` = ?, `email` = ?, `birthdate` = ?, `address` = ?, `state` = ?, `city` = ?, `country` = ?, `zip_code` = ?, `status` = ? WHERE `id` = ?";
            $sql = "UPDATE `users` SET `name` = ?, `last_name` = ?, `email` = ?, `birthdate` = ?, `occupation` = ?, `profession` = ?, `ssn` = ?, `address` = ?, `city` = ?, `province_id` = ?, `country_id` = ?, `zip_code` = ?, `status` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['name'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['last_name'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['email'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['birthdate'], \PDO::PARAM_INT);
            $query->bindParam(5, $data['occupation'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['profession'], \PDO::PARAM_STR);
            $query->bindParam(7, $data['ssn'], \PDO::PARAM_STR);
            $query->bindParam(8, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(9, $data['city'], \PDO::PARAM_STR);
            $query->bindParam(10, $data['province_id'], \PDO::PARAM_INT);
            $query->bindParam(11, $data['country_id'], \PDO::PARAM_INT);
            $query->bindParam(12, $data['zip_code'], \PDO::PARAM_STR);
            $query->bindParam(13, $data['status'], \PDO::PARAM_INT);
            $query->bindParam(14, $data['id'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function delete($id) {

    }
    
    public static function getAll() {
        
        try {
		
        	$connection = Database::instance();
			$sql = "SELECT * FROM `users`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();
		
        } catch(\PDOException $e) {
		
        	print "Error!: " . $e->getMessage();
		
        }
    }

    public static function readByEmail($email) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `users` WHERE `email` = ? LIMIT 1";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $email, \PDO::PARAM_STR);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    public static function addTry($try, $user) {
        
        try {
        
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `tries_login` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $try, \PDO::PARAM_INT);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function userBlock($user) {
        
        try {
        
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `status` = 0,`tries_login` = 0 WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function restoreLoginTries($user) {
        
        try {
        
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `tries_login` = 0 WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function validateEmail($email, $token) {
        
        try {
        
            $connection = Database::instance();
            $sql = "SELECT `id` FROM `users` WHERE `email` = ? AND `token` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $email, \PDO::PARAM_STR);
            $query->bindParam(2, $token, \PDO::PARAM_STR);
            $query->execute();
            return $query->fetch();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function changeStatus($user, $status) {
        
        try {
        
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `status`= ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $status, \PDO::PARAM_INT);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function updateToken($user, $token) {
        
        try {
        
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `token`= ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $token, \PDO::PARAM_STR);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function updateAvatar($avatar, $user) {
        
        try {
        
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `avatar` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $avatar, \PDO::PARAM_STR);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function addStripeId($stripe, $user) {
        
        try {
        
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `stripe_id` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $stripe, \PDO::PARAM_STR);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function changePassword($password, $user) {
        
        try {
        
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `password` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $password, \PDO::PARAM_STR);
            $query->bindParam(2, $user, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function addAuthyId($authy_id, $id) {
        
        try {
        
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `authy_id` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $authy_id, \PDO::PARAM_STR);
            $query->bindParam(2, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function verifyPhone($id) {
        
        try {
        
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `verified_phone` = 1, `2fa` = 1 WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function updateLimits($data) {
        
        try {
        
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `limit_buy_eth_card` = ?, `limit_buy_eth_ach` = ?, `limit_buy_eth_usd` = ?  WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['limit_buy_eth_card'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['limit_buy_eth_ach'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['limit_buy_eth_usd'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['id'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function totalUsers() {
        
        try {
        
            $connection = Database::instance();
            $sql = "SELECT COUNT(`id`) AS `total` FROM `users`";
            $query = $connection->prepare($sql);
            $query->execute();
            return $query->fetch();
        
        } catch(\PDOException $e) {
        
            print "Error!: " . $e->getMessage();
        
        }
    }

    public static function resetG2fa($user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `users` SET `google_2fa_code` = NULL WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_STR);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    public static function getTotalUsers() {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT COUNT(`id`) AS `total` FROM `users`";
            $query = $connection->prepare($sql);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

}
?>