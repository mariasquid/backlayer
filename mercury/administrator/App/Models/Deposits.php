<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Deposits implements Crud {
    
    public static function create($data) {
    
        try {
    
            $connection = Database::instance();
            $sql = "INSERT INTO `deposits`(`stripe_id`, `wire_code`, `usd_amount`, `commission`, `method`, `date`, `status`, `usd_wallet`, `user`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['stripe_id'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['wire_code'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['usd_amount'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['commission'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['method'], \PDO::PARAM_INT);
            $query->bindParam(6, $data['date'], \PDO::PARAM_INT);
            $query->bindParam(7, $data['status'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['user'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }
    
    public static function read($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `deposits` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }
    
    public static function update($data) {

        $connection = Database::instance();
        $sql = "UPDATE `deposits` SET `stripe_id` = ?, `wire_code` = ?, `usd_amount` = ?, `commission` = ?, `method` = ?, `date` = ?, `status` = ?, `user` = ? WHERE `id` = ?";
        $query = $connection->prepare($sql);
        $query->bindParam(1, $data['stripe_id'], \PDO::PARAM_STR);
        $query->bindParam(2, $data['wire_code'], \PDO::PARAM_STR);
        $query->bindParam(3, $data['usd_amount'], \PDO::PARAM_STR);
        $query->bindParam(4, $data['commission'], \PDO::PARAM_STR);
        $query->bindParam(5, $data['method'], \PDO::PARAM_INT);
        $query->bindParam(6, $data['date'], \PDO::PARAM_INT);
        $query->bindParam(7, $data['status'], \PDO::PARAM_INT);
        $query->bindParam(8, $data['user'], \PDO::PARAM_INT);
        $query->bindParam(9, $data['id'], \PDO::PARAM_INT);
        return $query->execute();
    
    }
    

    public static function delete($id) {
    
    }
    

    public static function getAll() {
    
        try {
	
    		$connection = Database::instance();
			$sql = "SELECT `deposits`.*, `users`.`name`, `users`.`last_name` FROM `deposits` INNER JOIN `users` ON `users`.`id` = `deposits`.`user`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll(\PDO::FETCH_ASSOC);
		
        } catch(\PDOException $e) {
		
        	return "Error!: " . $e->getMessage();
		
        }

    }

    public static function getByUser($user, $range) {
    
        try {

            $end = time();
            $start = $end - 86400;

            switch ($range) {
                case 'day':
                    $start = $end - 86400;
                    break;
                case 'week':
                    $start = $end - 604800;
                    break;
                case 'month':
                    $start = $end - 2629743;
                    break;
                case 'year':
                    $start = $end - 31556926;
                    break;  
                default:
                    $start = $end - 86400;
                    break;
            }
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `deposits` WHERE `user` = ? AND `date` BETWEEN ? AND ? ORDER BY `date` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->bindParam(2, $start, \PDO::PARAM_INT);
            $query->bindParam(3, $end, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    public static function getDates($from, $to, $user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `deposits` WHERE `date` BETWEEN ? AND ? AND `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $from, \PDO::PARAM_INT);
            $query->bindParam(2, $to, \PDO::PARAM_INT);
            $query->bindParam(3, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

    public static function getLastFive($user) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `deposits` WHERE `user` = ? ORDER BY `date` DESC LIMIT 5";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }

    public static function getByRange($range) {
    
        try {

            $end = time();
            $start = $end - 86400;

            switch ($range) {
                case 'day':
                    $start = $end - 86400;
                    break;
                case 'week':
                    $start = $end - 604800;
                    break;
                case 'month':
                    $start = $end - 2629743;
                    break;
                case 'year':
                    $start = $end - 31556926;
                    break;  
                default:
                    $start = $end - 86400;
                    break;
            }
    
            $connection = Database::instance();
            $sql = "SELECT `deposits`.*, `users`.`name`, `users`.`last_name` FROM `deposits` INNER JOIN `users` ON `users`.`id` = `deposits`.`user` WHERE `deposits`.`date` BETWEEN ? AND ? ORDER BY `deposits`.`date` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $start, \PDO::PARAM_INT);
            $query->bindParam(2, $end, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }

    public static function getTotalsByRange($range) {
    
        try {

            $end = time();
            $start = $end - 86400;

            switch ($range) {
                case 'day':
                    $start = $end - 86400;
                    break;
                case 'week':
                    $start = $end - 604800;
                    break;
                case 'month':
                    $start = $end - 2629743;
                    break;
                case 'year':
                    $start = $end - 31556926;
                    break;  
                default:
                    $start = $end - 86400;
                    break;
            }
    
            $connection = Database::instance();
            $sql = "SELECT SUM(`usd_amount`) AS `totalusd`, SUM(`commission`) AS `totalcommission` FROM `deposits` WHERE `date` BETWEEN ? AND ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $start, \PDO::PARAM_INT);
            $query->bindParam(2, $end, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }

    }

    public static function getDataDeposit($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT t.*, u.id AS id_user, u.name, u.last_name, u.email, u.ndocument, u.address, u.birthdate, u.occupation, ba.number, c.nicename FROM deposits t INNER JOIN users u ON u.id = t.user LEFT JOIN bank_accounts ba ON ba.user = t.user LEFT JOIN country c ON c.id = u.country_id WHERE t.id = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
}
?>