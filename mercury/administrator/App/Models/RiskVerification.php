<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class RiskVerification implements Crud {
    
    public static function create($data) {
    
        try {
    
            $connection = Database::instance();
            $sql = "INSERT INTO `risk_verification`(`average_risk_level`, `world_check_match_strenght`, `world_check_source_type`, `ofacs_list`, `naics_code`, `notes`, `user`) VALUES (?, ?, ?, ?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['average_risk_level'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['world_check_match_strenght'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['world_check_source_type'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['ofacs_list'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['naics_code'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['notes'], \PDO::PARAM_STR);
            $query->bindParam(7, $data['user'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {

            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function read($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `risk_verification` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function update($data) {
    
        try {
    
            $connection = Database::instance();
            $sql = "UPDATE `risk_verification` SET `average_risk_level` = ?, `world_check_match_strenght` = ?, `world_check_source_type` = ?, `ofacs_list` = ?, `naics_code` = ?, `notes` = ?, `user` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['average_risk_level'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['world_check_match_strenght'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['world_check_source_type'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['ofacs_list'], \PDO::PARAM_STR);
            $query->bindParam(5, $data['naics_code'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['notes'], \PDO::PARAM_STR);
            $query->bindParam(7, $data['user'], \PDO::PARAM_INT);
            $query->bindParam(8, $data['id'], \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }
    
    public static function delete($id) {

    try {
    
            $connection = Database::instance();
            $sql = "DELETE FROM `risk_verification` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(4, $id, \PDO::PARAM_INT);
            return $query->execute();
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
    
    }
    

    public static function getAll() {
    
        try {
	
    		$connection = Database::instance();
			$sql = "SELECT * FROM `risk_verification`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll(\PDO::FETCH_ASSOC);
		
        } catch(\PDOException $e) {
		
        	return "Error!: " . $e->getMessage();
		
        }
        
    }

    public static function getByUser($id) {
    
        try {
    
            $connection = Database::instance();
            $sql = "SELECT * FROM `risk_verification` WHERE `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch(\PDO::FETCH_ASSOC);
        
        } catch(\PDOException $e) {
        
            return "Error!: " . $e->getMessage();
        
        }
        
    }

}
?>