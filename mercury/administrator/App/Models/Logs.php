<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Logs implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `logs_login`(`ip`, `browser`, `date`, `user`) VALUES (?, ?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['ip'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['browser'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['date'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['user'], \PDO::PARAM_INT);
            $query->execute();
            return $connection->lastInsertId();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `users` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `users` SET `name` = ?, `last_name` = ?, `email` = ?, `birthdate` = ?, `address` = ?, `city` = ?, `country` = ?, `zip_code` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['name'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['last_name'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['email'], \PDO::PARAM_STR);
            $query->bindParam(4, $data['birthdate'], \PDO::PARAM_INT);
            $query->bindParam(5, $data['address'], \PDO::PARAM_STR);
            $query->bindParam(6, $data['city'], \PDO::PARAM_STR);
            $query->bindParam(7, $data['country'], \PDO::PARAM_STR);
            $query->bindParam(8, $data['zip_code'], \PDO::PARAM_STR);
            $query->bindParam(9, $data['id'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function delete($id) {

    }

    public static function getAll() {

        try {

			$connection = Database::instance();
			$sql = "SELECT * FROM `users`";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();

		} catch(\PDOException $e) {

			print "Error!: " . $e->getMessage();

		}

    }

    public static function getByUser($user) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `logs_login` WHERE `user` = ? ORDER BY `id` DESC";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetchAll();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function deleteByUser($user) {

        try {

            $connection = Database::instance();
            $sql = "DELETE FROM `logs_login` WHERE `user` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $user, \PDO::PARAM_INT);
            return$query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

}
?>