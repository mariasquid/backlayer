<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View;

class Mailer {

    public static function documentsRejected($data) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "admin_documents_rejected";

        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $data['to'] . "&merge_message=" . $data['message'];

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    public static function documentsApproved($email) {

        $url = "https://api.elasticemail.com/v2/email/send";

        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "admin_documents_approved";

        $post = "apikey=" . $apikey . "&template=" . $template . "&to=" . $email;

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    public static function sendMail($email, $subject, $message, $button, $redirect) {

        $url = "https://api.elasticemail.com/v2/email/send";
        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "new_favorite";

        $post = "apikey=" . $apikey . "&subject=" . $subject . "&to=" . $email . "&template=" . $template . "&merge_message=" . $message . "&merge_redirect=" . $redirect . "&merge_button=" . $button . "&to" . $button;
        
        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        
        return $response;

    }

    public static function send() {
        require_once LIBSPATH . "/Elasticmail/init.php";
        $recipients = ["alejandro8924@gmail.com"];
        $subject = "Your subject";
        $from = "support@mercury.cash";
        $fromName = "Rafael Moreno";
        $text = "Text body";
        $html = "<h1>Html Body</h1>";
 
        $EEemail = new \ElasticEmailClient\Email();
        try {
            $response = $EEemail->Send($subject, $from, $fromName, null, null, null, null, null, null, $recipients, array(), array(), array(), array(), array(), null, null, $html, $text);     
        
        } catch (Exception $e) {
        
            echo 'Something went wrong: ', $e->getMessage(), '\n';
            return;
        }       
        echo 'MsgID to store locally: ' . $response->messageid . '<br>';
        echo 'TransactionID to store locally: ' . $response->transactionid;
    }


    public static function validate_document($email, $status, $description = NULL) {

        $url = "https://api.elasticemail.com/v2/email/send";
        $apikey = "0460c754-721b-4b28-96fc-2d90237559b5";
        $template = "new_favorite";

        $subject = ($status == 1) ? "Mercury Cash - Your ID has been Approved!" : "Mercury Cash - Your ID was Rejected";

        if ($status == 1) {
            $message = '<h3 style="text-align: center;">Hello,</h3>';
            $message .= '<p style="text-align: center;">Our validation team has approved your documents, from now on, you can enjoy all of Mercury Cash\'s features.</p>';
            $message .= '<p style="text-align: center;">Regards.</p>';
            $message .= '<h5 style="text-align: center;">Mercury Cash support team</h5>';
        } else {
            $message = '<h3 style="text-align: center;">Hello,</h3>';
            $message .= '<p style="text-align: center;">After an exhaustive analysis, our validation team weren\'t able to aprove your ID Document. Please, check below the reasons for this decision</p>';
            $message .= '<p style="text-align: center;">'. $description .'</p>';
            $message .= '<p style="text-align: center;">We are sorry for the inconvenience, you can contact our Support Team for more information.</p>';
            $message .= '<p style="text-align: center;">Regards.</p>';
            $message .= '<h5 style="text-align: center;">Mercury Cash support team</h5>';
        }

        $button = "Go to Mercury Cash";
        $post = "apikey=" . $apikey . "&subject=" . $subject . "&to=" . $email . "&template=" . $template . "&merge_message=" . $message . "&merge_redirect=https://www.mercury.cash/dashboard" . "&merge_button=" . $button . "&to" . $button;
        
        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_HTTPGET, FALSE);
        curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_POSTFIELDS, $post);
        curl_setopt($con, CURLOPT_HEADER, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($con);
        curl_close($con);
        $response = json_decode($response);
        return $response;

    }

    /*
    public static function mail($email, $subject, $message, $button) {
        $body = file_get_contents("public/email-templates/template.html");
        $body = str_replace("[\]", '', $body);
        $body = str_replace("{{DIR_URL}}", DIR_URL, $body);
        $body = str_replace("{{message}}", $message, $body);
        $body = str_replace("{{button}}", $button, $body);
        $body = str_replace("{{redirect}}", $redirect, $body);
        //para el envío en formato HTML 
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= "From: Mercury <mining@adenter.io>\r\n";
        // ENVIAR CORREOS
        if (mail($email, $subject, $body, $headers)) {
            return 1;
        } else {
            return 0;
        }
    }
    public static function pay_processed($name, $email, $ethereum) {
        $message = '<h2>Congratulations ' . $name . '!</h2><p>Your payment has been approved, we have transferred ' . $ethereum . ' ETH to your wallet</p>';
        $body = file_get_contents("public/email-templates/pay_processed.html");
        $body = str_replace("[\]", '', $body);
        $body = str_replace("{{DIR_URL}}", DIR_URL, $body);
        $body = str_replace("{{message}}", $message, $body);
        $body = str_replace("{{button}}", "Check Balance", $body);
        $body = str_replace("{{redirect}}", "https://www.mercury.cash/dashboard", $body);
        //para el envío en formato HTML 
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= "From: Mercury <mining@adenter.io>\r\n";
        // ENVIAR CORREOS
        if (mail($email, "Congratulations " . $name . "!", $body, $headers)) {
            return 1;
        } else {
            return 0;
        }
    }
    */
}
?>