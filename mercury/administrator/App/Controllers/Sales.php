<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Users,
    \App\Models\ETHWallets,
    \App\Models\Etherscan,
    \App\Models\ETHWalletAdmin,
    \App\Models\USDWalletAdmin,
    \App\Models\USDWallets,
    \App\Models\Sales as SalesModel,
    \App\Models\Limits,
    \App\Models\Providers,
    \App\Models\BankAccounts;

class Sales {
    
    public function sale($id) {
    
        if (!empty($_SESSION['admin_id'])) {
            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH =  Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);
            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */
            $bankAccount = NULL;
            if (is_numeric($id)) {
                $transaction = SalesModel::read($id);
                $user = Users::read($transaction['user']);
                if (!is_null($transaction['bank_account'])) {
                    $bankAccount = BankAccounts::read($transaction['bank_account']);
                }
            }
            View::set("bankAccount", $bankAccount);
            View::set("user", $user);
            View::set("transaction", $transaction);
            View::set("balanceETH", $balanceETH);
            View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);
            View::set("title", "Sale");
            View::render("sale_details");
        } else {
            header("Location: " . DIR_URL . "/administrator/home");
        }
    }
    
    public function process() {
    
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        if (!empty($_SESSION['admin_id'])) {
            
            if (is_numeric($_POST['id'])) {
                
                $id = $_POST['id'];
                $transaction = SalesModel::read($id);
                $user = Users::read($transaction['user']);
                
                if ($transaction['payment_method'] == 2) {
                    $_SESSION['id_sale_transaction'] = $id;
                    $ethwalletadmin = ETHWalletAdmin::read(1);
                    $ethwallet = ETHWallets::getPrimaryByUser($user['id']);
                    echo json_encode(array(
                        "status" => 1,
                        "type" => 2,
                        "sender" => $ethwallet['address'],
                        "receiver" => $ethwalletadmin['address'],
                        "amount" => $transaction['eth_amount'],
                        "privateKey" => $ethwallet['private_key']
                        ));
                }
            }
        }
    }
    
    public function account() {
    
        require_once LIBSPATH . "/stripe/init.php";
        $customer = \Stripe\Customer::retrieve("cus_AieqAh1SfSaDLu");
        $bank_account = $customer->sources->retrieve("ba_1ANVBqEBjWVgARVvrlxdijuA");
        $account = $bank_account->account;
        echo json_encode($bank_account);
    }
    
    public function transfer() {
    
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        if (!empty($_SESSION['admin_id']) && !empty($_SESSION['id_sale_transaction'])) {
            $transaction = SalesModel::read($_SESSION['id_sale_transaction']);
            require_once LIBSPATH . "/stripe/init.php";
            $user = Users::read($transaction['user']);
            try {
                $customer = \Stripe\Customer::retrieve($user['stripe_id']);
                $bank_account = $customer->sources->retrieve($transaction['ach_token']);
                $account = $bank_account->account;
                $transfer = \Stripe\Transfer::create(array(
                    "amount" => $transaction['usd_amount'],
                    "currency" => "usd",
                    "destination" => $bank_account->account,
                    "transfer_group" => "ORDER_95"
                    ));
                if (!empty($transfer->id)) {
                    $result = SalesModel::changeStatus($id, '1');
                    if ($result) {
                        unset($_SESSION['id_sale_transaction']);
                        echo json_encode(array(
                            "status" => 1,
                            "description" => "The transaction has been processed"
                            ));
                    } else {
                        echo json_encode(array(
                            "status" => 0,
                            "description" => "An error has occurred, please try again."
                            ));
                    }
                }
            } catch(\Exception $e) {
                echo json_encode(array(
                    "status" => 0,
                    "description" => $e->getMessage()
                    ));
            }
        } else {
            echo json_encode(array(
                "status" => 0,
                "description" => "Error!"
                ));
        }
    }
    
    public function approve() {
    
        if (!empty($_SESSION['admin_id'])) {

            $sale = SalesModel::read($_POST['id']);
            if (!empty($sale['id']) && $sale['status'] == 2) {

                for ($i=0; $i < 3; $i++) {

                    $fee = (is_numeric($_POST['fee'][$i])) ? (($_POST['price'][$i] * $_POST['fee'][$i]) / 100) : NULL;

                    if (!empty($_POST['provider'][$i]) &&
                        !empty($_POST['price'][$i]) && is_numeric($_POST['price'][$i]) &&
                        !empty($_POST['fee'][$i]) && is_numeric($_POST['fee'][$i])) {
                        $data = array();
                        $data['provider'] = htmlspecialchars($_POST['provider'][$i], ENT_QUOTES);
                        $data['quantity'] = (is_numeric($_POST['quantity'][$i])) ? $_POST['quantity'][$i] : NULL;
                        $data['price'] = (is_numeric($_POST['price'][$i])) ? $_POST['price'][$i] : NULL;
                        $data['fee'] = $fee;
                        $data['tx_id'] = $_POST['id'];
                        $data['tx_type'] = 4;
                        Providers::create($data);
                    }
                }
                
                $result = SalesModel::changeStatus($sale['id'], 1);
                if ($result) {
                    
                    $user = Users::read($sale['user']);
                    Mailer::sendMail($user['email'], "Mercury Cash - Sell", "Your transaction has been processed successfully, in 2 to 5 days your funds will be available in your bank account", "Go to Mercury Cash", DIR_URL . "/dashboard");
                    $_SESSION['alert'] = "The transaction has been processed successfully";
                    header("Location: " . DIR_URL . "/administrator/sales/sale/" . $_POST['id']);
                    exit;
                } else {
                    $_SESSION['alert'] = "An error has occurred, please try again.";
                    header("Location: " . DIR_URL . "/administrator/sales/sale/" . $_POST['id']);
                    exit;
                }
            } else {
                $_SESSION['alert'] = "An error has occurred, please try again.";
                header("Location: " . DIR_URL . "/administrator/sales/sale/" . $_POST['id']);
                exit;
            }

        }
    }
    
    public function _approve($id) {
    
        if (!empty($_SESSION['admin_id'])) {
            
            if (is_numeric($id)) {
                
                $sale = SalesModel::read($id);
                if (!empty($sale['id']) && $sale['status'] == 2 && $sale['bank_account'] != NULL) {
                    
                    $result = SalesModel::changeStatus($id, 1);
                    if ($result) {
                        
                        $user = Users::read($sale['user']);
                        Mailer::sendMail($user['email'], "Mercury Cash - Sell", "Your application has been processed successfully, in 2 to 5 days your funds will be available in your bank account", "Go to Mercury Cash", DIR_URL . "/dashboard");
                        $_SESSION['alert'] = "The transaction has been processed successfully";
                        header("Location: " . DIR_URL . "/administrator/sales/sale/" . $id);
                        exit;
                    } else {
                        $_SESSION['alert'] = "An error has occurred, please try again.";
                        header("Location: " . DIR_URL . "/administrator/sales/sale/" . $id);
                        exit;
                    }
                } else {
                    $_SESSION['alert'] = "An error has occurred, please try again.";
                    header("Location: " . DIR_URL . "/administrator/sales/sale/" . $id);
                    exit;
                }
            } else {
                $_SESSION['alert'] = "The data you are trying to process, are not correct";
                header("Location: " . DIR_URL . "/administrator/sales/sale/" . $id);
                exit;
            }
        } else {
            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }
    }
    
    public function reject($id) {
    
        if (!empty($_SESSION['admin_id'])) {
            
            if (is_numeric($id)) {
                    
                    $sale = SalesModel::read($id);
                    if (!empty($sale['id']) && $sale['status'] == 2 && $sale['bank_account'] != NULL) {
                        
                        $result = SalesModel::changeStatus($id, 0);
                        if ($result) {
                            
                            Mailer::sendMail($user['email'], "Mercury Cash - Sell", "We regret to inform you that your request has not been processed", "Go to Mercury Cash", DIR_URL . "/dashboard");
                            $_SESSION['alert'] = "The transaction has been processed successfully";
                            header("Location: " . DIR_URL . "/administrator/sales/sale/" . $id);
                            exit;
                        } else {
                            $_SESSION['alert'] = "An error has occurred, please try again.";
                            header("Location: " . DIR_URL . "/administrator/sales/sale/" . $id);
                            exit;
                        }
                    } else {
                        $_SESSION['alert'] = "An error has occurred, please try again.";
                        header("Location: " . DIR_URL . "/administrator/sales/sale/" . $id);
                        exit;
                    }
                } else {
                    $_SESSION['alert'] = "The data you are trying to process, are not correct";
                    header("Location: " . DIR_URL . "/administrator/sales/sale/" . $id);
                    exit;
                }
        } else {
            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }
    }
}
?>