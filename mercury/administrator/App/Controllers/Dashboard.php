<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Dashboard as DashboardModel,
    \App\Models\Documents,
    \App\Models\USDWallets,
    \App\Models\ETHWalletAdmin,
    \App\Models\USDWalletAdmin,
    \App\Models\Etherscan,
    \App\Models\ETHWallets,
    \App\Models\Users;

class Dashboard {

    public function index() {

    	if (!empty($_SESSION['admin_id'])) {

            /* HEADER */
            $masterWalletBalance =  ETHWalletAdmin::getBalance();
            $lastPrice = Etherscan::getLastPrice();
            /* HEADER */

            $documents = Documents::documentsPending();
            $users = Users::getTotalUsers();
            $ethBalanceMercury = ETHWallets::getTotalBalance();
            $usdBalanceMercury = USDWallets::getTotalBalance();
            $documents = Documents::documentsPending();

            View::set("documents", $documents);
            View::set("usdBalanceMercury", $usdBalanceMercury['total']);
            View::set("ethBalanceMercury", $ethBalanceMercury['total']);
            View::set("users", $users['total']);
            View::set("lastPrice", $lastPrice->ethusd);
            View::set("masterWalletBalance", $masterWalletBalance);
            View::set("title", "Dashboard");
	        View::render("dashboard");
	        
    	} else {

            header("Location: " . DIR_URL . "/administrator/home");
            
        }
    }

    public function price() {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
        header("Content-type: application/json");

        $price = Etherscan::getLastPrice();
        echo json_encode(array(
            "price" => "$" . number_format($price->ethusd, 2)
        ));

    }

    public function transactionsDay() {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
        header("Content-type: application/json");

        if (!empty($_SESSION['admin_id'])) {

            $end = time();
            $start = $end - 86400;
            
            $transactions = DashboardModel::transactionsRange($start, $end);
            echo $transactions[0]['total'];

        }

    }

    public function transactionsWeek() {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
        header("Content-type: application/json");

        if (!empty($_SESSION['admin_id'])) {

            $end = time();
            $start = $end - 604800;
            
            $transactions = DashboardModel::transactionsRange($start, $end);
            echo $transactions[0]['total'];

        }

    }

    public function transactionsMonth() {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
        header("Content-type: application/json");

        if (!empty($_SESSION['admin_id'])) {

            $end = time();
            $start = $end - 2629743;
            
            $transactions = DashboardModel::transactionsRange($start, $end);
            echo $transactions[0]['total'];

        }

    }

    public function transactionsYear() {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
        header("Content-type: application/json");

        if (!empty($_SESSION['admin_id'])) {

            $end = time();
            $start = $end - 31556926;
            
            $transactions = DashboardModel::transactionsRange($start, $end);
            echo $transactions[0]['total'];

        }

    }
}
?>