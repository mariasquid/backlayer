<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Wallets as WalletsModel;

class Wallets {

    public function create() {

        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");

        if (!empty($_POST['json_wallet']) && !empty($_POST['private_key']) && !empty($_POST['public_key']) && !empty($_POST['address']) && !empty($_POST['user'])) {
            $result = WalletsModel::create($_POST);
            if ($result) {
                echo json_encode(array(
                    "status" => 1,
                    "description" => "You have successfully registered on our platform",
                    "redirect" => DIR_URL . "/users",
                    "user" => $user,
                    "password" => $password
                    ));
            }
        }

    }

}
?>