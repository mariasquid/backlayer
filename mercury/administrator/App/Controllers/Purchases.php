<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Buy,
    \App\Models\Users,
    \App\Models\ETHWallets,
    \App\Models\WalletAdmin,
    \App\Models\ETHWalletAdmin,
    \App\Models\Etherscan,
    \App\Models\USDWalletAdmin,
    \App\Models\Purchases as PurchasesModel,
    \App\Models\Transactions,
    \App\Models\USDWallets,
    \App\Models\Providers,
    \App\Controllers\Mailer,
    \App\Models\Access;

class Purchases {

    public function index() {

    	if (!empty($_SESSION['admin_id'])) {

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH =  Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);

            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */

            $purchases = Transactions::getPurchases();

            View::set("purchases", $purchases);
            View::set("balanceETH", $balanceETH);
            View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);
            View::set("title", "Purchases");
            View::render("purchases");

        } else {
            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }

    }

    public function purchase($id) {

        if (!empty($_SESSION['admin_id'])) {
            
            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH =  Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);

            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */

            if (is_numeric($id)) {

                $transaction = PurchasesModel::read($id);
                $user = Users::read($transaction['user']);

            }

            View::set("user", $user);
            View::set("transaction", $transaction);
            View::set("balanceETH", $balanceETH);
            View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);
            View::set("title", "Purchase");
            View::render("purchase-details");

        } else {
            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }

    }

    public function process() {

        if (!empty($_SESSION['admin_id'])) {
            
            $purchase = PurchasesModel::read($_POST['id']);
            $user = Users::read($purchase['user']);

            for ($i=0; $i < 3; $i++) {

                $fee = (is_numeric($_POST['fee'][$i])) ? (($_POST['price'][$i] * $_POST['fee'][$i]) / 100) : NULL;

                if (!empty($_POST['provider'][$i]) &&
                    !empty($_POST['price'][$i]) && is_numeric($_POST['price'][$i]) &&
                    !empty($_POST['fee'][$i]) && is_numeric($_POST['fee'][$i])) {
                    $data = array();
                    $data['provider'] = htmlspecialchars($_POST['provider'][$i], ENT_QUOTES);
                    $data['quantity'] = (is_numeric($_POST['quantity'][$i])) ? $_POST['quantity'][$i] : NULL;
                    $data['price'] = (is_numeric($_POST['price'][$i])) ? $_POST['price'][$i] : NULL;
                    $data['fee'] = $fee;
                    $data['tx_id'] = $_POST['id'];
                    $data['tx_type'] = 3;
                    Providers::create($data);
                }
            }

            //$fee = ($_POST['price'] * $_POST['fee']) / 100;
            //$gross_income = $purchase['commission'] + (($purchase['eth_value'] - ($_POST['price'] + $fee)) * $purchase['eth_amount']);

            $result = PurchasesModel::changeStatus($purchase['id'], 1);
            if ($result) {
                $message = '<h3 style="text-align: center;">Hello,</h3>';
                $message .= '<p style="text-align: center;">We are glad to inform that your ETH purchase has been approved and processed succesfully!</p>';
                $message .= '<p style="text-align: center;">Your ETH will be available soon in your wallet.</p>';
                $message .= '<p style="text-align: center;">Thank you for using Mercury Cash</p>';
                $message .= '<p style="text-align: center;">Regards.</p>';
                $message .= '<h5 style="text-align: center;">Mercury Cash support team</h5>';
                Mailer::sendMail($user['email'], "Mercury Cash - Buy", $message, "Go to Mercury Cash", DIR_URL . "/buy");
                $_SESSION['alert'] = "Your transaction was successful";
                header("Location: " . DIR_URL . "/administrator/purchases/purchase/" . $_POST['id']);
                exit;
            } else {
                $_SESSION['alert'] = "Contact the developer";
                header("Location: " . DIR_URL . "/administrator/purchases/purchase/" . $_POST['id']);
                exit;
            }

        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;

        }

    }
   
   public function reject($id) {

        if ($_SESSION['admin_id']) {
            
            if (is_numeric($id)) {
                $purchase = PurchasesModel::read($id);

                $result = PurchasesModel::changeStatus($purchase['id'], 0);
                if ($result) {
                    $user = Users::read($purchase['user']);
                    $message = '<h3 style="text-align: center;">Hello,</h3>';
                    $message .= '<p style="text-align: center;">We are glad to inform that your ETH purchase has been approved and processed succesfully!</p>';
                    $message .= '<p style="text-align: center;">Your ETH will be available soon in your wallet.</p>';
                    $message .= '<p style="text-align: center;">Thank you for using Mercury Cash</p>';
                    $message .= '<p style="text-align: center;">Regards.</p>';
                    $message .= '<h5 style="text-align: center;">Mercury Cash support team</h5>';
                    $_SESSION['alert'] = "Your transaction was successful";
                    header("Location: " . DIR_URL . "/administrator/purchases/purchase/" . $id);
                    exit;

                } else {

                    $_SESSION['alert'] = "An error occurred, please try again.";
                    header("Location: " . DIR_URL . "/administrator/purchases/purchase/" . $id);
                    exit;

                }
            } else {

                header("Location: " . DIR_URL . "/administrator/dashboard");
                exit;

            }
            
        } else {
            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }

    }

}
?>