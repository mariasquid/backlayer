<?php

namespace App\Controllers;

defined("APPPATH") OR die("Access denied");



use \Core\View,

	\App\Models\Administrators,

    \App\Models\ETHWalletAdmin;



class Home {



    public function loginDev($user) {



        $user = Administrators::read($user);

        $_SESSION['admin_id'] = $user['id'];

        $_SESSION['admin_name'] = $user['name'] . " " . $user['last_name'];

        $_SESSION['admin_email'] = $user['email'];

        $_SESSION['admin_avatar'] = DIR_URL . "/administrator/public/assets/img/" . $user['avatar'];

        $_SESSION['master_address'] = ETHWalletAdmin::read(1)['address'];



        // LOGIN EXITOSO

        header("Location: " . DIR_URL . "/administrator/dashboard");

        exit;





    }

    

    public function index() {

    

        session_destroy();

        View::set("title", "Sign In");

        View::render("login");

    }

    

    public function login() {

    

    	if (!empty($_POST['email']) && !empty($_POST['password'])) {

            $email = htmlspecialchars($_POST['email'], ENT_QUOTES);

            $password = md5($_POST['password']);

            $user = Administrators::readByEmail($email);

            if (!empty($user)) {

                $tries = 0;

                

                if ($email == $user['email'] && $password == $user['password']) {

                    

                    $_SESSION['admin_id'] = $user['id'];

                    

                    //comentar estas sesiones

                    $_SESSION['admin_name'] = $user['name'] . " " . $user['last_name'];
                  $_SESSION['admin_email'] = $user['email'];
                  $_SESSION['admin_avatar'] = DIR_URL . "/administrator/public/assets/img/" . $user['avatar'];
                  $_SESSION['master_address'] = ETHWalletAdmin::read(1)['address'];
                  $_SESSION['level'] = 1;
                   Administrators::restoreLoginTries($user['id']);

                    

                    

                    // LOGIN EXITOSO

                    $_SESSION['home'] = DIR_URL . "/administrator/dashboard";

                    header("Location: " . DIR_URL . "/administrator/dashboard");

                    exit;



                } else {

                    $tries = $user['tries_login'] + 1;

                    if ($tries >= 5) {

                        

                        Administrators::userBlock($user['id']);

                        $_SESSION['alert'] = "Credential error, user has been blocked. Contact your administrator";

                        header("Location: " . DIR_URL . "/administrator/home");

                        exit;



                    } else {



                        Administrators::addTry($tries, $user['id']);

                        $_SESSION['alert'] = "Credential error";

                        header("Location: " . DIR_URL . "/administrator/home");

                        exit;



                    }

                }

            } else {

                

                $_SESSION['alert'] = "Credential error";

                header("Location: " . DIR_URL . "/administrator/home");

                exit;



            }

        } else {

            echo json_encode(array(

                "status" => 0,

                "description" => "Los campos estan vacios",

                ));



            $_SESSION['alert'] = "Fields are empty";

            header("Location: " . DIR_URL . "/administrator/home");

            exit;



        }

    }





    public function google_authentication() {



        if (!empty($_SESSION['admin_id'])) {



            $user = Administrators::read($_SESSION['admin_id']);



            require_once LIBSPATH . "/gauthentication/init.php";

            $auth = new \Auth();

            $tfa = $auth->getTfa();

            $auth->setSecret();

            $user = Administrators::read($_SESSION['admin_id']);

            $_SESSION['secret_google'] = $user['google_2fa_code'];

            $qr = $auth->getQr($user['name'] . " " . $user['last_name'], $_SESSION['secret_google']);

            

            View::set("active", $user['google_tfa_active']);

            View::set("secret", $_SESSION['secret_google']);

            View::set("codeqr", $qr);

            View::set("title", "Sign In");

            View::render("google_authentication");



        }



    }



    public function google_validate() {



        if (!empty($_SESSION['admin_id'])) {

            

            if (!empty($_POST['code']) && is_numeric($_POST['code'])) {

                $code = htmlspecialchars($_POST['code'], ENT_QUOTES);

                $user = Administrators::read($_SESSION['admin_id']);

                require_once LIBSPATH . "/gauthentication/init.php";

                $auth = new \Auth();

                $result = NULL;

                if (is_null($user['google_2fa_code'])) {

                    $result = $auth->validate($_SESSION['secret_google'], $code);

                    Administrators::saveGoogleCode($_SESSION['secret_google'], $_SESSION['admin_id']);

                    unset($_SESSION['secret_google']);

                } else {

                    $result = $auth->validate($user['google_2fa_code'], $code);

                }

                if ($result) {



                    if ($user['google_tfa_active'] == 0) {

                        Administrators::activeGoogle2fa($_SESSION['admin_id']);

                    }

                    

                    $_SESSION['admin_name'] = $user['name'] . " " . $user['last_name'];

                    $_SESSION['admin_email'] = $user['email'];

                    $_SESSION['admin_avatar'] = DIR_URL . "/administrator/public/assets/img/" . $user['avatar'];

                    $_SESSION['master_address'] = ETHWalletAdmin::read(1)['address'];

                    $_SESSION['level'] = 1;

                    Administrators::restoreLoginTries($user['id']);

                    

                    // LOGIN EXITOSO

                    header("Location: " . DIR_URL . "/administrator/dashboard");

                    exit;

                    

                } else {

                    header("Location: " . DIR_URL . "/administrator/home/google_authentication?error=codeinvalid");

                    exit;

                }

            } else {

                header("Location: " . DIR_URL . "/administrator/home/google_authentication?error=codeinvalid");

                exit;

            }

        } else {

            session_destroy();

            header("Location: " . DIR_URL . "/administrator/home");

            exit;

        }



    }

}

?>