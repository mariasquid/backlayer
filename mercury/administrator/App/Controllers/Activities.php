<?php

namespace App\Controllers;

defined("APPPATH") OR die("Access denied");



use \Core\View,

	\App\Models\Devices,

    \App\Models\Etherscan,
    \App\Models\USDWalletAdmin,

    \App\Controllers\Functions;



class Activities {



    /*

    public function index() {



    	if (!empty($_SESSION)) {



    		$pending = Documents::getByStatusAndUserStatus();



	    	View::set("pending", $pending);

	    	View::set("title", "Dashboard");

	        View::render("dashboard");

	        

    	} else {

            header("Location: " . DIR_URL . "/administrator/home");

        }



    }

    */



    public function activities($user) {



        if (!empty($_SESSION)) {

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH =  Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);

            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */



            $devices = Devices::getAllByUser($user);



            for ($i=0; $i < count($devices); $i++) { 

                $devices[$i]['browser'] = Functions::getBrowser($devices[$i]['browser']);

                $details = json_decode(file_get_contents("http://freegeoip.net/json/" . $devices[$i]['ip']));

                $devices[$i]['near'] = $details->country_name . ", " . $details->region_name;

            }

            



            View::set("user", $user);

            View::set("devices", $devices);

            View::set("balanceETH", $balanceETH);
            View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);

            View::set("title", "Activities");

            View::render("activities-user");

            

        } else {

            header("Location: " . DIR_URL . "/administrator/home");

        }



    }



    public function disableDevice($id) {



        if (!empty($_SESSION)) {



            $result = Devices::changeStatus($id, 0, 0);



            if ($result) {

                

                echo json_encode(array(

                    "status" => 1,

                    "description" => "The device has been disabled"

                    ));



            } else {



                echo json_encode(array(

                    "status" => 0,

                    "description" => "An error has occurred, please try again."

                    ));



            }

            

        } else {

            header("Location: " . DIR_URL . "/administrator/home");

        }



    }



}

?>