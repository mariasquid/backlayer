<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Users,
    \App\Models\ETHWallets,
    \App\Models\ETHWalletAdmin,
    \App\Models\Etherscan,
    \App\Models\USDWallets,
    \App\Models\USDWalletAdmin,
    \App\Models\BankAccounts,
    \App\Models\Withdraw;

class Withdraws {

    public function withdraw($id) {

        if (!empty($_SESSION['admin_id'])) {

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH =  Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);

            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */

            $bankAccount = NULL;

            if (is_numeric($id)) {

                $transaction = Withdraw::read($id);
                $user = Users::read($transaction['user']);

                if (!is_null($transaction['bank_account'])) {
                    $bankAccount = BankAccounts::read($transaction['bank_account']);
                }

            }

            View::set("bankAccount", $bankAccount);
            View::set("user", $user);
            View::set("transaction", $transaction);
            View::set("balanceETH", $balanceETH);
            View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);
            View::set("title", "Withdraws");
            View::render("withdraws_details");

        } else {
            header("Location: " . DIR_URL . "/administrator/home");
        }

    }

    public function approve($id) {

        if (!empty($_SESSION['admin_id'])) {

            if (is_numeric($id)) {
                    
                    $withdraw = Withdraw::read($id);
                    $result = Withdraw::changeStatus(1, $id);
                    if ($result) {
                        
                        $user = Users::read($withdraw['user']);
                        Mailer::sendMail($user['email'], "Mercury Cash - withdraws", "We have approved and processed your withdrawal from your USDWallet of Mercury Cash", "Go to Mercury Cash", DIR_URL . "/accounts/withdraw");
                        $_SESSION['alert'] = "Your transaction was successful";
                        header("Location: " . DIR_URL . "/administrator/withdraws/withdraw/" . $id);
                        exit;

                    } else {

                        $_SESSION['alert'] = "An error occurred, please try again.";
                        header("Location: " . DIR_URL . "/administrator/withdraws/withdraw/" . $id);
                        exit;

                    }

                } else {

                    $_SESSION['alert'] = "The data is invalid";
                    header("Location: " . DIR_URL . "/administrator/withdraws/withdraw/" . $id);
                    exit;

                }

        } else {

            header("Location: " . DIR_URL . "/administrator/home");

        }

    }


     public function reject($id) {

        if (!empty($_SESSION['admin_id'])) {

            if (is_numeric($id)) {

                    $withdraw = Withdraw::read($id);
                    if (!empty($withdraw['id'])) {

                        $result = Withdraw::changeStatus(0, $id);
                        if ($result) {
                            
                            $user = Users::read($withdraw['user']);
                            Mailer::sendMail($user['email'], "Mercury Cash - withdraws", "Unfortunately, your withdrawal request from your USDWallet of Mercury Cash has been declined", "Go to Mercury Cash", DIR_URL . "/accounts/withdraw");
                            $_SESSION['alert'] = "Your transaction was successful";
                            header("Location: " . DIR_URL . "/administrator/withdraws/withdraw/" . $id);
                            exit;

                        } else {

                            $_SESSION['alert'] = "An error occurred, please try again.";
                            header("Location: " . DIR_URL . "/administrator/withdraws/withdraw/" . $id);
                            exit;

                        }

                    } else {

                        $_SESSION['alert'] = "Transaction you want to approve does not exist";
                        header("Location: " . DIR_URL . "/administrator/withdraws/withdraw/" . $id);
                        exit;

                    }

                } else {

                    $_SESSION['alert'] = "The data is invalid";
                    header("Location: " . DIR_URL . "/administrator/withdraws/withdraw/" . $id);
                    exit;

                }

        } else {

            header("Location: " . DIR_URL . "/administrator/home");

        }

     }


}

?>