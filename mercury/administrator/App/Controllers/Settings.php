<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\WalletAdmin,
    \App\Models\Users,
    \App\Models\ETHWallets,
    \App\Models\USDWallets,
    \App\Models\USDWalletAdmin,
    \App\Models\Etherscan,
    \App\Models\Earnings;

class Settings {
    
    public function wallet() {
    
        if (!empty($_SESSION['admin_id'])) {
            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH =  Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);
            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */
            $wallet = WalletAdmin::read(1);
            View::set("wallet", $wallet);
            View::set("balanceETH", $balanceETH);
            View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);
            View::set("title", "Wallet");
            View::render("wallet-system");
            
        } else {
            header("Location: " . DIR_URL . "/administrator/home");
        }
    }
    
    public function updateWallet() {
    
        if (!empty($_SESSION['admin_id'])) {
            $wallet = WalletAdmin::read(1);
            $data = array();
            $data['private_key'] = (!empty($_POST['private_key'])) ? $_POST['private_key'] : $wallet['private_key'];
            $data['public_key'] = (!empty($_POST['public_key'])) ? $_POST['public_key'] : $wallet['public_key'];
            $data['address'] = (!empty($_POST['address'])) ? $_POST['address'] : $wallet['address'];
            $data['id'] = 1;
            $result = WalletAdmin::update($data);
            if ($result) {
                echo json_encode(array(
                    "status" => 1,
                    "description" => "Los datos del wallet se actualizaron con exito"
                    ));
            } else {
                echo json_encode(array(
                    "status" => 0,
                    "description" => "Ha ocurrido un error"
                    ));
            }
            
        } else {
            header("Location: " . DIR_URL . "/administrator/home");
        }
    }
    
    public function generalAmounts() {
    
        if (!empty($_SESSION['admin_id'])) {
            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH =  Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);
            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */
            $usdwallets = USDWallets::getAll();
            $total_count_usd = count($usdwallets);
            $total_usd_full = 0;
            for ($i=0; $i < $total_count_usd; $i++) { 
                $total_usd_full = $total_usd_full + $usdwallets[$i]['balance'];
            }
            $users = Users::getAll();
            $json = array();
            foreach ($users as $user) {
                
                array_push($json, array(
                    "user" => $user['id'],
                    "fullname" => $user['name'] . " " . $user['last_name'],
                    "eth_balance" => Etherscan::getTotalAmountByUser($user['id']),
                    "usd_balance" => USDWallets::getByUser($user['id'])['balance']
                    ));
            }
            View::set("rows", $json);
            View::set("total_usd", $total_usd_full);
            View::set("total_eth", Etherscan::getTotalAmountMercury());
            View::set("balanceETH", $balanceETH);
            View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);
            View::set("title", "General Amount");
            View::render("general_amounts");
        }
    }
    
    public function getGeneralAmounts() {
    
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        if (!empty($_SESSION['admin_id'])) {
            
            $users = Users::getAll();
            $json = array();
            foreach ($users as $user) {
                
                array_push($json, array(
                    "user" => $user['id'],
                    "eth_balance" => Etherscan::getTotalAmountByUser($user['id']),
                    "usd_balance" => USDWallets::getByUser($user['id'])['balance']
                    ));
            }
            echo json_encode($json);
        }
    }
    
    //public function earnings() {
    public function index() {
    
        if (!empty($_SESSION['admin_id'])) {
            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH =  Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);
            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */
            $earnings = Earnings::read(1);
            View::set("priceEth", $price->ethusd);
            View::set("earnings", $earnings);
            View::set("balanceETH", $balanceETH);
            View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);
            View::set("title", "Earnings");
            View::render("earnings");
        }
    }

    public function earningsCalc($percentage, $method) {

        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
    
        if (is_numeric($percentage) && is_numeric($method)) {
            
            $price = Etherscan::getLastPrice();
            $total = ($price->ethusd * $percentage) / 100;

            if ($method == 1) { // BUY
                $total = $price->ethusd + $total;

                echo json_encode(array(
                    "total" => number_format($total, 2)
                    ));
            } elseif ($method == 2) { // SELL
                $total = $price->ethusd - $total;

                echo json_encode(array(
                    "total" => number_format($total, 2)
                    ));
            }

        }

    }
    
    public function earningsUpdate() {
    
        if (!empty($_SESSION['admin_id'])) {
            if (is_numeric($_POST['buy']) && is_numeric($_POST['sell'])) {
                
                $data = array();
                $data['buy'] = $_POST['buy'];
                $data['sell'] = $_POST['sell'];
                $result = Earnings::update($data);
                if ($result) {
                    
                    $_SESSION['alert'] = "The information was updated successfully";
                    header("Location: " . DIR_URL . "/administrator/settings");
                    exit;
                } else {
                    $_SESSION['alert'] = "An error has occurred, please try again.";
                    header("Location: " . DIR_URL . "/administrator/settings");
                    exit;
                }
            } else {
                $_SESSION['alert'] = "You must enter numeric data";
                header("Location: " . DIR_URL . "/administrator/settings");
                exit;
            }
        } else {
            header("Location: " . DIR_URL);
            exit;
        }
    }
}
?>