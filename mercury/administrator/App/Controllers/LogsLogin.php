<?php

namespace App\Controllers;

defined("APPPATH") OR die("Access denied");



use \Core\View,

	\App\Models\Logs,

    \App\Models\Etherscan,
    \App\Models\USDWalletAdmin,

    \App\Controllers\Functions;



class Logslogin {



    /*

    public function index() {



    	if (!empty($_SESSION)) {



    		$pending = Documents::getByStatusAndUserStatus();



	    	View::set("pending", $pending);

	    	View::set("title", "Dashboard");

	        View::render("dashboard");

	        

    	} else {

            header("Location: " . DIR_URL . "/administrator/home");

        }



    }

    */



    public function logs($user) {



        if (!empty($_SESSION)) {

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH =  Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);

            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */



            $logs = Logs::getByUser($user);



            for ($i=0; $i < count($logs); $i++) { 

                $logs[$i]['browser'] = Functions::getBrowser($logs[$i]['browser']);

                $details = json_decode(file_get_contents("http://freegeoip.net/json/" . $logs[$i]['ip']));

                $logs[$i]['near'] = $details->country_name . ", " . $details->region_name;

            }

            



            View::set("logs", $logs);

            View::set("balanceETH", $balanceETH);
            View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);

            View::set("title", "Activities");

            View::render("logsLogin");

            

        } else {

            header("Location: " . DIR_URL . "/administrator/home");

        }



    }





}

?>