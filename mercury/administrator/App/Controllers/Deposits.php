<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Users,
    \App\Models\ETHWallets,
    \App\Models\ETHWalletAdmin,
    \App\Models\Etherscan,
    \App\Models\USDWallets,
    \App\Models\USDWalletAdmin,
    \App\Models\Deposits as DepositsModel;

class Deposits {

    public function deposit($id) {

        if (!empty($_SESSION['admin_id'])) {

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH =  Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);

            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);

            /* HEADER */

            $bankAccount = NULL;

            if (is_numeric($id)) {

                $transaction = DepositsModel::read($id);
                $user = Users::read($transaction['user']);

            }

            View::set("bankAccount", $bankAccount);
            View::set("user", $user);
            View::set("transaction", $transaction);
            View::set("balanceETH", $balanceETH);
            View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);
            View::set("title", "Withdraws");
            View::render("deposits_details");

        } else {
            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }

    }

    public function approve($id) {

        if (!empty($_SESSION['admin_id'])) {

            if (is_numeric($id)) {
                    
                $deposit = DepositsModel::read($id);
                if (!empty($deposit['id'])) {

                    $data = array();
                    $data['stripe_id'] = NULL;
                    $data['wire_code'] = $deposit['wire_code'];
                    $data['usd_amount'] = $deposit['usd_amount'];
                    $data['commission'] = $deposit['commission'];
                    $data['method'] = $deposit['method'];
                    $data['date'] = $deposit['date'];
                    $data['status'] = 1;
                    $data['user'] = $deposit['user'];
                    $data['id'] = $deposit['id'];
                    $result = DepositsModel::update($data);

                    if ($result) {
                        
                        $user = Users::read($deposit['user']);
                        Mailer::sendMail($user['email'], "Mercury Cash - Deposit", "We have approved and processed your deposit to your USDWallet of Mercury Cash", "Go to Mercury Cash", DIR_URL . "/accounts/withdraw");
                        $_SESSION['alert'] = "Your transaction was successful";
                        header("Location: " . DIR_URL . "/administrator/deposits/deposit/" . $id);

                    } else {

                        $_SESSION['alert'] = "An error occurred, please try again.";
                        header("Location: " . DIR_URL . "/administrator/deposits/deposit/" . $id);

                    }

                } else {

                    $_SESSION['alert'] = "Transaction you want to approve does not exist";
                    header("Location: " . DIR_URL . "/administrator/deposits/deposit/" . $id);

                }

            } else {

                $_SESSION['alert'] = "The data is invalid";
                header("Location: " . DIR_URL . "/administrator/deposits/deposit/" . $id);

            }

        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;

        }

    }


     public function reject($id) {

        if (!empty($_SESSION['admin_id'])) {

            if (is_numeric($id)) {

                $deposit = DepositsModel::read($id);
                if (!empty($deposit['id'])) {

                    $data = array();
                    $data['stripe_id'] = NULL;
                    $data['wire_code'] = $deposit['wire_code'];
                    $data['usd_amount'] = $deposit['usd_amount'];
                    $data['commission'] = $deposit['commission'];
                    $data['method'] = $deposit['method'];
                    $data['date'] = $deposit['date'];
                    $data['status'] = 0;
                    $data['user'] = $deposit['user'];
                    $data['id'] = $deposit['id'];
                    $result = DepositsModel::update($data);

                    if ($result) {
                        
                        $user = Users::read($deposit['user']);
                        Mailer::sendMail($user['email'], "Mercury Cash - Deposit", "Unfortunately, your deposit to your USDWallet of Mercury Cash has been declined", "Go to Mercury Cash", DIR_URL . "/accounts/withdraw");
                        $_SESSION['alert'] = "Your transaction was successful";
                        header("Location: " . DIR_URL . "/administrator/deposits/deposit/" . $id);

                    } else {

                        $_SESSION['alert'] = "An error occurred, please try again.";
                        header("Location: " . DIR_URL . "/administrator/deposits/deposit/" . $id);

                    }

                } else {

                    $_SESSION['alert'] = "Transaction you want to approve does not exist";
                    header("Location: " . DIR_URL . "/administrator/deposits/deposit/" . $id);

                }

            } else {

                $_SESSION['alert'] = "The data is invalid";
                header("Location: " . DIR_URL . "/administrator/deposits/deposit/" . $id);
            }

        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;

        }

     }


}

?>