<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\Users,
    \App\Models\ETHWallets,
    \App\Models\ETHWalletAdmin,
    \App\Models\USDWallets,
    \App\Models\Purchases,
    \App\Models\Sales,
    \App\Models\Etherscan,
    \App\Models\USDWalletAdmin,
    \App\Models\Limits,
    \App\Models\Transfers,
    \App\Models\Deposits,
    \App\Models\Transactions as TransactionsModel,
    \App\Models\Withdraw;

class Transactions {
    
    public function index($range = 'day') {
    
        if (!empty($_SESSION['admin_id'])) {

            if ($_SESSION['level'] != 4) {
                
                /* HEADER */
                $price = Etherscan::getLastPrice();
                $masterWalletBalance =  ETHWalletAdmin::getBalance();
                $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
                $lastPrice = Etherscan::getLastPrice();
                /* HEADER */

                $transactions = array();
                $purchases = Purchases::getByRange($range);
                $purchasesTotals = Purchases::getTotalsByRange($range);
                $sales = Sales::getByRange($range);
                $salesTotals = Sales::getTotalsByRange($range);
                $withdraws = Withdraw::getByRange($range);
                $withdrawsTotals = Withdraw::getTotalsByRange($range);
                $deposits = Deposits::getByRange($range);
                $depositsTotals = Deposits::getTotalsByRange($range);

                /*
                $totals = array(
                    'totaleth' => ($purchasesTotals['totaleth'] + $salesTotals['totaleth'] + $withdrawsTotals['totaleth']),
                    'totalusd' => ($purchasesTotals['totalusd'] + $salesTotals['totalusd'] + $withdrawsTotals['totalusd'] + $depositsTotals['totalusd']),
                    'totalcommission' => ($purchasesTotals['totalcommission'] + $salesTotals['totalcommission'] + $withdrawsTotals['totalcommission'] + $depositsTotals['totalcommission']),
                    'totalprice' => ($purchasesTotals['totalprice'] + $salesTotals['totalprice']),
                    'totalfee' => ($purchasesTotals['totalfee'] + $salesTotals['totalfee']),
                    'totalgross_income' => ($purchasesTotals['totalgross_income'] + $salesTotals['totalgross_income'] + $withdrawsTotals['totalcommission'] + $depositsTotals['totalcommission']),
                    );
                */
                for ($i=0; $i < count($purchases); $i++) { 
                    $purchases[$i]['type_transaction'] = 1;
                    array_push($transactions, $purchases[$i]);
                }

                for ($i=0; $i < count($sales); $i++) { 
                    $sales[$i]['type_transaction'] = 2;
                    array_push($transactions, $sales[$i]);
                }

                for ($i=0; $i < count($withdraws); $i++) { 
                    $withdraws[$i]['type_transaction'] = 4;
                    array_push($transactions, $withdraws[$i]);
                }

                for ($i=0; $i < count($deposits); $i++) { 
                    $deposits[$i]['type_transaction'] = 5;
                    array_push($transactions, $deposits[$i]);
                }

                $total = count($transactions);
                for ($i=0; $i < $total; $i++) { 
                    for ($j=0; $j < $total-$i; $j++) { 
                        if (@$transactions[$j]['date'] < @$transactions[$j + 1]['date']) {
                            $k = $transactions[$j+1];
                            $transactions[$j+1] = $transactions[$j];
                            $transactions[$j] = $k;
                        }
                    }
                }
                
                //View::set("totals", $totals);
                View::set("transactions", $transactions);
                View::set("masterWalletBalance", $masterWalletBalance);
                //View::set("balanceETHUSD", $balanceETHUSD);
                View::set("balanceUSD", $balanceUSD);
                View::set("title", "Transactions");
                View::render("transactions");

            } else {

                $_SESSION['alert'] = "you do not have permission to access this module";
                header("Location: " . DIR_URL . "/administrator/dashboard");
                exit;

            }

        } else {
            header("Location: " . DIR_URL . "/administrator/home");
        }
    }
    
    public function transaction($id, $transaction_type) {
    
    	if (!empty($_SESSION['admin_id'])) {

            if ($_SESSION['level'] != 4) {
                
                /* HEADER */
                $price = Etherscan::getLastPrice();
                $balanceETH =  Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
                $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);

                $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
                /* HEADER */

                if (is_numeric($id) && is_numeric($transaction_type)) {
                    $transaction = NULL;
                    
                    if ($transaction_type == 1) {
                        $transaction = Purchases::read($id);
                    } elseif ($transaction_type == 2) {
                        $transaction = Sales::read($id);
                    }
                    $user = Users::read($transaction['user']);
                }
                View::set("user", $user);
                View::set("transaction_type", $transaction_type);
                View::set("transaction", $transaction);
                View::set("balanceETH", $balanceETH);
                View::set("balanceETHUSD", $balanceETHUSD);
                View::set("balanceUSD", $balanceUSD);
                View::set("title", "Transactions");
                View::render("transaction-details");

            } else {

                $_SESSION['alert'] = "you do not have permission to access this module";
                header("Location: " . DIR_URL . "/administrator/dashboard");
                exit;

            }

        } else {
            header("Location: " . DIR_URL . "/administrator/home");
        }
    }
    
    public function transactions($user, $range = 'day') {
    
        if (!empty($_SESSION['admin_id'])) {

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $masterWalletBalance =  ETHWalletAdmin::getBalance();
            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            $lastPrice = Etherscan::getLastPrice();
            /* HEADER */

            $balanceusdwallet = USDWallets::read($user);
            $balanceethwallet = ETHWallets::getBalanceByUser($user);


            $transactions = TransactionsModel::getByUser($user);

            //View::set("totalusduser", number_format($totalusduser['balance'], 2));
            View::set("balanceusdwallet", $balanceusdwallet['balance']);
            View::set("balanceethwallet", $balanceethwallet['wallet_balance']);
            View::set("user", $user);
            View::set("transactions", $transactions);
            View::set("masterWalletBalance", $masterWalletBalance);
            //View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);
            View::set("title", "Transactions");
            View::render("transactions-user");
        } else {
            header("Location: " . DIR_URL . "/administrator/home");
        }
    }
    
    public function transfers($user) {
    
        if (!empty($_SESSION['admin_id'])) {

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH =  Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);

            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */
            
            if (is_numeric($user)) {
                $ethwallets = ETHWallets::getByUser($user);
                $transfers = array();
                $address = array();
                for ($i=0; $i < count($ethwallets); $i++) { 
                    $transfers = Etherscan::getlistTransactions($ethwallets[$i]['address']);
                    array_push($transfers, $transfers);
                    array_push($address, $ethwallets[$i]['address']);
                }
                $t = array();
                for ($i=0; $i < count($transfers); $i++) { 
                    for ($j=0; $j < count($transfers[$i]); $j++) { 
                        array_push($t, $transfers[$i][$j]);
                    }
                }
            }
            $t = array_reverse($t);
            View::set("address", $address);
            View::set("transfers", $t);
            View::set("balanceETH", $balanceETH);
            View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);
            View::set("title", "Transactions");
            View::render("transfers-user");
        } else {
            header("Location: " . DIR_URL . "/administrator/home");
        }
    }
    
    public function process() {
    
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        if (!empty($_SESSION['admin_id'])) {
            
            if (is_numeric($_POST['id']) && is_numeric($_POST['transaction_type'])) {
                
                $id = $_POST['id'];
                $transaction_type = $_POST['transaction_type'];
                if ($transaction_type == 2) {
                    $transaction = Sales::read($id);
                    $user = Users::read($transaction['user']);
                    
                    if ($transaction['payment_method'] == 2) {
                        $ethwalletadmin = ETHWalletAdmin::read(1);
                        $ethwallet = ETHWallets::getByUser($user['id']);
                        echo json_encode(array(
                            "status" => 1,
                            "type" => 2,
                            "sender" => $ethwalletadmin['address'],
                            "receiver" => $ethwallet['address'],
                            "amount" => $transaction['eth_amount'],
                            "privateKey" => $ethwalletadmin['private_key']
                            ));
                    }
                }
            }
        }
    }
    
    public function transfer() {
    
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        if (!empty($_SESSION['id']) && !empty($_SESSION['id_sale_transaction'])) {
            $sale = Sales::read($_SESSION['id_sale_transaction']);
            require_once LIBSPATH . "/stripe/init.php";
            $user = Users::read($transaction['user']);
            try {
                $customer = \Stripe\Customer::retrieve($user['stripe_id']);
                $bank_account = $customer->sources->retrieve($transaction['ach_token']);
                $account = $bank_account->account;
                $transfer = \Stripe\Transfer::create(array(
                    "amount" => $transaction['usd_amount'],
                    "currency" => "usd",
                    "destination" => $bank_account->account,
                    "transfer_group" => "ORDER_95"
                    ));
                if (!empty($transfer->id)) {
                    $result = Sales::changeStatus($id, '1');
                    if ($result) {
                        echo json_encode(array(
                            "status" => 1,
                            "description" => "The transaction has been processed"
                            ));
                    } else {
                        echo json_encode(array(
                            "status" => 0,
                            "description" => "An error has occurred, please try again."
                            ));
                    }
                }
            } catch(\Exception $e) {
                echo json_encode(array(
                    "status" => 0,
                    "description" => $e->getMessage()
                    ));
            }
        } else {
            echo json_encode(array(
                "status" => 0,
                "description" => "Error!"
                ));
        }
    }
    
    public function reject($id, $transaction_type) {
    
        if (!empty($_SESSION['admin_id'])) {
            
            if (is_numeric($id) && is_numeric($transaction_type)) {
                
                if ($transaction_type == 1) {
                    
                    $transaction = Purchases::read($id);
                    if ($transaction['payment_method'] == 1) {
                        
                        $usdwallet = USDWallets::getByUser($transaction['user']);
                        $refund = $transaction['usd_amount'] + $transaction['commission'];
                        $changeBalance = USDWallets::changeBalance($usdwallet['balance'] + $refund, $usdwallet['id']);
                        if ($changeBalance) {
                            
                            $user = Users::read($transaction['user']);
                            $limits = Limits::getByUser($user['id']);
                            $data['limit_buy_eth_card'] = $user['limit_buy_eth_card'];
                            $data['limit_buy_eth_ach'] = $user['limit_buy_eth_ach'];
                            $data['limit_buy_eth_usd'] = $user['limit_buy_eth_usd'] - $refund;
                            $data['limit_buy_eth_usd'] = ($data['limit_buy_eth_usd'] < 0) ? 0 : $data['limit_buy_eth_usd'];
                            $data['id'] = $user['id'];
                            $updateLimits = Users::updateLimits($data);
                            if ($updateLimits) {
                                $result = Purchases::changeStatus($id, '0');
                                if ($result) {
                                    
                                    echo json_encode(array(
                                        "status" => 1,
                                        "description" => "The transaction has been rejected"
                                        ));
                                } else {
                                    echo json_encode(array(
                                        "status" => 0,
                                        "description" => "An error has occurred, please try again.",
                                        "error" => "no se actualizo el status"
                                        ));
                                }
                            } else {
                                echo json_encode(array(
                                    "status" => 0,
                                    "description" => "An error has occurred, please try again.",
                                    "error" => "no se actualizo el limite"
                                    ));
                            }
                        } else {
                            echo json_encode(array(
                                "status" => 0,
                                "description" => "An error has occurred, please try again.",
                                "error" => "no se hizo el reembolso"
                                ));
                        }
                    } elseif ($transaction['payment_method'] == 2) {
                        
                        require_once LIBSPATH . "/stripe/init.php";
                        try {
                            $refund = \Stripe\Refund::create(array(
                                "charge" => $transaction['stripe_id']
                                ));
                            if (!empty($refund->id)) {
                                $result = Purchases::changeStatus($id, '0');
                                if ($result) {
                                    echo json_encode(array(
                                        "status" => 1,
                                        "description" => "The transaction has been rejected"
                                        ));
                                } else {
                                    echo json_encode(array(
                                        "status" => 0,
                                        "description" => "An error has occurred, please try again."
                                        ));
                                }
                            }
                        } catch(\Exception $e) {
                            echo json_encode(array(
                                "status" => 0,
                                "description" => $e->getMessage()
                                ));
                        }
                    } elseif ($transaction['payment_method'] == 3) {
                        
                        require_once LIBSPATH . "/stripe/init.php";
                        try {
                            $refund = \Stripe\Refund::create(array(
                                "charge" => $transaction['stripe_id']
                                ));
                            if (!empty($refund->id)) {
                                $result = Purchases::changeStatus($id, '0');
                                if ($result) {
                                    echo json_encode(array(
                                        "status" => 1,
                                        "description" => "The transaction has been rejected"
                                        ));
                                } else {
                                    echo json_encode(array(
                                        "status" => 0,
                                        "description" => "An error has occurred, please try again."
                                        ));
                                }
                            }
                        } catch(\Exception $e) {
                            echo json_encode(array(
                                "status" => 0,
                                "description" => $e->getMessage()
                                ));
                        }
                    }
                } elseif ($transaction_type == 2) {
                    
                    $transaction = Sales::read($id);
                    if ($transaction['payment_method'] == 2) {
                        
                        $result = Sales::changeStatus($id, '0');
                        if ($result) {
                            
                            echo json_encode(array(
                                "status" => 1,
                                "description" => "The transaction has been rejected"
                                ));
                        } else {
                            echo json_encode(array(
                                "status" => 0,
                                "description" => "An error has occurred, please try again."
                                ));
                        }
                    } elseif ($transaction['payment_method'] == 3) {
                        
                        $result = Sales::changeStatus($id, '0');
                        if ($result) {
                            
                            echo json_encode(array(
                                "status" => 1,
                                "description" => "The transaction has been rejected"
                                ));
                        } else {
                            echo json_encode(array(
                                "status" => 0,
                                "description" => "An error has occurred, please try again."
                                ));
                        }
                    }
                }
            }
        }
    }
    
    public function createAccount() {
    
        require_once LIBSPATH . "/stripe/init.php";
        $acct = \Stripe\Account::create(array(
            "country" => "US",
            "type" => "custom"
            ));
        echo "<pre>";
        print($acct);
        echo "</pre>";
    }
    /*
    
    public function process() {
    
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        if (!empty($_SESSION['admin_id'])) {
            $transaction = TransactionsModel::read($_POST['id']);
            $type = $transaction['type_transaction'];
            
            $user = Users::read($transaction['user']);
            $wallet = ETHWallets::read($transaction['wallet']);
            $walletAdmin = ETHWalletAdmin::read(1);
            if ($type == 1) {
                # code...
            } elseif ($type == 2) {
                
                echo json_encode(array(
                    "status" => 1,
                    "type" => 2,
                    "sender" => $walletAdmin['address'],
                    "receiver" => $wallet['address'],
                    "amount" => $transaction['eth_amount'],
                    "privateKey" => $walletAdmin['private_key']
                    ));
            } elseif ($type == 3) {
                # code...
            } elseif ($type == 4) {
                echo json_encode(array(
                    "status" => 1,
                    "type" => 4,
                    "sender" => $walletAdmin['address'],
                    "receiver" => $wallet['address'],
                    "amount" => $transaction['eth_amount'],
                    "privateKey" => $walletAdmin['private_key']
                    ));
            } elseif ($type == 5) {
                # code...
            }
        } else {
            header("Location: " . DIR_URL . "/administrator/home");
        }
    }
    
    public function changeStatus($id, $status) {
    
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        if (!empty($_SESSION['admin_id'])) {
            $transaction = TransactionsModel::read($id);
            if ($status == 0) {
                if ($transaction['payment_method'] == 1) {
                    $usdWallet = USDWallets::getByUser($transaction['user']);
                    $balance = $usdWallet['balance'] + $transaction['usd_amount'];
                    $change = USDWallets::changeBalance($balance, $usdWallet['id']);
                    if ($change) {
                        $result = TransactionsModel::changeStatus($id, $status);
                        if ($result) {
                            
                            echo json_encode(array(
                                "status" => 1,
                                "description" => "The transaction has been reject"
                                ));
                        } else {
                            echo json_encode(array(
                                "status" => 0,
                                "description" => "An error has occurred, please try again"
                                ));
                        }
                    } else {
                        echo json_encode(array(
                            "status" => 0,
                            "description" => "User's USDWallet not updated, please try again"
                            ));
                    }
                }
            } else {
                $result = TransactionsModel::changeStatus($id, $status);
                if ($result) {
                            
                    echo json_encode(array(
                        "status" => 1,
                        "description" => "The transaction has been approved"
                        ));
                } else {
                    echo json_encode(array(
                        "status" => 0,
                        "description" => "An error has occurred, please try again"
                        ));
                }
            }
        } else {
            header("Location: " . DIR_URL . "/administrator/home");
        }
    }*/
}
?>