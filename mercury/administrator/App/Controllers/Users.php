<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Users as UsersModel,
    \App\Models\Suspensions,
    \App\Models\Etherscan,
    \App\Models\ETHWalletAdmin,
    \App\Models\USDWalletAdmin,
    \App\Models\Limits,
    \App\Models\Documents,
    \App\Models\Country,
    \App\Models\ETHWallets,
    \App\Models\USDWallets,
    \App\Models\RiskVerification,
    \App\Models\ResetG2faLogs,
    \App\Controllers\Mailer;

class Users {
    
    public function index() {
    
        if (!empty($_SESSION['admin_id'])) {

            /* HEADER */
            $masterWalletBalance =  ETHWalletAdmin::getBalance();
            $lastPrice = Etherscan::getLastPrice();
            /* HEADER */


            $users = UsersModel::getAll();
        	View::set("users", $users);
            View::set("masterWalletBalance", $masterWalletBalance);
        	View::set("title", "Users");
            View::render("all-users");
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }
    }
    
    public function add() {
    
        if (!empty($_SESSION['admin_id'])) {

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH = Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);

            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */

            $countrys = Country::getAll();
            
            View::set("countrys", $countrys);
            View::set("balanceETH", $balanceETH);
            View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);
            View::set("title", "Create User");
            View::render("create_user");
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }
    }
    
    public function details($user) {
    
        if (!empty($_SESSION['admin_id'])) {

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH = Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);

            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */

            $suspension = Suspensions::getByUser($user);
            $limits = Limits::getByUser($user);
            $user = UsersModel::read($user);
            View::set("limits", $limits);
            View::set("user", $user);
            View::set("suspension", $suspension);
            View::set("balanceETH", $balanceETH);
            View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);
            View::set("title", "Details of " . $user['name'] . " " . $user['last_name']);
            View::render("details-user");
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }
    }
    
    public function create() {

        if (!empty($_SESSION['admin_id'])) {

            if (!empty($_POST['name']) &&
                !empty($_POST['last_name']) &&
                !empty($_POST['email']) &&
                !empty($_POST['code_country']) &&
                !empty($_POST['phone']) &&
                !empty($_POST['password']) &&
                !empty($_POST['confirmPassword']) &&
                !empty($_POST['status'])) {
                
                $validate = UsersModel::readByEmail($_POST['email']);

                if (empty($validate)) {
                    
                    if ($_POST['password'] == $_POST['confirmPassword']) {
                        
                        $data = array();
                        $data['name'] = htmlspecialchars($_POST['name'], ENT_QUOTES);
                        $data['last_name'] = htmlspecialchars($_POST['last_name'], ENT_QUOTES);
                        $data['email'] = htmlspecialchars($_POST['email'], ENT_QUOTES);
                        $data['phonecode'] = htmlspecialchars($_POST['code_country'], ENT_QUOTES);
                        $data['phone'] = str_replace("-", "", htmlspecialchars($_POST['phone'], ENT_QUOTES));
                        $data['password'] = md5($_POST['password']);
                        $data['token'] = md5($_POST['email'] . time()); hash('sha256', md5($_POST['email'] . md5($_POST['password']) . time()));
                        $data['date_admission'] = time();
                        $data['type_user'] = 1;
                        $data['status'] = htmlspecialchars($_POST['status'], ENT_QUOTES);;

                        require_once LIBSPATH . "/authy/init.php";

                        $createAuthy = $authy_api->registerUser($_POST['email'], $_POST['phone'], $_POST['code_country']);

                        if ($createAuthy->ok()) {
                            $data['authy_id'] = $createAuthy->id();
                        } else {

                            foreach ($createAuthy->errors() as $field => $message) {

                                $_SESSION['alert'] = $message;
                                header("Location: " . DIR_URL . "/register");
                                exit;

                            }

                        }

                        $user = UsersModel::create($data);
                        if ($user > 0) {
                            
                            $url = "https://node.mercury.cash/create";
                            $post = "user=" . $user . "&password=" . $data['password'] . "";
                            $con = curl_init();
                            curl_setopt($con, CURLOPT_URL, $url);
                            curl_setopt($con, CURLOPT_HTTPGET, FALSE);
                            curl_setopt($con, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($post)));
                            curl_setopt($con, CURLOPT_POST, 1);
                            curl_setopt($con, CURLOPT_POSTFIELDS, $post);
                            curl_setopt($con, CURLOPT_HEADER, FALSE);
                            curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
                            $response = curl_exec($con);
                            curl_close($con);
                            $response = json_decode($response);

                            if (!empty($response)) {

                                $data['json_wallet'] = $response->json_wallet;
                                $data['private_key'] = $response->private_key;
                                $data['public_key'] = $response->public_key;
                                $data['address'] = $response->address;
                                $data['primary'] = 1;
                                $data['user'] = $response->user;
                                $ethwallet = ETHWallets::create($data);

                            }

                            $data['buy_eth_card'] = 200;
                            $data['buy_eth_ach'] = 500;
                            $data['buy_eth_usdwallet'] = 15000;
                            $data['sell'] = 15000;
                            $data['user'] = $user;

                            Limits::create($data);

                            $data['address'] = hash('sha256', md5($user . $data['email'] . time()));
                            $data['balance'] = 0.00;
                            $data['user'] = $user;

                            USDWallets::create($data);

                            if ($data['status'] == 1) {
                                
                                $message = "<h3>Congratulations</h3>";
                                $message .= "<p>You now have an account on our platform</p>";
                                $message .= "<p>Your login password is: " . $_POST['password'] . "</p>";
                                $message .= "<p>For security reasons it is advisable to remove this email</p>";
                                Mailer::sendMail($data['email'], "Mercury Cash - Congratulations", $message, "Go to Mercury Cash", DIR_URL . "/login");

                            } elseif ($data['status'] == 2) {
                                
                                $message = "<p>Thank you for registering with Mercury Cash, in order to validate your account we will need to verify your email.</p>";
                                $message .= "<p>Once done you will be redirected to the Dashboard where you can finish the validation process attaching a DNI, Driver&#39;s License or Passport document.</p>";
                                $message .= "<p>To proceed please click on the button below</p>";
                                Mailer::sendMail($data['email'], "Mercury Cash - Email Validation Process", $message, "Go to Mercury Cash", DIR_URL . "/validate/email/" . $data['token'] . "/" . $data['email']);

                            } elseif ($data['status'] == 3) {
                                
                                $message = "<p>Thank you for registering with Mercury Cash, in order to validate your account we will need to verify your email.</p>";
                                $message .= "<p>Once done you will be redirected to the Dashboard where you can finish the validation process attaching a DNI, Driver&#39;s License or Passport document.</p>";
                                $message .= "<p>To proceed please click on the button below</p>";
                                Mailer::sendMail($data['email'], "Mercury Cash - Email Validation Process", $message, "Go to Mercury Cash", DIR_URL . "/login");

                            }

                            $_SESSION['alert'] = "The client " . $data['name'] . " " . $data['last_name'] . " has been created successfully";
                            header("Location: " . DIR_URL . "/administrator/users/add");
                            exit;

                        } else {

                            $_SESSION['alert'] = "An error occurred while trying to register, please try again.";
                            header("Location: " . DIR_URL . "/administrator/users/add");
                            exit;

                        }

                    } else {

                        $_SESSION['alert'] = "Password does not match";
                        header("Location: " . DIR_URL . "/administrator/users/add");
                        exit;

                    }

                } else {

                    $_SESSION['alert'] = "E-mail already exists";
                    header("Location: " . DIR_URL . "/administrator/users/add");
                    exit;

                }
            
            } else {

                $_SESSION['alert'] = "There are empty fields that are required";
                header("Location: " . DIR_URL . "/administrator/users/add");
                exit;
            }

        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;

        }

    }

    public function update() {

        if (!empty($_SESSION['admin_id'])) {
            
            if (!empty($_POST['id']) && is_numeric($_POST['id'])) {
                
                $id = $_POST['id'];

                $user = UsersModel::read($id);
                $limits = Limits::getByUser($id);

                $data = array();
                $data['name'] = (!empty($_POST['name'])) ? htmlspecialchars($_POST['name'], ENT_QUOTES) : $user['name'];
                $data['last_name'] = (!empty($_POST['last_name'])) ? htmlspecialchars($_POST['last_name'], ENT_QUOTES) : $user['last_name'];
                $data['email'] = $user['email'];
                $data['birthdate'] = (!empty($_POST['birthdate'])) ? htmlspecialchars($_POST['birthdate'], ENT_QUOTES) : $user['birthdate'];
                $data['occupation'] = (!empty($_POST['occupation'])) ? htmlspecialchars($_POST['occupation'], ENT_QUOTES) : $user['occupation'];
                $data['profession'] = (!empty($_POST['profession'])) ? htmlspecialchars($_POST['profession'], ENT_QUOTES) : $user['profession'];
                $data['ssn'] = $user['ssn'];
                $data['address'] = (!empty($_POST['address'])) ? htmlspecialchars($_POST['address'], ENT_QUOTES) : $user['address'];
                $data['city'] = (!empty($_POST['city'])) ? htmlspecialchars($_POST['city'], ENT_QUOTES) : $user['city'];
                $data['province_id'] = (!empty($_POST['province_id']) && is_numeric($_POST['province_id'])) ? $_POST['province_id'] : $user['province_id'];
                $data['country_id'] = (!empty($_POST['country_id']) && is_numeric($_POST['country_id'])) ? $_POST['country_id'] : $user['country_id'];
                $data['zip_code'] = (!empty($_POST['zip_code'])) ? htmlspecialchars($_POST['zip_code'], ENT_QUOTES) : $user['zip_code'];
                $data['id'] = $id;

                $data['buy_eth_card'] = (!empty($_POST['buy_eth_card']) && is_numeric($_POST['buy_eth_card'])) ? $_POST['buy_eth_card'] : $limits['buy_eth_card'];
                $data['buy_eth_ach'] = (!empty($_POST['buy_eth_ach']) && is_numeric($_POST['buy_eth_ach'])) ? $_POST['buy_eth_ach'] : $limits['buy_eth_ach'];
                $data['buy_eth_usdwallet'] = (!empty($_POST['buy_eth_usdwallet']) && is_numeric($_POST['buy_eth_usdwallet'])) ? $_POST['buy_eth_usdwallet'] : $limits['buy_eth_usdwallet'];
                $data['user'] = $id;

                $data['status'] =  $user['status'];
                if (is_numeric($_POST['status']) || $_POST['status'] == 0) {
                    $data['status'] = (!empty($_POST['status']) || $_POST['status'] == 0) ? $_POST['status'] : $user['status'];
                }

                $result = UsersModel::update($data);
                if ($result) {
                    
                    $_SESSION['alert'] = "The data was successfully updated";

                    Limits::updateByUser($data);
                    if (!$result) {
                        $_SESSION['alert'] .= "<br>There was an error updating the limits";
                    }

                    header("Location: " . DIR_URL . "/administrator/users/details/" . $id);
                    exit;

                } else {

                    $_SESSION['alert'] = "An error has occurred, please try again.";
                    header("Location: " . DIR_URL . "/administrator/users/details/" . $id);
                    exit;

                }
            
            } else {

                $_SESSION['alert'] = "Has sent empty or invalid data to process";
                header("Location: " . DIR_URL . "/administrator/users/details/" . $_POST['id']);
                exit;

            }

        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;

        }

    }


    
    public function _update() {
    
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");

        $user = UsersModel::read($_POST['id']);
        $limits = Limits::getByUser($_POST['id']);
        $data = array();
        $data['id'] = $_POST['id'];
        $data['user'] = $_POST['id'];
        $data['name'] = (!empty($_POST['name'])) ? htmlspecialchars($_POST['name'], ENT_QUOTES) : $user['name'];
        $data['last_name'] = (!empty($_POST['last_name'])) ? htmlspecialchars($_POST['last_name'], ENT_QUOTES) : $user['last_name'];
        $data['email'] = (!empty($_POST['email'])) ? htmlspecialchars($_POST['email'], ENT_QUOTES) : $user['email'];
        $data['password'] = $user['password'];
        $data['address'] = (!empty($_POST['address'])) ? htmlspecialchars($_POST['address'], ENT_QUOTES) : $user['address'];
        $data['city'] = (!empty($_POST['city'])) ? htmlspecialchars($_POST['city'], ENT_QUOTES) : $user['city'];
        $data['country'] = (!empty($_POST['country'])) ? htmlspecialchars($_POST['country'], ENT_QUOTES) : $user['country'];
        $data['zip_code'] = (!empty($_POST['zip_code'])) ? htmlspecialchars($_POST['zip_code'], ENT_QUOTES) : $user['zip_code'];
        $data['status'] = (!empty($_POST['status'])) ? htmlspecialchars($_POST['status'], ENT_QUOTES) : $user['status'];
        $data['buy_eth_card'] = (!empty($_POST['buy_eth_card'])) ? htmlspecialchars($_POST['buy_eth_card'], ENT_QUOTES) : $limits['buy_eth_card'];
        $data['buy_eth_ach'] = (!empty($_POST['buy_eth_ach'])) ? htmlspecialchars($_POST['buy_eth_ach'], ENT_QUOTES) : $limits['buy_eth_ach'];
        $data['buy_eth_usdwallet'] = (!empty($_POST['buy_eth_usdwallet'])) ? htmlspecialchars($_POST['buy_eth_usdwallet'], ENT_QUOTES) : $limits['buy_eth_usdwallet'];
        if (!empty($_POST['birthdate'])) {
            $birthdate = str_replace("/", "-", $_POST['birthdate']);
            $birthdate = strtotime($birthdate);
            $data['birthdate'] = $birthdate;
        
        } else {

            $data['birthdate'] = $user['birthdate'];
        }
        Limits::updateByUser($data);
        $result = UsersModel::update($data);
        if ($user['status'] != 0) {
            if (!empty($_POST['suspension']) && $_POST['status'] == 0) {
                $data['description'] = htmlspecialchars($_POST['suspension'], ENT_QUOTES);
                $data['date'] = time();
                $data['user'] = $_POST['id'];
                Suspensions::create($data);
            }
        }
        if ($result) {
            echo json_encode(array(
                "status" => 1,
                "description" => "User successfully update"
                ));
        
        } else {

            echo json_encode(array(
                "status" => 0,
                "description" => "An error occurred while trying to update, please try again"
                ));
        }
    }
    
    public function suspend($user) {
    
        if (!empty($_SESSION['admin_id'])) {

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH = Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);

            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */

            $wallet = Wallets::getByUser($user);
            $balance = Etherscan::getBalance($wallet['address']);
            $balance = $balance / 1000000000000000000;
            $user = UsersModel::read($user);
            View::set("user", $user);
            View::set("wallet", $wallet);
            View::set("balance", $balance);
            View::set("balanceETH", $balanceETH);
            View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);
            View::set("title", "Suspend user");
            View::render("suspend_user");
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }
    }
    
    public function suspendUser() {
    
        if (!empty($_SESSION['admin_id'])) {

            if (!empty($_POST['description'])) {
                $description = htmlspecialchars($_POST['description'], ENT_QUOTES);
                $data = array(
                    "description" => $description,
                    "date" => time(),
                    "user" => $_POST['user']
                    );
                $result = Suspensions::create($data);
                if ($result) {
                    $result = UsersModel::changeStatus($_POST['user'], 0);
                    if ($result) {
                        header("Location: " . DIR_URL . "/administrator/users");
                    }
                }
            }
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }
    }
    
    /*
    public function delete($user) {
    
        if (!empty($_SESSION['admin_id'])) {

            $result = Wallets::deleteByUser($user);
            if ($result) {
                $result = UsersModel::delete($user);
                if ($result) {
                    echo json_encode(array(
                        "status" => 1,
                        "description" => "User deleted successfully"
                        ));
                
                } else {

                    echo json_encode(array(
                        "status" => 0,
                        "description" => "There was an error deleting the user, try again"
                        ));
                }
            
            } else {

                echo json_encode(array(
                    "status" => 0,
                    "description" => "There was an error deleting the user, try again"
                    ));
            }
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }
    }
    */
    
    public function documents($user) {
    
        if (!empty($_SESSION['admin_id'])) {

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH = Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);

            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */

            if (is_numeric($user)) {
                $documents = Documents::getByUser($user);
                $user = UsersModel::read($user);
                View::set("user", $user);
                View::set("documents", $documents);
                View::set("balanceETH", $balanceETH);
                View::set("balanceETHUSD", $balanceETHUSD);
                View::set("balanceUSD", $balanceUSD);
                View::set("title", "Documents");
                View::render("documents");
            }
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }
    }
    
    public function loadDocument($document, $user) {
    
        if (!empty($_SESSION['admin_id'])) {

            if (is_numeric($document) && is_numeric($user)) {
                $_SESSION['user'] = UsersModel::read($user);
                $_SESSION['document'] = Documents::read($document);
                header("Location: " . DIR_URL . "/administrator/users/documents/" . $user);
                exit;
            }
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;

        }
    }
    
    public function downloadDocument($document, $user) {
    
        if (!empty($_SESSION['admin_id'])) {

            if (is_numeric($document) && is_numeric($user)) {
                $document = Documents::read($document);
                $mime = "image/jpeg";
                if (pathinfo("public/identifications/" . $document['name'], PATHINFO_EXTENSION) == "pdf") {
                    $mime = "application/pdf";
                }
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . $document['original_name'] . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize("public/identifications/" . $document['name']));
                readfile("public/identifications/" . $document['name']);
                exit;
            }
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;

        }
    }
    
    public function approveDocument() {
    
        if (!empty($_SESSION['admin_id'])) {

            if (is_numeric($_POST['document']) && is_numeric($_POST['user'])) {
                $user = UsersModel::read($_POST['user']);
                $result = Documents::changeStatus($_POST['document'], 1);
                if ($result) {
                    UsersModel::changeStatus($user['id'], 1);

                    Mailer::documentsApproved($user['email']);

                    $_SESSION['alert'] = "An email has been sent to " . $user['name'] . " " . $user['last_name'] . " to report on the status of this document";
                    header("Location: " . DIR_URL . "/administrator/users/documents/" . $user['id']);
                    exit;
                
                } else {

                    $_SESSION['alert'] = "An error has occurred, try again or contact the developer";
                    header("Location: " . DIR_URL . "/administrator/users/documents/" . $user['id']);
                    exit;
                }
                
            
            } else {

                $_SESSION['alert'] = "The data you are trying to send is invalid";
                header("Location: " . DIR_URL . "/administrator/users/documents/" . $user['id']);
                exit;
            }
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }
    }
    
    public function denyDocument() {
    
        if (!empty($_SESSION['admin_id'])) {

            if (is_numeric($_POST['document']) && is_numeric($_POST['user'])) {
                $description = htmlspecialchars($_POST['description'], ENT_QUOTES);
                $user = UsersModel::read($_POST['user']);
                $result = Documents::changeStatus($_POST['document'], 0);
                if ($result) {
                    Documents::addDescription($description, $_POST['document']);

                    $dataMail = array(
                        "to" => $user['email'],
                        "message" => $description
                        );

                    Mailer::documentsRejected($dataMail);

                    $_SESSION['alert'] = "An email has been sent to " . $user['name'] . " " . $user['last_name'] . " to report on the status of his document";
                    header("Location: " . DIR_URL . "/administrator/users/documents/" . $user['id']);
                    exit;
                
                } else {

                    $_SESSION['alert'] = "An error has occurred, try again or contact the developer";
                    header("Location: " . DIR_URL . "/administrator/users/documents/" . $user['id']);
                    exit;
                }
                
            
            } else {

                $_SESSION['alert'] = "The data you are trying to send is invalid";
                header("Location: " . DIR_URL . "/administrator/users/documents/" . $user['id']);
                exit;
            }
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }
    }

    public function riskVerification($user) {
    
        if (!empty($_SESSION['admin_id'])) {

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH = Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);

            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */

            $user = UsersModel::read($user);
            $risk = RiskVerification::getByUser($user['id']);

            View::set("risk", $risk);
            View::set("user", $user);
            View::set("balanceETH", $balanceETH);
            View::set("balanceETHUSD", $balanceETHUSD);
            View::set("balanceUSD", $balanceUSD);
            View::set("title", "Risk Verification");
            View::render("riskVerification");
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }
    }

    public function riskVerificationUpdate() {
    
        if (!empty($_SESSION['admin_id'])) {

            if (!empty($_POST['user']) && is_numeric($_POST['user'])) {
                
                $data = array();
                $data['average_risk_level'] = (!empty($_POST['average_risk_level']) && is_numeric($_POST['average_risk_level'])) ? $_POST['average_risk_level'] : NULL;
                $data['world_check_match_strenght'] = (!empty($_POST['world_check_match_strenght']) && is_numeric($_POST['world_check_match_strenght'])) ? $_POST['world_check_match_strenght'] : NULL;
                $data['world_check_source_type'] = (!empty($_POST['world_check_source_type']) && is_numeric($_POST['world_check_source_type'])) ? $_POST['world_check_source_type'] : NULL;
                $data['ofacs_list'] = (!empty($_POST['ofacs_list']) && is_numeric($_POST['ofacs_list'])) ? $_POST['ofacs_list'] : NULL;
                $data['naics_code'] = (!empty($_POST['naics_code']) && is_numeric($_POST['naics_code'])) ? $_POST['naics_code'] : NULL;
                $data['notes'] = (!empty($_POST['notes'])) ? htmlspecialchars($_POST['notes'], ENT_QUOTES) : NULL;
                $data['user'] = $_POST['user'];

                if (!empty($_POST['id']) && is_numeric($_POST['id'])) {
                    $data['id'] = $_POST['id'];

                    $risk = RiskVerification::read($data['id']);
                    if (!empty($risk['id'])) {
                        
                        $result = RiskVerification::update($data);
                        if ($result) {
                            
                            $_SESSION['alert'] = "The data was successfully updated";
                            header("Location: " . DIR_URL . "/administrator/users/riskVerification/" . $data['user']);
                            exit;

                        } else {

                            $_SESSION['alert'] = "An error has occurred, please try again.";
                            header("Location: " . DIR_URL . "/administrator/users/riskVerification/" . $data['user']);
                            exit;

                        }

                    }

                } else {

                    $result = RiskVerification::create($data);
                    if ($result) {
                        
                        $_SESSION['alert'] = "The data was successfully updated";
                        header("Location: " . DIR_URL . "/administrator/users/riskVerification/" . $data['user']);
                        exit;

                    } else {

                        $_SESSION['alert'] = "An error has occurred, please try again.";
                        header("Location: " . DIR_URL . "/administrator/users/riskVerification/" . $data['user']);
                        exit;

                    }

                }

            } else {

                $_SESSION['alert'] = "Is trying to send data that is incorrect";
                header("Location: " . DIR_URL . "/administrator/users/riskVerification/" . $_POST['user']);
                exit;

            }
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }
    }

    public function resetg2fa($user) {

        if (!empty($_SESSION['admin_id'])) {

            /* HEADER */
            $price = Etherscan::getLastPrice();
            $balanceETH = Etherscan::fromWei(Etherscan::getBalance($_SESSION['master_address']));
            $balanceETHUSD = number_format($balanceETH * $price->ethusd, 2);

            $balanceUSD = number_format(USDWalletAdmin::read(1)['balance'], 2);
            /* HEADER */

            if (is_numeric($user)) {
                
                $user = UsersModel::read($user);
                $logs = ResetG2faLogs::getAllByUser($user['id']);

                View::set("logs", $logs);
                View::set("user", $user);
                View::set("balanceETH", $balanceETH);
                View::set("balanceETHUSD", $balanceETHUSD);
                View::set("balanceUSD", $balanceUSD);
                View::set("title", "Reset G2FA");
                View::render("reset_g2fa");

            } else {

                header("Location: " . DIR_URL . "/administrator/home");
                exit;

            }
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }

    }

    public function resetg2faMethod() {

        if (!empty($_SESSION['admin_id'])) {

            if (!empty($_POST['reason']) && !empty($_POST['user']) && is_numeric($_POST['user'])) {
                $reason = htmlspecialchars($_POST['reason'], ENT_QUOTES);

                $result = UsersModel::resetG2fa($_POST['user']);
                if ($result) {
                    
                    $data = array();
                    $data['reason'] = $reason;
                    $data['date'] = date("Y-m-d H:i:s", time());
                    $data['customer'] = $_POST['user'];
                    $data['administrator'] = $_SESSION['admin_id'];
                    $result = ResetG2faLogs::create($data);
                    if ($result) {
                        
                        $_SESSION['alert'] = "google authenticator token has been successfully reset";
                        header("Location: " . DIR_URL . "/administrator/users/resetg2fa/" . $_POST['user']);
                        exit;

                    } else {

                        $_SESSION['alert'] = "an error has occurred, try again or contact the developer";
                        header("Location: " . DIR_URL . "/administrator/users/resetg2fa/" . $_POST['user']);
                        exit;

                    }

                    $_SESSION['alert'] = "the google authenticator token has successfully been reset, however an error occurred while registering it in the system";
                    header("Location: " . DIR_URL . "/administrator/users/resetg2fa/" . $_POST['user']);
                    exit;

                } else {

                    $_SESSION['alert'] = "an error has occurred, try again or contact the developer";
                    header("Location: " . DIR_URL . "/administrator/users/resetg2fa/" . $_POST['user']);
                    exit;

                }

            } else {

                $_SESSION['alert'] = "you are passing invalid or empty data";
                header("Location: " . DIR_URL . "/administrator/users/resetg2fa/" . $_POST['user']);
                exit;

            }
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }

    }

    public function usdwallets() {

        if (!empty($_SESSION['admin_id'])) {

            /* HEADER */
            $masterWalletBalance =  ETHWalletAdmin::getBalance();
            $lastPrice = Etherscan::getLastPrice();
            /* HEADER */


            $usdwallets = USDWallets::getAll();
            View::set("usdwallets", $usdwallets);
            View::set("masterWalletBalance", $masterWalletBalance);
            View::set("title", "USD Wallets");
            View::render("usdwallets");
        
        } else {

            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }
    
    }

    public function download_statement_usdwallets() {

        if (!empty($_SESSION['admin_id'])) {
            
            require_once LIBSPATH . "/phpexcel/init.php";
            $usdwallets = USDWallets::getAll();

            $excel = \PHPExcel_IOFactory::load('template.xlsx');
            $excel->setActiveSheetIndex(0);
            
            $count = 3;
            foreach ($usdwallets as $usdwallet) {
                $excel->getActiveSheet()->insertNewRowBefore($count, 1);
                $excel->getActiveSheet()
                    ->setCellValue('A' . $count, $usdwallet['id'])
                    ->setCellValue('B' . $count, $usdwallet['name'])
                    ->setCellValue('C' . $count, $usdwallet['last_name'])
                    ->setCellValue('D' . $count, $usdwallet['balance']);
                $count++;
            }

            $excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
            $excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
            $excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

            // make table headers
            $excel->getActiveSheet()
                ->setCellValue('A2', 'ID')
                ->setCellValue('B2', 'NAME')
                ->setCellValue('C2', 'LAST NAME')
                ->setCellValue('D2', 'BALANCE');

            // marging the title
            $excel->getActiveSheet()->mergeCells('A1:D1');

            // aligning
            $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal('right');

            // styling
            $excel->getActiveSheet()->getStyle('A1')->applyFromArray(
                array(
                    'font' => array(
                        'size' => 24
                    )
                )
            );

            $excel->getActiveSheet()->getStyle('A2:D2')->applyFromArray(
                array(
                    'font' => array(
                        'bold' => true
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                )
            );

            $date = date('Y_m_d');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="report_usdwallets_' . $date . '.xlsx"');  //File name extension was wrong
            header('Cache-Control: max-age=0');

            $file = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
            $file->save('php://output');

        } else {
            header("Location: " . DIR_URL . "/administrator/home");
            exit;
        }
    
    }
}
?>