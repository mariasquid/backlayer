<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/administrator/public/assets/css/metricsgraphics.css">
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/metricsgraphics.min.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6">
								<div class="block block-first">
									<div class="block-header">
										<span class="text-yellow">Transaction</span> information
										<a href="<?php echo DIR_URL; ?>/administrator/operationsForm/generate/1/<?php echo $transaction['id']; ?>" target="_blank" class="btn-header">Report</a>
									</div>
									<div class="block-body">
										<?php if (!empty($_SESSION['alert'])) { ?>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="alertGrey">
                                                    <?php echo $_SESSION['alert']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php unset($_SESSION['alert']); ?>
                                        <?php } ?>
										<table class="data-transactions">
											<tbody>
												<tr>
													<td class="tags-transaction">Transaction ID</td>
													<td><?php echo $transaction['id']; ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Wire Code</td>
													<td><?php echo $transaction['wire_code']; ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">USD Amount</td>
													<td><?php echo "$". number_format($transaction['usd_amount'], 2); ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Fee</td>
													<td><?php echo "$". number_format($transaction['commission'], 2); ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Price With Fee</td>
													<td><?php echo "$". number_format(($transaction['usd_amount'] + $transaction['commission']), 2); ?></td>
												</tr>
												<?php if (!is_null($bankAccount)) { ?>
												<tr>
													<td class="tags-transaction">Bank Account</td>
													<td><?php echo $bankAccount['number']; ?></td>
												</tr>
												<?php } ?>
												<tr>
													<td class="tags-transaction">Status</td>
													<td>
														<?php if ($transaction['status'] == 0) { ?>
														<span class="status-reject">Rejected</span>
														<?php } elseif ($transaction['status'] == 1) { ?>
														<span class="status-success">Approved</span>
														<?php } elseif ($transaction['status'] == 2) { ?>
														<span class="status-pending">Pending</span>
														<?php } ?>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="block block-first">
									<div class="block-header">
										<span class="text-yellow">Client</span> information
									</div>
									<div class="block-body">
										<table class="data-transactions">
											<tbody>
												<tr>
													<td class="tags-transaction">Full Name</td>
													<td><?php echo $user['name'] . " " . $user['last_name']; ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Email</td>
													<td><?php echo $user['email']; ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Status</td>
													<td>
														<?php if ($user['status'] == 0) { ?>
														<span class="status-reject">Rejected</span>
														<?php } elseif ($user['status'] == 1) { ?>
														<span class="status-success">Approved</span>
														<?php } elseif ($user['status'] == 2) { ?>
														<span class="status-pending">Pending</span>
														<?php } ?>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<?php if (!is_null($bankAccount)) { ?>
						<div class="row">
							<div class="col-md-6">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Bank</span> Transfer Information
									</div>
									<div class="block-body">
										<table class="data-transactions">
											<tbody>
												<tr>
													<td class="tags-transaction">Ammount to Transfer</td>
													<td><?php echo "$" . number_format($transaction['usd_amount'], 2); ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Beneficiary Name</td>
													<td><?php echo $bankAccount['beneficiary_name']; ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Bank Account</td>
													<td><?php echo $bankAccount['number']; ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">ABA</td>
													<td><?php echo $bankAccount['routing']; ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Address</td>
													<td><?php echo $bankAccount['address']; ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Phone Number</td>
													<td><?php echo "+" . $user['phonecode'] . " " . $user['phone']; ?></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
						<?php if ($transaction['status'] == 2) { ?>
						<div class="row" style="margin-top: 25px;">
							<div class="col-md-4 col-md-offset-2">
								<a href="<?php echo DIR_URL; ?>/administrator/withdraws/approve/<?php echo $transaction['id']; ?>" class="btn-mercury-yellow">APPROVE</a>
							</div>
							<div class="col-md-4">
								<a href="<?php echo DIR_URL; ?>/administrator/withdraws/reject/<?php echo $transaction['id']; ?>" class="btn-mercury-black">REJECT</a>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>