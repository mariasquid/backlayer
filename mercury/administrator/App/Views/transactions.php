<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/administrator/public/assets/css/metricsgraphics.css">
		<link href="<?php echo DIR_URL; ?>/administrator/public/vendor/plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/metricsgraphics.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo DIR_URL; ?>/administrator/public/vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
		<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/transactions.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">All</span> transactions
										<div class="group-btn-block">
											<a href="<?php echo DIR_URL; ?>/administrator/transactions/index/day" id="new-wallet" class="btn-header <?php echo ($_SERVER['REQUEST_URI'] == '/administrator/transactions' || $_SERVER['REQUEST_URI'] == '/administrator/transactions/index/day') ? 'active' : ''; ?>">Day</a>
											<a href="<?php echo DIR_URL; ?>/administrator/transactions/index/week" id="new-wallet" class="btn-header <?php echo ($_SERVER['REQUEST_URI'] == '/administrator/transactions/index/week') ? 'active' : ''; ?>">Week</a>
											<a href="<?php echo DIR_URL; ?>/administrator/transactions/index/month" id="new-wallet" class="btn-header <?php echo ($_SERVER['REQUEST_URI'] == '/administrator/transactions/index/month') ? 'active' : ''; ?>">Month</a>
											<a href="<?php echo DIR_URL; ?>/administrator/transactions/index/year" id="new-wallet" class="btn-header <?php echo ($_SERVER['REQUEST_URI'] == '/administrator/transactions/index/year') ? 'active' : ''; ?>">Year</a>
										</div>
									</div>
									<div class="block-body">
										<table class="table-m" id="datatable">
											<thead>
												<tr>
													<th>ID</th>
													<th>Date</th>
													<th>Wire Code</th>
													<th>Customer</th>
													<th>Rate ETH</th>
													<th>ETH Qty.</th>
													<th>USD Amount</th>
													<th>Fee</th>
													<th>Status</th>
													<th>Provider</th>
													<th>Price</th>
													<th>Fee</th>
													<th>Gross Income</th>
													<th>TX Type</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($transactions as $transaction) { ?>
												<tr>
													<?php if ($transaction['type_transaction'] == 1) { ?>
													<td><a href="<?php echo DIR_URL; ?>/administrator/purchases/purchase/<?php echo $transaction['id']; ?>"><?php echo $transaction['id']; ?></a></td>
													<?php } elseif ($transaction['type_transaction'] == 2) { ?>
													<td><a href="<?php echo DIR_URL; ?>/administrator/sales/sale/<?php echo $transaction['id']; ?>"><?php echo $transaction['id']; ?></a></td>
													<?php } elseif ($transaction['type_transaction'] == 4) { ?>
													<td><a href="<?php echo DIR_URL; ?>/administrator/withdraws/withdraw/<?php echo $transaction['id']; ?>"><?php echo $transaction['id']; ?></a></td>
													<?php } elseif ($transaction['type_transaction'] == 5) { ?>
													<td><a href="<?php echo DIR_URL; ?>/administrator/deposits/deposit/<?php echo $transaction['id']; ?>"><?php echo $transaction['id']; ?></a></td>
													<?php } ?>
													<td><?php echo date("m-d-Y H:i:s", $transaction['date']); ?></td>
													<td><?php echo (isset($transaction['wire_code'])) ? $transaction['wire_code'] : ''; ?></td>
													<td><a href="<?php echo DIR_URL; ?>/administrator/users/details/<?php echo $transaction['user']; ?>"><?php echo $transaction['name']; ?> <?php echo $transaction['last_name']; ?></a></td>
													<td><?php echo (!empty($transaction['eth_value'])) ? "$" . $transaction['eth_value'] : ""; ?></td>
													<td class="totaleth"><?php echo (!empty($transaction['eth_amount'])) ? number_format($transaction['eth_amount'], 5) : ""; ?></td>
													<td><?php echo (!empty($transaction['usd_amount'])) ? "$<span class='totalusd'>" . $transaction['usd_amount'] . "</span>" : ""; ?></td>
													<td><?php echo (!empty($transaction['commission'])) ? "$<span class='totalcommission'>" . $transaction['commission'] . "" : "</span>"; ?></td>
													<?php if ($transaction['status'] == 0) { ?>
													<td><span class="label label-danger mr5 mb10 ib lh15">Rejected</span></td>
													<?php } elseif ($transaction['status'] == 1) { ?>
													<td><span class="label label-success mr5 mb10 ib lh15">Approved</span></td>
													<?php } elseif ($transaction['status'] == 2) { ?>
													<td><span class="label label-warning mr5 mb10 ib lh15">pending</span></td>
													<?php } ?>
													<td><?php echo (!empty($transaction['provider'])) ? $transaction['provider'] : ''; ?></td>
													<td><?php echo (!empty($transaction['price'])) ? "$<span class='totalprice'>" . $transaction['price'] . "</span>" : ''; ?></td>
													<td><?php echo (!empty($transaction['fee'])) ? "$<span class='totalfee'>" . $transaction['fee'] . "</span>" : ''; ?></td>
													<?php if ($transaction['type_transaction'] == 1) { ?>
													<td><?php echo (!empty($transaction['gross_income'])) ? "$<span class='totalgross_income'>" . $transaction['gross_income'] . "</span>" : ''; ?></td>
													<td>Buy</td>
													<?php echo (5 == 5) ? '<td><a href="' . DIR_URL . '/administrator/purchases/purchase/' . $transaction['id'] . '">Details</a></td>' : ''; ?>
													<?php } elseif ($transaction['type_transaction'] == 2) { ?>
													<td><?php echo (!empty($transaction['gross_income'])) ? "$<span class='totalgross_income'>" . $transaction['gross_income'] . "</span>" : ''; ?></td>
													<td>Sell</td>
													<?php echo (5 == 5) ? '<td><a href="' . DIR_URL . '/administrator/sales/sale/' . $transaction['id'] . '">Details</a></td>' : ''; ?>
													<?php } elseif ($transaction['type_transaction'] == 4) { ?>
													<td><?php echo (!empty($transaction['commission'])) ? "$<span class='totalgross_income'>" . $transaction['commission'] . "</span>" : ''; ?></td>
													<td>Withdraw</td>
													<?php echo (5 == 5) ? '<td><a href="' . DIR_URL . '/administrator/withdraws/withdraw/' . $transaction['id'] . '">Details</a></td>' : ''; ?>
													<?php } elseif ($transaction['type_transaction'] == 5) { ?>
													<td><?php echo (!empty($transaction['commission'])) ? "$<span class='totalgross_income'>" . $transaction['commission'] . "</span>" : ''; ?></td>
													<td>Deposit</td>
													<?php echo (5 == 5) ? '<td><a href="' . DIR_URL . '/administrator/deposits/deposit/' . $transaction['id'] . '">Details</a></td>' : ''; ?>
													<?php } ?>
												</tr>
												<?php } ?>
											</tbody>
										</table>
										<table id="footerTable" class="table-m">
											<thead>
												<tr>
													<th>ID</th>
													<th>Date</th>
													<th>Wire Code</th>
													<th>Customer</th>
													<th>Rate ETH</th>
													<th>ETH Qty.</th>
													<th>USD Amount</th>
													<th>Fee</th>
													<th>Status</th>
													<th>Provider</th>
													<th>Price</th>
													<th>Fee</th>
													<th>Gross Income</th>
													<th>TX Type</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td id="totaleth"></td>
													<td id="totalusd"></td>
													<td id="totalcommission"></td>
													<td></td>
													<td></td>
													<td id="totalprice"></td>
													<td id="totalfee"></td>
													<td id="totalgross_income"></td>
													<td></td>
													<?php echo (5 == 5) ? '<th></th>' : ''; ?>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>