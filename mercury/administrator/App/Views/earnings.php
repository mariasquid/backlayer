<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/administrator/public/assets/css/metricsgraphics.css">
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/metricsgraphics.min.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/earnings.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-3 col-lg-offset-4">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Earnings</span>
									</div>
									<div class="block-body">
										<?php if (!empty($_SESSION['alert'])) { ?>
										<div class="row">
											<div class="col-md-12">
												<div class="alertGrey">
													<?php echo $_SESSION['alert']; ?>
												</div>
											</div>
										</div>
										<?php unset($_SESSION['alert']); ?>
										<?php } ?>
										<form action="<?php echo DIR_URL; ?>/administrator/settings/earningsUpdate" method="POST" autocomplete="off">
											<div class="row">
												<div class="col-md-12">
													<label for="price_etherscan">Price Etherscan</label>
													<div class="form-elements">
														<input type="text" name="price_etherscan" id="price_etherscan" disabled="disabled" value="<?php echo "$" . $priceEth . " USD"; ?>">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-6">
													<label for="buy">Buy percentage</label>
													<div class="form-elements">
														<input type="text" name="buy" id="buy" value="<?php echo $earnings['buy']; ?>" style="padding-right: 40px;">
														<span class="percentage">%</span>
													</div>
												</div>
												<div class="col-xs-6">
													<label for="price_buy">Price in "Buy"</label>
													<div class="form-elements">
														<input type="text" name="price_buy" id="price_buy" disabled="disabled" value="">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-6">
													<label for="sell">Sell percentage</label>
													<div class="form-elements">
														<input type="text" name="sell" id="sell" value="<?php echo $earnings['sell']; ?>" style="padding-right: 40px;">
														<span class="percentage">%</span>
													</div>
												</div>
												<div class="col-xs-6">
													<label for="price_sell">Price in "Sell"</label>
													<div class="form-elements">
														<input type="text" name="price_sell" id="price_sell" disabled="disabled" value="">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-md-offset-3">
													<button type="submit" class="btn-mercury-black" style="float: right;">UPDATE</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>