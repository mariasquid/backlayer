<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/dashboard.js?v=<?php echo time(); ?>"></script>
	</head>
	<body>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-2">
								<div id="balance_master" class="block" data-toggle="modal" data-target="#detailsMaster">
									<div class="block-header">
										<span class="text-yellow">Master ETH</span> Wallet Balance 
									</div>
									<div class="block-body" style="text-align: center;">
										<h3><?php echo number_format($masterWalletBalance, 5); ?> ETH</h3>
										<h3>$<?php echo number_format(($masterWalletBalance * $lastPrice), 2); ?></h3>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Total</span> Ethereum 
									</div>
									<div class="block-body" style="text-align: center;">
										<h3><?php echo $ethBalanceMercury; ?> ETH</h3>
										<h3>$<?php echo number_format(($ethBalanceMercury * $lastPrice), 2); ?></h3>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Total</span> USD 
									</div>
									<div class="block-body" style="text-align: center;">
										<h2>$<?php echo $usdBalanceMercury; ?></h2>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Total</span> Users 
									</div>
									<div class="block-body" style="text-align: center;">
										<h2><?php echo $users; ?> Users</h2>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Price</span> ETH 
									</div>
									<div class="block-body" style="text-align: center;">
										<h2 id="price_eth">$<?php echo $lastPrice; ?></h2>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="block">
									<div class="block-header">
										<a href="#" id="lastDay"><span class="text-yellow">24</span></a> |
										<a href="#" id="lastWeek"><span class="text-yellow">7 Days</span></a> |
										<a href="#" id="lastMonth"><span class="text-yellow">30 Days</span></a> |
										<a href="#" id="lastYear"><span class="text-yellow">365 Days</span></a> 
									</div>
									<div class="block-body" style="text-align: center;">
										<h2 id="transactionsTotal" style="margin: 5px;"></h2>
										<p>Transactions</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="block" style="margin-top: 30px;">
									<div class="block-header">
										<span class="text-yellow">Pending</span> documents
									</div>
									<div class="block-body">
										<table class="table-m" id="datatable" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>Customer</th>
													<th>Document Type</th>
													<th>Document</th>
													<th>Date</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($documents as $document) { ?>
												<tr>
													<td><a href="<?php echo DIR_URL; ?>/administrator/users/details/<?php echo $document['user']; ?>"><?php echo $document['nameUser'] . " " . $document['lastnameUser']; ?></a></td>
													<td>
														<?php
														if ($document['type'] == 1) {
															echo "Passport";
														} elseif ($document['type'] == 2) {
															echo "Bank statement";
														} elseif ($document['type'] == 3) {
															echo "Driver's licence";
														} elseif ($document['type'] == 4) {
															echo "ID confirmation photo";
														}
														?>
													</td>
													<td><a href="<?php echo DIR_URL; ?>/administrator/users/documents/<?php echo $document['user']; ?>"><?php echo $document['original_name']; ?></a></td>
													<td><?php echo date("m-d-Y H:i:s", $document['date']); ?></td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>