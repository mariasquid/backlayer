<!DOCTYPE html>

<html lang="es">

	<head>

		<meta charset="UTF-8">

		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

		<title><?php echo $title; ?></title>

		<?php include "inc/styles.php"; ?>

		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/administrator/public/assets/css/metricsgraphics.css">

		<?php include "inc/scripts.php"; ?>

		<script src="https://d3js.org/d3.v4.min.js"></script>

		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/metricsgraphics.min.js"></script>

	</head>

	<body>

		<div class="wrapper">

			<?php include "inc/aside.php"; ?>

			<div class="main-panel">

				<?php include "inc/header.php"; ?>

				<div class="content">

					<div class="container-fluid">

						<div class="row">

							<div class="col-md-12">

								<div class="block">

									<div class="block-header">

										<span class="text-yellow">Logins</span>

									</div>

									<div class="block-body">

										<table class="table-m" id="datatable" cellspacing="0" width="100%">

											<thead>

												<tr>

													<th>ID</th>

													<th>Date</th>

													<th>Browser</th>

													<th>IP</th>

													<th>Near</th>

												</tr>

											</thead>

											<tbody>

												<?php foreach ($logs as $log) { ?>

											<tr>

												<td><?php echo $log['id']; ?></td>

												<td><?php echo date("d/m/Y H:i:s", $log['date']); ?></td>

												<td><?php echo $log['browser']; ?></td>

												<td><?php echo $log['ip']; ?></td>

												<td><?php echo $log['near']; ?></td>

											</tr>

											<?php } ?>

											</tbody>

										</table>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php include "inc/close.php"; ?>

	</body>

</html>