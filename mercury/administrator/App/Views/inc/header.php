<nav class="navbar navbar-default navbar-fixed">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><b>Master ETH Wallet Balance:</b> <?php echo $masterWalletBalance; ?> ETH - $0.00 USD | <b>Master USD Wallet Balance:</b> $0.00 USD</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo $_SESSION['admin_avatar']; ?>" id="avatar" class="img-responsive hidden-xs"> <?php echo $_SESSION['admin_name']; ?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo DIR_URL; ?>/settings">Profile</a></li>
                        <li><a href="<?php echo DIR_URL; ?>/settings/security">Settings</a></li>
                        <li><a href="<?php echo DIR_URL; ?>/home">Log out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>