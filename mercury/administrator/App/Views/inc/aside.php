<div class="sidebar" data-color="purple" data-image="<?php echo DIR_URL; ?>/administrator/public/img/aside.jpg">
	<div class="sidebar-wrapper">
		<div class="logo">
			<a href="<?php echo DIR_URL; ?>/administrator/dashboard" class="simple-text">
				<img src="<?php echo DIR_URL; ?>/administrator/public/img/logo-ligth.png" alt="" class="img-responsive">
			</a>
		</div>
		<ul class="nav">
			<li <?php echo ($_SERVER['QUERY_STRING'] == "url=dashboard") ? 'class="active"' : ''; ?>>
				<a href="<?php echo DIR_URL; ?>/administrator/dashboard">
					<i class="pe-7s-graph"></i>
					<p>Dashboard</p>
				</a>
			</li>
			<li <?php echo ($_SERVER['QUERY_STRING'] == "url=users") ? 'class="active"' : ''; ?>>
				<a href="<?php echo DIR_URL; ?>/administrator/users">
					<i class="pe-7s-user"></i>
					<p>Customers</p>
				</a>
			</li>
			<li <?php echo ($_SERVER['QUERY_STRING'] == "url=transactions") ? 'class="active"' : ''; ?>>
				<a href="<?php echo DIR_URL; ?>/administrator/transactions">
					<i class="pe-7s-repeat"></i>
					<p>Transactions</p>
				</a>
			</li>
			<li <?php echo ($_SERVER['QUERY_STRING'] == "url=settings") ? 'class="active"' : ''; ?>>
				<a href="<?php echo DIR_URL; ?>/administrator/settings">
					<i class="pe-7s-config"></i>
					<p>Settings</p>
				</a>
			</li>
			<!--
			<li <?php //echo ($_SERVER['QUERY_STRING'] == "url=buy") ? 'class="active"' : ''; ?>>
				<a href="icons.html">
					<i class="pe-7s-science"></i>
					<p>Icons</p>
				</a>
			</li>
			<li <?php //echo ($_SERVER['QUERY_STRING'] == "url=buy") ? 'class="active"' : ''; ?>>
				<a href="maps.html">
					<i class="pe-7s-map-marker"></i>
					<p>Maps</p>
				</a>
			</li>
			<li <?php //echo ($_SERVER['QUERY_STRING'] == "url=buy") ? 'class="active"' : ''; ?>>
				<a href="notifications.html">
					<i class="pe-7s-bell"></i>
					<p>Notifications</p>
				</a>
			</li>
			<li class="active-pro">
				<a href="upgrade.html">
					<i class="pe-7s-rocket"></i>
					<p>Upgrade to PRO</p>
				</a>
			</li>
			-->
		</ul>
	</div>
</div>