<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/administrator/public/assets/css/metricsgraphics.css">
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/metricsgraphics.min.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="tabs-m">
									<ul>
										<?php echo ($_SESSION['level'] == 1 || $_SESSION['level'] == 4) ? '<li class="active"><a href="' . DIR_URL . '/administrator/users/details/' . $user['id'] . '">Details</a></li>' : ''; ?>
										<?php echo ($_SESSION['level'] == 1 || $_SESSION['level'] == 2) ? '<li><a href="' . DIR_URL . '/administrator/transactions/transactions/' . $user['id'] . '">Transactions</a></li>' : ''; ?>
										<?php echo ($_SESSION['level'] == 1 || $_SESSION['level'] == 3) ? '<li><a href="' . DIR_URL . '/administrator/users/documents/' . $user['id'] . '">Documents</a></li>' : ''; ?>
										<?php if ($_SESSION['level'] == 1) { ?>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/riskVerification/<?php echo $user['id']; ?>">Risk Verification</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/ctrReport/<?php echo $user['id']; ?>">CTR Report</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/resetg2fa/<?php echo $user['id']; ?>">Reset Google 2FA</a></li>
										<?php } ?>
									</ul>
								</div>
								<div class="block block-first">
									<div class="block-header">
										<span class="text-yellow">User details</span> <?php echo $user['name'] . " " . $user['last_name']; ?>
									</div>
									<div class="block-body">
										<?php if (!empty($_SESSION['alert'])) { ?>
										<div class="row">
											<div class="col-md-8 col-md-offset-2">
												<div class="alertGrey">
													<?php echo $_SESSION['alert']; ?>
												</div>
											</div>
										</div>
										<?php unset($_SESSION['alert']); ?>
										<?php } ?>
										<form action="<?php echo DIR_URL; ?>/administrator/users/update" method="POST" autocomplete="off">
											<div class="row">
												<div class="col-md-10">
													<div class="row">
														<div class="col-md-2">
															<div class="form-elements">
																<input type="text" name="name" id="name" value="<?php echo $user['name']; ?>" placeholder="First Name">
															</div>
														</div>
														<div class="col-md-2">
															<div class="form-elements">
																<input type="text" name="last_name" id="last_name" value="<?php echo $user['last_name']; ?>" placeholder="Last Name">
															</div>
														</div>
														<div class="col-md-2">
															<div class="form-elements">
																<input type="text" name="birthdate" id="birthdate" value="<?php echo (!is_null($user['birthdate'])) ? $user['birthdate'] : ''; ?>" placeholder="YYYY-MM-DD">
															</div>
														</div>
														<div class="col-md-2">
															<div class="form-elements">
																<input type="email" name="email" id="email" value="<?php echo $user['email']; ?>" placeholder="email" disabled="disabled">
															</div>
														</div>
														<div class="col-md-2">
															<div class="form-elements">
																<input type="text" name="phone" id="phone" value="<?php echo $user['phonecode']; ?> <?php echo $user['phone']; ?>" disabled="disabled">
															</div>
														</div>
														<div class="col-md-2">
															<div class="form-elements">
																<input type="text" name="ndocument" id="ndocument" value="<?php echo (substr($user['ndocument'], 0, 1) == "L") ? "DL: " . substr($user['ndocument'], 2, strlen($user['ndocument'])) : $user['ndocument']; ?>" disabled="disabled">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-8">
															<div class="form-elements">
																<textarea name="address" id="address" placeholder="Address" style="height: 148px;"><?php echo $user['address']; ?></textarea>
															</div>
														</div>
														<div class="col-md-4">
															<div class="form-elements">
																<input type="text" name="city" id="city" value="<?php echo $user['city']; ?>" placeholder="City">
															</div>
															<div class="form-elements">
																<input type="text" name="country" id="country" value="<?php echo $user['country_id']; ?>" placeholder="Country">
															</div>
															<div class="form-elements">
																<input type="text" name="zip_code" id="zip_code" value="<?php echo $user['zip_code']; ?>" placeholder="Zip Code">
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-2">
													<img src="<?php echo DIR_URL; ?>/administrator/public/avatars/<?php echo $user['avatar']; ?>" alt="<?php echo $user['name'] . ' ' . $user['last_name']; ?>" class="img-responsive">
												</div>
											</div>
											<div class="row">
												<div class="col-md-3">
													<div class="form-elements">
														<select name="status" id="status">
															<?php if ($user['status'] == 0) { ?>
															<option value="0">Inactive</option>
															<option value="1">Active</option>
															<option value="2">Without validating the email</option>
															<option value="3">Without validating the identity document</option>
															<?php } elseif ($user['status'] == 1) { ?>
															<option value="1">Active</option>
															<option value="0">Inactive</option>
															<option value="2">Without validating the email</option>
															<option value="3">Without validating the identity document</option>
															<?php } elseif ($user['status'] == 2) { ?>
															<option value="2">Without validating the email</option>
															<option value="0">Inactive</option>
															<option value="1">Active</option>
															<option value="3">Without validating the identity document</option>
															<?php } elseif ($user['status'] == 3) { ?>
															<option value="3">Without validating the identity document</option>
															<option value="0">Inactive</option>
															<option value="1">Active</option>
															<option value="2">Without validating the email</option>
															<?php } ?>
														</select>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-elements">
														<input type="text" name="profession" id="profession" value="<?php echo $user['profession']; ?>" placeholder="Profession">
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-elements">
														<input type="text" name="occupation" id="occupation" value="<?php echo $user['occupation']; ?>" placeholder="Ocupation">
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-elements">
														<input type="text" name="ssn" id="ssn" value="<?php echo $user['ssn']; ?>" placeholder="SSN" disabled="disabled">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-10">
													<div class="form-elements">
														<span style="margin-right: 30px;">Limits</span>
														debit/credit card
														<input type="text" name="buy_eth_card" id="buy_eth_card" value="<?php echo $limits['buy_eth_card']; ?>" style="width: auto; margin-right: 10px;">
														Ach
														<input type="text" name="buy_eth_ach" id="buy_eth_ach" value="<?php echo $limits['buy_eth_ach']; ?>" style="width: auto; margin-right: 10px;">
														USD Wallet
														<input type="text" name="buy_eth_usdwallet" id="buy_eth_usdwallet" value="<?php echo $limits['buy_eth_usdwallet']; ?>" style="width: auto; margin-right: 10px;">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-3 col-md-offset-4">
													<input type="hidden" name="id" id="id" value="<?php echo $user['id']; ?>">
													<button type="submit" class="btn-mercury-black">UPDATE</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>