<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/elevatezoom/jquery.elevatezoom.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/documents.js"></script>
	</head>
	<body>
		<!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-target=".bs-example-modal-sm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="<?php echo DIR_URL; ?>/administrator/users/denyDocument" method="POST">
                    	<div class="modal-header">
                    	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    	    <h4 class="modal-title" id="myModalLabel">Attention</h4>
                    	</div>
                    	<div class="modal-body" style="text-align: center;">
                    	    <div class="form-elements">
                    	    	<label for="description">Description</label>
                    	    	<textarea name="description" id="description" style="border: solid 1px #ccc;"></textarea>
                    	    </div>
                    	</div>
                    	<div class="modal-footer">
                    		<input type="hidden" id="document" name="document" value="<?php echo $_SESSION['document']['id']; ?>">
							<input type="hidden" id="user" name="user" value="<?php echo $user['id']; ?>">
                    	    <button type="button" class="btn btn-default closeModal" data-dismiss="modal">Close</button>
                    	    <button type="submit" class="btn btn-default">Deny</button>
                    	</div>
                    </form>
                </div>
            </div>
        </div>
		<div class="loading">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="tabs-m">
									<ul>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/details/<?php echo $user['id']; ?>">Details</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/transactions/transactions/<?php echo $user['id']; ?>">Transactions</a></li>
										<li class="active"><a href="<?php echo DIR_URL; ?>/administrator/users/documents/<?php echo $user['id']; ?>">Documents</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/riskVerification/<?php echo $user['id']; ?>">Risk Verification</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/ctrReport/<?php echo $user['id']; ?>">CTR Report</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/resetg2fa/<?php echo $user['id']; ?>">Reset Google 2FA</a></li>
									</ul>
								</div>
								<div class="block block-first">
									<div class="block-header">
										<span class="text-yellow">Documents</span> <?php echo $user['name'] . " " . $user['last_name']; ?>
									</div>
									<div class="block-body">
										<?php if (!empty($_SESSION['alert'])) { ?>
										<div class="row">
											<div class="col-md-8 col-md-offset-2">
												<div class="alertGrey">
													<?php echo $_SESSION['alert']; ?>
												</div>
											</div>
										</div>
										<?php unset($_SESSION['alert']); ?>
										<?php } ?>
										<table class="table-m" id="datatable" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>Name</th>
													<th>Date</th>
													<th>Type</th>
													<th>Size</th>
													<th>Satus</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($documents as $document) { ?>
												<tr>
													<td><?php echo $document['original_name']; ?></td>
													<td><?php echo date("d/m/Y", $document['date']); ?></td>
													<?php if ($document['type'] == 1) { ?>
													<td>Identity Document</td>
													<?php } elseif ($document['type'] == 2) { ?>
													<td>Proof of residence</td>
													<?php } elseif ($document['type'] == 3) { ?>
													<td>Identity Document</td>
													<?php } elseif ($document['type'] == 4) { ?>
													<td>ID confirmation</td>
													<?php } ?>
													<td><?php echo $document['size']; ?></td>
													<?php if ($document['status'] == 0) { ?>
													<td><span class="status-reject">Deny</span></td>
													<?php } elseif ($document['status'] == 1) { ?>
													<td><span class="status-success">Approved</span></td>
													<?php } elseif ($document['status'] == 2) { ?>
													<td><span class="status-pending">Pending</span></td>
													<?php } ?>
													<td>
														<?php if (pathinfo($document['name'], PATHINFO_EXTENSION) != "pdf") { ?>
														<a href="<?php echo DIR_URL; ?>/administrator/users/loadDocument/<?php echo $document['id']; ?>/<?php echo $user['id']; ?>">View</a> |
														<?php } else { ?>
														<a href="<?php echo DIR_URL; ?>/administrator/users/loadDocument/<?php echo $document['id']; ?>/<?php echo $user['id']; ?>">User details</a> |
														<?php } ?>
														<a href="<?php echo DIR_URL; ?>/administrator/users/downloadDocument/<?php echo $document['id']; ?>/<?php echo $user['id']; ?>" target="_blank">Download</a>
													</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<?php if (!empty($_SESSION['document'])) { ?>
						<div class="row">
							<div class="col-md-12">
								<div class="block" style="margin-top: 60px;">
									<div class="block-header">
										<span class="text-yellow">Documents</span> <?php echo $user['name'] . " " . $user['last_name']; ?>
									</div>
									<div class="block-body">
										<div class="row">
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-6">
														<ul class="data-transactions">
															<li><span class="tags-transaction">Fullname</span> <?php echo $_SESSION['user']['name'] . " " . $_SESSION['user']['last_name']; ?></li>
															<li><span class="tags-transaction">E-mail</span> <?php echo $_SESSION['user']['email']; ?></li>
															<li><span class="tags-transaction">Phone</span> <?php echo "+" . $_SESSION['user']['phonecode'] . " " . $_SESSION['user']['phone']; ?></li>
															<li><span class="tags-transaction">Birthdate</span> <?php echo (!is_null($_SESSION['user']['birthdate'])) ? date("d/m/Y", $_SESSION['user']['birthdate']) : ""; ?></li>
															<li><span class="tags-transaction">Occupation</span> <?php echo (!is_null($_SESSION['user']['occupation'])) ? $_SESSION['user']['occupation'] : ""; ?></li>
															<li><span class="tags-transaction">Profession</span> <?php echo (!is_null($_SESSION['user']['profession'])) ? $_SESSION['user']['profession'] : ""; ?></li>
															<li><span class="tags-transaction">SSN</span> <?php echo $_SESSION['user']['ssn']; ?></li>
														</ul>
													</div>
													<div class="col-md-6">
														<ul class="data-transactions">
															<li><span class="tags-transaction">Address</span> <?php echo $_SESSION['user']['address']; ?></li>
															<li><span class="tags-transaction">State</span> <?php echo $_SESSION['user']['state']; ?></li>
															<li><span class="tags-transaction">City</span> <?php echo $_SESSION['user']['city']; ?></li>
															<li><span class="tags-transaction">Country</span> <?php echo $_SESSION['user']['country']; ?></li>
															<li><span class="tags-transaction">ZIP Code</span> <?php echo $_SESSION['user']['zip_code']; ?></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<?php if (pathinfo($_SESSION['document']['name'], PATHINFO_EXTENSION) != "pdf") { ?>
												<img id="documentImage" src="<?php echo DIR_URL; ?>/administrator/public/identifications/<?php echo $_SESSION['document']['name']; ?>" data-zoom-image="<?php echo DIR_URL; ?>/administrator/public/identifications/<?php echo $_SESSION['document']['name']; ?>" width="100%" class="img-responsive">
												<?php } ?>
												<div class="row" style="margin-top: 20px;">
													<div class="col-md-6">
														<form action="<?php echo DIR_URL; ?>/administrator/users/approveDocument" method="POST">
															<input type="hidden" id="document" name="document" value="<?php echo $_SESSION['document']['id']; ?>">
															<input type="hidden" id="user" name="user" value="<?php echo $user['id']; ?>">
															<button type="submit" class="btn-mercury-yellow">Approve</button>
														</form>
													</div>
													<div class="col-md-6">
														<button type="button" class="btn-mercury-black" data-toggle="modal" data-target="#myModal">Deny</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php
						unset($_SESSION['document']);
						unset($_SESSION['user']);
						?>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>