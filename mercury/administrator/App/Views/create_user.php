<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/administrator/public/assets/css/metricsgraphics.css">
		<link href="<?php echo DIR_URL; ?>/administrator/public/vendor/plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/metricsgraphics.min.js"></script>
		<script src="<?php echo DIR_URL; ?>/administrator/public/vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?php echo DIR_URL; ?>/administrator/public/vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/users.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="tabs-m">
									<ul>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users">List</a></li>
										<li class="active"><a href="<?php echo DIR_URL; ?>/administrator/users/add">Create</a></li>
									</ul>
								</div>
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Create</span> User
									</div>
									<div class="block-body">
										<?php if (!empty($_SESSION['alert'])) { ?>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="alertGrey">
                                                    <?php echo $_SESSION['alert']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php unset($_SESSION['alert']); ?>
                                        <?php } ?>
										<form action="<?php echo DIR_URL; ?>/administrator/users/create" method="POST">
											<div class="row">
												<div class="col-md-6 col-md-offset-3">
													<div class="row">
														<div class="col-md-6">
															<div class="form-elements">
																<input type="text" name="name" id="name" placeholder="First Name">
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-elements">
																<input type="text" name="last_name" id="last_name" placeholder="Last Name">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-elements">
																<select name="code_country" id="code_country">
																	<option value="">Country</option>
																	<?php foreach ($countrys as $country) { ?>
																	<option value="<?php echo $country['phonecode']; ?>"><?php echo $country['nicename'] . " (+" .  $country['phonecode'] . ")"; ?></option>
																	<?php } ?>
																</select>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-elements">
																<input type="text" id="phone" name="phone" placeholder="Phone">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="form-elements">
																<input type="email" name="email" id="email" placeholder="email">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-elements">
																<input type="password" name="password" id="password" placeholder="Password">
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-elements">
																<input type="password" name="confirmPassword" id="confirmPassword" placeholder="Repeat Password">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="form-elements">
																<select name="status" id="status">
																	<option value="2">Without validating the email</option>
																	<option value="3">Without validating the identity document</option>
																	<option value="0">Inactive</option>
																	<option value="1">Active</option>
																</select>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-3 col-md-offset-4">
															<button type="submit" class="btn-mercury-black">CREATE</button>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>