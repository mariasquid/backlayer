<!DOCTYPE html>

<html lang="es">

	<head>

		<meta charset="UTF-8">

		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

		<title><?php echo $title; ?></title>

		<?php include "inc/styles.php"; ?>

		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/administrator/public/assets/css/metricsgraphics.css">

		<?php include "inc/scripts.php"; ?>

		<script src="https://d3js.org/d3.v4.min.js"></script>

		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/metricsgraphics.min.js"></script>

	</head>

	<body>

		<div class="wrapper">

			<?php include "inc/aside.php"; ?>

			<div class="main-panel">

				<?php include "inc/header.php"; ?>

				<div class="content">

					<div class="container-fluid">

						<div class="row">

							<div class="col-md-12">

								<div class="block">

									<div class="block-header">

										<span class="text-yellow">Devices</span>

										<a href="<?php echo DIR_URL; ?>/administrator/logsLogin/logs/<?php echo $user; ?>" class="btn-header">Logins</a>

									</div>

									<div class="block-body">

										<table class="table-m" id="datatable" cellspacing="0" width="100%">

											<thead>

												<tr>

													<th>ID</th>

													<th>Registration date</th>

													<th>Due date</th>

													<th>Remembered for 15 days</th>

													<th>Browser</th>

													<th>IP</th>

													<th>Near</th>

													<th>Status</th>

													<th>Action</th>

												</tr>

											</thead>

											<tbody>

												<?php foreach ($devices as $device) { ?>

											<tr>

												<td><?php echo $device['id']; ?></td>

												<td><?php echo ($device['date'] > 0) ? date("d/m/Y H:i:s", $device['date']) : ''; ?></td>

												<td><?php echo ($device['disable_tfa'] > 0) ? date("d/m/Y H:i:s", $device['disable_tfa']) : ''; ?></td>

												<td><?php echo ($device['remember'] == 1) ? 'Yes' : 'No'; ?></td>

												<td><?php echo $device['browser']; ?></td>

												<td><?php echo $device['ip']; ?></td>

												<td><?php echo $device['near']; ?></td>

												<td><?php echo ($device['status'] == 1) ? '<span class="label label-success mr5 mb10 ib lh15">Active</span>' : '<span class="label label-danger mr5 mb10 ib lh15">inactive</span>'; ?></td>

												<td>

													<?php if ($device['status'] == 1) { ?>

													<div class="btn-group">

														<a href="<?php echo DIR_URL; ?>/administrator/activities/disableDevice/<?php echo $device['id']; ?>" class="btn btn-danger disable">to disable</a>

														<!--<a href="button" class="btn btn-danger dark">remove</a>-->

													</div>

													<?php } ?>

												</td>

											</tr>

											<?php } ?>

											</tbody>

										</table>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php include "inc/close.php"; ?>

	</body>

</html>