<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/administrator/public/assets/css/metricsgraphics.css">
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/metricsgraphics.min.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="tabs-m">
									<ul>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/details/<?php echo $user['id']; ?>">Details</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/transactions/transactions/<?php echo $user['id']; ?>">Transactions</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/documents/<?php echo $user['id']; ?>">Documents</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/riskVerification/<?php echo $user['id']; ?>">Risk Verification</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/ctrReport/<?php echo $user['id']; ?>">CTR Report</a></li>
										<li class="active"><a href="<?php echo DIR_URL; ?>/administrator/users/resetg2fa/<?php echo $user['id']; ?>">Reset Google 2FA</a></li>
									</ul>
								</div>
								<?php if (!is_null($user['google_2fa_code'])) { ?>
								<div class="block block-first">
									<div class="block-header">
										<span class="text-yellow">Reset</span> Token
									</div>
									<div class="block-body">
										<form action="<?php echo DIR_URL; ?>/administrator/users/resetg2faMethod" method="POST">
											<div class="row">
												<div class="col-md-4 col-md-offset-4">
													<div class="form-elements">
														<input name="google_2fa_code" id="google_2fa_code" disabled="disabled" value="<?php echo $user['google_2fa_code']; ?>"></input>
													</div>
												</div>
												<div class="col-md-4 col-md-offset-4">
													<div class="form-elements">
														<textarea name="reason" id="reason" placeholder="Reason"></textarea>
													</div>
												</div>
												<div class="col-md-2 col-md-offset-5">
													<div class="form-elements">
														<input type="hidden" name="user" value="<?php echo $user['id']; ?>">
														<button type="submit" class="btn-mercury-black">RESET TOKEN GOOGLE 2FA</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
								<?php } ?>
								<div class="block <?php echo (is_null($user['google_2fa_code'])) ? 'block-first' : ''; ?>">
									<div class="block-header">
										<span class="text-yellow">Token</span> Reset Logs
									</div>
									<div class="block-body">
										<?php if (!empty($_SESSION['alert'])) { ?>
										<div class="row">
											<div class="col-md-8 col-md-offset-2">
												<div class="alertGrey">
													<?php echo $_SESSION['alert']; ?>
												</div>
											</div>
										</div>
										<?php unset($_SESSION['alert']); ?>
										<?php } ?>
										<table class="table-m">
											<thead>
												<tr>
													<th>Reason</th>
													<th>Date</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($logs as $log) { ?>
												<tr>
													<td><?php echo $log['reason']; ?></td>
													<td><?php echo $log['date']; ?></td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>