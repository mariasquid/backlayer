<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/administrator/public/assets/css/metricsgraphics.css">
		<link href="<?php echo DIR_URL; ?>/administrator/public/vendor/plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
		<?php include "inc/scripts.php"; ?>
		<script src="https://d3js.org/d3.v4.min.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/metricsgraphics.min.js"></script>
		<script src="<?php echo DIR_URL; ?>/administrator/public/vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?php echo DIR_URL; ?>/administrator/public/vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/transactions.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="collapse navbar-collapse">
							<ul class="nav navbar-nav navbar-left sub-menu">
								<li class="sub-active">
									<a href="<?php echo DIR_URL; ?>/administrator/users">
										List
									</a>
								</li>
								<li>
									<a href="<?php echo DIR_URL; ?>/administrator/users/add">
										Create
									</a>
								</li>
							</ul>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="block block-first">
									<div class="block-header">
										<span class="text-yellow">All transactions</span>
									</div>
									<div class="block-body">
										<table class="table-m" id="datatable">
											<thead>
												<tr>
													<th>Date</th>
													<th>ETH Value</th>
													<th>ETH Amount</th>
													<th>USD Amount</th>
													<th>Commission</th>
													<th>Status</th>
													<th>Transaction</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($transactions as $transaction) { ?>
												<tr>
													<td><?php echo date("d/m/Y H:i:s", $transaction['date']); ?></td>
													<td>$<?php echo number_format($transaction['eth_value'], 2); ?></td>
													<td><?php echo number_format($transaction['eth_amount'], 3); ?> ETH</td>
													<td>$<?php echo number_format($transaction['usd_amount'], 2); ?></td>
													<td>$<?php echo number_format($transaction['commission'], 2); ?></td>
													<?php if ($transaction['status'] == 0) { ?>
													<td><span class="label label-danger mr5 mb10 ib lh15">Rejected</span></td>
													<?php } elseif ($transaction['status'] == 1) { ?>
													<td><span class="label label-success mr5 mb10 ib lh15">Approved</span></td>
													<?php } elseif ($transaction['status'] == 2) { ?>
													<td><span class="label label-warning mr5 mb10 ib lh15">pending</span></td>
													<?php } ?>
													<?php if ($transaction['type_transaction'] == 1) { ?>
													<td>Purchase</td>
													<td><a href="<?php echo DIR_URL; ?>/administrator/Purchases/purchase/<?php echo $transaction['id']; ?>">Details</a></td>
													<?php } elseif ($transaction['type_transaction'] == 2) { ?>
													<td>Sell</td>
													<td><a href="<?php echo DIR_URL; ?>/administrator/sales/sale/<?php echo $transaction['id']; ?>">Details</a></td>
													<?php } ?>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>