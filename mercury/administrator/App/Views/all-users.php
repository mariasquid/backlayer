<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/administrator/public/assets/css/metricsgraphics.css">
		<link href="<?php echo DIR_URL; ?>/administrator/public/vendor/plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/metricsgraphics.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo DIR_URL; ?>/administrator/public/vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
		<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/users.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="tabs-m">
									<ul>
										<li class="active"><a href="<?php echo DIR_URL; ?>/administrator/users">List</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/add">Create</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/usdwallets">USD Wallets</a></li>
									</ul>
								</div>
								<div class="block block-first">
									<div class="block-header">
										<span class="text-yellow">All</span> users
									</div>
									<div class="block-body">
										<table class="table-m" id="datatable" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>Name</th>
													<th>Last Name</th>
													<th>email</th>
													<th>Satus</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($users as $user) { ?>
												<tr>
													<td><?php echo $user['name']; ?></td>
													<td><?php echo $user['last_name']; ?></td>
													<td><?php echo $user['email']; ?></td>
													<?php if ($user['status'] == 0) { ?>
													<td><span class="label label-danger mr5 mb10 ib lh15">Inactive</span></td>
													<?php } elseif ($user['status'] == 1) { ?>
													<td><span class="label label-success mr5 mb10 ib lh15">Active</span></td>
													<?php } elseif ($user['status'] == 2) { ?>
													<td><span class="label label-warning mr5 mb10 ib lh15">Without validating email</span></td>
													<?php } elseif ($user['status'] == 3) { ?>
													<td><span class="label label-warning mr5 mb10 ib lh15">Without validating identity document</span></td>
													<?php } ?>
													<td>
														<div class="btn-group" style="color: #c5a902">
															<a href="<?php echo DIR_URL; ?>/administrator/users/details/<?php echo $user['id']; ?>">Details</a> | <a href="<?php echo DIR_URL; ?>/administrator/activities/activities/<?php echo $user['id']; ?>">Activities</a>
														</div>
													</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>