<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/administrator/public/assets/css/metricsgraphics.css">
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/metricsgraphics.min.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="tabs-m">
									<ul>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/details/<?php echo $user['id']; ?>">Details</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/transactions/transactions/<?php echo $user['id']; ?>">Transactions</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/documents/<?php echo $user['id']; ?>">Documents</a></li>
										<li class="active"><a href="<?php echo DIR_URL; ?>/administrator/users/riskVerification/<?php echo $user['id']; ?>">Risk Verification</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/ctrReport/<?php echo $user['id']; ?>">CTR Report</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/resetg2fa/<?php echo $user['id']; ?>">Reset Google 2FA</a></li>
									</ul>
								</div>
								<div class="block block-first">
									<div class="block-header">
										<span class="text-yellow">Risk Verification</span> <?php echo $user['name'] . " " . $user['last_name']; ?>
									</div>
									<div class="block-body">
										<?php if (!empty($_SESSION['alert'])) { ?>
										<div class="row">
											<div class="col-md-8 col-md-offset-2">
												<div class="alertGrey">
													<?php echo $_SESSION['alert']; ?>
												</div>
											</div>
										</div>
										<?php unset($_SESSION['alert']); ?>
										<?php } ?>
										<form action="<?php echo DIR_URL; ?>/administrator/users/riskVerificationUpdate" method="POST" autocomplete="off">
											<div class="row">
												<div class="col-md-6 col-md-offset-3">
													<div class="form-elements">
														<input type="text" name="average_risk_level" id="average_risk_level" value="<?php echo (!empty($risk['average_risk_level'])) ? $risk['average_risk_level'] : ''; ?>" placeholder="Average Risk LEVEL">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-md-offset-3">
													<div class="form-elements">
														<input type="text" name="world_check_match_strenght" id="world_check_match_strenght" value="<?php echo (!empty($risk['world_check_match_strenght'])) ? $risk['world_check_match_strenght'] : ''; ?>" placeholder="World Check (Match Strenght)">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-md-offset-3">
													<div class="form-elements">
														<input type="text" name="world_check_source_type" id="world_check_source_type" value="<?php echo (!empty($risk['world_check_source_type'])) ? $risk['world_check_source_type'] : ''; ?>" placeholder="World Check (Source Type)">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-md-offset-3">
													<div class="form-elements">
														<input type="text" name="ofacs_list" id="ofacs_list" value="<?php echo (!empty($risk['ofacs_list'])) ? $risk['ofacs_list'] : ''; ?>" placeholder="Ofac´s List (Type)">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-md-offset-3">
													<div class="form-elements">
														<input type="text" name="naics_code" id="naics_code" value="<?php echo (!empty($risk['naics_code'])) ? $risk['naics_code'] : ''; ?>" placeholder="NAICS Code">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-md-offset-3">
													<div class="form-elements">
														<textarea name="notes" id="notes" placeholder="Notes" style="height: 148px;"><?php echo (!empty($risk['notes'])) ? $risk['notes'] : ''; ?></textarea>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4 col-md-offset-4">
													<?php echo (!empty($risk['id'])) ? '<input type="hidden" name="id" id="id" value="' . $risk['id'] . '">' : ''; ?>
													<input type="hidden" name="user" id="user" value="<?php echo $user['id']; ?>">
													<button type="submit" class="btn-mercury-black">UPDATE</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>