<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>/administrator/public/assets/css/metricsgraphics.css">
		<link href="<?php echo DIR_URL; ?>/administrator/public/vendor/plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/metricsgraphics.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo DIR_URL; ?>/administrator/public/vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
		<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/transactions.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="tabs-m">
									<ul>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/details/<?php echo $user; ?>">Details</a></li>
										<li class="active"><a href="<?php echo DIR_URL; ?>/administrator/transactions/transactions/<?php echo $user; ?>">Transactions</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/documents/<?php echo $user; ?>">Documents</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/riskVerification/<?php echo $user; ?>">Risk Verification</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/ctrReport/<?php echo $user; ?>">CTR Report</a></li>
										<li><a href="<?php echo DIR_URL; ?>/administrator/users/resetg2fa/<?php echo $user; ?>">Reset Google 2FA</a></li>
									</ul>
								</div>
								<div class="block block-first">
									<div class="block-header">
										<span class="text-yellow">All</span> transactions
										<div class="group-btn-block">
											<a href="<?php echo DIR_URL; ?>/administrator/transactions/transactions/<?php echo $user; ?>/day" id="new-wallet" class="btn-header <?php echo ($_SERVER['REQUEST_URI'] == '/administrator/transactions/transactions/' . $user || $_SERVER['REQUEST_URI'] == '/administrator/transactions/transactions/' . $user . '/day') ? 'active' : ''; ?>">Day</a>
											<a href="<?php echo DIR_URL; ?>/administrator/transactions/transactions/<?php echo $user; ?>/week" id="new-wallet" class="btn-header <?php echo ($_SERVER['REQUEST_URI'] == '/administrator/transactions/transactions/' . $user . '/week') ? 'active' : ''; ?>">Week</a>
											<a href="<?php echo DIR_URL; ?>/administrator/transactions/transactions/<?php echo $user; ?>/month" id="new-wallet" class="btn-header <?php echo ($_SERVER['REQUEST_URI'] == '/administrator/transactions/transactions/' . $user . '/month') ? 'active' : ''; ?>">Month</a>
											<a href="<?php echo DIR_URL; ?>/administrator/transactions/transactions/<?php echo $user; ?>/year" id="new-wallet" class="btn-header <?php echo ($_SERVER['REQUEST_URI'] == '/administrator/transactions/transactions/' . $user . '/year') ? 'active' : ''; ?>">Year</a>
										</div>
									</div>
									<div class="block-body">
										<div class="row">
											<div class="col-md-2 col-md-offset-10">
												<ul class="balance_user">
													<li><span>ETH Balance:</span> <?php echo $balanceethwallet; ?> ETH</li>
													<li><span>USD Balance:</span> $<?php echo $balanceusdwallet; ?></li>
												</ul>
											</div>
										</div>
										<table class="table-m" id="datatable">
											<thead>
												<tr>
													<th>ID</th>
													<th>Date</th>
													<th>Wire Code</th>
													<th>Rate ETH</th>
													<th>ETH QTY.</th>
													<th>USD Amount</th>
													<th>Fee</th>
													<th>Status</th>
													<th>TX Type</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($transactions as $transaction) { ?>
												<tr>
													<td>
														<?php if ($transaction['txtype'] == 3) { ?>
														<a href="<?php echo DIR_URL; ?>/administrator/Purchases/purchase/<?php echo $transaction['id']; ?>"><?php echo $transaction['type'] . "-" .  $transaction['id']; ?></a>
														<?php } elseif ($transaction['txtype'] == 4) { ?>
														<a href="<?php echo DIR_URL; ?>/administrator/sales/sale/<?php echo $transaction['id']; ?>"><?php echo $transaction['type'] . "-" .  $transaction['id']; ?></a>
														<?php } elseif ($transaction['txtype'] == 1) { ?>
														<a href="<?php echo DIR_URL; ?>/administrator/withdraws/withdraw/<?php echo $transaction['id']; ?>"><?php echo $transaction['type'] . "-" .  $transaction['id']; ?></a>
														<?php } elseif ($transaction['txtype'] == 2) { ?>
														<a href="<?php echo DIR_URL; ?>/administrator/deposits/deposit/<?php echo $transaction['id']; ?>"><?php echo $transaction['type'] . "-" .  $transaction['id']; ?></a>
														<?php } ?>
													</td>
													<td><?php echo date("Y-m-d H:i:s", $transaction['date']); ?></td>
													<td><?php echo $transaction['wire_code']; ?></td>
													<td><?php echo ($transaction['eth_value'] !== 'N/A') ? number_format($transaction['eth_value'], 2) : $transaction['eth_value']; ?></td>
													<td><?php echo ($transaction['eth_amount'] !== 'N/A') ? number_format($transaction['eth_amount'], 5) : $transaction['eth_amount']; ?></td>
													<td><?php echo $transaction['usd_amount']; ?></td>
													<td><?php echo number_format($transaction['commission'], 2); ?></td>
													<td>
														<?php if ($transaction['status'] == 0) { ?>
														<span class="label label-danger mr5 mb10 ib lh15">Rejected</span>
														<?php } elseif ($transaction['status'] == 1) { ?>
														<span class="label label-success mr5 mb10 ib lh15">Approved</span>
														<?php } elseif ($transaction['status'] == 2) { ?>
														<span class="label label-warning mr5 mb10 ib lh15">pending</span>
														<?php } ?>
													</td>
													<td>
														<?php if ($transaction['txtype'] == 3) { ?>
														BUY
														<?php } elseif ($transaction['txtype'] == 4) { ?>
														SELL
														<?php } elseif ($transaction['txtype'] == 1) { ?>
														WITHDRAW
														<?php } elseif ($transaction['txtype'] == 2) { ?>
														DEPOSIT
														<?php } ?>
													</td>
													<td>
														<?php if ($transaction['txtype'] == 3) { ?>
														<a href="<?php echo DIR_URL; ?>/administrator/Purchases/purchase/<?php echo $transaction['id']; ?>">Details</a>
														<?php } elseif ($transaction['txtype'] == 4) { ?>
														<a href="<?php echo DIR_URL; ?>/administrator/sales/sale/<?php echo $transaction['id']; ?>">Details</a>
														<?php } elseif ($transaction['txtype'] == 1) { ?>
														<a href="<?php echo DIR_URL; ?>/administrator/withdraws/withdraw/<?php echo $transaction['id']; ?>">Details</a>
														<?php } elseif ($transaction['txtype'] == 2) { ?>
														<a href="<?php echo DIR_URL; ?>/administrator/deposits/deposit/<?php echo $transaction['id']; ?>">Details</a>
														<?php } ?>
													</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>