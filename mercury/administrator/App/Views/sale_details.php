<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<?php include "inc/scripts.php"; ?>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/sell.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/public/assets/js/purchases.js"></script>
	</head>
	<body>
		<div class="modal fade" id="approveSell" tabindex="-1" role="dialog" aria-labelledby="approveSellLabel" data-target=".bs-example-modal-sm">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="approveSellLabel">Attention</h4>
					</div>
					<form action="<?php echo DIR_URL; ?>/administrator/sales/approve" method="POST" autocomplete="off">
						<div class="modal-body" style="text-align: center;">
							<div class="row">
								<div class="col-md-3">
									<div class="form-elements">
										<label for="provider">Provider</label>
										<input type="text" name="provider[]" required="required" placeholder="Ejm: Kraken" style="border: solid 1px #cccccc;">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-elements">
										<label for="quantity">Quantity</label>
										<input type="text" name="quantity[]" required="required" placeholder="Ejm: 0.25698" style="border: solid 1px #cccccc;">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-elements">
										<label for="price">Price</label>
										<input type="text" name="price[]" required="required" placeholder="Ejm: 380.00" style="border: solid 1px #cccccc;">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-elements">
										<label for="fee">Fee (%)</label>
										<input type="text" name="fee[]" required="required" placeholder="Ejm: 0.25" style="border: solid 1px #cccccc;">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<div class="form-elements">
										<input type="text" name="provider[]" placeholder="Ejm: Kraken (optional)" style="border: solid 1px #cccccc;">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-elements">
										<input type="text" name="quantity[]" placeholder="Ejm: 0.25698 (optional)" style="border: solid 1px #cccccc;">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-elements">
										<input type="text" name="price[]" placeholder="Ejm: 380.00 (optional)" style="border: solid 1px #cccccc;">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-elements">
										<input type="text" name="fee[]" placeholder="Ejm: 0.25 (optional)" style="border: solid 1px #cccccc;">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<div class="form-elements">
										<input type="text" name="provider[]" placeholder="Ejm: Kraken (optional)" style="border: solid 1px #cccccc;">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-elements">
										<input type="text" name="quantity[]" placeholder="Ejm: 0.25 (optional)" style="border: solid 1px #cccccc;">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-elements">
										<input type="text" name="price[]" placeholder="Ejm: 380.00 (optional)" style="border: solid 1px #cccccc;">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-elements">
										<input type="text" name="fee[]" placeholder="Ejm: 0.25 (optional)" style="border: solid 1px #cccccc;">
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="id" value="<?php echo $transaction['id']; ?>">
							<button type="submit" class="btn btn-default">Approve</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="wrapper">
			<?php include "inc/aside.php"; ?>
			<div class="main-panel">
				<?php include "inc/header.php"; ?>
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Transaction</span> information
										<a href="<?php echo DIR_URL; ?>/administrator/operationsForm/generate/4/<?php echo $transaction['id']; ?>" target="_blank" class="btn-header">Report</a>
									</div>
									<div class="block-body">
										<?php if (!empty($_SESSION['alert'])) { ?>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="alertGrey">
                                                    <?php echo $_SESSION['alert']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php unset($_SESSION['alert']); ?>
                                        <?php } ?>
										<table class="data-transactions">
											<tbody>
												<tr>
													<td class="tags-transaction">Transaction ID</td>
													<td><?php echo $transaction['id']; ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">TxHash Transaction</td>
													<td><a href="https://etherscan.io/tx/<?php echo $transaction['txhash']; ?>" target="_blank"><?php echo $transaction['txhash']; ?></a></td>
												</tr>
												<tr>
													<td class="tags-transaction">USD Amount</td>
													<td><?php echo "$". number_format($transaction['usd_amount'], 2); ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Fee</td>
													<td><?php echo "$". number_format($transaction['commission'], 2); ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Price With Fee</td>
													<td><?php echo "$". number_format(($transaction['usd_amount'] - $transaction['commission']), 2); ?></td>
												</tr>
												<?php if (!is_null($bankAccount)) { ?>
												<tr>
													<td class="tags-transaction">Bank Account</td>
													<td><?php echo $bankAccount['number']; ?></td>
												</tr>
												<?php } ?>
												<tr>
													<td class="tags-transaction">Status</td>
													<td>
														<?php if ($transaction['status'] == 0) { ?>
														<span class="status-reject">Rejected</span>
														<?php } elseif ($transaction['status'] == 1) { ?>
														<span class="status-success">Approved</span>
														<?php } elseif ($transaction['status'] == 2) { ?>
														<span class="status-pending">Pending</span>
														<?php } ?>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Client</span> information
									</div>
									<div class="block-body">
										<table class="data-transactions">
											<tbody>
												<tr>
													<td class="tags-transaction">Full Name</td>
													<td><?php echo $user['name'] . " " . $user['last_name']; ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Email</td>
													<td><?php echo $user['email']; ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Status</td>
													<td>
														<?php if ($user['status'] == 0) { ?>
														<span class="status-reject">Rejected</span>
														<?php } elseif ($user['status'] == 1) { ?>
														<span class="status-success">Approved</span>
														<?php } elseif ($user['status'] == 2) { ?>
														<span class="status-pending">Pending</span>
														<?php } ?>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<?php if (!is_null($bankAccount)) { ?>
						<div class="row">
							<div class="col-md-6">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Bank</span> Transfer Information
									</div>
									<div class="block-body">
										<table class="data-transactions">
											<tbody>
												<tr>
													<td class="tags-transaction">Ammount to Transfer</td>
													<td><?php echo "$" . number_format($transaction['usd_amount'], 2); ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Beneficiary Name</td>
													<td><?php echo $bankAccount['beneficiary_name']; ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Bank Account</td>
													<td><?php echo $bankAccount['number']; ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">ABA</td>
													<td><?php echo $bankAccount['routing']; ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Address</td>
													<td><?php echo $bankAccount['address']; ?></td>
												</tr>
												<tr>
													<td class="tags-transaction">Phone Number</td>
													<td><?php echo "+" . $user['phonecode'] . " " . $user['phone']; ?></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
						<?php if ($transaction['status'] == 2) { ?>
						<div class="row" style="margin-top: 25px;">
							<div class="col-md-4 col-md-offset-2">
								<a href="#" data-toggle="modal" data-target="#approveSell" class="btn-mercury-yellow">APPROVE</a>
								<!--<a href="<?php //echo DIR_URL; ?>/administrator/sales/approve/<?php //echo $transaction['id']; ?>" class="btn-mercury-yellow">APPROVE</a>-->
							</div>
							<div class="col-md-4">
								<a href="<?php echo DIR_URL; ?>/administrator/sales/reject/<?php echo $transaction['id']; ?>" id="reject" class="btn-mercury-black">REJECT</a>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<?php include "inc/close.php"; ?>
	</body>
</html>