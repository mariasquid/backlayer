<!DOCTYPE html>
<html lang="es">
	<head>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo $title; ?></title>
		<?php include "inc/styles.php"; ?>
		<script src="<?php echo DIR_URL; ?>/administrator/public/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
		<script src="<?php echo DIR_URL; ?>/administrator/public/assets/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo DIR_URL; ?>/administrator/public/assets/js/bootstrap-notify.js"></script>
		<script type="text/javascript" src="<?php echo DIR_URL; ?>/administrator/public/assets/js/dir_url.js"></script>
	</head>
	<body style="background-color: #f5f5f5;">
		<div class="wrapper">
			<div class="main">
				<div class="content">
					<div class="container">
						<div class="row">
							<div class="col-sm-3 col-sm-offset-4">
								<div id="logo-register">
									<img src="<?php echo DIR_URL; ?>/administrator/public/img/logo-dark.png" alt="" class="img-responsive">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-md-offset-3">
								<div class="block">
									<div class="block-header">
										<span class="text-yellow">Sign up</span>
									</div>
									<div class="block-body">
										<?php if (!empty($_SESSION['alert'])) { ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="alertGrey">
                                                    <?php echo $_SESSION['alert']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php unset($_SESSION['alert']); ?>
                                        <?php } ?>
										<form method="POST" action="<?php echo DIR_URL; ?>/administrator/home/login" id="login-form" autocomplete="off">
											<div class="row">
												<div class="col-md-12">
													<div class="form-elements">
														<label>Email</label>
														<input type="email" id="email" name="email"  placeholder="Email">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="form-elements">
														<label>Password</label>
														<input type="password" id="password" name="password" placeholder="Password...">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4 col-md-offset-4">
													<button type="submit" id="enter" class="btn-m-l">SIGN IN</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src='https://www.google.com/recaptcha/api.js'></script>
	</body>
</html>