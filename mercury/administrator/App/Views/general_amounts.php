<!DOCTYPE html>

<html lang="es">

	<head>

		<meta charset="UTF-8">

		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

		<title><?php echo $title; ?></title>

		<?php include "inc/styles.php"; ?>

		<link rel="stylesheet" type="text/css" href="<?php echo DIR_URL; ?>administrator/public/assets/css/metricsgraphics.css">

		<link href="<?php echo DIR_URL; ?>administrator/public/vendor/plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">

		<?php include "inc/scripts.php"; ?>

		<script type="text/javascript" src="<?php echo DIR_URL; ?>administrator/public/assets/js/metricsgraphics.min.js"></script>

		<script src="<?php echo DIR_URL; ?>administrator/public/vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

		<script src="<?php echo DIR_URL; ?>administrator/public/vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

		<script type="text/javascript" src="<?php echo DIR_URL; ?>administrator/public/assets/js/general_amounts.js"></script>

	</head>

	<body>

		<div class="wrapper">

			<?php include "inc/aside.php"; ?>

			<div class="main-panel">

				<?php include "inc/header.php"; ?>

				<nav class="navbar navbar-default navbar-fixed">

					<div class="container-fluid">

						<div class="collapse navbar-collapse">

							<ul class="nav navbar-nav navbar-left sub-menu">

								<li>

									<a href="<?php echo DIR_URL; ?>administrator/settings/wallet">

										System Wallet

									</a>

								</li>

								<li class="sub-active">

									<a href="<?php echo DIR_URL; ?>administrator/settings/generalAmounts">

										General Amounts

									</a>

								</li>

							</ul>

						</div>

					</div>

				</nav>

				<div class="content">

					<div class="container-fluid">

						<div class="row">

							<div class="col-md-4 col-md-offset-2">

								<div class="block">

									<div class="block-header">

										<span class="text-yellow">Total ETH Wallets</span>

									</div>

									<div class="block-body" style="text-align: center;">

										<h1><?php echo number_format($total_eth, 3); ?> ETH</h1>

									</div>

								</div>

							</div>

							<div class="col-md-4">

								<div class="block">

									<div class="block-header">

										<span class="text-yellow">Total USD Wallets</span>

									</div>

									<div class="block-body" style="text-align: center;">

										<h1>$<?php echo $total_usd; ?></h1>

									</div>

								</div>

							</div>

						</div>

						<div class="row">

							<div class="col-md-12">

								<div class="block" style="margin-top: 40px;">

									<div class="block-header">

										<span class="text-yellow">All users</span>

									</div>

									<div class="block-body">

										<table class="table-m" id="datatable" cellspacing="0" width="100%">

											<thead>

												<tr>

													<th>ID</th>

													<th>Client</th>

													<th>Total ETH Amount</th>

													<th>USD Amount</th>

												</tr>

											</thead>

											<tbody>

												<?php foreach ($rows as $row) { ?>

												<tr>

													<td><?php echo $row['user']; ?></td>

													<td><?php echo $row['fullname']; ?></td>

													<td><?php echo number_format($row['eth_balance'], 8); ?> ETH</td>

													<td>$<?php echo (is_null($row['usd_balance'])) ? '0' : $row['usd_balance']; ?></td>

												</tr>

												<?php } ?>

											</tbody>

										</table>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php include "inc/close.php"; ?>

	</body>

</html>