<?php
require_once LIBSPATH . '/gauthentication/GoogleAuthenticator/PHPGangsta/GoogleAuthenticator.php';

/**
* Google Tow Factor Authenticator
*/
class Auth {

	protected $tfa;
	protected $secret;
	
	function __construct() {
		$this->tfa = new PHPGangsta_GoogleAuthenticator();
	}

	public function getTfa() {
		return $this->tfa;
	}

	public function setSecret() {
		$this->secret = $this->tfa->createSecret();
		$_SESSION['secret'] = $this->secret;
	}

	public function getSecret() {
		return $this->secret;
	}

	public function getQr($email, $secret) {
		return $this->tfa->getQRCodeGoogleUrl('Mercury Cash (' . $email . ')', $secret);
	}

	public function validate($secret, $code) {
		if($this->tfa->verifyCode($secret, $code, 1) === TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>