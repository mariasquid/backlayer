<?php
header("X-XSS-Protection: 1; mode=block");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

//Borrar para produccion
error_reporting(E_ALL);
ini_set('display_errors', 1);
//ini_set('allow_url_fopen', 1);
date_default_timezone_set('America/New_York');

//SESION
session_start();

//URL
define("DIR_URL", "http://mercury");

//directorio del proyecto
define("PROJECTPATH", dirname(__DIR__) . "\\mercury");

//directorio app
define("APPPATH", PROJECTPATH . '/App');

//directorio app
define("LIBSPATH", APPPATH . '/Libs');

//autoload con namespaces
function autoload_classes($class_name) {
    $filename = PROJECTPATH . '/' . str_replace('\\', '/', $class_name) . '.php';
    if(is_file($filename)) {
        include_once $filename;
    }
}

//registramos el autoload autoload_classes
spl_autoload_register('autoload_classes');

//instanciamos la app
$app = new \Core\App;

//lanzamos la app
$app->render();
?>